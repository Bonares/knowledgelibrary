import { MetaService } from './../../shared/meta.service';
import { Soildoc } from './../soil-doc/shared/Soildoc';
import { SoildocValidators } from './../soil-doc/shared/soildoc-validators';
import { UserEnabledErrorMessages } from './userenabled-error-messages';
import { UserValidators } from './../../user/shared/user.validators';
import { UntypedFormBuilder, Validators, UntypedFormArray, UntypedFormGroup } from '@angular/forms';
import { UserService } from './../../shared/user.service';
import { faPlus, faEnvelope, faSearch, faQuoteLeft,
  faFileAlt, faEdit, faProjectDiagram, faInfoCircle, faTractor, faSync,
  faExternalLinkAlt, faTrashAlt, faMapMarkedAlt,
  faUsers, faSpinner, faExclamationCircle, faCarrot, faCubes, faBookOpen, faLongArrowAltRight, faComment, faUser, faBookmark, faDownload, faFilter, faUndo, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faBookmark as farBookmark } from '@fortawesome/free-regular-svg-icons';
import { Observable, timer } from 'rxjs';
import { AuthService } from './../../shared/authentication/auth.service';
import { SoildocService } from './../soil-doc/shared/soildoc.service';
import { Component, OnInit, Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { take, distinctUntilChanged, map } from 'rxjs/operators';
import { PlatformLocation } from '@angular/common';
import { Options } from '@angular-slider/ngx-slider';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-soil-doc-search',
  templateUrl: './soil-doc-search.component.html',
  styleUrls: []
})
export class SoilDocSearchComponent implements OnInit {

  faUndo = faUndo;
  faFilter = faFilter;
  faDownload = faDownload;
  farBookmark = farBookmark;
  faBookmark = faBookmark;
  faUser = faUser;
  faComment = faComment;
  faLongArrowAltRight = faLongArrowAltRight;
  faBookOpen = faBookOpen;
  faCubes = faCubes;
  faPlus = faPlus;
  faProjectDiagram = faProjectDiagram;
  faTractor = faTractor;
  faMapMarkedAlt = faMapMarkedAlt;
  faInfoCircle = faInfoCircle;
  faCarrot = faCarrot;
  faEnvelope = faEnvelope;
  faSearch = faSearch;
  faSync = faSync;
  faExternalLinkAlt = faExternalLinkAlt;
  faFileAlt = faFileAlt;
  faQuoteLeft = faQuoteLeft;
  faEdit = faEdit;
  faUsers = faUsers;
  faTrashAlt = faTrashAlt;
  faExclamationCircle = faExclamationCircle;
  faSpinner = faSpinner;

  soildocCollection: Soildoc[] = [];
  soildocCollectionOriginal: Soildoc[];
  soildocCollectionOriginalOwn: Soildoc[];

  isLoading = true;
  errorLoading = true;
  failDelete = false;
  successDelete = false;

  startItem: number;
  endItem: number;

  isSorted = false;
  sortTarget: any;
  showOnlyOwn = false;
  showOnlyMarked = false;

  isLoggedIn$: Observable<boolean>;
  loggedInUser$: Observable<string>;
  loggedInAdmin$: Observable<string>;
  
  markedPapers: String[] = [];

  soildocCitation: Soildoc;

  soildocDelete: Soildoc;
  soildocOverview: Soildoc;

  soildocUsers: Soildoc;
  enabledUsers: UntypedFormArray;
  userForm: UntypedFormGroup;
  isLoadingUsers: Boolean = false;
  isSaveLoadingUsers: Boolean = false;
  errorLoadingModalUsers: Boolean = false;
  maximalUsers = 10;
  options_users = [];
  errors: { [key: string]: string } = {};
  userErrors: [{[key: string]: string}] = [{}];
  failSaveUsers = false;
  failSaveAuthorized = false;
  successSaveUsers = false;

  soildocComment: Soildoc;
  commentText: string = '';
  isLoadingComment: Boolean = false;
  isSaveLoadingComment: Boolean = false;
  errorLoadingModalComment: Boolean = false;
  failSaveComment = false;
  failSaveAuthorizedComment = false;
  successSaveComment = false;

  scrollOffset = 0;

  floor = 0;
  ceil = 0;

  minValue = 0;
  maxValue = 0;

  sliderTouched = false;

  options: Options = {
    floor: this.floor,
    ceil: this.ceil,
    hideLimitLabels: true,
    hidePointerLabels: false
  };
  modalDelete : bootstrap.Modal;
  modalCitation : bootstrap.Modal;
  modalUsers : bootstrap.Modal;
  modalComment : bootstrap.Modal;
  modalOverview : bootstrap.Modal;

  constructor(
    @Inject('API_URL') public api: string,
    private ss: SoildocService,
    private authService: AuthService,
    private us: UserService,
    private fb: UntypedFormBuilder,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.modalDelete = new bootstrap.Modal(document.getElementById('modalDelete'));
    this.modalCitation = new bootstrap.Modal(document.getElementById('modalCitation'));
    this.modalUsers = new bootstrap.Modal(document.getElementById('modalUsers'));
    this.modalComment = new bootstrap.Modal(document.getElementById('modalComment'));
    this.modalOverview = new bootstrap.Modal(document.getElementById('modalOverview'));
    this.metaService.changeMeta('SOILDOC.SEARCHTITLEMETA', 'SOILDOC.SEARCHDESCRIPTIONMETA');
    
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.loggedInUser$ = this.authService.isLoggedInUser;
    this.loggedInAdmin$ = this.authService.isLoggedInAdmin;
    this.isLoggedIn$.pipe(take(1)).subscribe(login => {
      if (login) {
        this.us.getMarkedPapers().subscribe({next: (papers) => {
          this.markedPapers = papers && papers.length ? papers : [];
        }});
      }
    });
    this.refreshEntries();
  }

  refreshEntries() {
    this.soildocCollection = [];
    this.soildocCollectionOriginal = [];
    this.soildocCollectionOriginalOwn = [];
    this.isLoading = true;
    this.ss.getAll().subscribe({next: (result) => {
      this.errorLoading = false;
      if (result.length > 0) {
        this.soildocCollectionOriginal = result;
        this.soildocCollectionOriginalOwn = result;
        this.loggedInUser$.pipe(take(1)).subscribe(loggedInUser => {
          this.soildocCollectionOriginalOwn = result.filter(paper => (paper.createdBy === loggedInUser) ||
          paper.enabledUsers.includes(loggedInUser));
        });
        this.soildocCollection = this.soildocCollectionOriginal.slice(0);
      }
      this.isLoading = false;
    },
    error: (error) => {
      this.isLoading = false;
      this.errorLoading = true;
    }});
  }

  updateYears(arr: any[]) {
    this.sliderTouched = false;
    if (arr.length > 0) {
      this.floor = Math.min(...arr.map(item => item.year));
      this.ceil = Math.max(...arr.map(item => item.year));
      const newOptions: Options = Object.assign({}, this.options);
      newOptions.floor = this.floor;
      newOptions.ceil = this.ceil;
      if (!this.between(this.minValue, this.floor, this.ceil)) {
        this.minValue = this.floor;
      }
      if (!this.between(this.maxValue, this.floor, this.ceil)) {
        this.maxValue = this.ceil;
      }
      if (this.minValue > this.floor || this.maxValue < this.ceil) {
        this.sliderTouched = true;
      }
      this.options = newOptions;
    }  
  }

  between(value: number, min: number, max: number) {
    return (value >= min && value <= max);
  }

  resetSorting() {
    if (this.isSorted && this.sortTarget) {
      this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
    }
  }

  sortPaper(event: any, identifier: string) {
    if (event.target.classList.value === 'sort-icon-search fa fa-sort') {
      if (this.isSorted && this.sortTarget) {
        this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      }
      this.isSorted = true;
      this.sortTarget = event.target;
      event.target.classList = 'sort-icon-search fa fa-sort-up';

      this.sortOneIdentifier(identifier, 1, -1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-up') {

      event.target.classList = 'sort-icon-search fa fa-sort-down';
      this.sortOneIdentifier(identifier, -1, 1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-down') {
      event.target.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
      this.sortOneIdentifier('modifiedDate', 1, -1, true);
    }
  }

  sortOneIdentifier(identifier: string, first: number, second: number, isDate?: boolean) {
    if (!isDate) {
      if (identifier === 'authors') {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a[identifier][0].name > b[identifier][0].name) {
            return first;
          }
          if (a[identifier][0].name < b[identifier][0].name) {
            return second;
          }
          return 0;
        });
      } else {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a[identifier] > b[identifier]) {
            return first;
          }
          if (a[identifier] < b[identifier]) {
            return second;
          }
          return 0;
        });
      }
    } else {
      this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
        return new Date(b.modifiedDate).getTime() - new Date(a.modifiedDate).getTime();
      });
    }
  }

  updatePapersYear() {
    if (this.minValue > this.floor || this.maxValue < this.ceil) {
      this.sliderTouched = true;
    } else {
      this.sliderTouched = false;
    }
  }

  resetYears() {
    this.sliderTouched = false;
    this.minValue = this.floor;
    this.maxValue = this.ceil;
  }

  openModalDelete(soildoc: Soildoc) {
    this.soildocDelete = soildoc;
    this.modalDelete.show();
  }

  deleteOne(soildoc: Soildoc) {
    if (soildoc._id) {
      this.ss.remove(soildoc._id)
          .subscribe({
              next: () => {
                  this.failDelete = false;
                  this.successDelete = true;
                  this.refreshEntries();
                  window.scrollTo(0, 0);
                  const source = timer(5000);
                  source.subscribe(val => {
                    this.successDelete = false;
                  });
              },
              error: (error) => {
                this.failDelete = true;
                window.scrollTo(0, 0);
                const source = timer(5000);
                source.subscribe(val => {
                  this.failDelete = false;
                });
              }
            });
    }
  }

  openModalCitation(soildoc: Soildoc) {
    this.soildocCitation = soildoc;
    this.modalCitation.show();
  }

  openModalOverview(soildoc: Soildoc) {
    if (this.soildocOverview && (this.soildocOverview._id === soildoc._id)) {
      this.modalOverview.show();
    } else {
      this.modalOverview.show();
      this.soildocOverview = soildoc;
    }
  }

  openModalUsers(soildoc: Soildoc) {
    this.isLoadingUsers = true;
    this.isSaveLoadingUsers = false;
    this.failSaveUsers = false;
    this.failSaveAuthorized = false;
    this.successSaveUsers = false;
    this.ss.getOne(soildoc._id).subscribe(soildocload => {
      this.soildocUsers = soildocload;
      this.enabledUsers = this.fb.array(
        this.soildocUsers.enabledUsers.map(
          u => this.fb.group({
            name: this.fb.control(u, [Validators.required], UserValidators.userExists(this.us))
          })
        )
      );
      this.userForm = this.fb.group({
        enabledUsers: this.enabledUsers
      }, {
        validators: SoildocValidators.uniqueValidator
      });
      this.userForm.statusChanges.subscribe(() => this.updateErrorMessages());
      this.us.getUsernames().subscribe(usernames => {
        if (usernames && usernames.length && usernames.length > 0) {
          this.options_users = usernames;
        } else {
          this.errorLoadingModalUsers = true;
        }
        this.isLoadingUsers = false;
        this.modalUsers.show();
      },
      error => {
        this.isLoadingUsers = false;
        this.errorLoadingModalUsers = true;
        this.modalUsers.show();
      });
    },
    error => {
      this.isLoadingUsers = false;
      this.errorLoadingModalUsers = true;
      this.modalUsers.show();
    });
  }

  openModalComment(soildoc: Soildoc) {
    this.isLoadingComment = true;
    this.isSaveLoadingComment = false;
    this.failSaveComment = false;
    this.failSaveAuthorizedComment = false;
    this.successSaveComment = false;
    this.errorLoadingModalComment = false;
    this.ss.getOne(soildoc._id).subscribe(soildocload => {
      this.soildocComment = soildocload;
      this.commentText = soildocload.comment ? soildocload.comment : '';
      this.modalComment.show();
      this.isLoadingComment = false;
    },
    error => {
      this.isLoadingComment = false;
      this.errorLoadingModalComment = true;
      this.modalComment.show();
    });
  }

  submitFormComment() {
    this.isSaveLoadingComment = true;
    this.ss.updateComment(this.commentText, this.soildocComment._id)
      .subscribe({
          next: (data) => {
              this.isSaveLoadingComment = false;
              this.failSaveComment = false;
              this.failSaveAuthorizedComment = false;
              this.successSaveComment = true;
              this.soildocCollection.find(soildoc => soildoc._id === this.soildocComment._id).comment = this.commentText;
              this.soildocCollectionOriginal.find(soildoc => soildoc._id === this.soildocComment._id).comment = this.commentText;
              if (this.soildocCollectionOriginalOwn.find(soildoc => soildoc._id === this.soildocComment._id)) {
                this.soildocCollectionOriginalOwn.find(soildoc => soildoc._id === this.soildocComment._id).comment = this.commentText;
              }
              timer(3000).subscribe(val => {
                this.successSaveComment = false;
              });
          },
          error: (error) => {
            if (error.status === 401) {
              this.failSaveAuthorizedComment = true;
            } else {
              this.failSaveComment = true;
            }
            this.isSaveLoadingComment = false;
          }
        });
  }

  addUser() {
    this.enabledUsers.push(
      this.fb.group({
        name: this.fb.control('', [Validators.required], UserValidators.userExists(this.us))
      })
    );
    this.userForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeUser(userIndex: number) {
    this.enabledUsers.removeAt(userIndex);
    this.userForm.markAsDirty();
    this.updateErrorMessages();
  }

  submitFormUsers() {
    if (this.userForm.valid) {
      this.isSaveLoadingUsers = true;
      const users = [];
      for (let i = 0; i < this.userForm.value.enabledUsers.length; i++) {
        users.push(this.userForm.value.enabledUsers[i].name);
      }
      this.ss.updateEnabledUsers(users, this.soildocUsers._id)
        .subscribe({
            next: () => {
                this.isSaveLoadingUsers = false;
                this.failSaveUsers = false;
                this.failSaveAuthorized = false;
                this.successSaveUsers = true;
                this.userForm.markAsPristine();
                timer(3000).subscribe(val => {
                  this.successSaveUsers = false;
                });
            },
            error: (error) => {
              if (error.status === 401) {
                this.failSaveAuthorized = true;
              } else {
                this.failSaveUsers = true;
              }
              this.isSaveLoadingUsers = false;
            }
          });
    }
  }

  markPaper(soildoc_id: string) {
    if (!this.markedPapers.includes(soildoc_id)) {
      this.markedPapers.push(soildoc_id);
      this.markedPapers = this.markedPapers.slice(0);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  unmarkPaper(soildoc_id: string) {
    if (this.markedPapers.includes(soildoc_id)) {
      this.markedPapers = this.markedPapers.filter(paper => paper !== soildoc_id);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  untagAllPapers() {
    this.markedPapers = [];
    this.us.updateMarkedPapers(this.markedPapers).subscribe({
      next: () => {},
      error: (err) => {console.error(err)}
    });
  }

  updateErrorMessages() {
    this.resetUserErrorArray();
    this.errors = {};
    for (const message of UserEnabledErrorMessages) {
      if (message.forControl === '') {
        if (this.userForm &&
          this.userForm.dirty &&
          this.userForm.invalid &&
          this.userForm.errors &&
          this.userForm.errors[message.forValidator]) {
          this.errors[message.forValidator] = message.text;
        }
      } else {
        const control = this.userForm.get(message.forControl);
        if (!control) {
          for (let i = 0; i < this.enabledUsers.controls.length ; i++) {
            const controluser = this.enabledUsers.controls[i].get(message.forControl);
            if (controluser &&
              controluser.dirty &&
              controluser.invalid &&
              controluser.errors[message.forValidator] &&
              !this.userErrors[i][message.forControl]) {
              this.userErrors[i][message.forControl] = message.text;
            }
          }
        }
      }
    }
  }

  resetUserErrorArray() {
    this.userErrors = [{}];
    for (let i = 0; i < this.enabledUsers.controls.length ; i++) {
      this.userErrors[i] = {'name': ''};
    }
  }

  search = (text$: Observable<String>) =>
    text$.pipe(
      // debounceTime(100),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : this.options_users.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

}
