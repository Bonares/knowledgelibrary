export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const UserEnabledErrorMessages = [
  new ErrorMessage('', 'notunique', 'SOILDOC.FAILFORM.UNIQUECONTRIBUTOR' ),
  new ErrorMessage('name', 'required', 'USER.LOGIN.FAILFORM.REQUIREDUSERNAME' ),
  new ErrorMessage('name', 'userEnabled', 'USER.LOGIN.FAILFORM.ENABLEDUSERNAME')
];
