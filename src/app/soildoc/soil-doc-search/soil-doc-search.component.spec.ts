import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SoilDocSearchComponent } from './soil-doc-search.component';

describe('SoilDocSearchComponent', () => {
  let component: SoilDocSearchComponent;
  let fixture: ComponentFixture<SoilDocSearchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilDocSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilDocSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
