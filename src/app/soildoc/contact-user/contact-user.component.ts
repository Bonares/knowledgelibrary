import { MetaService } from './../../shared/meta.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ContactUserService } from './shared/contact-user.service';
import { faAsterisk, faSpinner, faEnvelope, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from './../../shared/authentication/auth.service';
import { take } from 'rxjs/operators';
import { Observable, timer } from 'rxjs';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { ContactUserFactory } from './shared/ContactUserFactory';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kl-contact-user',
  templateUrl: './contact-user.component.html',
  styleUrls: []
})
export class ContactUserComponent implements OnInit {

  faPaperPlane = faPaperPlane;
  faEnvelope = faEnvelope;
  faAsterisk = faAsterisk;
  faSpinner = faSpinner;

  contactuser = ContactUserFactory.empty();
  contactuserForm: UntypedFormGroup;

  loggedInUser$: Observable<string>;

  isLoadingSend = false;
  failSend = false;
  failSendNoUser = false;
  successSend = false;

  constructor(
    private fb: UntypedFormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private cus: ContactUserService,
    private metaService: MetaService) { }

  ngOnInit(): void {
    this.metaService.changeMeta('SOILDOC.CONTACTUSER.HEADING', 'SOILDOC.CONTACTUSER.DESCRIPTIONMETA');
    this.loggedInUser$ = this.authService.isLoggedInUser;
    this.contactuser.recipient = this.route.snapshot.params.username ? this.route.snapshot.params.username : '';
    this.loggedInUser$.pipe(take(1)).subscribe(loggedInUser => {
      this.contactuser.username = loggedInUser;
      this.initContactUser();
    });
  }

  initContactUser() {
    this.contactuserForm = this.fb.group({
      username: [this.contactuser.username,
        [
          Validators.required
        ]],
      recipient: [{value: this.contactuser.recipient, disabled: true},
        [
          Validators.required
        ]],
      message: [this.contactuser.message,
        [
          Validators.required
        ]],
      checked: [this.contactuser.checked,
        [
          Validators.requiredTrue
        ]],
    });
  }

  submitForm() {
    if (this.contactuserForm.valid) {
      this.isLoadingSend = true;
      const contact = ContactUserFactory.fromObject(this.contactuserForm.getRawValue());
      this.cus.sendMessage(contact)
          .subscribe(
              data => {
                  this.failSend = false;
                  this.failSendNoUser = false;
                  this.successSend = true;
                  this.isLoadingSend = false;
                  this.initContactUser();
                  timer(15000).subscribe(val => {
                    this.successSend = false;
                  });
              },
              error => {
                this.handleError(error);
                this.isLoadingSend = false;
              }
          );
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 400) {
      this.failSendNoUser = true;
    } else {
      this.failSend = true;
    }
  }

}
