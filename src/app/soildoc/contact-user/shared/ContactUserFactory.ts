import { ContactUserRaw } from './contactuser-raw';
import { ContactUser } from './ContactUser';
export class ContactUserFactory {

  static empty(): ContactUser {
    return new ContactUser('', '', '', false);
  }

  static fromObject(rawContactuser: ContactUserRaw | any): ContactUser {
    return new ContactUser(
      rawContactuser.username,
      rawContactuser.recipient,
      rawContactuser.message,
      rawContactuser.checked
    );
  }
}
