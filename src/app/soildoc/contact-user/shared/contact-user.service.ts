import { AuthGuard } from './../../../shared/auth.guard';
import { ContactUser } from './ContactUser';
import { catchError, timeout } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactUserService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  sendMessage(contactuser: ContactUser): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/user/contact`, contactuser)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
