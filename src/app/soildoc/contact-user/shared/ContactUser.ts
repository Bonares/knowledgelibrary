export class ContactUser {
  constructor(
    public username: string,
    public recipient: string,
    public message: string,
    public checked: boolean
  ) {}
}
