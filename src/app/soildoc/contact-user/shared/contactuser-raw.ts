export interface ContactUserRaw {
  username: string;
  recipient: string;
  message: string;
  checked: boolean;
}
