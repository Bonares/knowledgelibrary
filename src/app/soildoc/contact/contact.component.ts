import { MetaService } from './../../shared/meta.service';
import { Component, OnInit } from '@angular/core';
import { faCaretRight, faEnvelope, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-contact',
  templateUrl: './contact.component.html',
  styleUrls: []
})
export class ContactComponent implements OnInit {

  faSearch = faSearch;
  faCaretRight = faCaretRight;
  faEnvelope = faEnvelope;

  constructor(
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILDOC.CONTACT.HEADING', 'SOILDOC.CONTACT.DESCRIPTION');
  }

}
