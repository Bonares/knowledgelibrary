import { ContactUserComponent } from './contact-user/contact-user.component';
import { ReferencesComponent } from './references/references.component';
import { ContactComponent } from './contact/contact.component';
import { SoildocResolverService } from './soil-doc/shared/soildoc-resolver.service';
import { SoilDocSearchComponent } from './soil-doc-search/soil-doc-search.component';
import { AuthGuard } from './../shared/auth.guard';
import { SoilDocComponent } from './soil-doc/soil-doc.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path: '',
    redirectTo: 'soil-doc-search',
    pathMatch: 'full'
  },
  {
    path: 'soil-doc-search',
    component: SoilDocSearchComponent
},
{
    path: 'soil-doc',
    component: SoilDocComponent,
    canActivate: [AuthGuard]
},
{
  path: 'contact',
  component: ContactComponent
},
{
  path: 'references',
  component: ReferencesComponent
},
{
  path: 'soil-doc/:id',
  component: SoilDocComponent,
  resolve: {
    soildoc: SoildocResolverService
  },
  canActivate: [AuthGuard]
},
{
  path: 'contact-user/:username',
  component: ContactUserComponent,
  canActivate: [AuthGuard]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoildocRoutingModule { }
