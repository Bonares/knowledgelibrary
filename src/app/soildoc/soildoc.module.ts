import { SearchbarModule } from './../shared/searchbar/searchbar.module';
import { PaginationModule } from './../shared/pagination/pagination.module';
import { DownloadPaperModule } from './../shared/download-paper/download-paper.module';
import { PaperOverviewModule } from './../shared/paper-overview/paper-overview.module';
import { SharedModule } from './../shared/shared.module';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ContactComponent } from './contact/contact.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';

import { SoildocRoutingModule } from './soildoc-routing';
import { SoilDocSearchComponent } from './soil-doc-search/soil-doc-search.component';
import { SoilDocComponent } from './soil-doc/soil-doc.component';
import { DragdropDirective } from './common/dragdrop/dragdrop.directive';
import { NgbTypeaheadModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ReferencesComponent } from './references/references.component';
import { ContactUserComponent } from './contact-user/contact-user.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

@NgModule({
  declarations: [SoilDocSearchComponent, SoilDocComponent, ContactComponent, DragdropDirective, ReferencesComponent, ContactUserComponent],
  imports: [
    SoildocRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    FormsModule,
    CommonModule,
    SharedModule,
    PaperOverviewModule,
    NgxSliderModule,
    DownloadPaperModule,
    NgbTypeaheadModule,
    NgbTooltipModule,
    PaginationModule,
    SearchbarModule
  ]
})
export class SoildocModule { }
