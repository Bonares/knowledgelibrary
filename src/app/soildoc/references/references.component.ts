import { MetaService } from './../../shared/meta.service';
import { TranslateService } from '@ngx-translate/core';
import { SoildocService } from './../soil-doc/shared/soildoc.service';
import { Component, OnInit } from '@angular/core';
import { faCarrot, faCaretRight, faSync, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'kl-references',
  templateUrl: './references.component.html',
  styleUrls: []
})
export class ReferencesComponent implements OnInit {

  faSearch = faSearch;
  faCaretRight = faCaretRight;
  faCarrot = faCarrot;
  faSync = faSync;

  isLoading = false;
  isColEnabled = false;
  colString = '';

  constructor(
    private ss: SoildocService,
    private metaService: MetaService
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('SOILDOC.REFERENCES.HEADING', 'SOILDOC.REFERENCES.DESCRIPTION');
    this.ss.getCOLCitation().subscribe(datas => {
      if (datas && datas.citation) {
        this.colString = datas.citation;
        this.isColEnabled = true;
      } else {
        this.isColEnabled = false;
      }
      this.isLoading = false;
    },
    error => {
      this.isLoading = false;
      this.isColEnabled = false;
    });
  }
}
