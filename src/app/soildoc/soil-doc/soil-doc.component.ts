import { MetaService } from './../../shared/meta.service';
import { timer } from 'rxjs';
import { SoildocService } from './shared/soildoc.service';
import { SoildocErrorMessages } from './shared/soildoc-error-messages';
import { SoildocFactory } from './shared/SoildocFactory';
import { UntypedFormGroup, UntypedFormBuilder, Validators, UntypedFormArray, AbstractControl } from '@angular/forms';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Soildoc } from './shared/soildoc';
import { SoildocValidators } from './shared/soildoc-validators';
import { ActivatedRoute, Router } from '@angular/router';
import { faTrashAlt, faCloudUploadAlt, faExclamationCircle, faSignInAlt, faLongArrowAltRight, faHandPointRight, faSearch, faForward,
faSpinner, faAsterisk, faPlus, faSave, faQuestionCircle, faBookOpen } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-soil-doc',
  templateUrl: './soil-doc.component.html',
  styleUrls: []
})
export class SoilDocComponent implements OnInit {

  @ViewChild('fillInfoButton', {static: true}) buttonDOI: ElementRef;

  faBookOpen = faBookOpen;

  faQuestionCircle = faQuestionCircle;
  faSave = faSave;
  faPlus = faPlus;
  faAsterisk = faAsterisk;
  faSpinner = faSpinner;
  faForward = faForward;
  faSearch = faSearch;
  faHandPointRight = faHandPointRight;
  faLongArrowAltRight = faLongArrowAltRight;
  faSignInAlt = faSignInAlt;
  faExclamationCircle = faExclamationCircle;
  faCloudUploadAlt = faCloudUploadAlt;
  faTrashAlt = faTrashAlt;
  soildoc = SoildocFactory.empty();
  soildocForm: UntypedFormGroup;
  doiForm: UntypedFormGroup;
  errors: { [key: string]: string } = {};
  failSave: Boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  doiAlone: string;

  isLoading = false;
  isSaveLoading = false;
  isUploadLoading = false;

  originalSoildoc: Soildoc;

  authors: UntypedFormArray;
  authorErrors: [{[key: string]: string}] = [{}];
  fileErrors = [];

  doiPDF = [];

  typesAPI = [
    {id: 'book-section', label: 'Book Section'},
    {id: 'monograph', label: 'Monograph'},
    {id: 'report', label: 'Report'},
    {id: 'peer-review', label: 'Peer Review'},
    {id: 'book-track', label: 'Book Track'},
    {id: 'journal-article', label: 'Journal Article'},
    {id: 'book-part', label: 'Part'},
    {id: 'other', label: 'Other'},
    {id: 'book', label: 'Book'},
    {id: 'journal-volume', label: 'Journal Volume'},
    {id: 'book-set', label: 'Book Set'},
    {id: 'reference-entry', label: 'Reference Entry'},
    {id: 'proceedings-article', label: 'Proceedings Article'},
    {id: 'journal', label: 'Journal'},
    {id: 'component', label: 'Component'},
    {id: 'book-chapter', label: 'Book Chapter'},
    {id: 'proceedings-series', label: 'Proceedings Series'},
    {id: 'report-series', label: 'Report Series'},
    {id: 'proceedings', label: 'Proceedings'},
    {id: 'standard', label: 'Standard'},
    {id: 'reference-book', label: 'Reference Book'},
    {id: 'posted-content', label: 'Posted Content'},
    {id: 'journal-issue', label: 'Journal Issue'},
    {id: 'dissertation', label: 'Dissertation'},
    {id: 'dataset', label: 'Dataset'},
    {id: 'book-series', label: 'Book Series'},
    {id: 'edited-book', label: 'Edited Book'},
    {id: 'standard-series', label: 'Standard Series'}
  ];

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private ss: SoildocService,
    private router: Router,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILDOC.TITLEMETA', 'SOILDOC.DESCRIPTIONMETA');
    this.failSave = false;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.isEdit = true;
      this.soildoc = data['soildoc'];
      this.originalSoildoc = data['soildoc'];
    }
    this.initSoildoc();
  }

  initSoildoc() {

    this.buildAuthorsArray();

    this.doiForm = this.fb.group({
      doiAlone: [this.doiAlone,
        [
          Validators.required,
          SoildocValidators.doiFormat
        ], SoildocValidators.soildocDOIAvailable(this.ss, this.originalSoildoc ? this.originalSoildoc.doi : null)]
    });

    this.doiForm.statusChanges.subscribe(() => this.updateErrorMessagesDOI());

    this.soildocForm = this.fb.group({
      doi: [this.soildoc.doi,
        [
          SoildocValidators.doiFormat
        ], SoildocValidators.soildocDOIAvailable(this.ss, this.originalSoildoc ? this.originalSoildoc.doi : null)],
      type: [this.soildoc.type,
        [
          Validators.required
        ]],
      title: [this.soildoc.title,
        [
          Validators.required
        ]],
      authors: this.authors,
      year: [this.soildoc.year,
        [
          Validators.required,
          SoildocValidators.isNumber
        ]],
      media_title: [this.soildoc.media_title,
        [
        ]],
      publisher: [this.soildoc.publisher,
        [
        ]],
      institution: [this.soildoc.institution,
        [
        ]],
      volume: [this.soildoc.volume,
        [
          SoildocValidators.issueNumber
        ]],
      issue_number: [this.soildoc.issue_number,
        [
          SoildocValidators.issueNumber
        ]],
      page_first: [this.soildoc.page_first,
        [
          SoildocValidators.isPageNumber
        ]],
      page_last: [this.soildoc.page_last,
        [
          SoildocValidators.isPageNumber
        ]],
      url: [this.soildoc.url,
        [
          SoildocValidators.urlFormat
        ]],
      isbn: [this.soildoc.isbn,
        [
          SoildocValidators.isbnFormat
        ]],
      abstract: [this.soildoc.abstract,
        [
        ]],
      data_doi: [this.soildoc.data_doi,
        [
          SoildocValidators.doiFormat
        ]]
    });

    this.soildocForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  buildAuthorsArray() {
    this.authors = this.fb.array(
      this.soildoc.authors.map(
        a => this.fb.group({
          name: this.fb.control(a.name, [Validators.required, SoildocValidators.authorFormat])
        })
      )
    );
    this.resetAuthorErrorArray();
  }

  addAuthorControl() {
    this.authors.push(this.fb.group({
      name: this.fb.control('', [Validators.required, SoildocValidators.authorFormat])
    })
    );
    this.updateErrorMessages();
  }

  removeAuthorControl(authorIndex: number) {
    this.authors.removeAt(authorIndex);
    this.updateErrorMessages();
  }

  resetAuthorErrorArray() {
    this.authorErrors = [{}];
    for (let i = 0; i < this.authors.controls.length ; i++) {
      this.authorErrors[i] = {'name': ''};
    }
  }

  uploadFile(filelist: FileList) {
    this.isUploadLoading = true;
    this.fileErrors = [];
    this.doiPDF = [];
    if (filelist[0]) {
      const file = filelist.item(0);
      if (file.type === 'application/pdf') {
        const formData = new FormData();
        formData.append('file', file, file.name);
        this.ss.uploadPDF(formData)
        .subscribe(
          data => {
            if (data.length === 0) {
              this.fileErrors.push('SOILDOC.FAILFORM.NODOISFOUND');
            } else {
              this.doiPDF = data;
              if (this.doiPDF.length === 1) {
                this.changeDOI(this.doiPDF[0]);
              } else {
                this.updateValueAndDirty(this.doiForm.controls['doiAlone'], '');
              }
            }
            this.isUploadLoading = false;
          },
          error => {
            this.isUploadLoading = false;
            this.fileErrors.push('SOILDOC.FAILFORM.FAILUPLOAD');
          }
        );
      } else {
        this.isUploadLoading = false;
        this.fileErrors.push('SOILDOC.FAILFORM.NOTPDF');
      }
    }
  }

  changeDOI(doi: string) {
    this.updateValueAndDirty(this.doiForm.controls['doiAlone'], doi);
    timer(500).subscribe(val => {
      if (this.doiForm.valid) {
        this.buttonDOI.nativeElement.click();
      }
    });
  }

  fillInformationByDOI() {

    const doi = this.doiForm.controls['doiAlone'].value;
    this.soildoc.authors = [];
    this.authors.clear();
    this.addAuthorControl();
    this.resetForm();
    this.isLoading = true;
    this.ss.getInformationByDOI(doi)
      .subscribe(
        data => {
          if (data.message) {
            this.updateFormByCrossref(data);
          }

          this.isLoading = false;
        },
        error => {
          this.setErrorDOI(this.doiForm.controls['doiAlone'], error);
          this.isLoading = false;
        }
      );
  }

  updateFormByCrossref(data: any) {
    this.updateValueAndDirty(this.soildocForm.controls['type'], data.message.type ? data.message.type : '');
    if (this.checkObjHasKeys(data.message, ['title', 0])) {
      this.updateValueAndDirty(this.soildocForm.controls['title'], data.message.title[0] ? data.message.title[0] : '');
    }
    if (data.message.author) {
      let authorIterator = 0;
      for (let i = 0; i < data.message.author.length ; i++) {
        if (data.message.author[i].given && data.message.author[i].family) {
          if (i > 0) {
            this.addAuthorControl();
            authorIterator += 1;
          }
          this.updateValueAndDirty(this.authors.controls[authorIterator]['controls']['name'],
          data.message.author[i].family + ', ' + data.message.author[i].given.split(' ').map((n) => n[0]).join('. ') + '.');
        }
      }
    }
    if (this.checkObjHasKeys(data.message, ['issued', 'date-parts', 0])) {
      // eslint-disable-next-line max-len
      this.updateValueAndDirty(this.soildocForm.controls['year'], data.message['issued']['date-parts'][0][0] ? data.message['issued']['date-parts'][0][0] : '');
    }
    if (this.checkObjHasKeys(data.message, ['container-title', 0])) {
      this.updateValueAndDirty(this.soildocForm.controls['media_title'], data.message['container-title'][0] ? data.message['container-title'][0] : '');
    }
    this.updateValueAndDirty(this.soildocForm.controls['publisher'], data.message.publisher ? data.message.publisher : '');
    this.updateValueAndDirty(this.soildocForm.controls['volume'], data.message.volume ? data.message.volume : '');
    this.updateValueAndDirty(this.soildocForm.controls['issue_number'], data.message.issue ? data.message.issue : '');
    if (data.message.page) {
      if (data.message.page.includes('-')) {
        this.updateValueAndDirty(this.soildocForm.controls['page_first'], data.message.page.split('-')[0] ? data.message.page.split('-')[0] : '');
        this.updateValueAndDirty(this.soildocForm.controls['page_last'], data.message.page.split('-')[1] ? data.message.page.split('-')[1] : '');
      } /** else {
        this.soildocForm.controls['book_pages'].setValue(data.message.page ? data.message.page : '');
      } **/
    }
    this.updateValueAndDirty(this.soildocForm.controls['url'], data.message.URL ? data.message.URL : '');
    /**
    if (this.checkObjHasKeys(data.message, ['link', 0, 'URL'])) {
      this.updateValueAndDirty(this.soildocForm.controls['url'], data.message.link[0].URL ? data.message.link[0].URL : '');
    } */
    this.updateValueAndDirty(this.soildocForm.controls['doi'], data.message.DOI ? data.message.DOI : '');

    if (this.checkObjHasKeys(data.message, ['ISBN', 0])) {
      this.updateValueAndDirty(this.soildocForm.controls['isbn'], data.message['ISBN'][0] ? data.message['ISBN'][0] : '');
    } else if (this.checkObjHasKeys(data.message, ['ISSN', 0])) {
      this.updateValueAndDirty(this.soildocForm.controls['isbn'], data.message['ISSN'][0] ? data.message['ISSN'][0] : '');
    }
    this.updateValueAndDirty(this.soildocForm.controls['abstract'], data.message.abstract ? data.message.abstract : '');
  }

  updateValueAndDirty(control: AbstractControl, value: any) {
    control.markAsDirty();
    control.setValue(value);
  }

  checkObjHasKeys(obj, keys) {
    for (let i = 0; i < keys.length ; i++) {
      if ( ! obj.hasOwnProperty(keys[i])) {
        return false;
      }
      obj = obj[keys[i]];
    }
    return true;
  }

  submitFormSoildoc() {
    if (this.soildocForm.valid) {
      this.isSaveLoading = true;
      const soildoc = SoildocFactory.fromObject(this.soildocForm.value);
        if (this.isEdit) {
          soildoc._id = this.soildoc._id;
          this.ss.update(soildoc)
            .subscribe(
                datas => {
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.router.navigate(['/soilstudy-detail/type-of-study/' + soildoc._id]);
                    this.isSaveLoading = false;
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.ss.create(soildoc)
          .subscribe(
              datas => {
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.router.navigate(['/soilstudy-detail/type-of-study/' + datas._id]);
                  this.isSaveLoading = false;
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  resetForm() {
    this.soildocForm.reset(SoildocFactory.empty());
  }

  setErrorDOI(control: AbstractControl, error: any) {

    if (error.status === 404) {
      if (error.error === 'Resource not found.') {
        control.setErrors({notfound: true});
      } else {
        control.setErrors({general: true});
      }
    } else {
      if (error['name']) {
        if (error['name'] === 'TimeoutError') {
          control.setErrors({timeout: true});
        } else {
          control.setErrors({general: true});
        }
      } else {
        control.setErrors({general: true});
      }

    }
  }

  updateErrorMessages() {
    this.errors = {};
    this.resetAuthorErrorArray();

    for (const message of SoildocErrorMessages) {
      const control = this.soildocForm.get(message.forControl);
      if (!control) {
        for (let i = 0; i < this.authors.controls.length ; i++) {
          const controlauthor = this.authors.controls[i].get(message.forControl);
          if (controlauthor &&
            controlauthor.dirty &&
            controlauthor.invalid &&
            controlauthor.errors[message.forValidator] &&
            !this.authorErrors[i][message.forControl]) {
            this.authorErrors[i][message.forControl] = message.text;
          }
        }
      } else {
          if (control &&
            control.dirty &&
            control.invalid &&
            control.errors[message.forValidator] &&
            !this.errors[message.forControl]) {
            this.errors[message.forControl] = message.text;
        }
      }
    }
  }

  updateErrorMessagesDOI() {
    this.errors = {};

    for (const message of SoildocErrorMessages) {
      const control = this.doiForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

}
