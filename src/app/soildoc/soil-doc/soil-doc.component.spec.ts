import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SoilDocComponent } from './soil-doc.component';

describe('SoilDocComponent', () => {
  let component: SoilDocComponent;
  let fixture: ComponentFixture<SoilDocComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
