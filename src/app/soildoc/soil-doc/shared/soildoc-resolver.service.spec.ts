import { TestBed } from '@angular/core/testing';

import { SoildocResolverService } from './soildoc-resolver.service';

describe('SoildocResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoildocResolverService = TestBed.get(SoildocResolverService);
    expect(service).toBeTruthy();
  });
});
