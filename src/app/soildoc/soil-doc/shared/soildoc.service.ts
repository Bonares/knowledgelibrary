import { SoildocFactory } from './SoildocFactory';
import { Soildoc } from './Soildoc';
import { catchError, timeout, retry, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AuthGuard } from './../../../shared/auth.guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { SoildocRaw } from './soildoc-raw';

@Injectable({
  providedIn: 'root'
})
export class SoildocService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Soildoc> {
    return this.httpClient
      .get<SoildocRaw>(`${this.api}/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawSoildoc => SoildocFactory.fromObject(rawSoildoc),
        ),
        catchError(this.handleError)
      );
  }

  create(soildoc: Soildoc): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/soildoc`, soildoc)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(soildoc: Soildoc): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/soildoc/${soildoc._id}`, soildoc)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }

  remove(id: String): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .delete(`${this.api}/soildoc/${id}`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  doiAvailable(doi: string): Observable<Boolean> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
        .post<Boolean>(`${this.api}/soildoc/check/doi`, {doi: doi})
        .pipe(
          timeout(15000),
          catchError(this.handleError)
        );
    }
  }

  getInformationByDOI(doi: string): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .get(`https://api.crossref.org/works/${encodeURIComponent(doi)}?mailto=klibrary@bonares.de`)
      .pipe(
        timeout(30000),
        catchError(this.handleError) // then handle the error
      );
    }
  }

  getAll(): Observable<Array<Soildoc>> {
    return this.httpClient
      .get<SoildocRaw[]>(`${this.api}/soildocs`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        map(rawSoildocs => rawSoildocs
          .map(rawSoildoc => SoildocFactory.fromObject(rawSoildoc)),
        ),
        timeout(15000),
        catchError(this.handleError)
      );
  }

  getAllById(soildocs: String[]): Observable<Array<Soildoc>> {
    return this.httpClient
      .post<SoildocRaw[]>(`${this.api}/soildocsid`, soildocs)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        map(rawSoildocs => rawSoildocs
          .map(rawSoildoc => SoildocFactory.fromObject(rawSoildoc)),
        ),
        timeout(15000),
        catchError(this.handleError)
      );
  }

  uploadPDF(data: FormData): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/soildoc/pdf`, data)
      .pipe(
        timeout(25000),
        catchError(this.handleError)
      );
    }
  }

  updateEnabledUsers(usernames: String[], id: String): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/soildoc/enabledusers/${id}`, usernames)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  updateComment(comment: string, id: String): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/soildoc/comment/${id}`, {comment: comment})
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  getCOLCitation(): Observable<any> {
    return this.httpClient
    .get('https://api.catalogueoflife.org/dataset/3LR')
    .pipe(
      timeout(15000),
      catchError(this.handleError)
    );
  }

  getCountUsers(): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .get(`${this.api}/soildocs/usercount`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  downloadPaperInformation(papers: String[]): Observable<any> {
    return this.httpClient
      .post(`${this.api}/download/papers`, {papers: papers})
      .pipe(
        timeout(30000),
        catchError(this.handleError)
      );
  }
}
