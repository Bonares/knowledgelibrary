import { Author } from './Authors';
export class Soildoc {
  constructor(
    public _id: string,
    public doi: string,
    public bibtex: string,
    public type: string,
    public title: string,
    public authors: Author[],
    public year: number,
    public media_title: string,
    public publisher: string,
    public institution: string,
    public volume: string,
    public issue_number: string,
    public page_first: string,
    public page_last: string,
    public url: string,
    public isbn: string,
    public abstract: string,
    public citation: string,
    public lastModifiedBy: string,
    public modifiedDate: any,
    public createdBy: string,
    public enabledUsers: String[],
    public comment: string,
    public data_doi: string
   ) {}
}
