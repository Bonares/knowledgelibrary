import { Observable, timer, of } from 'rxjs';
import { UntypedFormControl, FormGroup, AbstractControl } from '@angular/forms';
import { SoildocService } from './soildoc.service';
import { switchMap, map } from 'rxjs/operators';
export class SoildocValidators {

  static soildocDOIAvailable(ss: SoildocService, editOriginalDOI: string) {
    return function(control: UntypedFormControl): Observable<{
      [error: string]: any }> {
        return timer(200).pipe(
          switchMap(() => {
            if (!control.value) {
              return of(null);
            }
            if (editOriginalDOI) {
              if (editOriginalDOI === control.value) {
                return of(null);
              }
            }
            return ss.doiAvailable(control.value)
            .pipe(
              map(available =>
                available ? null : { doiAvailable: { valid: false }})
            );
          })
        );
      };
  }

  static doiFormat(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const doiPattern = /^10.\d{4,9}\/[-._;()/:A-Z0-9\[\]]+$/i;
    return doiPattern.test(control.value) ? null : {
      doiFormat: {valid: false}
    };
  }

  static authorFormat(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const authorPattern = /^[A-Za-zÀ-ȕ]+(( |-){1}[A-Za-zÀ-ȕ]+)*, ([A-Za-zÀ-ȕ]{1}\.)( [A-Za-zÀ-ȕ]{1}\.)*$/;
    return authorPattern.test(control.value) ? null : {
      authorFormat: {valid: false}
    };
  }

  static urlFormat(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const urlPattern = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+(?:[a-z\u00a1-\uffff]{2,}\.?))(?::\d{2,5})?(?:[/?#]\S*)?$/i;
    return urlPattern.test(control.value) ? null : {
      urlFormat: {valid: false}
    };
  }

  static isbnFormat(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const isbnPattern = /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;
    const issnPattern = /^[\S]{4}\-[\S]{4}$/;

    if (!isbnPattern.test(control.value)) {
      return issnPattern.test(control.value) ? null : {
        isbnFormat: {valid: false}
      };
    } else {
      return null;
    }
  }

  static issueNumber(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }
    const numberPattern = /^((\d)+|(\d)+\-(\d)+)$/;
    return numberPattern.test(control.value) ? null : {
      issuenumberFormat: {valid: false}
    };
  }

  static isPageNumber(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^S*(\d)+$/i;
    return numberPattern.test(control.value) ? null : {
      pagenumberFormat: {valid: false}
    };
  }

  static isNumber(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(\d)+$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat: {valid: false}
    };
  }

  static uniqueValidator(control: AbstractControl): { [error: string]: any } {
    if (control.value) {
      const uniqueValues = new Set(control.value.enabledUsers.map(v => v.name));
      if (uniqueValues.size < control.value.enabledUsers.length) {
          return {notunique:  {valid: false}};
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

}
