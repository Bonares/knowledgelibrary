export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const SoildocErrorMessages = [
  new ErrorMessage('doiAlone', 'required', 'SOILDOC.FAILFORM.REQUIREDFILL'),
  new ErrorMessage('doiAlone', 'doiFormat', 'SOILDOC.FAILFORM.DOIFORMAT'),
  new ErrorMessage('doiAlone', 'doiAvailable', 'SOILDOC.FAILFORM.DOIAVAILABLE'),
  new ErrorMessage('doiAlone', 'notfound', 'SOILDOC.FAILFORM.DOINOTFOUND'),
  new ErrorMessage('doiAlone', 'timeout', 'SOILDOC.FAILFORM.DOITIMEOUT'),
  new ErrorMessage('doiAlone', 'general', 'SOILDOC.FAILFORM.DOIGENERAL'),
  new ErrorMessage('doi', 'doiFormat', 'SOILDOC.FAILFORM.DOIFORMAT'),
  new ErrorMessage('doi', 'doiAvailable', 'SOILDOC.FAILFORM.DOIAVAILABLE'),
  new ErrorMessage('data_doi', 'doiFormat', 'SOILDOC.FAILFORM.DOIFORMAT'),
  new ErrorMessage('type', 'required', 'SOILDOC.FAILFORM.REQUIRED'),
  new ErrorMessage('title', 'required', 'SOILDOC.FAILFORM.REQUIRED'),
  new ErrorMessage('name', 'required', 'SOILDOC.FAILFORM.REQUIRED'),
  new ErrorMessage('name', 'authorFormat', 'SOILDOC.FAILFORM.AUTHORNAME'),
  new ErrorMessage('year', 'required', 'SOILDOC.FAILFORM.REQUIRED'),
  new ErrorMessage('year', 'numberFormat', 'SOILDOC.FAILFORM.NUMBER'),
  new ErrorMessage('volume', 'issuenumberFormat', 'SOILDOC.FAILFORM.ISSUENUMBER'),
  new ErrorMessage('issue_number', 'issuenumberFormat', 'SOILDOC.FAILFORM.ISSUENUMBER'),
  new ErrorMessage('page_first', 'pagenumberFormat', 'SOILDOC.FAILFORM.PAGENUMBER'),
  new ErrorMessage('page_last', 'pagenumberFormat', 'SOILDOC.FAILFORM.PAGENUMBER'),
  new ErrorMessage('url', 'urlFormat', 'SOILDOC.FAILFORM.URLFORMAT'),
  new ErrorMessage('isbn', 'isbnFormat', 'SOILDOC.FAILFORM.ISBNFORMAT')
];
