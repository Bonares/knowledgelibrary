import { User } from './User';
import { Author } from './Authors';
export interface SoildocRaw {
  _id: string;
  doi: string;
  bibtex: string;
  type: string;
  title: string;
  authors: Author[];
  year: number;
  media_title: string;
  publisher: string;
  institution: string;
  volume: string;
  issue_number: string;
  page_first: number;
  page_last: number;
  url: string;
  isbn: string;
  abstract: string;
  citation: string;
  lastModifiedBy: string;
  modifiedDate: any;
  createdBy: string;
  enabledUsers: User[];
  comment: string;
  data_doi: string;
}
