import { AuthGuard } from './../../../shared/auth.guard';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Soildoc } from './Soildoc';
import { SoildocService } from './soildoc.service';

@Injectable({
  providedIn: 'root'
})
export class SoildocResolverService {

  constructor(private ss: SoildocService,
    private ag: AuthGuard) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Soildoc> {
    return this.ss.getOne(route.params['id']);
  }
}
