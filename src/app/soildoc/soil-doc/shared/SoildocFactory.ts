import { Soildoc } from './Soildoc';
import { SoildocRaw } from './soildoc-raw';

export class SoildocFactory {

  static empty(): Soildoc {
    return new Soildoc('', '', '', '', '', [{name: ''}], null, '', '', '', '', null, null, null,
    '', '', '', '', '', '', '', [], '', '');
  }

  static fromObject(rawSoildoc: SoildocRaw | any): Soildoc {
    return new Soildoc(
      rawSoildoc._id,
      rawSoildoc.doi,
      rawSoildoc.bibtex,
      rawSoildoc.type,
      rawSoildoc.title,
      rawSoildoc.authors,
      rawSoildoc.year,
      rawSoildoc.media_title,
      rawSoildoc.publisher,
      rawSoildoc.institution,
      rawSoildoc.volume,
      rawSoildoc.issue_number,
      rawSoildoc.page_first,
      rawSoildoc.page_last,
      rawSoildoc.url,
      rawSoildoc.isbn,
      rawSoildoc.abstract,
      rawSoildoc.citation,
      rawSoildoc.lastModifiedBy,
      rawSoildoc.modifiedDate,
      rawSoildoc.createdBy,
      rawSoildoc.enabledUsers,
      rawSoildoc.comment,
      rawSoildoc.data_doi
    );
  }
}
