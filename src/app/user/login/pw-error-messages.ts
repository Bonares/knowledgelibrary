export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const PasswordErrorMessages = [
  new ErrorMessage('email', 'required', 'USER.LOGIN.FAILFORM.REQUIREDEMAIL' ),
  new ErrorMessage('email', 'mailRegistered', 'USER.LOGIN.FAILFORM.MAILNOTREGISTERED' )
];
