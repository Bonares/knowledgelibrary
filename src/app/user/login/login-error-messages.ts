export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const LoginErrorMessages = [
  new ErrorMessage('username', 'required', 'USER.LOGIN.FAILFORM.REQUIREDUSERNAME' ),
  new ErrorMessage('username', 'userEnabled', 'USER.LOGIN.FAILFORM.ENABLEDUSERNAME'),
  new ErrorMessage('password', 'required', 'USER.LOGIN.FAILFORM.REQUIREDPASSWORD'),
  new ErrorMessage('password', 'incorrect', 'USER.LOGIN.FAILFORM.WRONGPASSWORD')
];
