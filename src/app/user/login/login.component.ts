import { MetaService } from './../../shared/meta.service';
import { timer } from 'rxjs';
import { PasswordErrorMessages } from './pw-error-messages';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './../../shared/authentication/auth.service';
import { UserValidators } from './../shared/user.validators';
import { UserService } from './../../shared/user.service';
import { UserFactory } from './../../shared/UserFactory';
import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { LoginErrorMessages } from './login-error-messages';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { faSpinner, faEnvelope, faUser, faLock, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'nl-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  faUserPlus = faUserPlus;
  faEnvelope = faEnvelope;
  faLock = faLock;
  faUser = faUser;
  faSpinner = faSpinner;
  user = UserFactory.empty();
  errors: { [key: string]: string } = {};
  errorsPW: { [key: string]: string } = {};
  loginForm: UntypedFormGroup;
  forgotpwForm: UntypedFormGroup;
  expired: Boolean;
  failLogin: Boolean;
  successRegister: Boolean = false;
  failRegister: Boolean = false;
  isLoading: Boolean = false;

  returnUrl: string;

  failPW: Boolean;
  successPW: Boolean;
  isLoadingPW: Boolean = false;
  email = '';

  modalPassword : bootstrap.Modal;

  constructor(
    private route: ActivatedRoute,
    private us: UserService,
    private fb: UntypedFormBuilder,
    private as: AuthService,
    private router: Router,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('USER.LOGIN.HEADING', 'USER.LOGIN.HEADING');
    this.modalPassword = new bootstrap.Modal(document.getElementById('modalPassword'));
    this.initUser();
    this.initPW();
    if (this.as.getToken()) {
      this.expired = this.as.isTokenExpired();
    }

    const data = this.route.snapshot.data;
    if (data['register'] === true) {
      this.successRegister = true;
    } else if (data['register'] === false) {
      this.failRegister = true;
    }

    this.failLogin = false;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/soildoc/soil-doc-search';
  }

  initUser() {
    this.loginForm = this.fb.group({
      username: [this.user.username,
        [
          Validators.required
        ], UserValidators.userExists(this.us)],
      password: [this.user.password,
        [
          Validators.required
        ]]
    });

    this.loginForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  initPW() {
    this.forgotpwForm = this.fb.group({
      email: ['',
        [
          Validators.required
        ], UserValidators.mailRegistered(this.us)]
    });

    this.forgotpwForm.statusChanges.subscribe(() => this.updateErrorMessagesPW());
  }

  updateErrorMessages() {
    this.errors = {};
    for (const message of LoginErrorMessages) {
      const control = this.loginForm.get(message.forControl);
      if (control &&
          control.dirty &&
          control.invalid &&
          control.errors[message.forValidator] &&
          !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

  updateErrorMessagesPW() {
    this.errorsPW = {};
    for (const message of PasswordErrorMessages) {
      const control = this.forgotpwForm.get(message.forControl);
      if (control &&
          control.dirty &&
          control.invalid &&
          control.errors[message.forValidator] &&
          !this.errorsPW[message.forControl]) {
        this.errorsPW[message.forControl] = message.text;
      }
    }
  }

  submitForm() {
    if (this.loginForm.valid) {
      this.isLoading = true;
      const user = UserFactory.fromObject(this.loginForm.value);
      if (user.username && user.password) {
        this.as.login(user.username, user.password)
            .subscribe(
                data => {
                    this.failLogin = false;
                    this.isLoading = false;
                    this.router.navigateByUrl(this.returnUrl);
                },
                error => {
                  this.handleError(error);
                  this.isLoading = false;
                }
            );
      }
      // this.user = UserFactory.empty();
      // this.loginForm.reset(UserFactory.empty());
    }
  }

  openModalPassword() {
    this.modalPassword.show();
  }

  submitFormPW() {
    this.isLoadingPW = true;
    if (this.forgotpwForm.valid) {
      const email = {
        email: this.forgotpwForm.value.email
      };
      this.us.resetPW(email)
        .subscribe(
            data => {
              this.failPW = false;
              this.successPW = true;
              this.isLoadingPW = false;
              this.forgotpwForm.reset();
              timer(10000).subscribe(val => {
                this.successPW = false;
              });
            },
            error => {
            this.failPW = true;
            this.isLoadingPW = false;
            }
        );
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 401) {
      this.loginForm.controls.password.setErrors({incorrect: true});
    } else {
      this.failLogin = true;
    }
  }

}
