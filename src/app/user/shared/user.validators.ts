import { UserService } from './../../shared/user.service';
import { map, switchMap } from 'rxjs/operators';
import { FormControl, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { timer, of } from 'rxjs';

export class UserValidators {

static usernameFormat(control: AbstractControl): ValidationErrors | null {
    if (!control.value) { return null; }

    const usernamePattern = /^([a-z0-9])+$/;
    return usernamePattern.test(control.value) ? null : {
      usernameFormat: {valid: false}
    };
}

static passwordFormat(control: AbstractControl): ValidationErrors | null {
  if (!control.value) { return null; }

  const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#@$!%*?&.:\-_])[A-Za-z\d#@$!%*?&.:\-_]{1,}$/;
  return passwordPattern.test(control.value) ? null : {
    passwordFormat: {valid: false}
  };
}

static passwordsMatch(ac: AbstractControl): ValidationErrors | null {
  if (ac.get('password').value !== ac.get('repeatpassword').value) {
    ac.get('repeatpassword').setErrors( {notequal: true} );
  } else {
    return null;
  }
}

static userExists(us: UserService) {
  return function(control: AbstractControl): Observable<{
    [error: string]: any }> {
      return timer(200).pipe(
        switchMap(() => {
          if (!control.value) {
            return of(null);
          }
          return us.userEnabled(control.value).pipe(
            map(exists =>
              exists ? null : { userEnabled: { valid: false }})
          );
        })
      );

    };
}

static userAvailable(us: UserService) {
  return function(control: AbstractControl): Observable<{
    [error: string]: any }> {
      return timer(200).pipe(
        switchMap(() => {
          if (!control.value) {
            return of(null);
          }
          return us.userAvailable(control.value).pipe(
            map(available =>
              available ? null : { userAvailable: { valid: false }})
          );
        })
      );
    };
}

static mailAvailable(us: UserService) {
  return function(control: AbstractControl): Observable<{
    [error: string]: any }> {
      return timer(200).pipe(
        switchMap(() => {
          if (!control.value) {
            return of(null);
          }
          return us.mailAvailable(control.value).pipe(
            map(available =>
              available ? null : { mailAvailable: { valid: false }})
          );
        })
      );
    };
}

static mailRegistered(us: UserService) {
  return function(control: AbstractControl): Observable<{
    [error: string]: any }> {
      return timer(300).pipe(
        switchMap(() => {
          if (!control.value) {
            return of(null);
          }
          return us.mailAvailable(control.value).pipe(
            map(available =>
              available ? { mailRegistered: { valid: false }} : null)
          );
        })
      );
    };
}
}
