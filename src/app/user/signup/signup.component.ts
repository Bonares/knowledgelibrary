import { MetaService } from './../../shared/meta.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserValidators } from './../shared/user.validators';
import { UserFactory } from './../../shared/UserFactory';
import { SignupErrorMessages } from './signup-error-messages';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { UserService } from './../../shared/user.service';
import { Component, OnInit } from '@angular/core';
import { faAsterisk, faSpinner, faUser, faLock, faIdCard, faUniversity, faEnvelope, faLanguage } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-signup',
  templateUrl: './signup.component.html',
  styles: []
})
export class SignupComponent implements OnInit {

  faLanguage = faLanguage;
  faEnvelope = faEnvelope;
  faUniversity = faUniversity;
  faIdCard = faIdCard;
  faLock = faLock;
  faUser = faUser;
  faAsterisk = faAsterisk;
  faSpinner = faSpinner;
  user = UserFactory.empty();
  errors: { [key: string]: string } = {};
  signupForm: UntypedFormGroup;
  failSubmit: Boolean;
  successRegister: Boolean = false;
  isLoading: Boolean = false;

  languageOptions = ['en', 'de'];

  constructor(
    private us: UserService,
    private fb: UntypedFormBuilder,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('USER.SIGNUP.HEADING', 'USER.SIGNUP.HEADING');
    this.initUser();
  }

  initUser() {
    this.signupForm = this.fb.group({
      username: [this.user.username, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
        UserValidators.usernameFormat
      ], UserValidators.userAvailable(this.us)],
      password: [this.user.password, [
        Validators.required,
        Validators.minLength(9),
        UserValidators.passwordFormat
      ]],
      repeatpassword: [null, [
        Validators.required,
        Validators.minLength(9)
      ]],
      firstname: [this.user.firstname, [
        Validators.required
      ]],
      lastname: [this.user.lastname, [
        Validators.required
      ]],
      institution: [this.user.institution, [
      ]],
      email: [this.user.email, [
        Validators.required,
        Validators.email
      ], UserValidators.mailAvailable(this.us)],
      language: [this.user.language, [
        Validators.required
      ]],
    }, {
      validators: UserValidators.passwordsMatch // your validation method
    });

    this.signupForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  updateErrorMessages() {
    this.errors = {};
    for (const message of SignupErrorMessages) {
      const control = this.signupForm.get(message.forControl);
      if (control &&
          control.dirty &&
          control.invalid &&
          control.errors[message.forValidator] &&
          !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

  submitForm() {
    if (this.signupForm.valid) {
      this.isLoading = true;
      const user = UserFactory.fromObject(this.signupForm.value);
      if (user.username && user.password && user.email) {
        this.us.createUser(user)
            .subscribe(
                data => {
                    this.failSubmit = false;
                    this.successRegister = true;
                    this.isLoading = false;
                    this.initUser();
                },
                error => {
                  this.handleError(error);
                  this.isLoading = false;
                }
            );
      }
    }
  }

  private handleError(error: HttpErrorResponse) {
    this.failSubmit = true;
  }

}
