import { UserService } from './../../shared/user.service';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegisterConfirmResolver {

  constructor(private us: UserService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Boolean> {
    return this.us.confirmOne(route.params['id']);
  }
}
