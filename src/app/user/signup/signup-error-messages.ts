export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const SignupErrorMessages = [
  new ErrorMessage('username', 'required', 'USER.SIGNUP.FAILFORM.REQUIREDUSERNAME'),
  new ErrorMessage('username', 'minlength', 'USER.SIGNUP.FAILFORM.MINLENGTHUSERNAME'),
  new ErrorMessage('username', 'maxlength', 'USER.SIGNUP.FAILFORM.MAXLENGTHUSERNAME'),
  new ErrorMessage('username', 'usernameFormat', 'USER.SIGNUP.FAILFORM.FORMATUSERNAME'),
  new ErrorMessage('username', 'userAvailable', 'USER.SIGNUP.FAILFORM.AVAILABLEUSERNAME'),
  new ErrorMessage('password', 'required', 'USER.SIGNUP.FAILFORM.REQUIREDPASSWORD'),
  new ErrorMessage('password', 'minlength', 'USER.SIGNUP.FAILFORM.MINLENGTHPASSWORD'),
  new ErrorMessage('password', 'passwordFormat', 'USER.SIGNUP.FAILFORM.FORMATPASSWORD'),
  new ErrorMessage('repeatpassword', 'required', 'USER.SIGNUP.FAILFORM.REQUIREDREPEATPASSWORD'),
  new ErrorMessage('repeatpassword', 'notequal', 'USER.SIGNUP.FAILFORM.NOTEQUALREPEATPASSWORD'),
  new ErrorMessage('firstname', 'required', 'USER.SIGNUP.FAILFORM.REQUIRED'),
  new ErrorMessage('lastname', 'required', 'USER.SIGNUP.FAILFORM.REQUIRED'),
  new ErrorMessage('institution', 'required', 'USER.SIGNUP.FAILFORM.REQUIRED'),
  new ErrorMessage('email', 'required', 'USER.SIGNUP.FAILFORM.REQUIREDEMAIL'),
  new ErrorMessage('email', 'email', 'USER.SIGNUP.FAILFORM.FORMATEMAIL'),
  new ErrorMessage('email', 'mailAvailable', 'USER.SIGNUP.FAILFORM.AVAILABLEMAIL')
];
