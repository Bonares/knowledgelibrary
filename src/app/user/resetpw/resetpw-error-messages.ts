export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const ResetpwErrorMessages = [
  new ErrorMessage('password', 'required', 'USER.SIGNUP.FAILFORM.REQUIREDPASSWORD'),
  new ErrorMessage('password', 'minlength', 'USER.SIGNUP.FAILFORM.MINLENGTHPASSWORD'),
  new ErrorMessage('password', 'passwordFormat', 'USER.SIGNUP.FAILFORM.FORMATPASSWORD'),
  new ErrorMessage('repeatpassword', 'required', 'USER.SIGNUP.FAILFORM.REQUIREDREPEATPASSWORD'),
  new ErrorMessage('repeatpassword', 'notequal', 'USER.SIGNUP.FAILFORM.NOTEQUALREPEATPASSWORD')
];
