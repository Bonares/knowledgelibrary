import { MetaService } from './../../shared/meta.service';
import { timer } from 'rxjs';
import { UserService } from './../../shared/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ResetpwErrorMessages } from './resetpw-error-messages';
import { UserValidators } from './../shared/user.validators';
import { UntypedFormGroup, Validators, UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kl-resetpw',
  templateUrl: './resetpw.component.html',
  styleUrls: []
})
export class ResetpwComponent implements OnInit {

  resetpwForm: UntypedFormGroup;
  errors: { [key: string]: string } = {};
  failChange: Boolean = false;
  failChangeAuth: Boolean = false;
  successChange: Boolean = false;
  isLoading: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private fb: UntypedFormBuilder,
    private us: UserService,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('USER.RESET.HEADING', 'USER.RESET.HEADING');
    this.initForm();
  }

  initForm() {
    this.resetpwForm = this.fb.group({
      password: ['', [
        Validators.required,
        Validators.minLength(9),
        UserValidators.passwordFormat
      ]],
      repeatpassword: [null, [
        Validators.required,
        Validators.minLength(9)
      ]]
    }, {
      validator: UserValidators.passwordsMatch // your validation method
    });

    this.resetpwForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  updateErrorMessages() {
    this.errors = {};
    for (const message of ResetpwErrorMessages) {
      const control = this.resetpwForm.get(message.forControl);
      if (control &&
          control.dirty &&
          control.invalid &&
          control.errors[message.forValidator] &&
          !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

  submitForm() {
    if (this.resetpwForm.valid) {
      this.isLoading = true;
      const pwobj = {
        newpw: this.resetpwForm.value.password,
        newpwrepeat: this.resetpwForm.value.repeatpassword,
        resetcode: this.route.snapshot.paramMap.get('resetcode')
      };
      this.us.changePW(pwobj)
        .subscribe(
            data => {
              this.failChange = false;
              this.successChange = true;
              this.isLoading = false;
              this.resetpwForm.reset();
              this.failChangeAuth = false;
              timer(5000).subscribe(val => {
                this.successChange = false;
              });
            },
            error => {
              this.handleError(error);
              this.successChange = false;
              this.isLoading = false;
            }
        );
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 401) {
      this.failChangeAuth = true;
    } else {
      this.failChange = true;
    }
  }

}
