import { ResetpwComponent } from './resetpw/resetpw.component';
import { RegisterConfirmResolver } from './signup/register-confirm-resolver.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { LoginGuard } from '../shared/login.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'login/:id',
    component: LoginComponent,
    resolve: {
      register: RegisterConfirmResolver
    }
  },
  {
    path: 'resetpw/:resetcode',
    component: ResetpwComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
