export interface UserRaw {
  firstname: string;
  lastname: string;
  institution: string;
  username: string;
  password: string;
  email: string;
  isEnabled: boolean;
  isAdmin: boolean;
  language: string;
  markedPapers: String[];
}
