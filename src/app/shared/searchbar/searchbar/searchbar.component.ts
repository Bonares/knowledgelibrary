import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'kl-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: []
})
export class SearchbarComponent implements OnChanges {

  faSearch = faSearch;

  constructor() { }

  soildocCollection = [];
  searchValue = '';

  @Input() yearsEnabled?: Boolean;

  @Input() filterEnabled?: Boolean;

  @Input() soildocCollectionOriginal: Soildoc[];
  @Input() soildocCollectionOriginalOwn?: Soildoc[];

  @Input() showOnlyOwn?: Boolean;
  @Input() showOnlyMarked: Boolean;

  @Input() minValue?: number;
  @Input() maxValue?: number;

  @Input() markedPapers: String[];

  @Output() collectionChange = new EventEmitter<any[]>();
  @Output() yearChange = new EventEmitter<any[]>(true);


  ngOnChanges(changes: SimpleChanges) {
    if (this.soildocCollectionOriginal.length > 0) {
      this.updateSearch((changes['minValue'] || changes['maxValue'] || changes['markedPapers']) ? false : true);
    }
  }

  updateSearch(updateYears?: boolean) {
    this.soildocCollection = [];
    let collection = [];
    if (this.showOnlyOwn) {
      collection = this.soildocCollectionOriginalOwn.slice(0);
    } else {
      collection = this.soildocCollectionOriginal.slice(0);
    }
    if (this.showOnlyMarked && collection.length > 0) {
      if (this.filterEnabled) {
        collection = collection.filter(elem => this.markedPapers.includes(elem.soildoc._id));
      } else {
        collection = collection.filter(paper => this.markedPapers.includes(paper._id));
      }
    }
    if (this.searchValue !== '') {
      if (this.filterEnabled) {
        collection = collection.filter(elem => elem.soildoc.title.toLowerCase().includes(this.searchValue.toLowerCase()) || 
        elem.soildoc.year.toString().includes(this.searchValue.toLowerCase()) || 
        elem.soildoc.authors.some(author => author.name.toLowerCase().includes(this.searchValue.toLowerCase())));
      } else {
        collection = collection.filter(soildoc => soildoc.title.toLowerCase().includes(this.searchValue.toLowerCase()) || 
        soildoc.year.toString().includes(this.searchValue.toLowerCase()) || 
        soildoc.authors.some(author => author.name.toLowerCase().includes(this.searchValue.toLowerCase())));
      }
    }
    this.soildocCollection = collection;
    if (this.yearsEnabled) {
      if (updateYears) {
        this.yearChange.emit(this.soildocCollection);
      }
      this.soildocCollection = collection.filter(soildoc => this.between(soildoc.year, this.minValue, this.maxValue));
    }
    this.collectionChange.emit(this.soildocCollection);
  }

  between(value: number, min: number, max: number) {
    return (value >= min && value <= max);
  }

}
