import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadPaperComponent } from './download-paper/download-paper.component';



@NgModule({
  declarations: [
    DownloadPaperComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FontAwesomeModule
  ],
  exports: [
    DownloadPaperComponent
  ]
})
export class DownloadPaperModule { }
