import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadPaperComponent } from './download-paper.component';

describe('DownloadPaperComponent', () => {
  let component: DownloadPaperComponent;
  let fixture: ComponentFixture<DownloadPaperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadPaperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
