import { faDownload, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from './../../authentication/auth.service';
import { SoildocService } from './../../../soildoc/soil-doc/shared/soildoc.service';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'kl-download-paper',
  templateUrl: './download-paper.component.html',
  styleUrls: []
})
export class DownloadPaperComponent implements OnInit {

  faDownload = faDownload;
  faSpinner = faSpinner;
  isLoadingDownload = false;
  errorDownload = false;

  isLoggedIn$: Observable<boolean>;

  constructor(
    private ss: SoildocService,
    private authService: AuthService
  ) { }

  @Input() soildocs: any[] = [];

  ngOnInit(): void {
    this.isLoggedIn$ = this.authService.isLoggedIn;
  }

  downloadPapers() {
    this.isLoadingDownload = true;
    this.errorDownload = false;
    this.ss.downloadPaperInformation(this.soildocs[0]._id ? this.soildocs.map(soildoc => soildoc._id) : this.soildocs).subscribe({
      next: (resp) => {
        this.downloadJSON(resp, 'papers.json');
        this.isLoadingDownload = false;
        this.errorDownload = false;
      },
      error: (err) => {
        console.error(err); 
        this.isLoadingDownload = false;
        this.errorDownload = true;
      }
    })
  }

  downloadJSON(data: any, fileName: string) {
    const file: Blob = new Blob([JSON.stringify(data)], {type: 'application/json'});
    const url: string = window.URL.createObjectURL(file);
    const anchorElement: HTMLAnchorElement = document.createElement('a');
    anchorElement.download = fileName;
    anchorElement.href = url;
    anchorElement.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
  }

}
