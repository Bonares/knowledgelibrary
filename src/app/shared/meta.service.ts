import { Observable, Subscription } from 'rxjs';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { Meta, Title } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MetaService {

    subscription: Subscription;

    constructor(
        public title: Title,
        public meta: Meta,
        private ts: TranslateService
    ) {}

    changeMeta(title: string, desc: string) {
        this.subscription ? this.subscription.unsubscribe() : null;
        this.ts.get(title).subscribe((res: string) => {
        this.title.setTitle(res + ' | Knowledge Library');
        });
        this.ts.get(desc).subscribe((res: string) => {
        this.meta.updateTag({ name: 'description', content: res });
        });
        this.subscription = this.ts.onLangChange.subscribe((event: LangChangeEvent) => {
            this.ts.get(title).subscribe((res: string) => {
                this.title.setTitle(res + ' | Knowledge Library');
            });
            this.ts.get(desc).subscribe((res: string) => {
                this.meta.updateTag({ name: 'description', content: res });
            });
        });
    }

}
