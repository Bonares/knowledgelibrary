import { UserRaw } from './user-raw';
import { User } from './User';
export class UserFactory {

  static empty(): User {
    return new User('', '', '', '', '', '', false, 'false', 'en', []);
  }

  static fromObject(rawUser: UserRaw | any): User {
    return new User(
      rawUser.firstname,
      rawUser.lastname,
      rawUser.institution,
      rawUser.username,
      rawUser.password,
      rawUser.email,
      rawUser.isEnabled,
      rawUser.isAdmin,
      rawUser.language,
      rawUser.markedPapers
    );
  }
}
