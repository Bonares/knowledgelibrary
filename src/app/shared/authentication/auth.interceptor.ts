import { AuthService } from './auth.service';
import { Injectable, Inject } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'node_modules/rxjs';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public auth: AuthService,
      @Inject('API_URL') private api: string) {}

    intercept(req: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {

        const idToken = localStorage.getItem('id_token');
        if (idToken && req.url.includes(this.api)) {
          if (!this.auth.isTokenExpired()) {
            const cloned = req.clone({
              setHeaders: {
                Authorization: `Bearer ${idToken}`
              }
            });
            return next.handle(cloned);
           } else {
            return next.handle(req);
           }
          } else {
            return next.handle(req);
        }
    }
}
