import { SafePipe } from './safe.pipe';
import { NgModule } from '@angular/core';
@NgModule({
  declarations: [
    SafePipe
  ],
  imports: [
  ],
  exports: [
    SafePipe
  ]
})
export class SharedModule { }
