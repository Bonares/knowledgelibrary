import { Router } from '@angular/router';
import { Soildoc } from 'src/app/soildoc/soil-doc/shared/Soildoc';
import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter, OnChanges } from '@angular/core';
import { faAngleDoubleLeft, faAngleDoubleRight, faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: []
})
export class PaginationComponent implements OnInit, OnChanges {

  faAngleLeft = faAngleLeft;
  faAngleDoubleLeft = faAngleDoubleLeft;
  faAngleRight = faAngleRight;
  faAngleDoubleRight = faAngleDoubleRight;

  activePage = 0;
  startPage = 0;
  endPage: number;
  pageSize = 10;
  maxPageButtons = 5;
  countPageButtonsArr: number[];
  soildocCount = 0;

  numPages: number;
  pageSizeOptions = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50];
  startItem: number;
  endItem: number;

  constructor(private router: Router) {}

  @Input() soildocs: Soildoc[];
  
  @Output() startItemChange = new EventEmitter<number>();
  @Output() endItemChange = new EventEmitter<number>();

  ngOnInit(): void {
    if (localStorage.getItem('klibrary_search_page_size') && (this.router.url === '/soildoc/soil-doc-search')) {
      this.updatePageSize(localStorage.getItem('klibrary_search_page_size'));
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['soildocs']) {
      this.updatePages();
    }
  }

  updatePages() {
    this.soildocCount = this.soildocs.length;
    this.numPages = Math.ceil(this.soildocCount / this.pageSize);
    this.updateCountPageButtonsArr();
    this.updatePage(1);
  }

  updatePageButtons() {
    if (this.activePage === (this.numPages - 1)) {
      this.endPage = this.numPages;
      this.startPage = this.activePage - this.maxPageButtons < 0 ? 0 : this.activePage - this.maxPageButtons + 1;
    } else if (this.activePage === 0) {
      this.startPage = 0;
      this.endPage = this.maxPageButtons > this.numPages ? this.numPages : this.activePage + this.maxPageButtons;
    } else {
      this.startPage = (this.activePage - 2) < 0 ? 0 : this.activePage - 2;
      this.endPage = this.activePage + 3 > this.numPages ? this.numPages : this.activePage + 3;
      if (((this.endPage - this.startPage) < this.maxPageButtons) && (this.numPages >= this.maxPageButtons)) {
        if (this.activePage === 1) {
          this.endPage += 1;
        } else {
          this.startPage -= 1;
        }
      }
    }
  }

  updateItems() {
    this.startItem = this.activePage * this.pageSize;
    this.startItemChange.emit(this.startItem);
    this.endItem = this.activePage * this.pageSize + this.pageSize > this.soildocCount ? this.soildocs.length : this.activePage * this.pageSize + this.pageSize;
    this.endItemChange.emit(this.endItem);
  }

  updatePage(page: any) {
    this.activePage = parseInt(page, 10) - 1;
    this.updateItems();
    this.updatePageButtons();
  }

  updateCountPageButtonsArr() {
    this.countPageButtonsArr =  Array(this.numPages).fill(0).map((x, i) => i);
  }

  updatePageSize(size: string) {
    this.pageSize = parseInt(size, 10);
    if (this.router.url === '/soildoc/soil-doc-search') {
      localStorage.setItem('klibrary_search_page_size', size);
    }
    this.updatePages();
  }

  ConvertToInt(val) {
    return parseInt(val, 10);
  }

}
