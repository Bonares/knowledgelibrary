import { SearchbarModule } from './../searchbar/searchbar.module';
import { PaginationModule } from './../pagination/pagination.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from './../shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { PaperModalComponent } from './paper-modal/paper-modal.component';
import { NgModule } from '@angular/core';
@NgModule({
    declarations: [PaperModalComponent],
    imports: [
      CommonModule,
      TranslateModule,
      FontAwesomeModule,
      SharedModule,
      RouterModule,
      FormsModule,
      PaginationModule,
      SearchbarModule
    ],
    exports: [
      PaperModalComponent
    ]
  })
  export class PaperModalModule { }