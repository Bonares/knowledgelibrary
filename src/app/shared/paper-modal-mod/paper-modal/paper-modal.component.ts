import { UserService } from './../../user.service';
import { take } from 'rxjs/operators';
import { AuthService } from './../../authentication/auth.service';
import { faSync, faSearch, faExternalLinkAlt, faFileAlt, faProjectDiagram, faCompressAlt, faThumbtack, faBookmark, faTrashAlt, faDownload } from '@fortawesome/free-solid-svg-icons';
import { faBookmark as farBookmark } from '@fortawesome/free-regular-svg-icons';
import { Soildoc } from '../../../soildoc/soil-doc/shared/Soildoc';
import { Component, OnInit, OnChanges, Input, Output, EventEmitter, Inject } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'kl-paper-modal',
  templateUrl: './paper-modal.component.html',
  styleUrls: []
})
export class PaperModalComponent implements OnInit, OnChanges {

  faDownload = faDownload;
  faTrashAlt = faTrashAlt;
  farBookmark = farBookmark;
  faBookmark = faBookmark;

  faThumbTack = faThumbtack;
  faSync = faSync;
  faSearch = faSearch;
  faExternalLinkAlt = faExternalLinkAlt;
  faFileAlt = faFileAlt;
  faProjectDiagram = faProjectDiagram;

  isLoadingModal: Boolean;

  searchValue = '';
  isSorted = false;
  sortTarget: any;

  soildocCollection: Soildoc[] = [];
  soildocCollectionOriginal: Soildoc[];

  startItem = 0;
  endItem: number;

  @Input() soildocs: Soildoc[];
  @Input() propertyFirst: String;
  @Input() propertyLast: String;
  @Input() text: String;
  @Output() clickOverview = new EventEmitter<any>();
  @Input() center_id?: String;

  isLoggedIn$: Observable<boolean>;
  showOnlyMarked = false;
  markedPapers: String[] = [];

  constructor(
    @Inject('API_URL') public api: string,
    private authService: AuthService,
    private us: UserService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.searchValue = '';
    this.isLoadingModal = true;
    this.soildocCollection = [];
    this.resetSorting();
    this.soildocCollectionOriginal = this.soildocs;
    this.soildocCollection = this.soildocCollectionOriginal.slice(0);
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.isLoggedIn$.pipe(take(1)).subscribe(login => {
      if (login) {
        this.us.getMarkedPapers().subscribe({next: (papers) => {
          this.markedPapers = papers && papers.length ? papers : [];
        }});
      }
    });
    this.isLoadingModal = false;
  }

  resetSorting() {
    if (this.isSorted && this.sortTarget) {
      this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
    }
  }

  ConvertToInt(val) {
    return parseInt(val, 10);
  }

  sortPaper(event: any, identifier: string) {
    if (event.target.classList.value === 'sort-icon-search fa fa-sort') {
      if (this.isSorted && this.sortTarget) {
        this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      }
      this.isSorted = true;
      this.sortTarget = event.target;
      event.target.classList = 'sort-icon-search fa fa-sort-up';

      this.sortOneIdentifier(identifier, 1, -1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-up') {

      event.target.classList = 'sort-icon-search fa fa-sort-down';
      this.sortOneIdentifier(identifier, -1, 1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-down') {
      event.target.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
      this.sortOneIdentifier('modifiedDate', 1, -1, true);
    }
  }

  sortOneIdentifier(identifier: string, first: number, second: number, isDate?: boolean) {
    if (!isDate) {
      if (identifier === 'authors') {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a[identifier][0].name > b[identifier][0].name) {
            return first;
          }
          if (a[identifier][0].name < b[identifier][0].name) {
            return second;
          }
          return 0;
        });
      } else {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a[identifier] > b[identifier]) {
            return first;
          }
          if (a[identifier] < b[identifier]) {
            return second;
          }
          return 0;
        });
      }
    } else {
      this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
        return new Date(b.modifiedDate).getTime() - new Date(a.modifiedDate).getTime();
      });
    }
  }

  overview(soildoc_id: String) {
    this.clickOverview.emit(soildoc_id);
  }

  markPaper(soildoc_id: string) {
    if (!this.markedPapers.includes(soildoc_id)) {
      this.markedPapers.push(soildoc_id);
      this.markedPapers = this.markedPapers.slice(0);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  unmarkPaper(soildoc_id: string) {
    if (this.markedPapers.includes(soildoc_id)) {
      this.markedPapers = this.markedPapers.filter(paper => paper !== soildoc_id);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  untagAllPapers() {
    this.markedPapers = [];
    this.us.updateMarkedPapers(this.markedPapers).subscribe({
      next: () => {},
      error: (err) => {console.error(err)}
    });
  }
}
