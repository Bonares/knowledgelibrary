import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { DropdownTreeviewCustomComponent } from './dropdown-treeview-custom/dropdown-treeview-custom.component';
import { DropdownTreeviewItemCustomComponent } from './dropdown-treeview-item-custom/dropdown-treeview-item-custom.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    DropdownTreeviewItemCustomComponent,
    DropdownTreeviewCustomComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    FontAwesomeModule,
    TranslateModule
  ],
  exports: [
    DropdownTreeviewItemCustomComponent,
    DropdownTreeviewCustomComponent
  ]
})
export class DropdownTreeviewModule { }
