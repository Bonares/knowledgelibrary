import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DropdownTreeviewItemCustomComponent } from './dropdown-treeview-item-custom.component';

describe('DropdownTreeviewItemCustomComponent', () => {
  let component: DropdownTreeviewItemCustomComponent;
  let fixture: ComponentFixture<DropdownTreeviewItemCustomComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownTreeviewItemCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownTreeviewItemCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
