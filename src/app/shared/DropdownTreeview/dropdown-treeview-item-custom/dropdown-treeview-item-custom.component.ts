import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'dropdown-treeview-item-custom',
  templateUrl: './dropdown-treeview-item-custom.component.html',
  styleUrls: []
})
export class DropdownTreeviewItemCustomComponent implements OnInit {

  constructor() { }

  @Input() node: TreeNode;
  @Input() isParentSelectionAllowed: boolean;
  @Output() checkedChange = new EventEmitter<any>();

  ngOnInit(): void {
  }

  toggleChild(node: TreeNode) {
    node.collapsed = !node.collapsed;
  }

  onCheckedChange(node: TreeNode) {
    this.checkedChange.emit(node);
  }

  onChildCheckedChange(node: TreeNode, event: TreeNode) {
    this.checkedChange.emit(event);
  }

}
