import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { timer } from 'rxjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, OnInit, Input, Output, OnChanges, EventEmitter, forwardRef, HostListener, ElementRef } from '@angular/core';

export const MULTI_SELECT_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  useExisting: forwardRef( () => DropdownTreeviewCustomComponent ),
  multi: true
};

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'dropdown-treeview-custom',
  templateUrl: './dropdown-treeview-custom.component.html',
  styleUrls: [],
  providers: [ MULTI_SELECT_INPUT_CONTROL_VALUE_ACCESSOR ]
})
export class DropdownTreeviewCustomComponent implements OnInit, OnChanges, ControlValueAccessor {

  selectedNode: TreeNode;
  filterText: string;
  originalTree: TreeNode[];
  filterTree: TreeNode[];
  isOpenMenu = false;
  toggleElement: any;
  helpFunc = false;

  private onTouched: Function;
  private onModelChange: Function;
  private selected: string;

  constructor(element: ElementRef) {
    this.toggleElement = element.nativeElement;
   }

  @Input() invalid: boolean;
  @Input() value: any;
  @Input() isParentSelectionAllowed: boolean;
  @Input() selectString: boolean;
  @Input() disabled: boolean;
  @Input() treeData: TreeNode[];
  @Input() placeholder: string;
  @Input() triggerManual: number;
  @Output() valueChange = new EventEmitter<any>();

  ngOnInit() {
  }

  @HostListener('keyup.esc')
  onKeyupEsc() {
    this.isOpenMenu = false;
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event) {
    if (event.clientX === 0) {
      this.isOpenMenu = false;
    }
    if (event.button !== 2 && !this.isEventFromToggle(event)) {
      this.isOpenMenu = false;
    }
  }

  isEventFromToggle(event) {
    return !isNil(this.toggleElement) && this.toggleElement.contains(event.target);
  }

  ngOnChanges(changes: any) {
    if (this.treeData && this.treeData !== undefined && changes.treeData) {
      this.originalTree = JSON.parse(JSON.stringify(this.treeData));
      this.filterTree = JSON.parse(JSON.stringify(this.treeData));
      this.filterText = '';
      this.reset();
    }
  }

  reset() {
    if (this.selected && this.selected !== '') {
      const itemFound = this.findItemInList(this.treeData, this.selected);
      if (!itemFound) {
        this.selected = '';
        this.selectedNode = undefined;
        timer(1).subscribe(val => {
          if (this.onModelChange) { this.onModelChange(this.selected); }
        });
      }
    }
  }

  onItemCheckedChange(item: TreeNode, event: TreeNode) {
    if (this.selectedNode !== event) {
      this.selectedNode = event;
      if (this.selectString !== undefined) {
        this.selected = this.selectedNode.text;
        if (this.onModelChange) { this.onModelChange(this.selected); }
        this.valueChange.emit(this.selectedNode);
      } else {
        this.selected = this.selectedNode.value;
        if (this.onModelChange) { this.onModelChange(this.selected); }
        this.valueChange.emit(this.selectedNode);
      }
    }
    this.isOpenMenu = false;
  }

  onFilterTextChange(event: any) {
    this.updateFilterItems();
  }

  updateFilterItems() {
    const _this = this;
    if (this.filterText !== '' && this.filterText.length > 1) {
      const filterItems_1 = [];
      const filterText_1 = this.filterText.toLowerCase();
      JSON.parse(JSON.stringify(this.originalTree)).forEach(function (item) {
        const newItem = _this.filterItem(item, filterText_1);
        if (!isNil(newItem)) {
          filterItems_1.push(newItem);
        }
      });
      this.updateCollapsed(filterItems_1, filterText_1);
      this.filterTree = filterItems_1;
    } else {
      this.filterTree = JSON.parse(JSON.stringify(this.originalTree));
    }
  }

  filterItem(item, filterText) {
    if (item.text.toLowerCase().includes(filterText)) {
      return item;
    } else {
      if (!isNil(item.children)) {
          const children_1 = [];
          for (const children of item.children) {
            const newChild = this.filterItem(children, filterText);
            if (!isNil(newChild)) {
                children_1.push(newChild);
            }
          }
          if (children_1.length > 0) {
            const newItem = item;
            newItem.children = children_1;
            newItem.collapsed = false;
            return newItem;
          }
      }
    }
    return undefined;
  }

  updateCollapsed(items, filterText) {
    for (const item of items) {
      if (item.children && item.children.length > 0) {
        this.helpFunc = true;
        this.checkIfText(item, filterText);
        item.collapsed = this.helpFunc;
        this.updateCollapsed(item.children, filterText);
      }
    }
  }

  checkIfText(item, filterText) {
    if (item.children.some(children => children.text.toLowerCase().includes(filterText))) {
      this.helpFunc = false;
    } else {
      for (const child of item.children) {
        this.checkIfText(child, filterText);
      }
    }
  }

    /**
   * This is called from a form input to set the internal value
   * @param {string} selected
  */

 writeValue( selected: string ): void {
  if ( this.selected === selected ) {
    return;
  }
  this.selected = selected;
  if (!selected || selected === '') {
    this.selectedNode = new TreeNode({
      value: undefined,
      text: this.placeholder
    });
    this.valueChange.emit(this.selectedNode);
  } else {
    const itemFound = this.findItemInList(this.treeData, this.selected);
    if (itemFound) {
      this.onItemCheckedChange(null, itemFound);
    }
  }
}

registerOnChange( fn: Function ) {
  this.onModelChange = fn;
}

registerOnTouched( fn: () => any ): void {
  this.onTouched = fn;
}

private findItem(root, value) {
  if (isNil(root)) {
      return undefined;
  }
  // eslint-disable-next-line eqeqeq
  if (root.value == value) {
      return root;
  }
  // eslint-disable-next-line eqeqeq
  if (root.text == value) {
    return root;
  }
  if (root.children) {
      for (let _i = 0, _a = root.children; _i < _a.length; _i++) {
          const child = _a[_i];
          const foundItem = this.findItem(child, value);
          if (foundItem) {
              return foundItem;
          }
      }
  }
  return undefined;
}

private findItemInList(list, value) {
    if (isNil(list)) {
        return undefined;
    }
    for (let _i = 0, list_1 = list; _i < list_1.length; _i++) {
        const item = list_1[_i];
        const foundItem = this.findItem(item, value);
        if (foundItem) {
            return foundItem;
        }
    }
    return undefined;
}
}

function isNil(elem) {
  return ((elem === undefined) || (elem === null));
}
