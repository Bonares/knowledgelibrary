import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DropdownTreeviewCustomComponent } from './dropdown-treeview-custom.component';

describe('DropdownTreeviewCustomComponent', () => {
  let component: DropdownTreeviewCustomComponent;
  let fixture: ComponentFixture<DropdownTreeviewCustomComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownTreeviewCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownTreeviewCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
