import { AuthService } from './authentication/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService) {}

  canActivate() {

    if (this.authService.getToken()) {
      if (!this.authService.isTokenExpired()) {
        this.router.navigate(['']);
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
}
