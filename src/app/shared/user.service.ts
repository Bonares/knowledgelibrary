import { AuthGuard } from './auth.guard';
import { User } from './User';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard) {
  }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  userEnabled(user: string): Observable<Boolean> {
    return this.httpClient
      .post<Boolean>(`${this.api}/users`, {checkusername: user})
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }

  userAvailable(user: string): Observable<Boolean> {
    return this.httpClient
      .post<Boolean>(`${this.api}/users/available/user`, {checkuser: user})
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }

  mailAvailable(mail: string): Observable<Boolean> {
    return this.httpClient
      .post<Boolean>(`${this.api}/users/available/email`, {checkemail: mail})
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }

  createUser(user: User): Observable<any> {
    return this.httpClient
    .post(`${this.api}/user`, user)
    .pipe(
      timeout(15000),
      catchError(this.handleError)
    );
  }

  confirmOne(id: String): Observable<Boolean> {
    return this.httpClient
      .get<Boolean>(`${this.api}/user/confirm/${id}`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }

  resetPW(email: any): Observable<any> {
    return this.httpClient
    .post(`${this.api}/userforgot`, email)
    .pipe(
      timeout(15000),
      catchError(this.handleError)
    );
  }

  changePW(pwobj: any): Observable<any> {
    return this.httpClient
    .post(`${this.api}/userforgot/change`, pwobj)
    .pipe(
      timeout(15000),
      catchError(this.handleError)
    );
  }

  getEmailbyId(id: String): Observable<String> {
    if (this.ag.canActivateMethod()) {
    return this.httpClient
      .get<String>(`${this.api}/user/email/${id}`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  getUsernames(): Observable<Array<String>> {
    if (this.ag.canActivateMethod()) {
    return this.httpClient
      .get<Array<String>>(`${this.api}/user/usernames`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  getMarkedPapers(): Observable<Array<String>> {
    if (this.ag.canActivateMethod()) {
    return this.httpClient
      .get<Array<String>>(`${this.api}/user/markedpapers`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  updateMarkedPapers(markedPapers: String[]): Observable<any> {
    if (this.ag.canActivateMethod()) {
    return this.httpClient
      .post(`${this.api}/user/markedpapers`, markedPapers)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    } else {
      return throwError(() => new Error('unauthorized'));
    }
  }
}
