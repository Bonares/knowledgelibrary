
export class User {
  constructor(
    public firstname: string,
    public lastname: string,
    public institution: string,
    public username: string,
    public password: string,
    public email: string,
    public isEnabled: boolean,
    public isAdmin: string,
    public language: string,
    public markedPapers: String[],
   ) {}

}
