import { RouterModule } from '@angular/router';
import { SharedModule } from './../shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaperOverviewComponent } from './paper-overview/paper-overview.component';

@NgModule({
  declarations: [PaperOverviewComponent],
  imports: [
    CommonModule,
    TranslateModule,
    FontAwesomeModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    PaperOverviewComponent
  ]
})
export class PaperOverviewModule { }
