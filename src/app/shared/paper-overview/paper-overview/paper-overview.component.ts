import { Observable } from 'rxjs';
import { MethodService } from './../../../soildoc-study/soilstudy-detail/methods/shared/method.service';
import { ModelService } from './../../../soildoc-study/soilstudy-detail/models/shared/model.service';
import { AuthService } from './../../authentication/auth.service';
import { RelationshipService } from './../../../soildoc-study/soilstudy-detail/relationship/shared/relationship.service';
import { PropertiesProcessService } from './../../../soildoc-study/soilstudy-detail/properties-process/shared/propertiesprocess.service';
import { ManagementService } from './../../../soildoc-study/soilstudy-detail/management/shared/management.service';
import { SitesService } from './../../../soildoc-study/soilstudy-detail/sites/shared/sites.service';
import { SitesoilService } from './../../../soildoc-study/soilstudy-detail/site-soil/shared/sitesoil.service';
import { TypeofstudyService } from './../../../soildoc-study/soilstudy-detail/type-of-study/shared/typeofstudy.service';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { Relationship } from './../../../soildoc-study/soilstudy-detail/relationship/shared/Relationship';
import { Model } from './../../../soildoc-study/soilstudy-detail/models/shared/Model';
import { Method } from './../../../soildoc-study/soilstudy-detail/methods/shared/Method';
import { PropertiesProcess } from './../../../soildoc-study/soilstudy-detail/properties-process/shared/PropertiesProcess';
import { Management } from './../../../soildoc-study/soilstudy-detail/management/shared/Management';
import { Sites } from './../../../soildoc-study/soilstudy-detail/sites/shared/Sites';
import { Sitesoil } from './../../../soildoc-study/soilstudy-detail/site-soil/shared/Sitesoil';
import { Typeofstudy } from './../../../soildoc-study/soilstudy-detail/type-of-study/shared/Typeofstudy';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { faMap, faCaretRight, faLayerGroup, faLongArrowAltRight, faCloudSunRain, faMapMarkedAlt, faTractor, faArrowsAltH, faEnvelope, faCubes, faSync } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-paper-overview',
  templateUrl: './paper-overview.component.html',
  styleUrls: []
})
export class PaperOverviewComponent implements OnInit, OnChanges {

  constructor(
    private typeService: TypeofstudyService,
    private sitesoilService: SitesoilService,
    private sitesService: SitesService,
    private manService: ManagementService,
    private ppService: PropertiesProcessService,
    private rsService: RelationshipService,
    private authService: AuthService,
    private modelService: ModelService,
    private methodService: MethodService,
  ) { }

  faSync = faSync;
  faMap = faMap;
  faCaretRight = faCaretRight;
  faLayerGroup = faLayerGroup;
  faLongArrowAltRight = faLongArrowAltRight;
  faCloudSunRain = faCloudSunRain;
  faMapMarkedAlt = faMapMarkedAlt;
  faTractor = faTractor;
  faArrowsAltH = faArrowsAltH;
  faEnvelope = faEnvelope;
  faCubes = faCubes;

  @Input() soildocOverview: Soildoc;
  typeofstudyOverview: Typeofstudy;
  sitesoilOverview: Sitesoil;
  sitesOverview: Sites;
  managementOverview: Management;
  propertiesprocessOverview: PropertiesProcess;
  methodOverview: Method;
  modelOverview: Model;
  relationshipOverview: Relationship;

  isLoadingModalOverview = true;
  errorLoadingModalOverview = true;

  isLoggedIn$: Observable<boolean>;

  ngOnInit(): void {
    this.isLoggedIn$ = this.authService.isLoggedIn;
  }

  ngOnChanges() {
    if (this.soildocOverview && this.soildocOverview._id) {
    this.isLoadingModalOverview = true;
    this.typeService.getOneBySoildoc(this.soildocOverview._id).subscribe(typeofstudy => {
    this.typeofstudyOverview = typeofstudy;
    if (this.typeofstudyOverview && (this.typeofstudyOverview.name === 'Meta-analysis' || this.typeofstudyOverview.name === 'Experiment')) {
      this.ppService.getOneBySoildoc(this.soildocOverview._id).subscribe(resulttt => {
        this.propertiesprocessOverview = resulttt;
        this.rsService.getOneBySoildoc(this.soildocOverview._id).subscribe(resultttt => {
          this.relationshipOverview = resultttt;
          this.manService.getOneBySoildoc(this.soildocOverview._id).subscribe(resulttttt => {
            this.managementOverview = resulttttt;
            if (this.typeofstudyOverview.name === 'Experiment' && this.typeofstudyOverview.experimentContext &&
            (!this.typeofstudyOverview.experimentContext.site_input_mode || this.typeofstudyOverview.experimentContext.site_input_mode === 'Individual')) {
              this.sitesoilService.getOneBySoildoc(this.soildocOverview._id).subscribe(result => {
                this.sitesoilOverview = result;
                this.isLoadingModalOverview = false;
                this.errorLoadingModalOverview = false;
              },
              error => {
                this.isLoadingModalOverview = false;
                this.errorLoadingModalOverview = true;
              });
            } else if (this.typeofstudyOverview.name === 'Meta-analysis' ||
            (this.typeofstudyOverview.name === 'Experiment' && this.typeofstudyOverview.experimentContext &&
            (this.typeofstudyOverview.experimentContext.site_input_mode && this.typeofstudyOverview.experimentContext.site_input_mode === 'Combined'))) {
              this.sitesService.getOneBySoildoc(this.soildocOverview._id).subscribe(result => {
                this.sitesOverview = result;
                this.isLoadingModalOverview = false;
                this.errorLoadingModalOverview = false;
              },
              error => {
                this.isLoadingModalOverview = false;
                this.errorLoadingModalOverview = true;
              });
            }
          },
          error => {
            this.isLoadingModalOverview = false;
            this.errorLoadingModalOverview = true;
          });
        },
        error => {
          this.isLoadingModalOverview = false;
          this.errorLoadingModalOverview = true;
        });
      },
      error => {
        this.isLoadingModalOverview = false;
        this.errorLoadingModalOverview = true;
      });
    } else if (this.typeofstudyOverview && (this.typeofstudyOverview.name === 'Model' || this.typeofstudyOverview.name === 'Method')) {
      this.sitesoilService.getOneBySoildoc(this.soildocOverview._id).subscribe(result => {
        this.sitesoilOverview = result;
        if (this.typeofstudyOverview.name === 'Model') {
          this.modelService.getOneBySoildoc(this.soildocOverview._id).subscribe(resultmodel => {
            this.modelOverview = resultmodel;
            this.isLoadingModalOverview = false;
            this.errorLoadingModalOverview = false;
          },
          error => {
            this.isLoadingModalOverview = false;
            this.errorLoadingModalOverview = true;
          });
        } else {
          this.methodService.getOneBySoildoc(this.soildocOverview._id).subscribe(resultmethod => {
            this.methodOverview = resultmethod;
            this.isLoadingModalOverview = false;
            this.errorLoadingModalOverview = false;
          },
          error => {
            this.isLoadingModalOverview = false;
            this.errorLoadingModalOverview = true;
          });
        }
      },
      error => {
        this.isLoadingModalOverview = false;
        this.errorLoadingModalOverview = true;
      });
    } else {
      this.isLoadingModalOverview = false;
      this.errorLoadingModalOverview = false;
    }

    },
    error => {
      this.isLoadingModalOverview = false;
      this.errorLoadingModalOverview = true;
    });
    }
  }

  valueExists(val: any) {
    return ((val !== undefined) && (val !== '') && (val !== null))
  }

}
