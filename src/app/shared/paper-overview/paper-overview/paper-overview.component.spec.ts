import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperOverviewComponent } from './paper-overview.component';

describe('PaperOverviewComponent', () => {
  let component: PaperOverviewComponent;
  let fixture: ComponentFixture<PaperOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaperOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaperOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
