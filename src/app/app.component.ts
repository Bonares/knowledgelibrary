import { Observable } from 'rxjs';
import { AuthService } from './shared/authentication/auth.service';
import { Component, Inject, OnInit } from '@angular/core';
import { faAngleDown, faExclamationCircle, faProjectDiagram, faSearch,
  faPlus, faMapMarkedAlt, faInfoCircle, faEnvelope, faTractor, faCubes, faBookOpen, faLongArrowAltRight, faBook , faEye, faQuestionCircle, faStream, faCarrot, faCircleNodes} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {

  faCarrot = faCarrot;
  faStream = faStream;
  faQuestionCircle = faQuestionCircle;
  faEye = faEye;
  faBook = faBook;
  faLongArrowAltRight = faLongArrowAltRight;
  faBookOpen = faBookOpen;
  faCubes = faCubes;
  faTractor = faTractor;
  faEnvelope = faEnvelope;
  faInfoCircle = faInfoCircle;
  faMapMarkedAlt = faMapMarkedAlt;
  faPlus = faPlus;
  faCircleNodes = faCircleNodes;
  faSearch = faSearch;
  faExclamationCircle = faExclamationCircle;
  faAngleDown = faAngleDown;
   isLoggedIn$: Observable<boolean>;
  isTestShown = true;

  constructor(private authService: AuthService,
    @Inject('production') public isProduction: boolean) { }

  ngOnInit() {
    if (!this.authService.isTokenExpired()) {
      this.authService.loggedIn.next(true);
    } else {
      this.authService.loggedIn.next(false);
    }

     this.isLoggedIn$ = this.authService.isLoggedIn;
  }
}
