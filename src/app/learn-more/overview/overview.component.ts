import { MetaService } from './../../shared/meta.service';
import { Component, OnInit } from '@angular/core';
import { faBookOpen, faCaretRight, faChevronCircleRight, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-overview',
  templateUrl: './overview.component.html',
  styleUrls: []
})
export class OverviewComponent implements OnInit {

  faSearch = faSearch;
  faChevronCircleRight = faChevronCircleRight;
  faCaretRight = faCaretRight;
  faBookOpen = faBookOpen;

  constructor(
    private metaService: MetaService
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('LEARNMORE.OVERVIEW.TITLEMETA', 'LEARNMORE.OVERVIEW.DESCRIPTIONMETA');
  }

}
