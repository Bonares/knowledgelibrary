import { ViewGenkeyComponent } from './soilstudy-detail/view-genkey/view-genkey.component';
import { SitesResolverService } from './soilstudy-detail/sites/shared/sites-resolver.service';
import { SitesComponent } from './soilstudy-detail/sites/sites.component';
import { RelationshipComponent } from './soilstudy-detail/relationship/relationship.component';
import { ModelResolverService } from './soilstudy-detail/models/shared/model-resolver.service';
import { MethodResolverService } from './soilstudy-detail/methods/shared/method-resolver.service';
import { PropertiesProcessResolverService } from './soilstudy-detail/properties-process/shared/propertiesprocess-resolver.service';
import { SitesoilResolverService } from './soilstudy-detail/site-soil/shared/sitesoil-resolver.service';
import { TypeofstudyResolverService } from './soilstudy-detail/type-of-study/shared/typeofstudy-resolver.service';
import { SoildocResolverService } from './../soildoc/soil-doc/shared/soildoc-resolver.service';
import { ModelsComponent } from './soilstudy-detail/models/models.component';
import { MethodsComponent } from './soilstudy-detail/methods/methods.component';
import { PropertiesProcessComponent } from './soilstudy-detail/properties-process/properties-process.component';
import { SiteSoilComponent } from './soilstudy-detail/site-soil/site-soil.component';
import { TypeOfStudyComponent } from './soilstudy-detail/type-of-study/type-of-study.component';
import { AuthGuard } from './../shared/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelationshipResolverService } from './soilstudy-detail/relationship/shared/relationship-resolver.service';
import { ManagementComponent } from './soilstudy-detail/management/management.component';
import { ManagementResolverService } from './soilstudy-detail/management/shared/management-resolver.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'type-of-study/:id', /** Type of study */
        component: TypeOfStudyComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'site-soil/:id', /** Site & Soil */
        component: SiteSoilComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          sitesoil: SitesoilResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'sites/:id', /** Site & Soil */
        component: SitesComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          sites: SitesResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'methods/:id', /** Method description */
        component: MethodsComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          method: MethodResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'models/:id', /** Model description */
        component: ModelsComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          model: ModelResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'management/:id',
        component: ManagementComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          management: ManagementResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'properties-process/:id', /** Properties */
        component: PropertiesProcessComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          propertiesprocess: PropertiesProcessResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'relationship/:id', /** Relationships */
        component: RelationshipComponent,
        resolve: {
          soildoc: SoildocResolverService,
          typeofstudy: TypeofstudyResolverService,
          relationship: RelationshipResolverService
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'view-genkeys', /** Admin only Genkeys */
        component: ViewGenkeyComponent,
        canActivate: [AuthGuard]
      }
    ],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoildocStudyRoutingModule { }
