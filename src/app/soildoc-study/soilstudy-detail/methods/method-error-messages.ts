export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const MethodErrorMessages = [
  new ErrorMessage('additional_information', 'maxlength', 'METHOD.FAILFORM.MAXLENGTH500')
];
