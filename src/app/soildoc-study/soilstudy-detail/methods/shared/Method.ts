import { MethodItem } from './MethodItem';
export class Method {
  constructor(
    public _id: string,
    public properties_measured: MethodItem[],
    public properties_prerequisite: MethodItem[],
    public soildoc_id: string
   ) {}
}
