import { Qualities } from './../../properties-process/shared/Qualities';
export class MethodItem {
  constructor(
    public name: string,
    public id: string,
    public qualitiesEnabled: boolean,
    public qualities: Qualities[],
    public combined: string,
    public category: string
   ) {}
}
