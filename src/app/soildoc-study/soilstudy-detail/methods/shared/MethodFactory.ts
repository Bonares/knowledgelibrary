import { MethodRaw } from './Method-raw';
import { MethodItem } from './MethodItem';
import { Method } from './Method';
export class MethodFactory {

  static empty(): Method {
    return new Method('', [new MethodItem('', '', false, [], '', '')], [new MethodItem('', '', false, [], '', '')], null);
  }

  static fromObject(rawMethod: MethodRaw | any): Method {
    if (rawMethod) {
      return new Method(
        rawMethod._id,
        rawMethod.properties_measured,
        rawMethod.properties_prerequisite,
        rawMethod.soildoc_id
      );
    } else {
      return null;
    }
  }
  }
