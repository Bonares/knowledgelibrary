import { Injectable } from '@angular/core';
import { Method } from './Method';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { MethodService } from './method.service';
import { AuthGuard } from './../../../../shared/auth.guard';
@Injectable({
  providedIn: 'root'
})
export class MethodResolverService {

  constructor(private ms: MethodService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Method> {
    if (this.ag.canActivateMethod()) {
    return this.ms.getOneBySoildoc(route.params['id']);
    }
  }
}
