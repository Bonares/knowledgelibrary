import { MethodFactory } from './MethodFactory';
import { retry, timeout, map, catchError } from 'rxjs/operators';
import { MethodRaw } from './Method-raw';
import { Method } from './Method';
import { throwError, Observable } from 'rxjs';
import { AuthGuard } from './../../../../shared/auth.guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class MethodService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Method> {
    return this.httpClient
      .get<MethodRaw>(`${this.api}/method/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawMethod => MethodFactory.fromObject(rawMethod),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Method> {
    return this.httpClient
      .get<MethodRaw>(`${this.api}/method/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawMethod => MethodFactory.fromObject(rawMethod),
        ),
        catchError(this.handleError)
      );
  }


  create(method: Method): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/method`, method)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(method: Method): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/method/${method._id}`, method)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
