import { MethodItem } from './MethodItem';
export interface MethodRaw {
  _id: string;
  properties_measured: MethodItem[];
  properties_prerequisite: MethodItem[];
  soildoc_id: string;
}
