import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SoilstudyDetailComponent } from './soilstudy-detail.component';

describe('SoilstudyDetailComponent', () => {
  let component: SoilstudyDetailComponent;
  let fixture: ComponentFixture<SoilstudyDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilstudyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilstudyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
