import { MetaService } from './../../../shared/meta.service';
import { PropertiesprocessErrorMessages } from './properties-process-error-messages';
import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { CombinekeyErrorMessages } from './../add-driver/combinekey-error-messages';
import { GenkeyErrorMessages } from './../add-driver/genkey-error-messages';
import { TypeofstudyService } from './../type-of-study/shared/typeofstudy.service';
import { AdddriverValidators } from './../add-driver/shared/adddriver-validators';
import { AdddriverService } from './../add-driver/shared/adddriver.service';
import { JsonService } from './../shared/json.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GenKeyFactory } from './../add-driver/shared/GenKeyFactory';
import { UntypedFormGroup, UntypedFormArray, UntypedFormBuilder, Validators } from '@angular/forms';
import { Typeofstudy } from './../type-of-study/shared/Typeofstudy';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { faTrashAlt, faMinus, faFileWord, faObjectGroup, faSearch, faQuestionCircle, faEdit, faPlus, faSave, faSpinner, faSync,
faExclamationCircle, faInfoCircle, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import { CombineKeyFactory } from './../add-driver/shared/CombineKeyFactory';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PropertiesProcessFactory } from './shared/PropertiesProcessFactory';
import { PropertiesProcessService } from './shared/propertiesprocess.service';
import { timer } from 'rxjs';
import { RelationshipService } from '../relationship/shared/relationship.service';
import { Relationship } from '../relationship/shared/Relationship';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-properties-process',
  templateUrl: './properties-process.component.html',
  providers: [],
  styleUrls: []
})
export class PropertiesProcessComponent implements OnInit {

  faBookOpen = faBookOpen;
  faExclamationCircle = faExclamationCircle;
  faSync = faSync;
  faSpinner = faSpinner;
  faSave = faSave;
  faPlus = faPlus;
  faEdit = faEdit;
  faQuestionCircle = faQuestionCircle;
  faSearch = faSearch;
  faFileWord = faFileWord;
  faObjectGroup = faObjectGroup;
  faTrashAlt = faTrashAlt;
  faMinus = faMinus;
  faInfoCircle = faInfoCircle;

  isLoading: Boolean;
  isSaveLoading: Boolean = false;
  soildoc: Soildoc;
  typeofstudy: Typeofstudy;
  relationship: Relationship;
  propertiesprocess = PropertiesProcessFactory.empty();
  propertiesprocessForm: UntypedFormGroup;
  genkeyForm: UntypedFormGroup;
  genkey = GenKeyFactory.empty();
  combinekeyForm: UntypedFormGroup;
  combinekey = CombineKeyFactory.empty();
  failSaveCombineKey: boolean;
  failSaveGenKey: boolean;
  failSave: boolean;
  failSaveAuthorized: Boolean;
  failEdaphobase: Boolean;
  isLoadingEdaphobase: Boolean[] = [];
  isEdit: Boolean;
  successSave = false;
  properties: UntypedFormArray;
  speciesUnavailable = [];

  options_properties: TreeNode[] = [];
  options_keyword: TreeNode[] = [];
  options_qualities: TreeNode[]  = [];
  options_qualities_switch: any = [];
  options_qualities_exclude: any = [];
  options_species: [TreeNode[]] = [[]];
  options_properties_unformatted: any;
  options_properties_unformatted_modelled: any;
  options_geography_unformatted: any;
  options_properties_usergen_unformatted: any = [];
  options_genkeys: TreeNode[] = [];
  options_genkeys_properties: TreeNode[] = [];
  options_genkeys_geography: TreeNode[] = [];
  options_combinekeys: TreeNode[] = [];
  options_unformatted = [];
  options_crops_unformatted = [];
  options_crops_form: TreeNode[] = [];

  category_options = ['Properties', 'Geography'];

  propertymodelArr = [];

  errorsGenkey: { [key: string]: string } = {};
  errorsCombinekey: { [key: string]: string } = {};

  maximalProperties = 40;
  maximalQualities = 5;
  maximalSpecies = 5;
  maximal_combine_items = 5;

  redirectUrl = '';

  itemErrors: [{[key: string]: string}];

  modalRedirect : bootstrap.Modal;
  modalSave : bootstrap.Modal;
  modalCombineKeyword : bootstrap.Modal;
  modalGenerateKeyword : bootstrap.Modal;

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private js: JsonService,
    private as: AdddriverService,
    private ts: TypeofstudyService,
    private rs: RelationshipService,
    private ps: PropertiesProcessService,
    private router: Router,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.PROPERTIES', 'PROPERTIESPROCESS.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }
    if (data['typeofstudy']) {
      this.typeofstudy = data['typeofstudy'];
    }

    if (data['propertiesprocess']) {
      this.propertiesprocess = data['propertiesprocess'];
      this.isEdit = true;
    }

    this.initPropertiesProcess();
    this.initCombineKey();

    this.js.getbyData(['Properties1', 'Geography511', 'UnitsGen', 'LandUse450', 'Management332']).subscribe(result => {
      if (result) {
        this.options_properties_unformatted = result;
        this.options_properties_unformatted_modelled = result[0];
        this.options_geography_unformatted = result[1];
        this.options_properties.push(new TreeNode(result[0]));
        this.options_properties.push(new TreeNode(result[1]));
        this.options_keyword.push(new TreeNode(result[0]));
        this.options_keyword.push(new TreeNode(result[2]));
        this.options_qualities.push(new TreeNode(this.findByProperty(result[0], 'value', '226')));
        this.options_qualities.push(new TreeNode(this.findByProperty(result[0], 'value', '266')));
        this.options_qualities.push(new TreeNode(this.findByProperty(result[0], 'value', '288')));
        this.options_qualities_switch.push(this.findByProperty(result[0], 'value', '177'));
        this.options_qualities_switch.push(this.findByProperty(result[0], 'value', '184'));
        this.options_qualities_exclude = ['275', '276', '277', '278'];
        this.options_crops_unformatted.push(this.findByProperty(this.options_properties_unformatted, 'value', '225'));
        this.options_unformatted.push(result[0].children);
        this.options_unformatted.push(result[1].children);
        this.options_unformatted.push(result[3].children);
        this.options_unformatted.push(result[4].children);
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[4], 'value', 'Main crop')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[4], 'value', '529')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[4], 'value', '600')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[4], 'value', '530')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[4], 'value', '869')));
          this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
            if (resultsgen) {
              for (let i = 0; i < resultsgen.length; i++) {
                if (resultsgen[i].category === 'Properties') {
                  this.options_genkeys_properties.push(new TreeNode(resultsgen[i]));
                } else if (resultsgen[i].category === 'Geography') {
                  this.options_genkeys_geography.push(new TreeNode(resultsgen[i]));
                }
                this.options_genkeys.push(new TreeNode(resultsgen[i]));
              }
              this.options_properties_usergen_unformatted.push(resultsgen);
            }
            this.as.getAllKeywordsCombinedByUser().subscribe(resultscom => {
              if (resultscom) {
                for (let i = 0; i < resultscom.length; i++) {
                  this.options_combinekeys.push(new TreeNode(resultscom[i]));
                }
                this.options_properties_usergen_unformatted.push(resultscom);
              }
              const usergenerated = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [
                  {
                    'value': '1000006',
                    'text': 'Properties',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_properties
                  },
                  {
                    'value': '1000007',
                    'text': 'Geography',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_geography
                  }]
                }, {
                  'value': '1000003',
                  'text': 'Combined Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_combinekeys
                }]
              };
              const usergeneratedcombine = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [
                  {
                    'value': '1000006',
                    'text': 'Properties',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_properties
                  },
                  {
                    'value': '1000007',
                    'text': 'Geography',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_geography
                  }]
                }]
              };
              this.options_keyword.unshift(new TreeNode(usergeneratedcombine));
              this.options_properties.unshift(new TreeNode(usergenerated));
              this.rs.getOneBySoildoc(this.soildoc._id).subscribe(relationship => {
                this.relationship = relationship;
                this.initGenKey();
                this.resetSpeciesEnabled();
                this.updateErrorMessages();
                this.resetPropertyModelArray();
                this.isLoading = false;
                timer(1).subscribe(val => {
                  this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));
                  this.modalSave = new bootstrap.Modal(document.getElementById('modalSave'));
                  this.modalCombineKeyword = new bootstrap.Modal(document.getElementById('modalCombineKeyword'));
                  this.modalGenerateKeyword = new bootstrap.Modal(document.getElementById('modalGenerateKeyword'));
                });
              });
            });
          });
      }
    });
  }

  convertObjToTreeNodes(arr: any[]) {
    const treeViewObj = [];
    for (let i = 0; i < arr.length; i++) {
      treeViewObj.push(new TreeNode(arr[i]));
    }
    return treeViewObj;
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  onValueChangeProperty(propertyIndex: number, event: TreeNode) {
    this.properties.controls[propertyIndex]['controls']['name'].setValue(event.value ? event.text : '');
    this.properties.controls[propertyIndex]['controls']['parentText'].setValue(event.parentText ? event.parentText : '');
    this.properties.controls[propertyIndex]['controls']['flagged'].setValue(event.flagged ? event.flagged : false);
    this.actualizePropertyModelArray(propertyIndex);
    this.actualizeCategoryItem(propertyIndex, event);
    this.actualizeQualitiesEnabled(propertyIndex, event);
    this.actualizeSpeciesEnabled(propertyIndex, event);
    this.checkUnique();
    this.actualizeCropEnabled(propertyIndex);
  }

  actualizeCropEnabled(propIndex: number) {
    const object = this.findByProperty(this.options_crops_unformatted, 'value', this.propertiesprocessForm.value.properties[propIndex].id);
    if (object === undefined) {
      this.properties.controls[propIndex]['controls']['crop'].setValue('');
      this.properties.controls[propIndex]['controls']['isCrop'].setValue(false);
    } else {
      this.properties.controls[propIndex]['controls']['isCrop'].setValue(true);
    }
    this.properties.controls[propIndex]['controls']['crop'].updateValueAndValidity();
  }

  checkUnique() {
    for (let i = 0; i < this.propertiesprocessForm.value.properties.length; i++) {
      if (!this.properties.controls[i]['controls']['id'].hasError('required')) {
        if (this.propertiesprocessForm.value.properties[i].qualitiesEnabled && this.propertiesprocessForm.value.properties[i].qualities.length > 0) {
          if (this.propertiesprocessForm.value.properties.filter(item => (item.name !== '' &&
          item.name === this.propertiesprocessForm.value.properties[i].name &&
          (item.qualitiesEnabled))).length > 1) {
              if (this.propertiesprocessForm.value.properties.filter(item => (item.name !== '' &&
              item.name === this.propertiesprocessForm.value.properties[i].name &&
              item.qualitiesEnabled && item.qualities.length > 0 &&
              (this.arraysEqual(item.qualities, this.propertiesprocessForm.value.properties[i].qualities)))).length > 1) {
                this.properties.controls[i]['controls']['id'].setErrors({notunique: true});
              } else {
                this.properties.controls[i]['controls']['id'].setErrors(null);
              }
          } else {
            this.properties.controls[i]['controls']['id'].setErrors(null);
          }
        } else {
          if (this.propertiesprocessForm.value.properties.filter(item => (item.name !== '' &&
          item.name === this.propertiesprocessForm.value.properties[i].name &&
          (!item.qualitiesEnabled))).length > 1) {
              this.properties.controls[i]['controls']['id'].setErrors({notunique: true});
            } else {
              this.properties.controls[i]['controls']['id'].setErrors(null);
            }
        }
      }
    }
    this.updateErrorMessages();
  }

  updateErrorMessages() {
    this.resetItemErrorArray();
    for (const message of PropertiesprocessErrorMessages) {
      const control = this.propertiesprocessForm.get(message.forControl);
      if (!control) {
        for (let i = 0; i < this.properties.controls.length ; i++) {
          const controlitem = this.properties.controls[i].get(message.forControl);
          if (controlitem &&
            controlitem.invalid &&
            controlitem.errors[message.forValidator] &&
            !this.itemErrors[i][message.forControl]) {
            this.itemErrors[i][message.forControl] = message.text;
          }
        }
      }
    }
  }

  resetItemErrorArray() {
    this.itemErrors = [{}];
    for (let i = 0; i < this.properties.controls.length ; i++) {
      this.itemErrors[i] = {'id': ''};
    }
  }

  resetSpeciesEnabled() {
    for (let i = 0; i < this.propertiesprocessForm.value.properties.length; i++) {
      this.actualizeSpeciesEnabled(i, { value: this.propertiesprocessForm.value.properties[i].id });
    }
  }

  actualizeSpeciesEnabled(propertyIndex: number, event: any) {
    if (event.value) {
      const item = this.findByProperty(this.options_qualities_switch, 'value', event.value);
      this.properties.controls[propertyIndex]['controls']['speciesEnabled'].setValue(item ? true : false);
      if (this.properties.value[propertyIndex].speciesEnabled) {
        if (item.edaphoid && item.edaphoid !== '') {
          this.isLoadingEdaphobase[propertyIndex] = true;
          this.ps.getTaxonTreeByEdaphobaseId(item.edaphoid).subscribe(
            datas => {
              this.speciesUnavailable[propertyIndex] = false;
              if (datas.name) {
                const result = JSON.parse(JSON.stringify(datas).replace(/"displayName2":/g, '"text":').replace(/"id":/g, '"value":'));
                if (result.children && result.children.length && result.children.length > 0) {
                  this.options_species[propertyIndex] = this.convertObjToTreeNodes(result.children);
                } else {
                  this.options_species[propertyIndex] = [new TreeNode(result)];
                }
                this.isLoadingEdaphobase[propertyIndex] = false;
              } else {
                this.options_species[propertyIndex] = [];
                this.isLoadingEdaphobase[propertyIndex] = false;
                for (let i = 0; i < this.properties.controls[propertyIndex]['controls']['species']['controls'].length; i++) {
                  this.onValueChangeSpecieID(propertyIndex, i, {value: undefined});
                }
              }
            },
            error => {
              this.speciesUnavailable[propertyIndex] = true;
              this.options_species[propertyIndex] = [];
              this.isLoadingEdaphobase[propertyIndex] = false;
              for (let i = 0; i < this.properties.controls[propertyIndex]['controls']['species']['controls'].length; i++) {
                this.onValueChangeSpecieID(propertyIndex, i, {value: undefined});
              }
            }
          );
        } else if (item.colid && item.colid !== '') {
          this.isLoadingEdaphobase[propertyIndex] = true;
          this.ps.getTaxonTreeByColId(item.colid).subscribe(
            datas => {
              this.speciesUnavailable[propertyIndex] = false;
              if (datas[0] && datas[0].name) {
                const result = JSON.parse(JSON.stringify(datas[0]).replace(/"name":/g, '"text":').replace(/"id":/g, '"value":'));
                if (result.children && result.children.length && result.children.length > 0) {
                  this.options_species[propertyIndex] = this.convertObjToTreeNodes(result.children);
                } else {
                  this.options_species[propertyIndex] = [new TreeNode(result)];
                }
                this.isLoadingEdaphobase[propertyIndex] = false;
              } else {
                this.options_species[propertyIndex] = [];
                this.isLoadingEdaphobase[propertyIndex] = false;
                for (let i = 0; i < this.properties.controls[propertyIndex]['controls']['species']['controls'].length; i++) {
                  this.onValueChangeSpecieID(propertyIndex, i, {value: undefined});
                }
              }
            },
            error => {
              this.speciesUnavailable[propertyIndex] = true;
              this.options_species[propertyIndex] = [];
              this.isLoadingEdaphobase[propertyIndex] = false;
              for (let i = 0; i < this.properties.controls[propertyIndex]['controls']['species']['controls'].length; i++) {
                this.onValueChangeSpecieID(propertyIndex, i, {value: undefined});
              }
            }
          );
        } else {
          this.speciesUnavailable[propertyIndex] = false;
          this.options_species[propertyIndex] = [];
          for (let i = 0; i < this.properties.controls[propertyIndex]['controls']['species']['controls'].length; i++) {
            this.onValueChangeSpecieID(propertyIndex, i, {value: undefined});
          }
        }
      } else {
        this.properties.controls[propertyIndex]['controls']['speciesEnabled'].setValue(false);
        this.properties.controls[propertyIndex]['controls']['species'].reset();
        this.properties.controls[propertyIndex]['controls']['species'].clear();
      }
    } else {
      this.properties.controls[propertyIndex]['controls']['speciesEnabled'].setValue(false);
      this.properties.controls[propertyIndex]['controls']['species'].reset();
      this.properties.controls[propertyIndex]['controls']['species'].clear();
    }
  }

  actualizeQualitiesEnabled(propertyIndex: number, event: any) {
    if (event.value) {
      const item = this.findByProperty(this.options_qualities_switch, 'value', event.value);
      this.properties.controls[propertyIndex]['controls']['qualitiesEnabled'].setValue(
        item && !this.options_qualities_exclude.includes(item.id) ? true : false);
    } else {
      this.properties.controls[propertyIndex]['controls']['qualitiesEnabled'].setValue(false);
    }
    if (this.properties.value[propertyIndex].qualitiesEnabled) {
      this.properties.controls[propertyIndex]['controls']['qualities'].setValidators([Validators.required]);
      if (this.properties.controls[propertyIndex]['controls']['qualities'].length === 0) {
        this.addQuality(propertyIndex);
      }
    } else {
      this.properties.controls[propertyIndex]['controls']['qualities'].setValidators([]);
      this.properties.controls[propertyIndex]['controls']['qualities'].reset();
      this.properties.controls[propertyIndex]['controls']['qualities'].clear();
    }
  }

  actualizeCategoryItem(propertyIndex: number, event: any) {
    let object = this.findByProperty(this.options_properties_unformatted_modelled, 'value', this.propertiesprocessForm.value.properties[propertyIndex].id);
    if (object === undefined) {
      object = this.findByProperty(this.options_properties_usergen_unformatted, 'value', this.propertiesprocessForm.value.properties[propertyIndex].id);
      if (object === undefined) {
        object = this.findByProperty(this.options_geography_unformatted, 'value', this.propertiesprocessForm.value.properties[propertyIndex].id);
        if (object !== undefined) {
          this.properties.controls[propertyIndex]['controls']['category'].setValue('geography');
        } else {
          this.properties.controls[propertyIndex]['controls']['category'].setValue('properties');
        }
      } else {
        if (object.category === 'Properties') {
          this.properties.controls[propertyIndex]['controls']['category'].setValue('properties');
        } else {
          this.properties.controls[propertyIndex]['controls']['category'].setValue('geography');
        }
      }
    } else {
      this.properties.controls[propertyIndex]['controls']['category'].setValue('properties');
    }
  }

  onValueChangeQuality(propertyIndex: number, qualityIndex: number, event: any) {
    this.properties.controls[propertyIndex]['controls']['qualities']['controls'][qualityIndex]['controls']['name'].setValue(
      event.value ? event.text : '');
    this.checkUnique();
  }

  onValueChangeSpecie(propertyIndex: number, specieIndex: number, event: any) {
    this.properties.controls[propertyIndex]['controls']['species']['controls'][specieIndex]['controls']['name'].setValue(
      event.value ? event.text : '');
  }

  onValueChangeSpecieID(propertyIndex: number, specieIndex: number, event: any) {
    this.properties.controls[propertyIndex]['controls']['species']['controls'][specieIndex]['controls']['id'].setValue(
      event.value ? event.text : '');
  }

  resetPropertyModelArray() {
    for (let i = 0; i < this.properties.controls.length; i++) {
      this.actualizePropertyModelArray(i);
    }
  }

  actualizePropertyModelArray(propertyIndex: number) {
    let prop = this.findByProperty(this.options_properties_unformatted_modelled, 'value',
    this.propertiesprocessForm.value.properties[propertyIndex].id
    );
    if (prop) {
      this.propertymodelArr[propertyIndex] = true;
    } else {
      prop = this.findByProperty(this.options_properties_usergen_unformatted, 'value',
      this.propertiesprocessForm.value.properties[propertyIndex].id
      );
      this.propertymodelArr[propertyIndex] = prop ? true : false;
    }
  }

  addProperty(id?: string) {
    this.properties.insert(0,
      this.fb.group({
        name: this.fb.control('', [Validators.required]),
        id: this.fb.control(id ? id : '', Validators.required),
        parentText: this.fb.control(''),
        flagged: this.fb.control(false),
        isModel: this.fb.control(false),
        method: this.fb.control(''),
        model: this.fb.control(''),
        qualitiesEnabled: this.fb.control(false),
        qualities: this.fb.array([], [Validators.required]),
        combined: this.fb.control(''),
        category: this.fb.control(''),
        speciesEnabled: this.fb.control(false),
        species: this.fb.array([]),
        isCrop: this.fb.control(false),
        crop: this.fb.control('')
      })
    );
    timer(1).subscribe(val => {
      this.propertiesprocessForm.markAsDirty();
    });
    this.resetPropertyModelArray();
    this.resetSpeciesEnabled();
    this.updateErrorMessages();
  }

  removeProperty(propertyIndex: number) {
    this.properties.removeAt(propertyIndex);
    timer(1).subscribe(val => {
      this.propertiesprocessForm.markAsDirty();
    });
    this.resetSpeciesEnabled();
    this.resetPropertyModelArray();
    this.updateErrorMessages();
  }

  addSpecie(propertyIndex: number) {
    this.properties.controls[propertyIndex]['controls']['species'].push(
      this.fb.group({
        name: this.fb.control('', Validators.required),
        id: this.fb.control('')
      }));
    this.propertiesprocessForm.markAsDirty();
  }

  removeSpecie(propertyIndex: number, specieIndex: number) {
    this.properties.controls[propertyIndex]['controls']['species'].removeAt(specieIndex);
    timer(1).subscribe(val => {
      this.propertiesprocessForm.markAsDirty();
    });
  }

  addQuality(propertyIndex: number) {
    this.properties.controls[propertyIndex]['controls']['qualities'].push(
      this.fb.group({
        name: this.fb.control('', [Validators.required]),
        id: this.fb.control('', [Validators.required]),
      }));
    this.propertiesprocessForm.markAsDirty();
    this.checkUnique();
  }

  removeQuality(propertyIndex: number, qualityIndex: number) {
    this.properties.controls[propertyIndex]['controls']['qualities'].removeAt(qualityIndex);
    timer(1).subscribe(val => {
      this.propertiesprocessForm.markAsDirty();
    });
    this.checkUnique();
  }

  initPropertiesProcess() {
    this.properties = this.fb.array(
      this.propertiesprocess.properties.map(
        p => this.fb.group({
          name: this.fb.control(p.name, [Validators.required]),
          id: this.fb.control(p.id, Validators.required),
          parentText: this.fb.control(p.parentText),
          flagged: this.fb.control(p.flagged),
          isModel: this.fb.control(p.isModel),
          model: this.fb.control(p.model),
          method: this.fb.control(p.method),
          qualitiesEnabled: this.fb.control(p.qualitiesEnabled),
          qualities: this.fb.array(p.qualities.map(
            q => this.fb.group({
              id: this.fb.control(q.id, [Validators.required]),
              name: this.fb.control(q.name, [Validators.required]),
            })
          )),
          combined: this.fb.control(p.combined),
          category: this.fb.control(p.category),
          speciesEnabled: this.fb.control(p.speciesEnabled),
          species: this.fb.array(p.species.map(
            sp => this.fb.group({
              name: this.fb.control(sp.name, [Validators.required]),
              id: this.fb.control(sp.id)
            })
          )),
          isCrop: this.fb.control(p.isCrop),
          crop: this.fb.control(p.crop)
        })
      )
    );

    this.propertiesprocessForm = this.fb.group({
      properties: this.properties
    });

  }

  initGenKey() {
    this.genkeyForm = this.fb.group({
      name: this.fb.control('', Validators.required, [AdddriverValidators.genkeyAvailable(this.as),
      AdddriverValidators.keywordInTree(this.options_unformatted) ]),
      isNumeric: this.fb.control(true),
      category: this.fb.control('', Validators.required)
    });
    this.genkeyForm.statusChanges.subscribe(() => this.updateErrorMessagesGenkey());
  }

  updateErrorMessagesGenkey() {
    this.errorsGenkey = {};
    for (const message of GenkeyErrorMessages) {
      const control = this.genkeyForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errorsGenkey[message.forControl]) {
        this.errorsGenkey[message.forControl] = message.text;
      }
    }
  }

  updateErrorMessagesCombinekey() {
    this.errorsCombinekey = {};
    if (this.combinekeyForm &&
      this.combinekeyForm.dirty &&
      this.combinekeyForm.invalid &&
      this.combinekeyForm.errors &&
      this.combinekeyForm.errors[CombinekeyErrorMessages[0].forValidator]) {
      this.errorsCombinekey['combine'] = CombinekeyErrorMessages[0].text;
    }
  }

  genkeyGenerator() {
    this.modalGenerateKeyword.show();
  }

  combinekeyGenerator() {
    this.modalCombineKeyword.show();
  }

  initCombineKey() {
    this.combinekeyForm = this.fb.group({
      dividend_items: this.fb.array([], Validators.required),
      divisor_items: this.fb.array([]),
      isNumeric: this.fb.control(true)
    }, { asyncValidators: AdddriverValidators.combinekeyAvailable(this.as) });
    this.combinekeyForm.statusChanges.subscribe(() => this.updateErrorMessagesCombinekey());
  }

  addDividend() {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['dividend_items'].push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
  }

  addDivisor() {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['divisor_items'].push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
  }

  removeDividend(diviIndex: number) {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['dividend_items'].removeAt(diviIndex);
  }

  removeDivisor(diviIndex: number) {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['divisor_items'].removeAt(diviIndex);
  }

  openModalSave() {
    this.modalSave.show();
  }

  submitFormPropertiesprocess(redirect?: string) {
    if (this.propertiesprocessForm.valid) {
      this.isSaveLoading = true;
      const propertiesprocess = PropertiesProcessFactory.fromObject(this.propertiesprocessForm.value);
      propertiesprocess.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          propertiesprocess._id = this.propertiesprocess._id;
          this.ps.update(propertiesprocess)
            .subscribe(
                datas => {
                  this.isSaveLoading = false;
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.propertiesprocess = datas;
                    this.successSave = true;
                    this.initPropertiesProcess();
                    window.scrollTo(0, 0);
                    this.ts.getOneBySoildoc(this.soildoc._id).subscribe(result => {
                      this.typeofstudy = result;
                    });
                    if (redirect && redirect !== '') {
                      this.router.navigateByUrl(redirect);
                    } else {
                      const source = timer(3000);
                      source.subscribe(val => {
                        this.successSave = false;
                      });
                    }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.ps.create(propertiesprocess)
          .subscribe(
              datas => {
                this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.propertiesprocess = datas;
                  this.initPropertiesProcess();
                  this.isEdit = true;
                  this.successSave = true;
                  window.scrollTo(0, 0);
                  this.ts.getOneBySoildoc(this.soildoc._id).subscribe(result => {
                    this.typeofstudy = result;
                  });
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  submitFormGenkey() {
    if (this.genkeyForm.valid) {
      const genkey = GenKeyFactory.fromObject(this.genkeyForm.value);
      this.as.createGenKey(genkey)
          .subscribe(
              datas => {
                  this.failSaveGenKey = false;
                  this.modalGenerateKeyword.hide();
                  if (datas.category === 'Properties') {
                    this.options_properties[0].children[0].children[0].children.unshift(new TreeNode(datas));
                  } else if (datas.category === 'Geography') {
                    this.options_properties[0].children[0].children[1].children.unshift(new TreeNode(datas));
                  }
                  this.options_properties = this.options_properties.slice();
                  this.options_properties_usergen_unformatted.unshift(datas);
                  this.isLoading = false;
                  this.addProperty(datas.value);
                  this.initGenKey();
              },
              error => this.failSaveGenKey = true
          );
    }
  }

  submitFormCombinekey() {
    if (this.combinekeyForm.valid) {
      const combinekey = CombineKeyFactory.fromObject(this.combinekeyForm.value);
      this.as.createCombineKey(combinekey)
          .subscribe(
              datas => {
                  this.failSaveCombineKey = false;
                  this.modalCombineKeyword.hide();
                  this.options_properties[0].children[1].children.unshift(new TreeNode(datas));
                  this.options_properties_usergen_unformatted.unshift(datas);
                  this.options_properties = this.options_properties.slice();
                  this.isLoading = false;
                  this.addProperty(datas.value);
                  this.initCombineKey();
              },
              error => this.failSaveCombineKey = true
          );
    }
  }

  checkifsaved(routerlink: string) {
    if (this.propertiesprocessForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalRedirect.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }

  objectsEqual(o1, o2) {
    return Object.keys(o1).length === Object.keys(o2).length
    && Object.keys(o1).every(p => o1[p] === o2[p]);
  }

  arraysEqual(a1, a2) {
    return a1.length === a2.length &&
    a1.every((o, idx) => a2.some(obj => this.objectsEqual(o, obj))) &&
    a2.every((o, idx) => a1.some(obj => this.objectsEqual(o, obj)));
  }
}
