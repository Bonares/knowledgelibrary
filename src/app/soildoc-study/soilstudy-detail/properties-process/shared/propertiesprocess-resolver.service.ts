import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AuthGuard } from './../../../../shared/auth.guard';
import { PropertiesProcessService } from './propertiesprocess.service';
import { Injectable } from '@angular/core';
import { PropertiesProcess } from './PropertiesProcess';
@Injectable({
  providedIn: 'root'
})
export class PropertiesProcessResolverService {

  constructor(private ps: PropertiesProcessService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<PropertiesProcess> {
    if (this.ag.canActivateMethod()) {
    return this.ps.getOneBySoildoc(route.params['id']);
    }
  }
}
