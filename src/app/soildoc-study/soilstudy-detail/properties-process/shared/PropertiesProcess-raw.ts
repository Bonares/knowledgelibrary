import { Properties } from './Properties';
export interface PropertiesProcessRaw {
  _id: string;
  properties: Properties;
  soildoc_id: string;
}
