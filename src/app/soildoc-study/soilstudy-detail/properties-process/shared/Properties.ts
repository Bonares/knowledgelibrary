import { Specie } from './Specie';
import { Qualities } from './Qualities';
export class Properties {
  constructor(
    public _id: string,
    public name: string,
    public id: string,
    public parentText: string,
    public flagged: boolean,
    public isModel: boolean,
    public model: string,
    public method: string,
    public qualitiesEnabled: boolean,
    public qualities: Qualities[],
    public combined: string,
    public category: string,
    public speciesEnabled: boolean,
    public species: Specie[],
    public isCrop: boolean,
    public crop: string,
   ) {}
}
