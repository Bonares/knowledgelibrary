import { PropertiesProcessRaw } from './PropertiesProcess-raw';
import { PropertiesProcess } from './PropertiesProcess';
export class PropertiesProcessFactory {

  static empty(): PropertiesProcess {
    return new PropertiesProcess('', [], null);
  }

  static fromObject(rawPropertiesprocess: PropertiesProcessRaw | any): PropertiesProcess {
    if (rawPropertiesprocess) {
      return new PropertiesProcess(
        rawPropertiesprocess._id,
        rawPropertiesprocess.properties,
        rawPropertiesprocess.soildoc_id
      );
    } else {
      return null;
    }
  }
  }
