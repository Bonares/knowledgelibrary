import { PropertiesProcessFactory } from './PropertiesProcessFactory';
import { retry, timeout, map, catchError } from 'rxjs/operators';
import { PropertiesProcessRaw } from './PropertiesProcess-raw';
import { throwError, Observable } from 'rxjs';
import { AuthGuard } from './../../../../shared/auth.guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { PropertiesProcess } from './PropertiesProcess';
@Injectable({
  providedIn: 'root'
})
export class PropertiesProcessService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<PropertiesProcess> {
    return this.httpClient
      .get<PropertiesProcessRaw>(`${this.api}/propertiesprocess/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawPropertiesProcess => PropertiesProcessFactory.fromObject(rawPropertiesProcess),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<PropertiesProcess> {
    return this.httpClient
      .get<PropertiesProcessRaw>(`${this.api}/propertiesprocess/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawPropertiesProcess => PropertiesProcessFactory.fromObject(rawPropertiesProcess),
        ),
        catchError(this.handleError)
      );
  }


  create(propertiesprocess: PropertiesProcess): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/propertiesprocess`, propertiesprocess)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(propertiesprocess: PropertiesProcess): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/propertiesprocess/${propertiesprocess._id}`, propertiesprocess)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }

  getTaxonTreeByEdaphobaseId(id: string): Observable<any> {
    return this.httpClient
      .get(`https://api.edaphobase.org/taxon/${id}?method=tree&depth=-1&childFormat=array`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }

  getTaxonTreeByColId(id: string): Observable<any> {
    return this.httpClient
      .get(`https://api.catalogueoflife.org/dataset/3LR/export/${id}?nested=true`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }
}
