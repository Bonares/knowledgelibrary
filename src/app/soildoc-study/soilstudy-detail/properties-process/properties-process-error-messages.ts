export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const PropertiesprocessErrorMessages = [
  new ErrorMessage('id', 'notunique', 'PROPERTIESPROCESS.NOTUNIQUE' )
];
