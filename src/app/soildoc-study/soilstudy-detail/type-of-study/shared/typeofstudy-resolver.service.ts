import { AuthGuard } from './../../../../shared/auth.guard';
import { SoildocService } from './../../../../soildoc/soil-doc/shared/soildoc.service';
import { Typeofstudy } from './Typeofstudy';
import { TypeofstudyService } from './typeofstudy.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TypeofstudyResolverService {

  constructor(private ts: TypeofstudyService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Typeofstudy> {
    if (this.ag.canActivateMethod()) {
    return this.ts.getOneBySoildoc(route.params['id']);
    }
  }
}
