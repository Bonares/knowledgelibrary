export class MetaContext {
  constructor(
    public studies_number: number,
    public datapoints_number: number,
    public time_scale: string
  ) {}
}
