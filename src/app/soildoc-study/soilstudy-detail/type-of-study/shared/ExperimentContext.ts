export class ExperimentContext {
  constructor(
    public type: string,
    public site_input_mode: string,
    public time_scale: string
  ) {}
}
