import { TypeofstudyRaw } from './Typeofstudy-raw';
import { Typeofstudy } from './Typeofstudy';
export class TypeofstudyFactory {

static empty(): Typeofstudy {
  return new Typeofstudy('', '', null, false);
}

static fromObject(rawTypeofstudy: TypeofstudyRaw | any): Typeofstudy {
  if (rawTypeofstudy) {
    return new Typeofstudy(
      rawTypeofstudy._id,
      rawTypeofstudy.name,
      rawTypeofstudy.soildoc_id,
      rawTypeofstudy.impactonenabled,
      rawTypeofstudy.metaContext,
      rawTypeofstudy.experimentContext,
      rawTypeofstudy.methodContext,
      rawTypeofstudy.modelContext
    );
  } else {
    return null;
  }

}
}
