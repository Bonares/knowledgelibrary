import { AuthGuard } from './../../../../shared/auth.guard';
import { TypeofstudyFactory } from './TypeofstudyFactory';
import { TypeofstudyRaw } from './Typeofstudy-raw';
import { Typeofstudy } from './Typeofstudy';
import { catchError, timeout, retry, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TypeofstudyService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Typeofstudy> {
    return this.httpClient
      .get<TypeofstudyRaw>(`${this.api}/typeofstudy/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawTypeofstudy => TypeofstudyFactory.fromObject(rawTypeofstudy),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Typeofstudy> {
    return this.httpClient
      .get<TypeofstudyRaw>(`${this.api}/typeofstudy/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawTypeofstudy => TypeofstudyFactory.fromObject(rawTypeofstudy),
        ),
        catchError(this.handleError)
      );
  }

  create(typeofstudy: Typeofstudy): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/typeofstudy`, typeofstudy)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(typeofstudy: Typeofstudy): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/typeofstudy/${typeofstudy._id}`, typeofstudy)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
