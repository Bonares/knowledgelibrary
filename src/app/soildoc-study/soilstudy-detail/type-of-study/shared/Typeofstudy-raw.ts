import { MetaContext } from './MetaContext';
import { ModelContext } from './ModelContext';
import { ExperimentContext } from './ExperimentContext';
import { MethodContext } from './MethodContext';
export interface TypeofstudyRaw {
  _id: string;
  name: string;
  soildoc_id: string;
  impactonenabled: boolean;
  metaContext: MetaContext;
  experimentContext: ExperimentContext;
  methodContext: MethodContext;
  modelContext: ModelContext;
}
