export class MethodContext {
  constructor(
    public development: boolean,
    public application: boolean,
    public comparison: boolean
  ) {}
}
