import { MetaContext } from './MetaContext';
import { ExperimentContext } from './ExperimentContext';
import { MethodContext } from './MethodContext';
import { ModelContext } from './ModelContext';

export class Typeofstudy {
  constructor(
    public _id: string,
    public name: string,
    public soildoc_id: string,
    public impactonenabled: boolean,
    public metaContext?: MetaContext,
    public experimentContext?: ExperimentContext,
    public methodContext?: MethodContext,
    public modelContext?: ModelContext
   ) {}

}
