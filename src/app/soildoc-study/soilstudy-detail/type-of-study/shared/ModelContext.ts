export class ModelContext {
  constructor(
    public development: boolean,
    public application: boolean,
    public comparison: boolean
  ) {}
}
