import { UntypedFormControl} from '@angular/forms';
export class TypeofstudyValidators {

  static onlyNumber11000(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(1000|[1-9][0-9]?[0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat11000: {valid: false}
    };
  }

  static onlyNumber1100(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(100|[1-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat1100: {valid: false}
    };
  }
}
