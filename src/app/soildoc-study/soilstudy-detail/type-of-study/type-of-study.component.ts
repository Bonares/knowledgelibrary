import { MetaService } from './../../../shared/meta.service';
import { TypeofstudyErrorMessages } from './typeofstudy-error-messages';
import { TypeofstudyValidators } from './shared/typeofstudy-validators';
import { TypeofstudyService } from './shared/typeofstudy.service';
import { TypeofstudyFactory } from './shared/TypeofstudyFactory';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Soildoc } from 'src/app/soildoc/soil-doc/shared/Soildoc';
import { faSearch, faQuestionCircle, faEdit, faSave, faSpinner, faAsterisk, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { timer } from 'rxjs';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-type-of-study',
  templateUrl: './type-of-study.component.html',
  providers: [],
  styleUrls: []
})
export class TypeOfStudyComponent implements OnInit {

  faBookOpen = faBookOpen;
  faAsterisk = faAsterisk;
  faSpinner = faSpinner;
  faSave = faSave;
  faEdit = faEdit;
  faQuestionCircle = faQuestionCircle;
  faSearch = faSearch;
  soildoc: Soildoc;
  errors: { [key: string]: string } = {};
  typeofstudy = TypeofstudyFactory.empty();
  typeofstudyForm: UntypedFormGroup;
  failSave: Boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  metaContext: UntypedFormGroup;
  experimentContext: UntypedFormGroup;
  methodContext: UntypedFormGroup;
  modelContext: UntypedFormGroup;
  isLoading: Boolean;
  isSaveLoading: Boolean = false;
  errorLoading = false;
  successSave = false;

  redirectUrl = '';

  modalRedirect : bootstrap.Modal;

  typeOptions = ['Experiment', 'Method', 'Model', 'Meta-analysis'];
  type_experimentOptions = ['Field', 'Lab', 'Field & Lab'];
  site_inputOptions = ['Individual', 'Combined'];
  time_scaleOptions = ['Not Applicable', '< 1 hour', '< 1 day', '< 1 week', '< 1 month', '< 1 year', '< 1 decade', '> 1 decade'];

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private ts: TypeofstudyService,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.TYPE_OF_STUDY', 'TYPE_OF_STUDY.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }

    if (data['typeofstudy']) {
      this.isEdit = true;
      this.typeofstudy = data['typeofstudy'];
    }
    this.initTypeofstudy();

    if (this.typeofstudy) {
      this.changeOptionalElements(this.typeofstudy.name);
    }
    this.isLoading = false;
    timer(1).subscribe(val => {this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));});
  }

  initTypeofstudy() {
    this.buildOptionalElements();
    this.typeofstudyForm = this.fb.group({
      name: [this.typeofstudy.name,
        [
          Validators.required
        ]
      ],
      metaContext: this.metaContext,
      experimentContext: this.experimentContext,
      methodContext: this.methodContext,
      modelContext: this.modelContext
    });

    this.typeofstudyForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  buildOptionalElements() {

    this.metaContext =
    this.fb.group({
      studies_number: [this.typeofstudy.metaContext ? this.typeofstudy.metaContext.studies_number : '',
        [
          Validators.required, TypeofstudyValidators.onlyNumber11000
        ]
      ],
      datapoints_number: [this.typeofstudy.metaContext ? this.typeofstudy.metaContext.datapoints_number : '',
        [
          TypeofstudyValidators.onlyNumber11000
        ]
      ],
      time_scale: [this.typeofstudy.metaContext ? this.typeofstudy.metaContext.time_scale : '',
        [
          Validators.required
        ]
      ],
    });

    this.experimentContext =
    this.fb.group({
      type: [this.typeofstudy.experimentContext ? this.typeofstudy.experimentContext.type : '',
        [
          Validators.required
        ]
      ],
      site_input_mode: [this.typeofstudy.experimentContext ? this.typeofstudy.experimentContext.site_input_mode : '',
        [
          Validators.required
        ]
      ],
      time_scale: [this.typeofstudy.experimentContext ? this.typeofstudy.experimentContext.time_scale : '',
        [
          Validators.required
        ]
      ]
    });

    this.methodContext =
    this.fb.group({
      development: [this.typeofstudy.methodContext ? this.typeofstudy.methodContext.development : false],
      application: [this.typeofstudy.methodContext ? this.typeofstudy.methodContext.application : false],
      comparison: [this.typeofstudy.methodContext ? this.typeofstudy.methodContext.comparison : false],
    });

    this.modelContext =
    this.fb.group({
      development: [this.typeofstudy.modelContext ? this.typeofstudy.modelContext.development : false],
      application: [this.typeofstudy.modelContext ? this.typeofstudy.modelContext.application : false],
      comparison: [this.typeofstudy.modelContext ? this.typeofstudy.modelContext.comparison : false],
    });
  }

  changeOptionalElements(type: string) {
    switch (type) {
      case 'Experiment': {
        this.typeofstudyForm.addControl('experimentContext', this.experimentContext);
        this.typeofstudyForm.removeControl('methodContext');
        this.typeofstudyForm.removeControl('modelContext');
        this.typeofstudyForm.removeControl('metaContext');
        break;
      }
      case 'Method': {
        this.typeofstudyForm.addControl('methodContext', this.methodContext);
        this.typeofstudyForm.removeControl('experimentContext');
        this.typeofstudyForm.removeControl('modelContext');
        this.typeofstudyForm.removeControl('metaContext');
        break;
      }
      case 'Model': {
        this.typeofstudyForm.addControl('modelContext', this.modelContext);
        this.typeofstudyForm.removeControl('methodContext');
        this.typeofstudyForm.removeControl('experimentContext');
        this.typeofstudyForm.removeControl('metaContext');
        break;
      }
      case 'Meta-analysis': {
        this.typeofstudyForm.addControl('metaContext', this.metaContext);
        this.typeofstudyForm.removeControl('modelContext');
        this.typeofstudyForm.removeControl('methodContext');
        this.typeofstudyForm.removeControl('experimentContext');
        break;
      }
      default:
        break;
    }
  }

  submitFormTypeofstudy(redirect?: string) {
    if (this.typeofstudyForm.valid) {
      this.isSaveLoading = true;
      switch (this.typeofstudyForm.value['name']) {
        case 'Experiment': {
          this.typeofstudyForm.value['methodContext'] = null;
          this.typeofstudyForm.value['modelContext'] = null;
          this.typeofstudyForm.value['metaContext'] = null;
          this.typeofstudy.methodContext = null;
          this.typeofstudy.modelContext = null;
          this.typeofstudy.metaContext =  null;
          break;
        }
        case 'Method': {
          this.typeofstudyForm.value['experimentContext'] = null;
          this.typeofstudyForm.value['modelContext'] = null;
          this.typeofstudyForm.value['metaContext'] = null;
          this.typeofstudy.experimentContext = null;
          this.typeofstudy.modelContext = null;
          this.typeofstudy.metaContext =  null;
          break;
        }
        case 'Model': {
          this.typeofstudyForm.value['experimentContext'] = null;
          this.typeofstudyForm.value['methodContext'] = null;
          this.typeofstudyForm.value['metaContext'] = null;
          this.typeofstudy.experimentContext = null;
          this.typeofstudy.methodContext = null;
          this.typeofstudy.metaContext =  null;
          break;
        }
        case 'Meta-analysis': {
          this.typeofstudyForm.value['experimentContext'] = null;
          this.typeofstudyForm.value['methodContext'] = null;
          this.typeofstudyForm.value['modelContext'] = null;
          this.typeofstudy.experimentContext = null;
          this.typeofstudy.methodContext = null;
          this.typeofstudy.modelContext = null;
          break;
        }
        default:
          break;
      }
      const typeofstudy = TypeofstudyFactory.fromObject(this.typeofstudyForm.value);
      typeofstudy.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          typeofstudy._id = this.typeofstudy._id;
          this.ts.update(typeofstudy)
            .subscribe(
                datas => {
                  this.isSaveLoading = false;
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.typeofstudy = datas;
                    this.initTypeofstudy();
                    this.changeOptionalElements(datas.name);
                    this.successSave = true;
                    if (redirect && redirect !== '') {
                      this.router.navigateByUrl(redirect);
                    } else {
                      const source = timer(3000);
                      source.subscribe(val => {
                        this.successSave = false;
                      });
                    }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.ts.create(typeofstudy)
          .subscribe(
              datas => {
                this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.typeofstudy = datas;
                  this.initTypeofstudy();
                  this.isEdit = true;
                  this.successSave = true;
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  checkifsaved(routerlink: string) {
    if (this.typeofstudyForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalRedirect.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }

  updateErrorMessages() {
    this.errors = {};
    for (const message of TypeofstudyErrorMessages) {
      let control = this.typeofstudyForm.get(message.forControl);
      if (!control) {
        if (this.typeofstudyForm.controls['metaContext']) {
          control = this.typeofstudyForm.controls['metaContext'].get(message.forControl);
        if (control &&
          control.dirty &&
          control.invalid &&
          control.errors[message.forValidator] &&
          !this.errors[message.forControl]) {
          this.errors[message.forControl] = message.text;
        }
        }
      }
    }
  }
}
