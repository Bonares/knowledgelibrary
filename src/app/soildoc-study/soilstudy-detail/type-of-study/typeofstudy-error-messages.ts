export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const TypeofstudyErrorMessages = [
  new ErrorMessage('studies_number', 'required', 'TYPE_OF_STUDY.FAILFORM.REQUIRED' ),
  new ErrorMessage('studies_number', 'numberFormat11000', 'TYPE_OF_STUDY.FAILFORM.NUMBERFORMAT11000' ),
  new ErrorMessage('datapoints_number', 'numberFormat11000', 'TYPE_OF_STUDY.FAILFORM.NUMBERFORMAT11000' )
];
