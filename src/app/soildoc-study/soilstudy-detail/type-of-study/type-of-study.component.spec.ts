import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TypeOfStudyComponent } from './type-of-study.component';

describe('TypeOfStudyComponent', () => {
  let component: TypeOfStudyComponent;
  let fixture: ComponentFixture<TypeOfStudyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeOfStudyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeOfStudyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
