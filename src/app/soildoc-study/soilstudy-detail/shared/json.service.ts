import { AuthGuard } from './../../../shared/auth.guard';
import { catchError, timeout, retry, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getbyData(data: String[]): Observable<any> {
    return this.httpClient
      .post(`${this.api}/getdata`, data)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        catchError(this.handleError)
      );
    }
}
