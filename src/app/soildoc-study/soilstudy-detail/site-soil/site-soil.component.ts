import { MetaService } from './../../../shared/meta.service';
import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { RelationshipService } from 'src/app/soildoc-study/soilstudy-detail/relationship/shared/relationship.service';
import { GenKeyFactory } from './../add-driver/shared/GenKeyFactory';
import { GenkeyErrorMessages } from './../add-driver/genkey-error-messages';
import { AdddriverValidators } from './../add-driver/shared/adddriver-validators';
import { AdddriverService } from './../add-driver/shared/adddriver.service';
import { CropErrorMessages } from './crop-error-messages';
import { SitesoilService } from './shared/sitesoil.service';
import { CropGenFactory } from './shared/CropGenFactory';
import { SiteSoilErrorMessages } from './site-soil-error-messages';
import { JsonService } from './../shared/json.service';
import { Typeofstudy } from './../type-of-study/shared/Typeofstudy';
import { SitesoilValidators } from './shared/sitesoil-validators';
import { SitesoilFactory } from './shared/SitesoilFactory';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormArray, Validators } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef} from '@angular/core';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { ActivatedRoute, Router } from '@angular/router';
import { faSeedling, faTrashAlt, faMapMarkedAlt, faLayerGroup, faCloudSunRain, faMapMarkerAlt, faEdit, faSync, faTimes, faPlus, faSave,
  faLongArrowAltRight, faTractor, faMap, faCopy, faQuestionCircle, faSearch, faFileWord, faObjectGroup, faSpinner, faExclamationCircle,
  faInfoCircle, faExpandAlt, faBookOpen} from '@fortawesome/free-solid-svg-icons';
import { timer } from 'rxjs';
import * as _ from 'lodash';
import { Site } from './shared/Site';
import { Layer } from './shared/Layer';
import { Relationship } from '../relationship/shared/Relationship';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-site-soil',
  templateUrl: './site-soil.component.html',
  providers: []
})
export class SiteSoilComponent implements OnInit {

  faBookOpen = faBookOpen;
  faInfoCircle = faInfoCircle;
  faExclamationCircle = faExclamationCircle;
  faSpinner = faSpinner;
  faSave = faSave;
  faPlus = faPlus;
  faTimes = faTimes;
  faSync = faSync;
  faEdit = faEdit;
  faObjectGroup = faObjectGroup;
  faFileWord = faFileWord;
  faSearch = faSearch;
  faQuestionCircle = faQuestionCircle;
  faCopy = faCopy;
  faSeedling = faSeedling;
  faTrashAlt = faTrashAlt;
  faMapMarkedAlt = faMapMarkedAlt;
  faLayerGroup = faLayerGroup;
  faCloudSunRain = faCloudSunRain;
  faMapMarkerAlt = faMapMarkerAlt;
  faLongArrowAltRight = faLongArrowAltRight;
  faTractor = faTractor;
  faMap = faMap;
  faExpandAlt = faExpandAlt;

  soildoc: Soildoc;
  typeofstudy: Typeofstudy;
  relationship: Relationship;
  errors: { [key: string]: string } = {};
  errorsCrop: { [key: string]: string } = {};
  siteErrors: [{[key: string]: string}] = [{}];
  layerErrors: [[{[key: string]: string}]] = [[{}]];
  addpropertyErrors: [[[{[key: string]: string}]]] = [[[{}]]];
  manPracticeErrors: [[{[key: string]: string}]] = [[{}]];
  sitesoil = SitesoilFactory.empty();
  sitesoilForm: UntypedFormGroup;
  cropgen = CropGenFactory.empty();
  cropgenForm: UntypedFormGroup;
  failSave: Boolean;
  failSaveCrop: Boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  isLoading: Boolean;
  isSaveLoading: Boolean = false;
  coordsLoading: Boolean = false;
  errorLoading = false;
  successSave = false;
  crops: UntypedFormArray;
  sites: UntypedFormArray;
  activeSite = 0;
  activeLayer = 0;

  options_soil_type: [TreeNode[]] = [[]];
  options_soil_type_wrb: TreeNode[];
  options_soil_type_usda: TreeNode[];
  options_soil_type_ger: TreeNode[];
  options_soil_type_fao: TreeNode[];
  options_soil_classification: TreeNode[];
  options_soil_specifiers_start: TreeNode[];
  options_soil_specifiers: [TreeNode[]] = [[]];
  options_soil_qualifiers: [TreeNode[]] = [[]];
  options_soil_texture: [TreeNode[]] = [[]];
  options_soil_texture1187: TreeNode[];
  options_soil_texture1097: TreeNode[];
  texture_map_ger: any[];
  options_properties: TreeNode[];
  options_properties_original: any;
  options_climate_zone: TreeNode[];
  options_landuse: TreeNode[];
  options_management_practice: TreeNode[];
  options_management_practice_loaded: TreeNode[];
  options_management_practice_custom: TreeNode[];
  options_crops: TreeNode[] = [];
  options_crops_user: TreeNode[] = [];
  options_genkeys: TreeNode[] = [];
  options_genkeys_management: TreeNode[] = [];
  options_genkeys_landuse: TreeNode[] = [];
  options_unformatted = [];

  maximal_sites = 15;
  maximal_qualifiers = 5;
  maximal_layers = 10;
  maximal_additional_properties = 10;
  maximal_management_practices = 10;
  maximal_crops = 10;

  unitsNew = [];
  bulk_density_items = [];
  soil_organic_c_items = [];
  caco3_items = [];
  additional_properties_items = [[]];
  landuseArray: Boolean[] = [];
  valuesLanduseCropGen = ['451', '452', '482', '483', '484', '485', '486', '487', '488', '489', '490', '491', '492'];
  pH_options = ['pH (water)', 'pH (KCl)', 'pH (CaCl2)'];
  category_options = ['Management', 'Land Use'];

  siteDeleteID: number;
  siteDelete: Site;
  layerDeleteID: number;
  layerDelete: Layer;

  redirectUrl = '';

  qualifierUSDALink = [];
  qualifierUSDAnoLink: TreeNode;
  qualifierWRBLink = [];
  qualifierFAOnoLink: TreeNode;

  errorsGenkey: { [key: string]: string } = {};
  options_keyword: TreeNode[] = [];

  genkeyForm: UntypedFormGroup;
  genkey = GenKeyFactory.empty();

  failSaveGenKey: boolean;

  modalRedirect : bootstrap.Modal;
  modalSave : bootstrap.Modal;
  modalCropgen : bootstrap.Modal;
  modalDeleteSite : bootstrap.Modal;
  modalDeleteLayer : bootstrap.Modal;
  modalGenerateKeyword : bootstrap.Modal;

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private js: JsonService,
    private as: AdddriverService,
    private siteservice: SitesoilService,
    private rs: RelationshipService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.SITE_SOIL', 'SITESOIL.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }
    if (data['typeofstudy']) {
      this.typeofstudy = data['typeofstudy'];
    }

    if (data['sitesoil']) {
      this.isEdit = true;
      this.sitesoil = data['sitesoil'];
    }
    this.initSitesoil();
    this.initCropgen();

    this.js.getbyData(['SoilClassification1080', 'Properties1', 'LandUse450', 'Management332', 'SoilTypeWRB', 'SoilTypeUSDA', 'SoilTypeQualifiersWRB',
    'SoilTypeQualifiersUSDA', 'SoilTypeQualifiersNoneUSDA', 'Geiger',
    'SoilTypeGER', 'UnitsGen', 'SoilTypeQualifiersUSDAoldgroup', 'SoilTypeFAO',
    'SoilTypeQualifiersFAO', 'SoilTypeSpecifiersWRB', 'TextureMapGer', 'Geography511', 'unitsCompact']).subscribe(result => {
      if (result) {
        this.options_soil_classification = this.convertObjToTreeNodes(result[0].children);
        this.options_soil_texture1187 = this.convertObjToTreeNodes(this.findByProperty(result[1], 'value', '1187').children);
        this.options_soil_texture1097 = this.convertObjToTreeNodes(this.findByProperty(result[1], 'value', '1097').children);
        this.options_soil_type_wrb = this.convertObjToTreeNodes(result[4].children);
        this.options_soil_type_usda = this.convertObjToTreeNodes(result[5].children);
        this.qualifierWRBLink = result[6];
        this.qualifierUSDALink = result[7];
        this.qualifierUSDALink.push(...result[12]);
        this.qualifierUSDAnoLink = new TreeNode(result[8]);
        this.qualifierFAOnoLink = new TreeNode(result[14]);
        this.options_climate_zone = this.convertObjToTreeNodes(result[9]);
        this.options_soil_type_ger = this.convertObjToTreeNodes(result[10].children);
        this.options_soil_type_fao = this.convertObjToTreeNodes(result[13].children);
        this.options_soil_specifiers_start = this.convertObjToTreeNodes(result[15]);
        this.texture_map_ger = result[16];
        this.options_keyword.push(new TreeNode(result[1]));
        this.options_keyword.push(new TreeNode(result[11]));
        if (data['sitesoil']) {
          for (let i = 0; i < this.sitesoil.sites.length ; i++) {
            if (this.sitesoil.sites[i].soil_classification_id === '1081' || this.sitesoil.sites[i].soil_classification_id === '1082') {
              this.options_soil_texture[i] = this.options_soil_texture1097;
            } else {
              this.options_soil_texture[i] = this.options_soil_texture1187;
            }
            if (this.sitesoil.sites[i].soil_classification_id === '1081') {
              this.options_soil_type[i] = this.options_soil_type_wrb;
              this.options_soil_specifiers[i] = this.options_soil_specifiers_start;
            } else if (this.sitesoil.sites[i].soil_classification_id === '1082') {
              this.options_soil_type[i] = this.options_soil_type_usda;
              this.options_soil_specifiers[i] = [];
            } else if (this.sitesoil.sites[i].soil_classification_id === '10840') {
              this.options_soil_type[i] = this.options_soil_type_fao;
              this.options_soil_specifiers[i] = [];
            } else {
              this.options_soil_type[i] = this.options_soil_type_ger;
              this.options_soil_specifiers[i] = [];
            }
          }
        } else {
          this.options_soil_texture[0] = this.options_soil_texture1097;
          this.options_soil_type[0] = this.options_soil_type_wrb;
          this.options_soil_qualifiers[0] = [];
          this.cdRef.detectChanges();
        }
        this.options_properties = this.convertObjToTreeNodes(result[1].children);
        this.options_properties_original = result[1];
        this.options_landuse = this.convertObjToTreeNodes(result[2].children);

        this.unitsNew = result[18];

        this.bulk_density_items = this.unitsNew.filter(unit => unit.id === 4);
        this.soil_organic_c_items = this.unitsNew.filter(unit => unit.id === 0 || unit.id === 1);
        this.caco3_items = this.unitsNew.filter(unit => unit.id === 0);

        this.options_management_practice_loaded = this.convertObjToTreeNodes(result[3].children);
        this.initGenKey();
        this.options_management_practice = this.options_management_practice_loaded.slice();
        this.options_unformatted.push(result[1].children);
        this.options_unformatted.push(result[2].children);
        this.options_unformatted.push(result[3].children);
        this.options_unformatted.push(result[18].children);
        this.siteservice.getAllCropsByUser().subscribe(results => {
          if (results) {
            for (let i = 0; i < results.length; i++) {
              this.options_crops_user.unshift(new TreeNode(results[i]));
            }
          }
          this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
            if (resultsgen) {
              for (let i = 0; i < resultsgen.length; i++) {
                if (resultsgen[i].category === 'Management') {
                  this.options_genkeys_management.push(new TreeNode(resultsgen[i]));
                } else if (resultsgen[i].category === 'Land Use') {
                  this.options_genkeys_landuse.push(new TreeNode(resultsgen[i]));
                }
                this.options_genkeys.push(new TreeNode(resultsgen[i]));
              }
            }
              const usergenerated = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000001',
                  'text': 'Crop Rotation',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_crops_user
                }, {
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [{
                    'value': '1000004',
                    'text': 'Management',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_management
                  },
                  {
                    'value': '1000005',
                    'text': 'Land Use',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_landuse
                  }]
                }]
              };
              const landuse = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': this.options_genkeys_landuse
              };
              this.rs.getOneBySoildoc(this.soildoc._id).subscribe(relationship => {
                this.relationship = relationship;
              });
              this.options_management_practice.unshift(new TreeNode(usergenerated));
              this.options_landuse.unshift(new TreeNode(landuse));
              this.isLoading = false;
              timer(1).subscribe(val => {
                this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));
                this.modalSave = new bootstrap.Modal(document.getElementById('modalSave'));
                this.modalCropgen = new bootstrap.Modal(document.getElementById('modalCropgen'));
                this.modalDeleteSite = new bootstrap.Modal(document.getElementById('modalDeleteSite'));
                this.modalDeleteLayer = new bootstrap.Modal(document.getElementById('modalDeleteLayer'));
                this.modalGenerateKeyword = new bootstrap.Modal(document.getElementById('modalGenerateKeyword'));
              });
          });
        });
        this.options_crops.push(new TreeNode(this.findByProperty(result[3], 'value', 'Main crop')));
        this.options_crops.push(new TreeNode(this.findByProperty(result[3], 'value', '529')));
        this.options_crops.push(new TreeNode(this.findByProperty(result[3], 'value', '600')));
        this.options_crops.push(new TreeNode(this.findByProperty(result[3], 'value', '530')));
        this.options_crops.push(new TreeNode(this.findByProperty(result[3], 'value', '869')));
        this.resetTypeArrayAddProperties();
      }
    });
  }

  convertObjToTreeNodes(arr: []) {
    const treeViewObj = [];
    for (let i = 0; i < arr.length; i++) {
      treeViewObj.push(new TreeNode(arr[i]));
    }
    return treeViewObj;
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  onValueChangeClassification(obj: any, siteIndex: number) {
    if (obj.value) {
      if (obj.value === '1081' || obj.value === '1082') {
        this.options_soil_texture[siteIndex] = this.options_soil_texture1097;
      } else {
        this.options_soil_texture[siteIndex] = this.options_soil_texture1187;
      }
      if (obj.value === '1081') {
        this.options_soil_type[siteIndex] = this.options_soil_type_wrb;
        this.options_soil_specifiers[siteIndex] = this.options_soil_specifiers_start;
      } else if (obj.value === '1082') {
        this.options_soil_type[siteIndex] = this.options_soil_type_usda;
        this.options_soil_specifiers[siteIndex] = [];
      } else if (obj.value === '10840') {
        this.options_soil_type[siteIndex] = this.options_soil_type_fao;
        this.options_soil_specifiers[siteIndex] = [];
      } else {
        this.options_soil_type[siteIndex] = this.options_soil_type_ger;
        this.options_soil_qualifiers[siteIndex] = [];
        this.options_soil_specifiers[siteIndex] = [];
        this.sites.controls[siteIndex]['controls']['qualifier'].reset();
        this.sites.controls[siteIndex]['controls']['qualifier'].clear();
      }
    } else {
      this.options_soil_type[siteIndex] = this.options_soil_type_ger;
      this.options_soil_qualifiers[siteIndex] = [];
      this.options_soil_specifiers[siteIndex] = [];
      this.sites.controls[siteIndex]['controls']['qualifier'].reset();
      this.sites.controls[siteIndex]['controls']['qualifier'].clear();
    }
    this.cdRef.detectChanges();
  }

  onValueChangeType(obj: any, siteIndex: number) {
    this.options_soil_qualifiers[siteIndex] = [];
    if (obj.value && this.sitesoilForm.value.sites[siteIndex].soil_classification_id !== '') {
      if (this.sitesoilForm.value.sites[siteIndex].soil_classification_id === '1081') {
        const qualifierArr = this.qualifierWRBLink.filter(qualifier => qualifier.value === obj.value);
        const qualifierItemsPrincipal = [];
        const qualifierItemsSupplementary = [];
        for (let i = 0; i < qualifierArr[0].supplementary_qualifiers.length; i++) {
          qualifierItemsSupplementary.push(new TreeNode({
            'value': qualifierArr[0].supplementary_qualifiers[i],
            'text': qualifierArr[0].supplementary_qualifiers[i],
            'collapsed': true,
            'checked': false,
          }));
        }
        for (let i = 0; i < qualifierArr[0].principal_qualifiers.length; i++) {
          qualifierItemsPrincipal.push(new TreeNode({
            'value': qualifierArr[0].principal_qualifiers[i],
            'text': qualifierArr[0].principal_qualifiers[i],
            'collapsed': true,
            'checked': false,
          }));
        }
        const principalTreeview = {
          'value': 'Principal Qualifiers',
          'text': 'Principal Qualifiers',
          'collapsed': true,
          'checked': false,
          'children': qualifierItemsPrincipal
        };
        const supplementaryTreeview = {
          'value': 'Supplementary Qualifiers',
          'text': 'Supplementary Qualifiers',
          'collapsed': true,
          'checked': false,
          'children': qualifierItemsSupplementary
        };
        if (qualifierItemsPrincipal.length > 0) {
          this.options_soil_qualifiers[siteIndex].push(new TreeNode(principalTreeview));
        }
        if (qualifierItemsSupplementary.length > 0) {
          this.options_soil_qualifiers[siteIndex].push(new TreeNode(supplementaryTreeview));
        }
      } else if (this.sitesoilForm.value.sites[siteIndex].soil_classification_id === '1082') {
        const qualifierArr = this.qualifierUSDALink.filter(qualifier => qualifier.greatgroup.includes(obj.value));
        const qualifierItems = [];
        for (let i = 0; i < qualifierArr.length; i++) {
          qualifierItems.push(new TreeNode({
            'value': qualifierArr[i].value,
            'text': qualifierArr[i].value,
            'collapsed': true,
            'checked': false,
          }));
        }
        const adjectiveTreeview = {
          'value': 'Adjectives',
          'text': 'Adjectives',
          'collapsed': true,
          'checked': false,
          'children': qualifierItems
        };
        if (qualifierItems.length > 0) {
          this.options_soil_qualifiers[siteIndex].push(new TreeNode(adjectiveTreeview));
        }
        this.options_soil_qualifiers[siteIndex].push(this.qualifierUSDAnoLink);
      } else if (this.sitesoilForm.value.sites[siteIndex].soil_classification_id === '10840') {
        this.options_soil_qualifiers[siteIndex].push(this.qualifierFAOnoLink);
      } else {
        this.options_soil_qualifiers[siteIndex] = [];
        this.sites.controls[siteIndex]['controls']['qualifier'].reset();
        this.sites.controls[siteIndex]['controls']['qualifier'].clear();
      }
    }
    this.cdRef.detectChanges();
  }

  onValueChangeAdditionalProperty(node: TreeNode, siteIndex: number, layerIndex: number, addpropIndex: number) {
    this.actualizeTypeArrayAddProperties(siteIndex, layerIndex, addpropIndex);
    if (node.value !== undefined) {
      this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]
      ['controls']['additional_properties']['controls'][addpropIndex]['controls']['name'].setValue(node.text);
    } else {
      this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]
      ['controls']['additional_properties']['controls'][addpropIndex]['controls']['name'].setValue('');
    }
  }

  onValueChangeLandUse(obj: any, siteIndex: number) {
    this.landuseArray[siteIndex] = this.valuesLanduseCropGen.includes(obj.value);
    if (obj.value !== undefined) {
      this.sites.controls[siteIndex]['controls']['landuse']['controls']['land_use'].setValue(obj.text);
    } else {
      this.sites.controls[siteIndex]['controls']['landuse']['controls']['land_use'].setValue('');
    }
  }

  onValueChangeManPractice(node: TreeNode, siteIndex: number, manpracticeIndex: number) {
    if (node.value !== undefined) {
      this.sites.controls[siteIndex]['controls']['landuse']['controls']['management_practice']
      ['controls'][manpracticeIndex]['controls']['name'].setValue(node.text);
    } else {
      this.sites.controls[siteIndex]['controls']['landuse']['controls']['management_practice']
      ['controls'][manpracticeIndex]['controls']['name'].setValue('');
    }
  }

  addSite() {
    this.sites.push(
      this.fb.group({
        name: this.fb.control('Site' + (this.sites.length + 1), [ Validators.required ]),
        soil_classification_id: this.fb.control('', []),
        soil_type: this.fb.control('', []),
        qualifier: this.fb.array([
          this.fb.group({
            name: this.fb.control('', [Validators.required]),
            specifier: this.fb.control('', [ ]),
          })
        ]),
        layer: this.fb.array([
          this.fb.group({
            name_layer: this.fb.control('Layer1', [ Validators.required ]),
            soil_texture: this.fb.control('', []),
            depth_from: this.fb.control('', [ SitesoilValidators.onlyNumber01000DOT2 ]),
            depth_to: this.fb.control('', [ SitesoilValidators.onlyNumber01000DOT2 ]),
            sand_content: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
            silt_content: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
            clay_content: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
            pH: this.fb.control('', [ SitesoilValidators.onlyNumber014DOT2 ]),
            pH_type: this.fb.control('', [ ]),
            bulk_density: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            bulk_density_unit: this.fb.control('', []),
            bulk_density_default: this.fb.control('', []),
            bulk_density_factor: this.fb.control('', []),
            organic_c_type: this.fb.control('', []),
            organic_c: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            organic_c_unit: this.fb.control('', []),
            organic_c_default: this.fb.control('', []),
            organic_c_factor: this.fb.control('', []),
            caco3: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            caco3_unit: this.fb.control('', []),
            caco3_default: this.fb.control('', []),
            caco3_factor: this.fb.control('', []),
            additional_properties: this.fb.array([])
          }, {
            validators: [ SitesoilValidators.fromToDepth, SitesoilValidators.percentages100 ]
          })
        ]),
        location: this.fb.group({
          search_field: this.fb.control('', []),
          latitude: this.fb.control('', [ SitesoilValidators.isLatitudeFormat ]),
          longitude: this.fb.control('', [ SitesoilValidators.isLongitudeFormat ]),
          display_name: this.fb.control({value: '', disabled: true}, [])
        }),
        climate: this.fb.group({
          // eslint-disable-next-line max-len
          mean_annualair_temperature: this.fb.control('', [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
          // eslint-disable-next-line max-len
          annual_precipitation_sum: this.fb.control('', [ SitesoilValidators.onlyNumber030000DOT2 ]),
          climate_zone: this.fb.control('', [ ]),
          min_temperature: this.fb.control('', [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
          max_temperature: this.fb.control('', [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
        }),
        landuse: this.fb.group({
          land_use: this.fb.control('', [ ]),
          land_use_id: this.fb.control('', [ ]),
          management_practice: this.fb.array([])
        })
      })
    );
    this.activeSite = this.sites.length - 1;
    this.activeLayer = 0;
    timer(1).subscribe(val => {
      this.sitesoilForm.markAsDirty();
    });
    this.updateErrorMessages();
  }

  removeSite(siteIndex: number) {
    this.siteDeleteID = siteIndex;
    this.siteDelete = this.sitesoilForm.value.sites[siteIndex];
    this.modalDeleteSite.show();
  }

  deleteSite() {
    if (this.activeSite === this.siteDeleteID) {
      this.activeSite = 0;
    }
    this.sites.removeAt(this.siteDeleteID);
    timer(1).subscribe(val => {
      this.sitesoilForm.markAsDirty();
    });
    this.updateErrorMessages();
  }

  changeSite(siteIndex: number) {
    if (this.activeSite !== siteIndex) {
      this.activeSite = siteIndex;
    }
  }

  copySite(siteIndex: number, copyIndex: number) {
    if (!this.options_soil_qualifiers[siteIndex]) {
      this.options_soil_qualifiers[siteIndex] = [];
    }
    this.sites.controls[siteIndex]['controls']['layer'].reset();
    this.sites.controls[siteIndex]['controls']['layer'].clear();
    this.sites.controls[siteIndex]['controls']['landuse']['controls']['management_practice'].reset();
    this.sites.controls[siteIndex]['controls']['landuse']['controls']['management_practice'].clear();
    this.resetTypeArrayAddProperties();
    this.resetLanduseArray();
    for (let i = 0; i < this.sites.controls[copyIndex]['controls']['qualifier'].length; i++) {
      this.addQualifier(siteIndex);
    }
    for (let i = 0; i < this.sites.controls[copyIndex]['controls']['layer'].length; i++) {
      this.addLayer(siteIndex);
      for (let j = 0; j < this.sites.controls[copyIndex]['controls']['layer']
      ['controls'][i]['controls']['additional_properties'].length; j++) {
        this.addAdditionalProperty(siteIndex, i);
      }
    }
    for (let i = 0; i < this.sites.controls[copyIndex]['controls']['landuse']['controls']['management_practice'].length; i++) {
      this.addManagementPractice(siteIndex);
    }
    this.sites.controls[siteIndex].patchValue(this.sitesoilForm.getRawValue().sites[copyIndex]);
    this.sites.controls[siteIndex]['controls']['name'].setValue('Site' + (siteIndex + 1));
    updateTreeValidity(this.sites.controls[siteIndex] as UntypedFormGroup);
  }

  addQualifier(siteIndex: number, layerIndex?: number) {
    this.sites.controls[siteIndex]['controls']['qualifier'].push(
      this.fb.group({
        name: this.fb.control('', [Validators.required]),
        specifier: this.fb.control('', [])
      }));
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeQualifier(siteIndex: number, qualifierIndex: number) {
    this.sites.controls[siteIndex]['controls']['qualifier'].removeAt(qualifierIndex);
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  resetSpecifier(siteIndex: number, qualifierIndex: number) {
    this.sites.controls[siteIndex]['controls']['qualifier']['controls'][qualifierIndex]['controls']['specifier'].setValue('');
    this.sitesoilForm.markAsDirty();
  }

  resetClassification(siteIndex: number) {
    this.sites.controls[siteIndex]['controls']['soil_classification_id'].setValue('');
    this.options_soil_type[siteIndex] = this.options_soil_type_ger;
    this.sites.controls[siteIndex]['controls']['soil_type'].setValue('');
    this.options_soil_qualifiers[siteIndex] = [];
    this.options_soil_specifiers[siteIndex] = [];
    this.sites.controls[siteIndex]['controls']['qualifier'].clear();
    this.sitesoilForm.markAsDirty();
  }

  resetTexture(siteIndex: number, layerIndex: number) {
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('');
    this.sitesoilForm.markAsDirty();
  }

  resetDisplayName(siteIndex: number) {
    this.sites.controls[siteIndex]['controls']['location']['controls']['display_name'].setValue('');
    this.sitesoilForm.markAsDirty();
  }

  resetClimateZone(siteIndex: number) {
    this.sites.controls[siteIndex]['controls']['climate']['controls']['climate_zone'].setValue('');
    this.sitesoilForm.markAsDirty();
  }

  resetLanduse(siteIndex: number) {
    this.sites.controls[siteIndex]['controls']['landuse']['controls']['land_use_id'].setValue('');
    this.sitesoilForm.markAsDirty();
  }

  addLayer(siteIndex: number, layerIndex?: number) {
    this.sites.controls[siteIndex]['controls']['layer'].push(
      this.fb.group({
        // eslint-disable-next-line max-len
        name_layer: this.fb.control('Layer' + (this.sites.controls[siteIndex]['controls']['layer'].length + 1) , [ Validators.required ]),
        soil_texture: this.fb.control('', []),
        depth_from: this.fb.control('', [ SitesoilValidators.onlyNumber01000DOT2 ]),
        depth_to: this.fb.control('', [ SitesoilValidators.onlyNumber01000DOT2 ]),
        sand_content: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        silt_content: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        clay_content: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        pH: this.fb.control('', [ SitesoilValidators.onlyNumber014DOT2 ]),
        pH_type: this.fb.control('', [ ]),
        bulk_density: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        bulk_density_unit: this.fb.control('', []),
        bulk_density_default: this.fb.control('', []),
        bulk_density_factor: this.fb.control('', []),
        organic_c_type: this.fb.control('', []),
        organic_c: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        organic_c_unit: this.fb.control('', []),
        organic_c_default: this.fb.control('', []),
        organic_c_factor: this.fb.control('', []),
        caco3: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        caco3_unit: this.fb.control('', []),
        caco3_default: this.fb.control('', []),
        caco3_factor: this.fb.control('', []),
        additional_properties: this.fb.array([])
      }, {
        validators: [ SitesoilValidators.fromToDepth, SitesoilValidators.percentages100 ]
      }));
    this.activeLayer = this.sites.controls[siteIndex]['controls']['layer'].length - 1;
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeLayer(siteIndex: number, layerIndex: number) {
    this.siteDeleteID = siteIndex;
    this.layerDeleteID = layerIndex;
    this.layerDelete = this.sitesoilForm.value.sites[siteIndex].layer[layerIndex];
    this.modalDeleteLayer.show();
  }

  deleteLayer() {
    if (this.activeLayer === this.layerDeleteID) {
      this.activeLayer = 0;
    }
    this.sites.controls[this.siteDeleteID]['controls']['layer'].removeAt(this.layerDeleteID);
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  changeLayer(layerIndex: number) {
    if (this.activeLayer !== layerIndex) {
      this.activeLayer = layerIndex;
    }
  }

  addAdditionalProperty(siteIndex: number, layerIndex: number) {

    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['additional_properties'].push(
    this.fb.group({
      name: this.fb.control('', [ ]),
      id: this.fb.control('', [ ]),
      type: this.fb.control('', []),
      value: this.fb.control('', [SitesoilValidators.onlyNumberNegativeDOT5MAX15]),
      unit: this.fb.control('', []),
      unit_default: this.fb.control('', []),
      factor: this.fb.control('', [])
    }));
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeAdditionalProperty(siteIndex: number, layerIndex: number, addPropIndex: number) {
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['additional_properties'].removeAt(addPropIndex);
    this.resetTypeArrayAddPropertiesSpecific(siteIndex, layerIndex);
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  addManagementPractice(siteIndex: number, name?: string) {
    this.sites.controls[siteIndex]['controls']['landuse']['controls']['management_practice'].push(
      this.fb.group({
        id: this.fb.control(name ? name : '', [ ]),
        name: this.fb.control('', [ ])
      }));
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeManagementPractice(siteIndex: number, index: number) {
    this.sites.controls[siteIndex]['controls']['landuse']['controls']['management_practice'].removeAt(index);
    this.sitesoilForm.markAsDirty();
    this.updateErrorMessages();
  }

  initSitesoil() {
    this.sites = this.fb.array(
      this.sitesoil.sites.map(
        s => this.fb.group({
          name: this.fb.control(s.name, [ Validators.required ]),
          soil_classification_id: this.fb.control(s.soil_classification_id),
          soil_type: this.fb.control(s.soil_type, [ SitesoilValidators.onlyCharacterSpace, Validators.maxLength(50)]),
          qualifier: this.fb.array(s.qualifier.map(
            q => this.fb.group({
              name: this.fb.control(q.name, [Validators.required]),
              specifier: this.fb.control(q.specifier, [])
            })
          )),
          layer: this.fb.array(s.layer.map(
            l => this.fb.group({
              name_layer: this.fb.control(l.name_layer, [ Validators.required ]),
              soil_texture: this.fb.control(l.soil_texture, []),
              depth_from: this.fb.control(l.depth_from, [ SitesoilValidators.onlyNumber01000DOT2 ]),
              depth_to: this.fb.control(l.depth_to, [ SitesoilValidators.onlyNumber01000DOT2 ]),
              sand_content: this.fb.control(l.sand_content, [ SitesoilValidators.onlyNumber0100DOT2 ]),
              silt_content: this.fb.control(l.silt_content, [ SitesoilValidators.onlyNumber0100DOT2 ]),
              clay_content: this.fb.control(l.clay_content, [ SitesoilValidators.onlyNumber0100DOT2 ]),
              pH: this.fb.control(l.pH, [ SitesoilValidators.onlyNumber014DOT2 ]),
              pH_type: this.fb.control(l.pH_type, [ ]),
              bulk_density: this.fb.control(l.bulk_density, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
              bulk_density_unit: this.fb.control(l.bulk_density_unit, []),
              bulk_density_default: this.fb.control(l.bulk_density_default, []),
              bulk_density_factor: this.fb.control(l.bulk_density_factor, []),
              organic_c_type: this.fb.control(l.organic_c_type, []),
              organic_c: this.fb.control(l.organic_c, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
              organic_c_unit: this.fb.control(l.organic_c_unit, []),
              organic_c_default: this.fb.control(l.organic_c_default, []),
              organic_c_factor: this.fb.control(l.organic_c_factor, []),
              caco3: this.fb.control(l.caco3, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
              caco3_unit: this.fb.control(l.caco3_unit, []),
              caco3_default: this.fb.control(l.caco3_default, []),
              caco3_factor: this.fb.control(l.caco3_factor, []),
              additional_properties: this.fb.array(l.additional_properties.map(
                ad => this.fb.group({
                  name: this.fb.control(ad.name, []),
                  id: this.fb.control(ad.id, [Validators.required]),
                  type: this.fb.control(ad.type, []),
                  value: this.fb.control(ad.value, [ SitesoilValidators.onlyNumberNegativeDOT5MAX15 ]),
                  unit: this.fb.control(ad.unit, []),
                  factor: this.fb.control(ad.factor, []),
                  unit_default: this.fb.control(ad.unit_default, [])
                })
              ))
            }, {
              validators: [ SitesoilValidators.fromToDepth, SitesoilValidators.percentages100 ]
            })
          )),
          location: this.fb.group({
            search_field: this.fb.control(s.location.search_field, []),
            latitude: this.fb.control(s.location.latitude, [ SitesoilValidators.isLatitudeFormat ]),
            longitude: this.fb.control(s.location.longitude, [ SitesoilValidators.isLongitudeFormat ]),
            display_name: this.fb.control({value: s.location.display_name, disabled: true}, [])
          }),
          climate: this.fb.group({
            // eslint-disable-next-line max-len
            mean_annualair_temperature: this.fb.control(s.climate.mean_annualair_temperature, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
            // eslint-disable-next-line max-len
            annual_precipitation_sum: this.fb.control(s.climate.annual_precipitation_sum, [ SitesoilValidators.onlyNumber030000DOT2 ]),
            climate_zone: this.fb.control(s.climate.climate_zone, [ ]),
            min_temperature: this.fb.control(s.climate.min_temperature, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
            max_temperature: this.fb.control(s.climate.max_temperature, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
          }, {
            validators: SitesoilValidators.minMaxtemp
          }),
          landuse: this.fb.group({
            land_use: this.fb.control(s.landuse.land_use, [ ]),
            land_use_id: this.fb.control(s.landuse.land_use_id, [ ]),
            management_practice: this.fb.array(s.landuse.management_practice.map(
              mp => this.fb.group({
                name: this.fb.control(mp.name, []),
                id: this.fb.control(mp.id, [Validators.required])
              })
            ))
          })
        })
      )
    );

    this.sitesoilForm = this.fb.group({
      sites: this.sites
    });

    this.resetAddPropertyErrorArray();
    this.resetLayerErrorArray();
    this.resetManPracticeErrorArray();
    this.resetSiteErrors();
    this.resetLanduseArray();
    this.sitesoilForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  initCropgen() {
    this.crops = null;
    this.cropgenForm = null;
    this.crops = this.fb.array([],
      [ Validators.required ], SitesoilValidators.croprotationAvailable(this.siteservice)
      );

    this.cropgenForm = this.fb.group({
      crops: this.crops
    });

    this.cropgenForm.statusChanges.subscribe(() => this.updateErrorMessagesCrop());
  }

  updateErrorMessagesCrop() {
    this.errorsCrop = {};
    for (const message of CropErrorMessages) {
      const control = this.cropgenForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors &&
        control.errors[message.forValidator]) {
        this.errorsCrop[message.forControl] = message.text;
      }
    }
  }

  updateErrorMessages() {
    this.resetSiteErrors();
    this.errors = {};
    this.resetLayerErrorArray();
    this.resetAddPropertyErrorArray();
    this.resetManPracticeErrorArray();
    for (const message of SiteSoilErrorMessages) {
      const control = this.sitesoilForm.get(message.forControl);
      if (!control) {
        for (let i = 0; i < this.sites.controls.length ; i++) {
          let controlsite = this.sites.controls[i].get(message.forControl);
          if (!controlsite) {
            controlsite = this.sites.controls[i]['controls']['location'].get(message.forControl);
            if (!controlsite) {
              controlsite = this.sites.controls[i]['controls']['climate'].get(message.forControl);
            }
          }
          if (!controlsite) {
            for (let j = 0; j < this.sites.controls[i]['controls']['layer']['controls'].length ; j++) {
              const controllayer = this.sites.controls[i]['controls']['layer']['controls'][j].get(message.forControl);
              if (!controllayer) {
                for (let k = 0; k < this.sites.controls[i]['controls']['layer']['controls'][j]['controls']['additional_properties']['controls'].length ; k++) {
                  // eslint-disable-next-line max-len
                  const controladdprop = this.sites.controls[i]['controls']['layer']['controls'][j]['controls']['additional_properties']['controls'][k].get(message.forControl);
                  if (controladdprop &&
                    controladdprop.dirty &&
                    controladdprop.invalid &&
                    !controladdprop['controls'] &&
                    controladdprop.errors[message.forValidator] &&
                    !this.addpropertyErrors[i][j][k][message.forControl]) {
                    this.addpropertyErrors[i][j][k][message.forControl] = message.text;
                  }
                }
              } else {
                if (controllayer &&
                  controllayer.invalid &&
                  !controllayer['controls'] &&
                  controllayer.errors[message.forValidator] &&
                  !this.layerErrors[i][j][message.forControl]) {
                  this.layerErrors[i][j][message.forControl] = message.text;
                }
              }
            }
          } else {
            if (controlsite &&
              controlsite.dirty &&
              controlsite.invalid &&
              !controlsite['controls'] &&
              controlsite.errors[message.forValidator] &&
              !this.siteErrors[i][message.forControl]) {
              this.siteErrors[i][message.forControl] = message.text;
            }
          }

        }
      } else {
          if (control &&
            control.dirty &&
            control.invalid &&
            !control['controls'] &&
            control.errors[message.forValidator] &&
            !this.errors[message.forControl]) {
            this.errors[message.forControl] = message.text;
        }
      }
    }
  }

  resetSiteErrors() {
    this.siteErrors = [{}];
    for (let i = 0; i < this.sites.controls.length ; i++) {
      this.siteErrors[i] = {name: '', soil_type: '', latitude: '', longitude: '', coordinate_system: '',
      country: '', state: '', county: '', city: '', mean_annualair_temperature: '', annual_precipitation_sum: '',
      climate_zone: '', min_temperature: '', max_temperature: ''
    };
    }
  }

  resetLayerErrorArray() {
    this.layerErrors = [[{}]];
    for (let i = 0; i < this.sites.controls.length ; i++) {
      this.layerErrors[i] = [{}];
      for (let j = 0; j < this.sites.controls[i]['controls']['layer']['controls'].length ; j++) {
        this.layerErrors[i][j] = {name_layer: '', soil_texture: '',
        depth_from: '', depth_to: '', sand_content: '', silt_content: '', clay_content: '',
        pH: '', bulk_density: '', bulk_density_unit: '', organic_c_type: '',
        organic_c: '', organic_c_unit: '', caco3: '', caco3_unit: ''};
      }
    }
  }

  resetAddPropertyErrorArray() {
    this.addpropertyErrors = [[[{}]]];
    for (let i = 0; i < this.sites.controls.length ; i++) {
      this.addpropertyErrors[i] = [[{}]];
      for (let j = 0; j < this.sites.controls[i]['controls']['layer']['controls'].length ; j++) {
        this.addpropertyErrors[i][j] = [{}];
        // eslint-disable-next-line max-len
        for (let k = 0; k < this.sites.controls[i]['controls']['layer']['controls'][j]['controls']['additional_properties']['controls'].length ; k++) {
          this.addpropertyErrors[i][j][k] = {name: '', type: '',
          value: '', unit: ''};
        }
      }
    }
  }

  resetManPracticeErrorArray() {
    this.manPracticeErrors = [[{}]];
    for (let i = 0; i < this.sites.controls.length ; i++) {
      this.manPracticeErrors[i] = [{}];
      for (let j = 0; j < this.sites.controls[i]['controls']['landuse']['controls']['management_practice']['controls'].length ; j++) {
        this.manPracticeErrors[i][j] = {name: ''};
      }
    }
  }

  resetTypeArrayAddProperties() {
    const arrTypes = [[[]]];
    for (let i = 0; i < this.sites.controls.length ; i++) {
      if (!this.additional_properties_items[i]) {
        this.additional_properties_items[i] = [[]];
      }
      arrTypes[i] = [[]];
      for (let j = 0; j < this.sites.controls[i]['controls']['layer']['controls'].length ; j++) {
        if (!this.additional_properties_items[i][j]) {
          this.additional_properties_items[i][j] = [];
        }
        arrTypes[i][j] = [];
        // eslint-disable-next-line max-len
        for (let k = 0; k < this.sites.controls[i]['controls']['layer']['controls'][j]['controls']['additional_properties']['controls'].length ; k++) {
          this.actualizeTypeArrayAddProperties(i, j, k);
        }
      }
    }
  }

  resetTypeArrayAddPropertiesSpecific(siteIndex: number, layerIndex: number) {
    // eslint-disable-next-line max-len
    for (let k = 0; k < this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'].length ; k++) {
      this.actualizeTypeArrayAddProperties(siteIndex, layerIndex, k);
    }
  }

  actualizeTypeArrayAddProperties(siteIndex: number, layerIndex: number, addpropIndex: number) {
    if (!this.additional_properties_items[siteIndex]) {
      this.additional_properties_items[siteIndex] = [];
    }
    if (!this.additional_properties_items[siteIndex][layerIndex]) {
      this.additional_properties_items[siteIndex][layerIndex] = [];
    }
    if (!this.additional_properties_items[siteIndex][layerIndex][addpropIndex]) {
      this.additional_properties_items[siteIndex][layerIndex][addpropIndex] = {};
    }

    const propitem = this.findByProperty(this.options_properties_original, 'id',
    this.sitesoilForm.value.sites[siteIndex].layer[layerIndex].additional_properties[addpropIndex].id);
    if (propitem) {
      const unitIds = propitem.unitsId;
      const unitItems = this.unitsNew.filter(unit => propitem.unitsId.includes(unit.id));
      const types = [];

      for (const unit of unitItems) {
        if (unit.type) {
          types.push(unit.type);
        }
      }

      if ((unitIds.length === 0)  || (this.additional_properties_items[siteIndex][layerIndex][addpropIndex].units &&
      this.additional_properties_items[siteIndex][layerIndex][addpropIndex].units.length > 0 &&
      !unitItems.some(unit => unit.units.some(singleunit =>
        singleunit.value === this.sitesoilForm.value.sites[siteIndex].layer[layerIndex].additional_properties[addpropIndex].unit)))) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
          ['additional_properties']['controls'][addpropIndex]['controls']['unit'].setValue('');
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
          ['additional_properties']['controls'][addpropIndex]['controls']['factor'].setValue('');
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
          ['additional_properties']['controls'][addpropIndex]['controls']['unit_default'].setValue('');
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
          ['additional_properties']['controls'][addpropIndex]['controls']['type'].setValue('');
      }
      this.additional_properties_items[siteIndex][layerIndex][addpropIndex] = {
        units: unitIds,
        unitItems: unitItems,
        types: types
      };

      if (types.length === 0) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
          ['additional_properties']['controls'][addpropIndex]['controls']['type'].setValue('');
      }
    }
  }

  resetUnitFactor(siteIndex: number, layerIndex: number, element: string, secelement: string, thirelement: string) {
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls'][element].setValue(null);
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls'][secelement].setValue('');
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls'][thirelement].setValue('');
  }

  updateFactorByElement(unitForm: string, siteIndex: number, layerIndex: number, element: string, secelement: string) {
    const singleitem = this.unitsNew.find(item => item.units.some(unit => unit.value === unitForm));
    const singleunit = singleitem.units.find(unit => unit.value === unitForm);
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls'][element].setValue(
      singleunit && singleunit.factor ? singleunit.factor : null);
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls'][secelement].setValue(
      singleitem && singleitem.default ? singleitem.default : '');
  }

  updateFactorAddProp(unitForm: string, siteIndex: number, layerIndex: number, addpropIndex: number) {
    const singleitem = this.unitsNew.find(item => item.units.some(unit => unit.value === unitForm));
    const singleunit = singleitem.units.find(unit => unit.value === unitForm);
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
        ['additional_properties']['controls'][addpropIndex]['controls']['factor'].setValue(
      singleunit && singleunit.factor ? singleunit.factor : null);
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
    ['additional_properties']['controls'][addpropIndex]['controls']['unit_default'].setValue(
    singleitem && singleitem.default ? singleitem.default : '');
  }

  resetUnitFactorAddProp(siteIndex: number, layerIndex: number, addpropIndex: number) {
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
      ['additional_properties']['controls'][addpropIndex]['controls']['factor'].setValue(null);
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
      ['additional_properties']['controls'][addpropIndex]['controls']['unit_default'].setValue('');
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']
      ['additional_properties']['controls'][addpropIndex]['controls']['unit'].setValue('');
  }

  resetLanduseArray() {
    this.landuseArray = [];
    for (let i = 0; i < this.sites.length; i++) {
      this.landuseArray[i] = this.valuesLanduseCropGen.includes(this.sitesoilForm.value.sites[i].landuse.land_use_id);
    }
  }

  addCrop() {
    this.crops.push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
    this.cropgenForm.markAsDirty();
  }

  removeCrop(index: number) {
    this.crops.removeAt(index);
    this.cropgenForm.markAsDirty();
  }

  cropGenerator() {
    this.modalCropgen.show();
  }

  openModalSave() {
    this.modalSave.show();
  }

  updateCoords(siteIndex: number) {
    this.coordsLoading = true;
    if (this.sitesoilForm.value.sites[siteIndex].location.search_field && this.sitesoilForm.value.sites[siteIndex].location.search_field !== '') {
      this.siteservice.getLocationByString(this.sitesoilForm.value.sites[siteIndex].location.search_field)
    .subscribe(datas => {
      if (datas.length && datas.length === 1) {
        if (datas[0].lat && datas[0].lat !== '') {
          this.sites.controls[siteIndex]['controls']['location']['controls']['latitude'].setValue(datas[0].lat.substring(0, 8));
          this.sites.controls[siteIndex]['controls']['location']['controls']['latitude'].markAsDirty();
        } else {
          this.sites.controls[siteIndex]['controls']['location']['controls']['latitude'].setValue('');
        }
        if (datas[0].lon && datas[0].lon !== '') {
          this.sites.controls[siteIndex]['controls']['location']['controls']['longitude'].setValue(datas[0].lon.substring(0, 8));
          this.sites.controls[siteIndex]['controls']['location']['controls']['longitude'].markAsDirty();
        } else {
          this.sites.controls[siteIndex]['controls']['location']['controls']['longitude'].setValue('');
        }
        if (datas[0].display_name && datas[0].display_name !== '') {
          this.sites.controls[siteIndex]['controls']['location']['controls']['display_name'].setValue(datas[0].display_name);
        } else {
          this.sites.controls[siteIndex]['controls']['location']['controls']['display_name'].setValue('');
        }
      } else {
        this.sites.controls[siteIndex]['controls']['location']['controls']['latitude'].setValue('');
        this.sites.controls[siteIndex]['controls']['location']['controls']['longitude'].setValue('');
        this.sites.controls[siteIndex]['controls']['location']['controls']['display_name'].setValue('');
      }
      timer(1000).subscribe(val => {
        this.coordsLoading = false;
      });
    });
    }
  }

  completeContents(siteIndex: number, layerIndex: number) {
    if (this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['sand_content'].valid &&
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['silt_content'].valid &&
    this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['clay_content'].valid) {

      if ((this.sites.value[siteIndex].layer[layerIndex].sand_content !== '' && this.sites.value[siteIndex].layer[layerIndex].sand_content !== null) &&
      (this.sites.value[siteIndex].layer[layerIndex].silt_content !== '' && this.sites.value[siteIndex].layer[layerIndex].silt_content !== null) &&
      (this.sites.value[siteIndex].layer[layerIndex].clay_content === '' || this.sites.value[siteIndex].layer[layerIndex].clay_content === null)) {

        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['clay_content'].setValue(
          this.convertToFloat(100 - this.sites.value[siteIndex].layer[layerIndex].sand_content - this.sites.value[siteIndex].layer[layerIndex].silt_content));
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['clay_content'].markAsDirty();
        this.completeTexture(siteIndex, layerIndex);

      } else if ((this.sites.value[siteIndex].layer[layerIndex].sand_content !== '' && this.sites.value[siteIndex].layer[layerIndex].sand_content !== null) &&
      (this.sites.value[siteIndex].layer[layerIndex].silt_content === '' || this.sites.value[siteIndex].layer[layerIndex].silt_content === null) &&
      (this.sites.value[siteIndex].layer[layerIndex].clay_content !== '' && this.sites.value[siteIndex].layer[layerIndex].clay_content !== null)) {

        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['silt_content'].setValue(
          this.convertToFloat(100 - this.sites.value[siteIndex].layer[layerIndex].sand_content - this.sites.value[siteIndex].layer[layerIndex].clay_content));
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['silt_content'].markAsDirty();
        this.completeTexture(siteIndex, layerIndex);

      } else if ((this.sites.value[siteIndex].layer[layerIndex].sand_content === '' || this.sites.value[siteIndex].layer[layerIndex].sand_content === null) &&
      (this.sites.value[siteIndex].layer[layerIndex].silt_content !== '' && this.sites.value[siteIndex].layer[layerIndex].silt_content !== null) &&
      (this.sites.value[siteIndex].layer[layerIndex].clay_content !== '' && this.sites.value[siteIndex].layer[layerIndex].clay_content !== null)) {

        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['sand_content'].setValue(
          this.convertToFloat(100 - this.sites.value[siteIndex].layer[layerIndex].silt_content - this.sites.value[siteIndex].layer[layerIndex].clay_content));
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['sand_content'].markAsDirty();
        this.completeTexture(siteIndex, layerIndex);

      } else if ((this.sites.value[siteIndex].layer[layerIndex].sand_content !== '' && this.sites.value[siteIndex].layer[layerIndex].sand_content !== null) &&
      (this.sites.value[siteIndex].layer[layerIndex].silt_content !== '' && this.sites.value[siteIndex].layer[layerIndex].silt_content !== null) &&
      (this.sites.value[siteIndex].layer[layerIndex].clay_content !== '' && this.sites.value[siteIndex].layer[layerIndex].clay_content !== null)) {

        this.completeTexture(siteIndex, layerIndex);
      }
    }
  }

  completeTexture(siteIndex: number, layerIndex: number) {
    const sand = this.convertToFloat(this.sites.value[siteIndex].layer[layerIndex].sand_content);
    const silt = this.convertToFloat(this.sites.value[siteIndex].layer[layerIndex].silt_content);
    const clay = this.convertToFloat(this.sites.value[siteIndex].layer[layerIndex].clay_content);
    if (this.sitesoilForm.value.sites[siteIndex].soil_classification_id === '1081' ||
    this.sitesoilForm.value.sites[siteIndex].soil_classification_id === '1082') {
      /** usda wrb */
      if ((silt + 1.5 * clay) < 15) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Sand (unspecified, S)');
      } else if (((silt + 1.5 * clay) >= 15) && ((silt + 2 * clay) < 30)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Loamy sand (LS)');
      } else if ((clay >= 7 && clay < 20) && (sand > 52) && ((silt + 2 * clay) >= 30) || (clay < 7 && silt < 50 && (silt + 2 * clay) >= 30)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Sandy loam (SL)');
      } else if ((clay >= 7 && clay < 27) && (silt >= 28 && silt < 50) && (sand <= 52)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Loam (L)');
      } else if ((silt >= 50 && (clay >= 12 && clay < 27)) || ((silt >= 50 && silt < 80) && clay < 12)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Silt loam (SiL)');
      } else if (silt >= 80 && clay < 12) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Silt (Si)');
      } else if ((clay >= 20 && clay < 35) && (silt < 28) && (sand > 45)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Sandy clay loam (SCL)');
      } else if ((clay >= 27 && clay < 40) && (sand > 20 && sand <= 45)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Clay loam (CL)');
      } else if ((clay >= 27 && clay < 40) && (sand  <= 20)) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Silty clay loam (SiCL)');
      } else if (clay >= 35 && sand > 45) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Sandy clay (SC)');
      } else if (clay >= 40 && silt >= 40) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Silty clay (SiC)');
      } else if (clay >= 40 && sand <= 45 && silt < 40) {
        this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue('Clay (C)');
      }
    } else {
      /** german */
      for (let i = 0; i < this.texture_map_ger.length; i++) {
        if (this.checkIfBetween(sand, this.texture_map_ger[i].sand_min, this.texture_map_ger[i].sand_max) &&
          this.checkIfBetween(silt, this.texture_map_ger[i].silt_min, this.texture_map_ger[i].silt_max) &&
          this.checkIfBetween(clay, this.texture_map_ger[i].clay_min, this.texture_map_ger[i].clay_max)) {
          this.sites.controls[siteIndex]['controls']['layer']['controls'][layerIndex]['controls']['soil_texture'].setValue(this.texture_map_ger[i].name);
          break;
        }
      }
    }
  }

  submitFormSitesoil(redirect?: string) {
    if (this.sitesoilForm.valid) {
      this.isSaveLoading = true;
      const sitesoil = SitesoilFactory.fromObject(this.sitesoilForm.getRawValue());
      sitesoil.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          sitesoil._id = this.sitesoil._id;
          this.siteservice.update(sitesoil)
            .subscribe(
                datas => {
                  this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.sitesoil = datas;
                  this.successSave = true;
                  this.initSitesoil();
                  window.scrollTo(0, 0);
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.siteservice.create(sitesoil)
          .subscribe(
              datas => {
                this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.sitesoil = datas;
                  this.initSitesoil();
                  this.isEdit = true;
                  this.successSave = true;
                  window.scrollTo(0, 0);
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  submitFormCropgen() {
    if (this.cropgenForm.valid) {
      const cropgen = CropGenFactory.fromObject(this.cropgenForm.value);
      this.siteservice.createCrop(cropgen)
          .subscribe(
              datas => {
                  this.failSaveCrop = false;
                  this.modalCropgen.hide();
                  this.options_management_practice[0].children[0].children.unshift(new TreeNode(datas));
                  this.options_management_practice = this.options_management_practice.slice();
                  this.addManagementPractice(this.activeSite, datas.value);
                  this.initCropgen();
              },
              error => this.failSaveCrop = true
          );
    }
  }

  initGenKey() {
    this.genkeyForm = this.fb.group({
      name: this.fb.control('', Validators.required, [ AdddriverValidators.genkeyAvailable(this.as),
      AdddriverValidators.keywordInTree(this.options_unformatted) ]),
      isNumeric: this.fb.control(false),
      category: this.fb.control('', Validators.required)
    });
    this.genkeyForm.statusChanges.subscribe(() => this.updateErrorMessagesGenkey());
  }

  updateErrorMessagesGenkey() {
    this.errorsGenkey = {};
    for (const message of GenkeyErrorMessages) {
      const control = this.genkeyForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errorsGenkey[message.forControl]) {
        this.errorsGenkey[message.forControl] = message.text;
      }
    }
  }

  genkeyGenerator() {
    this.modalGenerateKeyword.show();
  }

  submitFormGenkey() {
    if (this.genkeyForm.valid) {
      const genkey = GenKeyFactory.fromObject(this.genkeyForm.value);
      this.as.createGenKey(genkey)
          .subscribe(
              datas => {
                  this.failSaveGenKey = false;
                  this.modalGenerateKeyword.hide();
                  if (datas.category === 'Management') {
                    this.options_management_practice[0].children[1].children[0].children.unshift(new TreeNode(datas));
                  } else if (datas.category === 'Land Use') {
                    this.options_landuse[0].children.unshift(new TreeNode(datas));
                    this.options_management_practice[0].children[1].children[1].children.unshift(new TreeNode(datas));
                    this.options_landuse = this.options_landuse.slice();
                  }
                  this.options_management_practice = this.options_management_practice.slice();
                  this.addManagementPractice(this.activeSite, datas.value);
                  this.initGenKey();
              },
              error => this.failSaveGenKey = true
          );
    }
  }

  expandAll() {
    document.querySelectorAll('.collapsecustom').forEach(elem => {new bootstrap.Collapse(elem, {toggle: false}).show()});
  }

  checkifsaved(routerlink: string) {
    if (this.sitesoilForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalRedirect.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }

  checkIfBetween(value: number, min: number, max: number) {
    return (value >= min && value <= max) ? true : false;
  }

  convertToFloat(val) {
    return parseFloat(parseFloat(val).toFixed(2));
  }
}

/**
 * Re-calculates the value and validation status of the entire controls tree.
 */
function updateTreeValidity(group: UntypedFormGroup | UntypedFormArray): void {
  Object.keys(group.controls).forEach((key: string) => {
    const abstractControl = group.controls[key];

    if (abstractControl instanceof UntypedFormGroup || abstractControl instanceof UntypedFormArray) {
      updateTreeValidity(abstractControl);
    } else {
      abstractControl.updateValueAndValidity();
      abstractControl.markAsDirty();
    }
  });
}
