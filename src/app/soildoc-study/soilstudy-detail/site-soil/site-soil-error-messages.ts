export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const SiteSoilErrorMessages = [
  new ErrorMessage('name', 'required', 'SITESOIL.FAILFORM.REQUIRED' ),
  new ErrorMessage('soil_type', 'characterSpaceFormat', 'SITESOIL.FAILFORM.CHARACTERSPACE'),
  new ErrorMessage('soil_type', 'maxlength', 'SITESOIL.FAILFORM.MAXLENGTH50'),
  new ErrorMessage('name_layer', 'required', 'SITESOIL.FAILFORM.REQUIRED' ),
  new ErrorMessage('depth_from', 'numberFormat01000DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT01000DOT2'),
  new ErrorMessage('depth_from', 'notvalid', 'SITESOIL.FAILFORM.FROMTO'),
  new ErrorMessage('depth_to', 'numberFormat01000DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT01000DOT2'),
  new ErrorMessage('sand_content', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('sand_content', 'notvalid', 'SITESOIL.FAILFORM.NOTVALIDPERCENTAGE'),
  new ErrorMessage('silt_content', 'notvalid', 'SITESOIL.FAILFORM.NOTVALIDPERCENTAGE'),
  new ErrorMessage('clay_content', 'notvalid', 'SITESOIL.FAILFORM.NOTVALIDPERCENTAGE'),
  new ErrorMessage('silt_content', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('clay_content', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('pH', 'numberFormat014DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT014DOT2'),
  new ErrorMessage('bulk_density', 'numberFormatDOT5MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT5MAX15'),
  new ErrorMessage('organic_c', 'numberFormatDOT5MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT5MAX15'),
  new ErrorMessage('caco3', 'numberFormatDOT5MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT5MAX15'),
  new ErrorMessage('value', 'numberFormatNegativeDOT5MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATNEGATIVEDOT5MAX15'),
  new ErrorMessage('latitude', 'latitudeFormat', 'SITESOIL.FAILFORM.LATITUDEFORMAT'),
  new ErrorMessage('longitude', 'longitudeFormat', 'SITESOIL.FAILFORM.LONGITUDEFORMAT'),
  new ErrorMessage('mean_annualair_temperature', 'numberFormatNegative50DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT50NEGATIVEDOT2'),
  new ErrorMessage('annual_precipitation_sum', 'numberFormat030000DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT030000DOT2'),
  new ErrorMessage('climate_zone', 'maxlength', 'SITESOIL.FAILFORM.MAXLENGTH20'),
  new ErrorMessage('min_temperature', 'numberFormatNegative50DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT50NEGATIVEDOT2'),
  new ErrorMessage('min_temperature', 'notvalid', 'SITESOIL.FAILFORM.NOTVALIDMINMAXTEMP'),
  new ErrorMessage('max_temperature', 'numberFormatNegative50DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT50NEGATIVEDOT2'),
];
