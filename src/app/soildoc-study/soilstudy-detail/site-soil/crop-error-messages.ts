export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const CropErrorMessages = [
  new ErrorMessage('crops', 'rotationAvailable', 'SITESOIL.FAILFORM.ROTATIONAVAILABLE' )
];
