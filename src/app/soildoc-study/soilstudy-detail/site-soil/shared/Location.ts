export class Location {
  constructor(
    public search_field: string,
    public latitude: number,
    public longitude: number,
    public display_name: string
   ) {}
}
