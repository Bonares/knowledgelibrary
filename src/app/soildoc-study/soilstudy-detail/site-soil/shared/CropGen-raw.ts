import { Crop } from './Crop';

export interface CropGenRaw {
  _id: string;
  crops: Crop[];
  lastModifiedBy: string;
  value: string;
  displayText: string;
  text: string;
  collapsed: boolean;
  checked: boolean;
}
