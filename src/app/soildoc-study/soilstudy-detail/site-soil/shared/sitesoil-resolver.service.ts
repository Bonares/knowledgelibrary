import { Sitesoil } from './Sitesoil';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthGuard } from './../../../../shared/auth.guard';
import { SitesoilService } from './sitesoil.service';
@Injectable({
  providedIn: 'root'
})
export class SitesoilResolverService {

  constructor(private sitesoilservice: SitesoilService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Sitesoil> {
    if (this.ag.canActivateMethod()) {
    return this.sitesoilservice.getOneBySoildoc(route.params['id']);
    }
  }
}
