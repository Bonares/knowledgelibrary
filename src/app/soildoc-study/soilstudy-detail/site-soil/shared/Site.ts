import { Qualifier } from './Qualifier';
import { Landuse } from './Landuse';
import { Climate } from './Climate';
import { Location } from './Location';
import { Layer } from './Layer';
export class Site {
  constructor(
    public name: string,
    public soil_type: string,
    public soil_classification_id: string,
    public qualifier: Qualifier[],
    public layer: Layer[],
    public location: Location,
    public climate: Climate,
    public landuse: Landuse

   ) {}
}
