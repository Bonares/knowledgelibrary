import { CropGenFactory } from './CropGenFactory';
import { CropGenRaw } from './CropGen-raw';
import { CropGen } from './CropGen';
import { SitesoilRaw } from './Sitesoil-raw';
import { Sitesoil } from './Sitesoil';
import { AuthGuard } from './../../../../shared/auth.guard';
import { catchError, timeout, retry, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { SitesoilFactory } from './SitesoilFactory';

@Injectable({
  providedIn: 'root'
})
export class SitesoilService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Sitesoil> {
    return this.httpClient
      .get<SitesoilRaw>(`${this.api}/sitesoil/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawSitesoil => SitesoilFactory.fromObject(rawSitesoil),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Sitesoil> {
    return this.httpClient
      .get<SitesoilRaw>(`${this.api}/sitesoil/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawSitesoil => SitesoilFactory.fromObject(rawSitesoil),
        ),
        catchError(this.handleError)
      );
  }


  create(sitesoil: Sitesoil): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/sitesoil`, sitesoil)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(sitesoil: Sitesoil): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/sitesoil/${sitesoil._id}`, sitesoil)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }

  createCrop(crop: CropGen): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/crop`, crop)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  getAllCropsByUser(): Observable<Array<CropGen>> {
      return this.httpClient
        .get<CropGenRaw[]>(`${this.api}/crops`)
        .pipe(
          retry(3), // retry a failed request up to 3 times
          map(rawCrops => rawCrops
            .map(rawCrop => CropGenFactory.fromObject(rawCrop)),
          ),
          catchError(this.handleError)
        );
  }

  getLocationByString(location: string): Observable<any> {
    return this.httpClient
      .get(`https://nominatim.openstreetmap.org/search?q=${location}&format=json&limit=1&accept-language=en`)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
  }

  croprotationAvailable(rotation: string): Observable<Boolean> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
        .post<Boolean>(`${this.api}/crop/check`, {rotation: rotation})
        .pipe(
          catchError(this.handleError)
        );
    }
  }
}
