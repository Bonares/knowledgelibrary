export class AdditionalProperty {
  constructor(
    public name: string,
    public id: string,
    public type: string,
    public value: number,
    public unit: string,
    public unit_default: string,
    public factor: number
   ) {}
}
