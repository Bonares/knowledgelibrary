export class Climate {
  constructor(
    public mean_annualair_temperature: number,
    public annual_precipitation_sum: number,
    public climate_zone: string,
    public min_temperature: number,
    public max_temperature: number
   ) {}
}
