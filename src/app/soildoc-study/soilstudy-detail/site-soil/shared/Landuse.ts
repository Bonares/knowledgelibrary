import { ManagementPractice } from './ManagementPractice';
export class Landuse {
  constructor(
    public land_use: string,
    public land_use_id: string,
    public management_practice: ManagementPractice[]
   ) {}
}
