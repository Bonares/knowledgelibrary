import { Site } from './Site';
export interface SitesoilRaw {
  _id: string;
  sites: Site[];
  soildoc_id: string;
}
