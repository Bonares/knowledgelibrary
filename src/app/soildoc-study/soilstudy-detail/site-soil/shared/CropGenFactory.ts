import { CropGenRaw } from './CropGen-raw';
import { CropGen } from './CropGen';
export class CropGenFactory {

  static empty(): CropGen {
    return new CropGen('', [], '', '', '', '', true, false);
  }

  static fromObject(rawCropGen: CropGenRaw | any): CropGen {
      return new CropGen(
        rawCropGen._id,
        rawCropGen.crops,
        rawCropGen.lastModifiedBy,
        rawCropGen.value,
        rawCropGen.displayText,
        rawCropGen.text,
        rawCropGen.collapsed,
        rawCropGen.checked
      );
  }
  }
