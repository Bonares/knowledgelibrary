export class Qualifier {
  constructor(
    public specifier: string,
    public name: string
   ) {}
}
