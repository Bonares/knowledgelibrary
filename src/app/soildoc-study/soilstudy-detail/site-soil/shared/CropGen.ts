import { Crop } from './Crop';
export class CropGen {
  constructor(
    public _id: string,
    public crops: Crop[],
    public lastModifiedBy: string,
    public value: string,
    public displayText: string,
    public text: string,
    public collapsed: boolean,
    public checked: boolean
   ) {}
}
