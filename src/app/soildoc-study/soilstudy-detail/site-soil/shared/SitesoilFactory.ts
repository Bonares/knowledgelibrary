import { SitesoilRaw } from './Sitesoil-raw';
import { Sitesoil } from './Sitesoil';
import { Location } from './Location';
import { Climate } from './Climate';
import { Landuse } from './Landuse';
import { Layer } from './Layer';
import { Qualifier } from './Qualifier';
export class SitesoilFactory {

static empty(): Sitesoil {
  return new Sitesoil('', [{name: 'Site1', soil_type: '', soil_classification_id: null, qualifier: [new Qualifier('', '')],
   layer: [new Layer('Layer1', '', null, null, null, null, null, null, '', null, '', '', null, '', null, '', '', null, null, '', '', null, [])],
   location: new Location('', null, null, ''),
   climate: new Climate(null, null, '', null, null),
   landuse: new Landuse('', '', [])
  }], null
  );
}

static fromObject(rawSitesoil: SitesoilRaw | any): Sitesoil {
  if (rawSitesoil) {
    return new Sitesoil(
      rawSitesoil._id,
      rawSitesoil.sites,
      rawSitesoil.soildoc_id
    );
  } else {
    return null;
  }
}
}
