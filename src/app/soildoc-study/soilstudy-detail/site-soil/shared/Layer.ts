import { AdditionalProperty } from './AdditionalProperty';
export class Layer {
  constructor(
    public name_layer: string,
    public soil_texture: string,
    public depth_from: number,
    public depth_to: number,
    public sand_content: number,
    public silt_content: number,
    public clay_content: number,
    public pH: number,
    public pH_type: string,
    public bulk_density: number,
    public bulk_density_unit: string,
    public bulk_density_default: string,
    public bulk_density_factor: number,
    public organic_c_type: string,
    public organic_c: number,
    public organic_c_unit: string,
    public organic_c_default: string,
    public organic_c_factor: number,
    public caco3: number,
    public caco3_unit: string,
    public caco3_default: string,
    public caco3_factor: number,
    public additional_properties: AdditionalProperty[]
   ) {}
}
