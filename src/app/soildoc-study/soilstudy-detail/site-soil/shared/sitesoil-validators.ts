import { switchMap, map } from 'rxjs/operators';
import { Observable, timer, of } from 'rxjs';
import { SitesoilService } from './sitesoil.service';
import { UntypedFormControl, AbstractControl, UntypedFormArray } from '@angular/forms';
export class SitesoilValidators {

  static isNumber(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(\d)+$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat: {valid: false}
    };
  }

  static onlyCharacterSpace(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const stringPattern = /^([a-zA-Z ])+$/;
    return stringPattern.test(control.value) ? null : {
      characterSpaceFormat: {valid: false}
    };
  }

  static onlyNumber01000(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(1000|[1-9][0-9]?[0-9]?|0)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat01000: {valid: false}
    };
  }

  static onlyNumber0100DOT2(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(100|[1-9]?[0-9]|[1-9]?[0-9]\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat0100DOT2: {valid: false}
    };
  }

  static onlyNumber01000DOT2(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(([1-9][0-9]{0,2})|(0)(\.[0-9][0-9]?)?|1000)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat01000DOT2: {valid: false}
    };
  }

  static onlyNumber014DOT2(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(1[0-4]|[0-9]|1[0-3]\.[0-9][0-9]?|[0-9]\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat014DOT2: {valid: false}
    };
  }

  static onlyNumberDOT2MAX15(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(\d{1,15}|\d{1,15}\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormatDOT2MAX15: {valid: false}
    };
  }

  static onlyNumberDOT5MAX15(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(\d{1,15}|\d{1,15}\.[0-9][0-9]{0,4})$/;
    return numberPattern.test(control.value) ? null : {
      numberFormatDOT5MAX15: {valid: false}
    };
  }

  static onlyNumberNegativeDOT2MAX15(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(-?\d{1,15}|-?\d{1,15}\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormatNegativeDOT2MAX15: {valid: false}
    };
  }

  static onlyNumberNegativeDOT5MAX15(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(-?\d{1,15}|-?\d{1,15}\.[0-9][0-9]{0,4})$/;
    return numberPattern.test(control.value) ? null : {
      numberFormatNegativeDOT5MAX15: {valid: false}
    };
  }

  static onlyNumberNegative50DOT2(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(-?50|-?[1-4]?[0-9]|-?[1-4]?[0-9]\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormatNegative50DOT2: {valid: false}
    };
  }

  static onlyNumber030000DOT2(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(30000|[1-2][0-9]{0,4}|[1-9][0-9]{0,3}|[1-9][0-9]{0,3}\.[0-9][0-9]?|[1-2][0-9]{0,4}\.[0-9][0-9]?|0|0\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormat030000DOT2: {valid: false}
    };
  }

  static isLongitudeFormat(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const longitudePatternDecimal = /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
    const longitudePatternDegree = /^((([0-1]?[0-7]?\d)|(\d\d))(°|\s)[0-5]?\d('|\s)([0-5]?\d(\.\d{1,6})?([']{2}|\"){1})?|180(°|\s)0?0('|\s)(0?0([']{2}|\"){1})?)\s?[EeOoWw]$/;
    if (longitudePatternDecimal.test(control.value)) {
      return null;
    } else {
      if (longitudePatternDegree.test(control.value)) {
        return null;
      } else {
        return {
          longitudeFormat: {valid: false}
        };
      }
    }
  }

  static isLatitudeFormat(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const latitudePatternDecimal = /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
    const latitudePatternDegree = /^([0-8]?\d(°|\s)[0-5]?\d('|\s)([0-5]?\d(\.\d{1,6})?([']{2}|\"){1})?|90(°|\s)0?0('|\s)(0?0([']{2}|\"){1})?)\s?[NnSs]$/;
    if (latitudePatternDecimal.test(control.value)) {
      return null;
    } else {
      if (latitudePatternDegree.test(control.value)) {
        return null;
      } else {
        return {
          latitudeFormat: {valid: false}
        };
      }
    }
  }

  static percentages100(ac: AbstractControl): { [error: string]: any } {
    let percentage = 0;
    if ((ac.get('sand_content').valid || ac.get('sand_content').hasError('notvalid')) &&
    (ac.get('silt_content').valid || ac.get('silt_content').hasError('notvalid')) &&
    (ac.get('clay_content').valid || ac.get('clay_content').hasError('notvalid'))) {
      if (!isNaN(parseInt(ac.get('sand_content').value, 10))) {
        percentage += parseInt(ac.get('sand_content').value, 10);
      }
      if (!isNaN(parseInt(ac.get('silt_content').value, 10))) {
        percentage += parseInt(ac.get('silt_content').value, 10);
      }
      if (!isNaN(parseInt(ac.get('clay_content').value, 10))) {
        percentage += parseInt(ac.get('clay_content').value, 10);
      }
      if ( percentage > 100) {
        ac.get('sand_content').setErrors( {notvalid: true} );
        ac.get('silt_content').setErrors( {notvalid: true} );
        ac.get('clay_content').setErrors( {notvalid: true} );
        ac.get('sand_content').markAsDirty();
        ac.get('silt_content').markAsDirty();
        ac.get('clay_content').markAsDirty();
      } else {
        ac.get('sand_content').updateValueAndValidity({onlySelf: true});
        ac.get('silt_content').updateValueAndValidity({onlySelf: true});
        ac.get('clay_content').updateValueAndValidity({onlySelf: true});
        return null;
      }
    } else {
      ac.get('sand_content').updateValueAndValidity({onlySelf: true});
      ac.get('silt_content').updateValueAndValidity({onlySelf: true});
      ac.get('clay_content').updateValueAndValidity({onlySelf: true});
      return null;
    }
  }

  static minMaxtemp(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('max_temperature').valid && !ac.get('min_temperature').hasError('numberFormatNegative50DOT2')) {
      if ((!isNaN(parseFloat(ac.get('min_temperature').value))) && (!isNaN(parseFloat(ac.get('max_temperature').value)))) {
        if (parseFloat(ac.get('min_temperature').value) >= parseFloat(ac.get('max_temperature').value)) {
          ac.get('min_temperature').setErrors( {notvalid: true});
        } else {
          ac.get('min_temperature').setErrors( {notvalid: false});
          ac.get('min_temperature').updateValueAndValidity({onlySelf: true});
          return null;
        }
      } else {
          ac.get('min_temperature').setErrors( {notvalid: false});
          ac.get('min_temperature').updateValueAndValidity({onlySelf: true});
          return null;
      }
    } else {
      ac.get('min_temperature').updateValueAndValidity({onlySelf: true});
      return null;
    }
  }

  static fromToDepth(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('depth_to').valid && !ac.get('depth_from').hasError('numberFormat01000DOT2')) {
      if ((!isNaN(parseFloat(ac.get('depth_from').value))) && (!isNaN(parseFloat(ac.get('depth_to').value)))) {
        if (parseFloat(ac.get('depth_from').value) >= parseFloat(ac.get('depth_to').value)) {
          ac.get('depth_from').setErrors( {notvalid: true});
        } else {
          ac.get('depth_from').setErrors( {notvalid: false});
          ac.get('depth_from').updateValueAndValidity({onlySelf: true});
          return null;
        }
      } else {
          ac.get('depth_from').setErrors( {notvalid: false});
          ac.get('depth_from').updateValueAndValidity({onlySelf: true});
          return null;
      }
    } else {
      ac.get('depth_from').updateValueAndValidity({onlySelf: true});
      return null;
    }
  }

  static croprotationAvailable(ss: SitesoilService) {
    return function(control: UntypedFormArray): Observable<{
      [error: string]: any }> {
        if (control.value.length === 0) {
          return of(null);
        }
        let rotation = '';
        for (let i = 0; i < control.value.length; i++) {
          rotation += control.value[i].name;
          if (i !== control.value.length - 1) {
            rotation += ' - ';
          }
        }
        return ss.croprotationAvailable(rotation)
        .pipe(
          map(available =>
            available ? null : { rotationAvailable: { valid: false }})
        );
      };
  }
}
