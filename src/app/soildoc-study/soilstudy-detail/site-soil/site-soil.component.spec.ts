import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SiteSoilComponent } from './site-soil.component';

describe('SiteSoilComponent', () => {
  let component: SiteSoilComponent;
  let fixture: ComponentFixture<SiteSoilComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteSoilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteSoilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
