import { MetaService } from './../../../shared/meta.service';
import { timer } from 'rxjs';
import { AdddriverService } from './../add-driver/shared/adddriver.service';
import { GenKey } from './../add-driver/shared/GenKey';
import { Component, Inject, OnInit } from '@angular/core';
import { faCaretDown, faExclamationCircle, faSearch, faSync, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-view-genkey',
  templateUrl: './view-genkey.component.html',
  styleUrls: []
})
export class ViewGenkeyComponent implements OnInit {

  faExclamationCircle = faExclamationCircle;
  faSearch = faSearch;
  faSync = faSync;
  faTrashAlt = faTrashAlt;
  faCaretDown = faCaretDown;

  isLoading = true;
  errorLoading = true;
  failDelete = false;
  successDelete = false;
  errorAuthorize = false;

  genkeyCollection: GenKey[] = [];
  genkeyCollectionOriginal: GenKey[] = [];
  genkeyCount = 0;

  genkeyDelete: GenKey;

  searchValue = '';
  isSorted = false;
  sortTarget: any;

  activePage = 0;
  startPage = 0;
  endPage: number;
  pageSize = 10;
  maxPageButtons = 5;
  countPageButtonsArr: number[];
  startItem = 0;
  endItem: number;
  numPages: number;
  pageSizeOptions = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50];

  modalDelete: bootstrap.Modal;

  constructor(
    private as: AdddriverService,
    private metaService: MetaService
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('GENKEY.TITLEMETA', 'GENKEY.DESCRIPTIONMETA');
    this.as.getAllKeywordsGenAdmin().subscribe(result => {
      if (result.length > 0) {
        this.genkeyCollectionOriginal = result;
        this.genkeyCollection = this.genkeyCollectionOriginal.slice(0);
        this.updatePages();
      }
      this.errorLoading = false;
      this.errorAuthorize = false;
      this.isLoading = false;
      this.modalDelete = new bootstrap.Modal(document.getElementById('modalDelete'));
    },
    error => {
      if (error.status === 401) {
        this.errorAuthorize = true;
      } else {
        this.errorLoading = true;
      }
      this.isLoading = false;
    });
  }

  refreshGenkey() {
    this.genkeyCollection = [];
    this.genkeyCount = 0;
    this.isLoading = true;
    this.as.getAllKeywordsGenAdmin().subscribe(result => {
      if (result.length > 0) {
        this.genkeyCollectionOriginal = result;
        this.genkeyCollection = this.genkeyCollectionOriginal.slice(0);
        this.updatePages();
      }
      this.errorLoading = false;
      this.errorAuthorize = false;
      this.isLoading = false;
    },
    error => {
      if (error.status === 401) {
        this.errorAuthorize = true;
      } else {
        this.errorLoading = true;
      }
      this.isLoading = false;
    });
  }

  openModalDelete(genkey: GenKey) {
    this.genkeyDelete = genkey;
    this.modalDelete.show();
  }

  deleteOne(genkey: GenKey) {
    if (genkey._id) {
      this.as.removeOneKeywordGenkey(genkey._id)
          .subscribe(
              data => {
                  this.failDelete = false;
                  this.successDelete = true;
                  this.refreshGenkey();
                  window.scrollTo(0, 0);
                  const source = timer(5000);
                  source.subscribe(val => {
                    this.successDelete = false;
                  });
              },
              error => {
                this.failDelete = true;
                window.scrollTo(0, 0);
                const source = timer(5000);
                source.subscribe(val => {
                  this.failDelete = false;
                });
              }
          );
    }
  }

  updateSearch(text: string) {
    this.genkeyCollection = [];
    const collection = this.genkeyCollectionOriginal.slice(0);
    if (text === '') {
      this.genkeyCollection = collection.slice(0);
    } else {
      for (let i = 0; i < collection.length; i++) {
        if (collection[i].text.toLowerCase().includes(text.toLowerCase())) {
          this.genkeyCollection.push(collection[i]);
        }
      }
    }
    this.genkeyCount = this.genkeyCollection.length;
    this.activePage = 0;
    this.updatePages();
    this.startItem = 0;
    this.endItem = this.startItem + this.pageSize;
    this.startPage = 0;
    this.endPage = this.activePage + this.maxPageButtons;
  }

  sortGenkey(event: any) {
    if (event.target.classList.value === 'sort-icon-search fa fa-sort') {
      if (this.isSorted && this.sortTarget) {
        this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      }
      this.isSorted = true;
      this.sortTarget = event.target;
      event.target.classList = 'sort-icon-search fa fa-sort-up';
      this.sortOneIdentifier(1, -1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-up') {

      event.target.classList = 'sort-icon-search fa fa-sort-down';
      this.sortOneIdentifier(-1, 1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-down') {
      event.target.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
      this.sortOneIdentifier(-1, 1);
    }
  }

  sortOneIdentifier(first: number, second: number) {
    this.genkeyCollection = this.genkeyCollection.slice().sort(function(a, b) {
      if (new Date(a.createdDate).getTime() > new Date(b.createdDate).getTime()) {
        return first;
      }
      if (new Date(a.createdDate).getTime() < new Date(b.createdDate).getTime()) {
        return second;
      }
      return 0;
    });
  }

  updatePages() {
    this.genkeyCount = this.genkeyCollection.length;
    this.numPages = Math.ceil(this.genkeyCount / this.pageSize);
    this.updateCountPageButtonsArr();
    this.setPageFirst();
  }

  updatePageSize(size: string) {
    this.pageSize = parseInt(size, 10);
    this.updatePages();
  }

  setPageFirst() {
    this.activePage = 0;
    this.startItem = 0;
    this.endItem = this.startItem + this.pageSize;
    this.startPage = 0;
    this.endPage = this.activePage + this.maxPageButtons;
  }

  updateCountPageButtonsArr() {
    this.countPageButtonsArr =  Array(this.numPages).fill(0).map((x, i) => i);
  }

  updatePage(page: string) {
    this.activePage = parseInt(page, 10) - 1;
    this.updateItems();
  }

  changePage(count: number) {
    this.activePage += count;

    if (count === (1)) {
      if (!(this.endPage === this.countPageButtonsArr.length)) {
        this.endPage += count;
        if (this.countPageButtonsArr.length > this.maxPageButtons) {
          if ((this.endPage - this.startPage) > this.maxPageButtons) {
            this.startPage += count;
          }
        }
      }
    } else {
      if (!(this.startPage === 0)) {
        this.startPage += count;
        if (this.countPageButtonsArr.length > this.maxPageButtons) {
          if ((this.endPage - this.startPage) > this.maxPageButtons) {
            this.endPage += count;
          }
        }
      }
    }
    this.updateItems();
  }

  updateItems() {
    this.startItem = this.activePage * this.pageSize;
    this.endItem = this.activePage * this.pageSize + this.pageSize;
  }

  ConvertToInt(val) {
    return parseInt(val, 10);
  }

}
