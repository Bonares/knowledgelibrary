import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewGenkeyComponent } from './view-genkey.component';

describe('ViewGenkeyComponent', () => {
  let component: ViewGenkeyComponent;
  let fixture: ComponentFixture<ViewGenkeyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGenkeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGenkeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
