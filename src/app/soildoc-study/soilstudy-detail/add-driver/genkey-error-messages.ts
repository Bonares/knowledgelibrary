export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const GenkeyErrorMessages = [
  new ErrorMessage('name', 'genkeyAvailable', 'ADDDRIVER.FAILFORM.GENKEYAVAILABLE'),
  new ErrorMessage('name', 'genkeyInTree', 'ADDDRIVER.FAILFORM.GENKEYAVAILABLETREE')
];
