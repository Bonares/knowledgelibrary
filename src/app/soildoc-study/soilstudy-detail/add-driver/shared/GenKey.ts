export class GenKey {
  constructor(
    public _id: string,
    public name: string,
    public lastModifiedBy: string,
    public value: string,
    public displayText: string,
    public text: string,
    public collapsed: boolean,
    public checked: boolean,
    public isNumeric: boolean,
    public category: string,
    public modifiedDate: string,
    public createdDate: string,
    public isUsed: boolean,
    public usedPapers: string[]
   ) {}
}
