import { CombineKeyRaw } from './CombineKey-raw';
import { CombineKey } from './CombineKey';
export class CombineKeyFactory {

  static empty(): CombineKey {
    return new CombineKey('',  [], [], '', '', '', '', true, false, false);
  }

  static fromObject(rawCombineKey: CombineKeyRaw | any): CombineKey {
      return new CombineKey(
        rawCombineKey._id,
        rawCombineKey.dividend_items,
        rawCombineKey.divisor_items,
        rawCombineKey.lastModifiedBy,
        rawCombineKey.value,
        rawCombineKey.displayText,
        rawCombineKey.text,
        rawCombineKey.collapsed,
        rawCombineKey.checked,
        rawCombineKey.isNumeric
      );
  }
  }
