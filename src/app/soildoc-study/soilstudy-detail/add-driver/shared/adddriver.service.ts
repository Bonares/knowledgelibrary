import { CombineKeyFactory } from './CombineKeyFactory';
import { CombineKeyRaw } from './CombineKey-raw';
import { CombineKey } from './CombineKey';
import { GenKeyFactory } from './GenKeyFactory';
import { GenKeyRaw } from './GenKey-raw';
import { GenKey } from './GenKey';
import { AuthGuard } from './../../../../shared/auth.guard';
import { catchError, timeout, retry, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdddriverService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  createGenKey(genkey: GenKey): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/genkey`, genkey)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  getAllKeywordsGenByUser(): Observable<Array<GenKey>> {
    return this.httpClient
      .get<GenKeyRaw[]>(`${this.api}/genkeys`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        map(rawGenKey => rawGenKey
          .map(rawGen => GenKeyFactory.fromObject(rawGen)),
        ),
        catchError(this.handleError)
      );
  }

  getAllKeywordsGenAdmin(): Observable<Array<GenKey>> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
        .get<GenKeyRaw[]>(`${this.api}/genkeysformattree`)
        .pipe(
          retry(3), // retry a failed request up to 3 times
          map(rawGenKey => rawGenKey
            .map(rawGen => GenKeyFactory.fromObject(rawGen)),
          ),
          catchError(this.handleError)
        );
    }
  }

  removeOneKeywordGenkey(id: String): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .delete(`${this.api}/genkey/${id}`)
      .pipe(
        catchError(this.handleError)
      );
    }
  }

  createCombineKey(combinekey: CombineKey): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/combinekey`, combinekey)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  getAllKeywordsCombinedByUser(): Observable<Array<CombineKey>> {
    return this.httpClient
      .get<CombineKeyRaw[]>(`${this.api}/combinekeys`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        map(rawCombineKey => rawCombineKey
          .map(rawCombine => CombineKeyFactory.fromObject(rawCombine)),
        ),
        catchError(this.handleError)
      );
  }

  genkeyAvailable(genkey: string): Observable<Boolean> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
        .post<Boolean>(`${this.api}/genkey/check`,  {genkey: genkey.trim()})
        .pipe(
          catchError(this.handleError)
        );
    }
  }

  combinekeyAvailable(combinekey: string): Observable<Boolean> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
        .post<Boolean>(`${this.api}/combinekey/check`,  {combinekey: combinekey})
        .pipe(
          catchError(this.handleError)
        );
    }
  }


}
