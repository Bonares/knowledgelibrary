import { Item } from './Item';
export interface CombineKeyRaw {
  _id: string;
  dividend_items: Item[];
  divisor_items: Item[];
  lastModifiedBy: string;
  value: string;
  displayText: string;
  text: string;
  collapsed: boolean;
  checked: boolean;
  isNumeric: boolean;
}
