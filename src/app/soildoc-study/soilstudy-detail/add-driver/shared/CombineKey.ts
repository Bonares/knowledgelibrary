import { Item } from './Item';
export class CombineKey {
  constructor(
    public _id: string,
    public dividend_items: Item[],
    public divisor_items: Item[],
    public lastModifiedBy: string,
    public value: string,
    public displayText: string,
    public text: string,
    public collapsed: boolean,
    public checked: boolean,
    public isNumeric: boolean
   ) {}
}
