import { GenKeyRaw } from './GenKey-raw';
import { GenKey } from './GenKey';
export class GenKeyFactory {

  static empty(): GenKey {
    return new GenKey('', '', '', '', '', '', true, false, false, '', '', '', false, []);
  }

  static fromObject(rawGenKey: GenKeyRaw | any): GenKey {
      return new GenKey(
        rawGenKey._id,
        rawGenKey.name,
        rawGenKey.lastModifiedBy,
        rawGenKey.value,
        rawGenKey.displayText,
        rawGenKey.text,
        rawGenKey.collapsed,
        rawGenKey.checked,
        rawGenKey.isNumeric,
        rawGenKey.category,
        rawGenKey.modifiedDate,
        rawGenKey.createdDate,
        rawGenKey.isUsed,
        rawGenKey.usedPapers
      );
  }
  }
