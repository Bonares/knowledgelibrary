export interface GenKeyRaw {
  _id: string;
  name: string;
  lastModifiedBy: string;
  value: string;
  displayText: string;
  text: string;
  collapsed: boolean;
  checked: boolean;
  isNumeric: boolean;
  category: string;
  modifiedDate: string;
  createdDate: string;
  isUsed: boolean;
  usedPapers: string[];
}
