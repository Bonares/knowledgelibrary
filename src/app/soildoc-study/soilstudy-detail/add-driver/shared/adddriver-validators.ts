import { switchMap, map } from 'rxjs/operators';
import { Observable, timer, of } from 'rxjs';
import { AdddriverService } from './adddriver.service';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
export class AdddriverValidators {

  static onlyNumberNegativeDOT2MAX15(control: UntypedFormControl): { [error: string]: any } {
    if (!control.value) { return null; }

    const numberPattern = /^(-?\d{1,15}|-?\d{1,15}\.[0-9][0-9]?)$/;
    return numberPattern.test(control.value) ? null : {
      numberFormatNegativeDOT2MAX15: {valid: false}
    };
  }

  static genkeyAvailable(as: AdddriverService) {
    return function(control: UntypedFormControl): Observable<{
      [error: string]: any }> {
        return timer(200).pipe(
          switchMap(() => {
            if (!control.value) {
              return of(null);
            }
            return as.genkeyAvailable(control.value)
            .pipe(
              map(available =>
                available ? null : { genkeyAvailable: { valid: false }})
            );
          })
        );
      };
  }

  static combinekeyAvailable(as: AdddriverService) {
    return function(control: UntypedFormGroup): Observable<{
      [error: string]: any }> {
        return timer(200).pipe(
          switchMap(() => {
            if (!control.value) {
              return of(null);
            }
            let combinedword = '';
            for (let i = 0; i < control.value.dividend_items.length; i++) {
                combinedword += control.value.dividend_items[i].name;
                if (i !== control.value.dividend_items.length - 1) {
                    combinedword += ' - ';
                }
            }
            if (control.value.divisor_items.length > 0) {
                combinedword += ' : ';
            }
            for (let i = 0; i < control.value.divisor_items.length; i++) {
                combinedword += control.value.divisor_items[i].name;
                if (i !== control.value.divisor_items.length - 1) {
                    combinedword += ' - ';
                }
            }
            return as.combinekeyAvailable(combinedword)
            .pipe(
              map(available =>
                available ? null : { combinekeyAvailable: { valid: false }})
            );
          })
        );
      };
  }

  static keywordInTree(object: any) {
    const test = function findByProperty(o: any, prop: string, value: any) {
      if ( o[prop] && o[prop].toLowerCase() === value.toLowerCase()) {
        return o;
      }
      let result, p;
      for (p in o) {
          if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
              result = test(o[p], prop, value);
              if (result) {
                  return result;
              }
          }
      }
      return result;
    };
    return function(control: UntypedFormControl): Observable<{
      [error: string]: any }> {
        return timer(200).pipe(
          map(() => {
            if (!control.value) {
              return null;
            }
            return test(object, 'text', control.value) === undefined ? null : { genkeyInTree: { valid: false }};
          })
        );
    };
  }


}
