export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const CombinekeyErrorMessages = [
  new ErrorMessage('', 'combinekeyAvailable', 'ADDDRIVER.FAILFORM.COMBINEKEYAVAILABLE')
];
