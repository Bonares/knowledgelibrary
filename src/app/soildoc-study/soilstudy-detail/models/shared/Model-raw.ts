import { ModelItem } from './ModelItem';
export interface ModelRaw {
  _id: string;
  properties_modelled: ModelItem[];
  properties_prerequisite: ModelItem[];
  soildoc_id: string;
}
