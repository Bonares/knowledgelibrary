import { ModelRaw } from './Model-raw';
import { Model } from './Model';
import { ModelItem } from './ModelItem';
export class ModelFactory {

  static empty(): Model {
    return new Model('', [new ModelItem('', '', false, [], '', '')], [new ModelItem('', '', false, [], '', '')], null);
  }

  static fromObject(rawModel: ModelRaw | any): Model {
    if (rawModel) {
      return new Model(
        rawModel._id,
        rawModel.properties_modelled,
        rawModel.properties_input,
        rawModel.soildoc_id
      );
    } else {
      return null;
    }
  }
  }
