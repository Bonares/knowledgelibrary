import { ModelItem } from './ModelItem';
export class Model {
  constructor(
    public _id: string,
    public properties_modelled: ModelItem[],
    public properties_input: ModelItem[],
    public soildoc_id: string
   ) {}
}
