import { Model } from './Model';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AuthGuard } from './../../../../shared/auth.guard';
import { ModelService } from './model.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ModelResolverService {

  constructor(private ms: ModelService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Model> {
    if (this.ag.canActivateMethod()) {
    return this.ms.getOneBySoildoc(route.params['id']);
    }
  }
}
