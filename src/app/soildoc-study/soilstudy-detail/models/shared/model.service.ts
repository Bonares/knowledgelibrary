import { ModelFactory } from './ModelFactory';
import { retry, timeout, map, catchError } from 'rxjs/operators';
import { ModelRaw } from './Model-raw';
import { Model } from './Model';
import { throwError, Observable } from 'rxjs';
import { AuthGuard } from './../../../../shared/auth.guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Model> {
    return this.httpClient
      .get<ModelRaw>(`${this.api}/model/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawModel => ModelFactory.fromObject(rawModel),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Model> {
    return this.httpClient
      .get<ModelRaw>(`${this.api}/model/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawModel => ModelFactory.fromObject(rawModel),
        ),
        catchError(this.handleError)
      );
  }


  create(model: Model): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/model`, model)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(model: Model): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/model/${model._id}`, model)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
