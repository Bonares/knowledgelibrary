import { MetaService } from './../../../shared/meta.service';
import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { GenkeyErrorMessages } from './../add-driver/genkey-error-messages';
import { CombinekeyErrorMessages } from './../add-driver/combinekey-error-messages';
import { AdddriverValidators } from './../add-driver/shared/adddriver-validators';
import { ModelErrorMessages } from './model-error-messages';
import { ModelService } from './shared/model.service';
import { SitesoilService } from './../site-soil/shared/sitesoil.service';
import { AdddriverService } from './../add-driver/shared/adddriver.service';
import { JsonService } from './../shared/json.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CombineKeyFactory } from './../add-driver/shared/CombineKeyFactory';
import { GenKeyFactory } from './../add-driver/shared/GenKeyFactory';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { ModelFactory } from './shared/ModelFactory';
import { Typeofstudy } from './../type-of-study/shared/Typeofstudy';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { faTrashAlt, faMinus, faFileWord, faObjectGroup, faSearch, faEdit, faPlus, faSave, faSpinner, faSync, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-models',
  templateUrl: './models.component.html',
  styleUrls: []
})
export class ModelsComponent implements OnInit {

  faBookOpen = faBookOpen;
  faSync = faSync;
  faSpinner = faSpinner;
  faSave = faSave;
  faPlus = faPlus;
  faEdit = faEdit;
  faSearch = faSearch;
  faTrashAlt = faTrashAlt;
  faMinus = faMinus;
  faFileWord = faFileWord;
  faObjectGroup = faObjectGroup;

  isLoading: Boolean;
  isSaveLoading: Boolean = false;
  soildoc: Soildoc;
  typeofstudy: Typeofstudy;
  model = ModelFactory.empty();
  modelForm: UntypedFormGroup;
  genkeyForm: UntypedFormGroup;
  genkey = GenKeyFactory.empty();
  combinekeyForm: UntypedFormGroup;
  combinekey = CombineKeyFactory.empty();
  failSaveCombineKey: boolean;
  failSaveGenKey: boolean;
  failSave: boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  successSave = false;

  errors: { [key: string]: string } = {};
  errorsCombinekey: { [key: string]: string } = {};
  errorsGenkey: { [key: string]: string } = {};

  options_properties: TreeNode[];
  options_keyword: TreeNode[];

  options_genkeys: TreeNode[] = [];
  options_genkeys_management: TreeNode[] = [];
  options_genkeys_landuse: TreeNode[] = [];
  options_genkeys_properties: TreeNode[] = [];
  options_genkeys_geography: TreeNode[] = [];
  options_combinekeys: TreeNode[] = [];
  options_crops: TreeNode[];

  options_unformatted = [];
  options_properties_unformatted: any;
  options_geography_unformatted: any;
  options_properties_usergen_unformatted = [];
  options_qualities_switch: any = [];
  options_qualities: TreeNode[] = [];

  category_options = ['Management', 'Land Use', 'Properties', 'Geography'];

  maximalProperties = 20;
  maximalQualities = 5;
  maximal_combine_items = 5;

  redirectUrl = '';

  modalRedirect : bootstrap.Modal;
  modalCombineKeyword : bootstrap.Modal;
  modalGenerateKeyword : bootstrap.Modal;

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private js: JsonService,
    private as: AdddriverService,
    private siteservice: SitesoilService,
    private ms: ModelService,
    private router: Router,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.MODEL', 'MODEL.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }
    if (data['typeofstudy']) {
      this.typeofstudy = data['typeofstudy'];
    }

    if (data['model']) {
      this.model = data['model'];
      this.isEdit = true;
    }

    this.js.getbyData(['Properties1', 'Geography511', 'UnitsGen', 'Management332', 'LandUse450']).subscribe(result => {
      if (result) {
        this.options_properties = [];
        this.options_keyword = [];
        this.options_genkeys = [];
        this.options_combinekeys = [];
        this.options_crops = [];
        this.options_unformatted.push(result[0].children);
        this.options_unformatted.push(result[1].children);
        this.options_unformatted.push(result[3].children);
        this.options_unformatted.push(result[4].children);
        this.options_properties.push(new TreeNode(result[0]));
        this.options_properties.push(new TreeNode(result[1]));
        this.options_keyword.push(new TreeNode(result[0]));
        this.options_keyword.push(new TreeNode(result[2]));
        this.options_properties_unformatted = result[0];
        this.options_geography_unformatted = result[1];
        this.options_qualities_switch.push(this.findByProperty(result[0], 'value', '177'));
        this.options_qualities_switch.push(this.findByProperty(result[0], 'value', '184'));
        this.options_qualities.push(new TreeNode(this.findByProperty(result[0], 'value', '226')));
        this.options_qualities.push(new TreeNode(this.findByProperty(result[0], 'value', '266')));
        this.options_qualities.push(new TreeNode(this.findByProperty(result[0], 'value', '288')));
        this.siteservice.getAllCropsByUser().subscribe(results => {
          if (results) {
            for (let i = 0; i < results.length; i++) {
              this.options_crops.push(new TreeNode(results[i]));
            }
            this.options_properties_usergen_unformatted.push(results);
          }
          this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
            if (resultsgen) {
              for (let i = 0; i < resultsgen.length; i++) {
                if (resultsgen[i].category === 'Properties') {
                  this.options_genkeys_properties.push(new TreeNode(resultsgen[i]));
                } else if (resultsgen[i].category === 'Geography') {
                  this.options_genkeys_geography.push(new TreeNode(resultsgen[i]));
                }
                this.options_genkeys.push(new TreeNode(resultsgen[i]));
              }
              this.options_properties_usergen_unformatted.push(resultsgen);
            }
            this.as.getAllKeywordsCombinedByUser().subscribe(resultscom => {
              if (resultscom) {
                for (let i = 0; i < resultscom.length; i++) {
                  this.options_combinekeys.push(new TreeNode(resultscom[i]));
                }
                this.options_properties_usergen_unformatted.push(resultscom);
              }
              const usergenerated = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000001',
                  'text': 'Crop Rotation',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_crops
                }, {
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [
                  {
                    'value': '1000006',
                    'text': 'Properties',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_properties
                  },
                  {
                    'value': '1000007',
                    'text': 'Geography',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_geography
                  }]
                }, {
                  'value': '1000003',
                  'text': 'Combined Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_combinekeys
                }]
              };
              const usergeneratedcombine = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [
                  {
                    'value': '1000006',
                    'text': 'Properties',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_properties
                  },
                  {
                    'value': '1000007',
                    'text': 'Geography',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_geography
                  }]
                }]
              };
              this.options_properties.unshift(new TreeNode(usergenerated));
              this.options_keyword.unshift(new TreeNode(usergeneratedcombine));
              this.initModel();
              this.initGenKey();
              this.initCombineKey();
              this.isLoading = false;
              timer(1).subscribe(val => {
                this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));
                this.modalCombineKeyword = new bootstrap.Modal(document.getElementById('modalCombineKeyword'));
                this.modalGenerateKeyword = new bootstrap.Modal(document.getElementById('modalGenerateKeyword'));
              });
            });
          });
        });
      }
    });
  }

  convertObjToTreeNodes(arr: []) {
    const treeViewObj = [];
    for (let i = 0; i < arr.length; i++) {
      treeViewObj.push(new TreeNode(arr[i]));
    }
    return treeViewObj;
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  initModel() {
    this.modelForm = this.fb.group({
      properties_modelled: this.fb.array(
        this.model.properties_modelled.map(
          pm => this.fb.group({
            name: this.fb.control(pm.name, [Validators.required]),
            id: this.fb.control(pm.id, Validators.required),
            qualitiesEnabled: this.fb.control(pm.qualitiesEnabled),
            qualities: this.fb.array(pm.qualities.map(
              q => this.fb.group({
                id: this.fb.control(q.id, [Validators.required]),
                name: this.fb.control(q.name, [Validators.required]),
              })
            )),
            combined: this.fb.control(pm.combined),
            category: this.fb.control(pm.category)
          })
        )
      ),
      properties_input: this.fb.array(
        this.model.properties_input.map(
          pp => this.fb.group({
            name: this.fb.control(pp.name, [Validators.required]),
            id: this.fb.control(pp.id, Validators.required),
            qualitiesEnabled: this.fb.control(pp.qualitiesEnabled),
            qualities: this.fb.array(pp.qualities.map(
              q => this.fb.group({
                id: this.fb.control(q.id, [Validators.required]),
                name: this.fb.control(q.name, [Validators.required]),
              })
            )),
            combined: this.fb.control(pp.combined),
            category: this.fb.control(pp.category)
          })
        )
      )
    });

    this.modelForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  onValueChangePropertyModelled(propertyIndex: number, event: any) {
    if (event === 'removeKey') {
      this.ngOnInit();
    } else {
      this.modelForm.controls['properties_modelled']['controls'][propertyIndex]['controls']['name'].setValue(event.value ? event.text : '');
      this.actualizeCategoryItem(propertyIndex, event.value, 'properties_modelled');
      this.actualizeQualitiesEnabled(propertyIndex, event.value, 'properties_modelled');
    }
  }

  onValueChangePropertyInput(propertyIndex: number, event: any) {
    if (event === 'removeKey') {
      this.ngOnInit();
    } else {
      this.modelForm.controls['properties_input']['controls'][propertyIndex]['controls']['name'].setValue(event.value ? event.text : '');
      this.actualizeCategoryItem(propertyIndex, event.value, 'properties_input');
      this.actualizeQualitiesEnabled(propertyIndex, event.value, 'properties_input');
    }
  }

  actualizeCategoryItem(propertyIndex: number, value: any, element: any) {
    let object = this.findByProperty(this.options_properties_unformatted, 'value', value);
    if (object === undefined) {
      object = this.findByProperty(this.options_properties_usergen_unformatted, 'value', value);
      if (object === undefined) {
        object = this.findByProperty(this.options_geography_unformatted, 'value', value);
        if (object !== undefined) {
          this.modelForm.controls[element]['controls'][propertyIndex]['controls']['category'].setValue('geography');
        } else {
          this.modelForm.controls[element]['controls'][propertyIndex]['controls']['category'].setValue('properties');
        }
      } else {
        if (object.category === 'Properties') {
          this.modelForm.controls[element]['controls'][propertyIndex]['controls']['category'].setValue('properties');
        } else {
          this.modelForm.controls[element]['controls'][propertyIndex]['controls']['category'].setValue('geography');
        }
      }
    } else {
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['category'].setValue('properties');
    }
  }

  actualizeQualitiesEnabled(propertyIndex: number, value: any, element: any) {
    if (value) {
      const item = this.findByProperty(this.options_qualities_switch, 'value', value);
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualitiesEnabled'].setValue(item ? true : false);
    } else {
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualitiesEnabled'].setValue(false);
    }
    if (this.modelForm.value[element][propertyIndex].qualitiesEnabled) {
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].setValidators([Validators.required]);
      if (this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].length === 0) {
        this.addQuality(propertyIndex, element);
      }
    } else {
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].setValidators([]);
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].reset();
      this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].clear();
    }
  }

  onValueChangeQuality(propertyIndex: number, qualityIndex: number, event: any, element: any) {
    this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities']['controls'][qualityIndex]['controls']['name'].setValue(
      event.value ? event.text : '');
  }

  addQuality(propertyIndex: number, element: any) {
    this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].push(
      this.fb.group({
        name: this.fb.control('', [Validators.required]),
        id: this.fb.control('', [Validators.required]),
      }));
    timer(1).subscribe(val => {
      this.modelForm.markAsDirty();
    });
  }

  removeQuality(propertyIndex: number, qualityIndex: number, element: any) {
    this.modelForm.controls[element]['controls'][propertyIndex]['controls']['qualities'].removeAt(qualityIndex);
    timer(1).subscribe(val => {
      this.modelForm.markAsDirty();
    });
  }

  addPropertyModelled() {
    // @ts-ignore: Unreachable code error
    this.modelForm['controls']['properties_modelled'].push(
      this.fb.group({
        name: this.fb.control('', [Validators.required]),
        id: this.fb.control('', Validators.required),
        qualitiesEnabled: this.fb.control(false),
        qualities: this.fb.array([], [Validators.required]),
        combined: this.fb.control(''),
        category: this.fb.control('')
      }));
      this.modelForm.markAsDirty();
  }

  removePropertyModelled(propIndex: number) {
    // @ts-ignore: Unreachable code error
    this.modelForm['controls']['properties_modelled'].removeAt(propIndex);
    this.modelForm.markAsDirty();
  }

  addPropertyInput() {
    // @ts-ignore: Unreachable code error
    this.modelForm['controls']['properties_input'].push(
      this.fb.group({
        name: this.fb.control('', [Validators.required]),
        id: this.fb.control('', Validators.required),
        qualitiesEnabled: this.fb.control(false),
        qualities: this.fb.array([], [Validators.required]),
        combined: this.fb.control(''),
        category: this.fb.control('')
      }));
      this.modelForm.markAsDirty();
  }

  removePropertyInput(propIndex: number) {
    // @ts-ignore: Unreachable code error
    this.modelForm['controls']['properties_input'].removeAt(propIndex);
    this.modelForm.markAsDirty();
  }

  initGenKey() {
    this.genkeyForm = this.fb.group({
      name: this.fb.control('', Validators.required, [AdddriverValidators.genkeyAvailable(this.as),
        AdddriverValidators.keywordInTree(this.options_unformatted) ]),
      isNumeric: this.fb.control(false),
      category: this.fb.control('', Validators.required)
    });
    this.genkeyForm.statusChanges.subscribe(() => this.updateErrorMessagesGenkey());
  }

  updateErrorMessagesGenkey() {
    this.errorsGenkey = {};
    for (const message of GenkeyErrorMessages) {
      const control = this.genkeyForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errorsGenkey[message.forControl]) {
        this.errorsGenkey[message.forControl] = message.text;
      }
    }
  }

  genkeyGenerator() {
    this.modalGenerateKeyword.show();
  }

  combinekeyGenerator() {
    this.modalCombineKeyword.show();
  }

  updateErrorMessagesCombinekey() {
    this.errorsCombinekey = {};
    if (this.combinekeyForm &&
      this.combinekeyForm.dirty &&
      this.combinekeyForm.invalid &&
      this.combinekeyForm.errors &&
      this.combinekeyForm.errors[CombinekeyErrorMessages[0].forValidator]) {
      this.errorsCombinekey['combine'] = CombinekeyErrorMessages[0].text;
    }
  }

  initCombineKey() {
    this.combinekeyForm = this.fb.group({
      dividend_items: this.fb.array([], Validators.required),
      divisor_items: this.fb.array([]),
      isNumeric: this.fb.control(false),
    }, { asyncValidators: AdddriverValidators.combinekeyAvailable(this.as) });
    this.combinekeyForm.statusChanges.subscribe(() => this.updateErrorMessagesCombinekey());
  }

  addDividend() {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['dividend_items'].push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
  }

  addDivisor() {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['divisor_items'].push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
  }

  removeDividend(diviIndex: number) {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['dividend_items'].removeAt(diviIndex);
  }

  removeDivisor(diviIndex: number) {
    // @ts-ignore: Unreachable code error
    this.combinekeyForm['controls']['divisor_items'].removeAt(diviIndex);
  }

  updateErrorMessages() {
    this.errors = {};

    for (const message of ModelErrorMessages) {
      const control = this.modelForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

  submitFormModel(redirect?: string) {
    if (this.modelForm.valid) {
      this.isSaveLoading = true;
      const model = ModelFactory.fromObject(this.modelForm.value);
      model.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          model._id = this.model._id;
          this.ms.update(model)
            .subscribe(
                datas => {
                  this.isSaveLoading = false;
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.model = datas;
                    this.successSave = true;
                    this.initModel();
                    window.scrollTo(0, 0);
                    if (redirect && redirect !== '') {
                      this.router.navigateByUrl(redirect);
                    } else {
                      const source = timer(3000);
                      source.subscribe(val => {
                        this.successSave = false;
                      });
                    }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.ms.create(model)
          .subscribe(
              datas => {
                this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.model = datas;
                  this.initModel();
                  this.isEdit = true;
                  this.successSave = true;
                  window.scrollTo(0, 0);
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  submitFormGenkey() {
    if (this.genkeyForm.valid) {
      const genkey = GenKeyFactory.fromObject(this.genkeyForm.value);
      this.as.createGenKey(genkey)
          .subscribe(
              datas => {
                  this.failSaveGenKey = false;
                  this.modalGenerateKeyword.hide();
                  if (datas.category === 'Properties') {
                    this.options_properties[0].children[1].children[0].children.unshift(new TreeNode(datas));
                  } else if (datas.category === 'Geography') {
                    this.options_properties[0].children[1].children[1].children.unshift(new TreeNode(datas));
                  }
                  this.options_properties = this.options_properties.slice();
                  this.initGenKey();
              },
              error => this.failSaveGenKey = true
          );
    }
  }

  submitFormCombinekey() {
    if (this.combinekeyForm.valid) {
      const combinekey = CombineKeyFactory.fromObject(this.combinekeyForm.value);
      this.as.createCombineKey(combinekey)
          .subscribe(
              datas => {
                  this.failSaveCombineKey = false;
                  this.modalCombineKeyword.hide();
                  this.options_properties[0].children[2].children.unshift(new TreeNode(datas));
                  this.options_properties = this.options_properties.slice();
                  this.initCombineKey();
              },
              error => this.failSaveCombineKey = true
          );
    }
  }

  checkifsaved(routerlink: string) {
    if (this.modelForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalRedirect.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }

}
