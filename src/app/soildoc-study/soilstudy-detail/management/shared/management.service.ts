import { retry, timeout, map, catchError } from 'rxjs/operators';
import { ManagementRaw } from './Management-raw';
import { Management } from './Management';
import { throwError, Observable } from 'rxjs';
import { AuthGuard } from './../../../../shared/auth.guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { ManagementFactory } from './ManagementFactory';
@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Management> {
    return this.httpClient
      .get<ManagementRaw>(`${this.api}/propertiesprocess/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawManagement => ManagementFactory.fromObject(rawManagement),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Management> {
    return this.httpClient
      .get<ManagementRaw>(`${this.api}/management/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawManagement => ManagementFactory.fromObject(rawManagement),
        ),
        catchError(this.handleError)
      );
  }


  create(management: Management): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/management`, management)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(management: Management): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/management/${management._id}`, management)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
