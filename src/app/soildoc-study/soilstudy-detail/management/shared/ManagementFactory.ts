import { ManagementRaw } from './Management-raw';
import { Management } from './Management';
export class ManagementFactory {

  static empty(): Management {
    return new Management('', [], null);
  }

  static fromObject(rawManagement: ManagementRaw | any): Management  {
    if (rawManagement) {
      return new Management(
        rawManagement._id,
        rawManagement.items,
        rawManagement.soildoc_id
      );
    } else {
      return null;
    }
  }
  }
