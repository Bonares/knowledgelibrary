import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthGuard } from './../../../../shared/auth.guard';
import { ManagementService } from './management.service';
import { Management } from './Management';
@Injectable({
  providedIn: 'root'
})
export class ManagementResolverService {

  constructor(private ms: ManagementService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Management> {
    if (this.ag.canActivateMethod()) {
    return this.ms.getOneBySoildoc(route.params['id']);
    }
  }
}
