export class Items {
  constructor(
    public _id: string,
    public name: string,
    public id: string,
    public parentText: string,
    public flagged: boolean,
    public reference: string,
    public reference_id: string,
    public reference_parentText: string,
    public reference_flagged: boolean,
    public isResidues: boolean,
    public crop: string,
    public isCategorical: boolean,
    public details: string,
    public combined: string,
    public category: string
   ) {}
}
