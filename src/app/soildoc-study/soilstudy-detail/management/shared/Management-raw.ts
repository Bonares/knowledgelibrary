import { Items } from './Items';
export interface ManagementRaw {
  _id: string;
  items: Items[];
  soildoc_id: string;
}
