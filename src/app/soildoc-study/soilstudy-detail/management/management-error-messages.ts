export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const ManagementErrorMessages = [
  new ErrorMessage('reference', 'notcategorical', 'MANAGEMENT.NOTCATEGORICAL' ),
  new ErrorMessage('id', 'notunique', 'MANAGEMENT.NOTUNIQUE' )
];
