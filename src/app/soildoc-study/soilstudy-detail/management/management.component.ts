import { MetaService } from './../../../shared/meta.service';
import { CropErrorMessages } from './../site-soil/crop-error-messages';
import { CropGenFactory } from './../site-soil/shared/CropGenFactory';
import { SitesoilValidators } from './../site-soil/shared/sitesoil-validators';
import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { ManagementErrorMessages } from './management-error-messages';
import { GenkeyErrorMessages } from './../add-driver/genkey-error-messages';
import { AdddriverValidators } from './../add-driver/shared/adddriver-validators';
import { timer } from 'rxjs';
import { GenKeyFactory } from './../add-driver/shared/GenKeyFactory';
import { Typeofstudy } from './../type-of-study/shared/Typeofstudy';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { faSearch, faTrashAlt, faMinus, faFileWord, faObjectGroup, faForward, faQuestionCircle, faEdit, faSync, faPlus, faSave, faSpinner,
faExclamationCircle, faAsterisk, faInfoCircle, faBookOpen, faSeedling, faLongArrowAltRight} from '@fortawesome/free-solid-svg-icons';
import { SitesoilService } from './../site-soil/shared/sitesoil.service';
import { TypeofstudyService } from './../type-of-study/shared/typeofstudy.service';
import { AdddriverService } from './../add-driver/shared/adddriver.service';
import { JsonService } from './../shared/json.service';
import { UntypedFormBuilder, UntypedFormGroup, UntypedFormArray, Validators } from '@angular/forms';
import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagementService } from './shared/management.service';
import { ManagementFactory } from './shared/ManagementFactory';
import { RelationshipService } from '../relationship/shared/relationship.service';
import { Relationship } from '../relationship/shared/Relationship';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kl-management',
  templateUrl: './management.component.html',
  providers: [],
  styleUrls: []
})
export class ManagementComponent implements OnInit {

  faLongArrowAltRight = faLongArrowAltRight;
  faSeedling = faSeedling;
  faBookOpen = faBookOpen;
  faAsterisk = faAsterisk;
  faExclamationCircle = faExclamationCircle;
  faSpinner = faSpinner;
  faSave = faSave;
  faPlus = faPlus;
  faSync = faSync;
  faEdit = faEdit;
  faForward = faForward;
  faSearch = faSearch;
  faTrashAlt = faTrashAlt;
  faMinus = faMinus;
  faFileWord = faFileWord;
  faObjectGroup = faObjectGroup;
  faQuestionCircle = faQuestionCircle;
  faInfoCircle = faInfoCircle;

  isLoading: Boolean;
  isSaveLoading = false;
  soildoc: Soildoc;
  typeofstudy: Typeofstudy;
  relationship: Relationship;
  management = ManagementFactory.empty();
  managementForm: UntypedFormGroup;
  genkeyForm: UntypedFormGroup;
  genkey = GenKeyFactory.empty();
  failSaveGenKey: boolean;
  failSave: boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  successSave = false;
  items: UntypedFormArray;

  errorsGenkey: { [key: string]: string } = {};
  driverErrors: [{[key: string]: string}] = [{}];
  valueErrors: [[{[key: string]: string}]] = [[{}]];

  unitArray = [];
  type_array_management = [];
  options_management: TreeNode[] = [];
  options_management_unformatted: any;
  options_residues_unformatted: any = [];
  options_landuse_unformatted: any;
  options_reference: TreeNode[];
  options_usergen_unformatted: any = [];
  options_keyword: TreeNode[] = [];
  options_genkeys: TreeNode[] = [];
  options_genkeys_management: TreeNode[] = [];
  options_genkeys_landuse: TreeNode[] = [];
  options_crops: TreeNode[] = [];
  options_crops_user: TreeNode[] = [];
  options_crops_form: TreeNode[] = [];
  options_unformatted = [];

  cropgen = CropGenFactory.empty();
  cropgenForm: UntypedFormGroup;
  crops: UntypedFormArray;
  errorsCrop: { [key: string]: string } = {};
  failSaveCrop: Boolean = false;
  maximal_crops = 10;

  category_options = ['Management', 'Land Use'];

  reference_errors: any[] = [];

  maximalManagement = 20;

  redirectUrl = '';

  itemErrors: [{[key: string]: string}] = [{}];

  modalRedirect : bootstrap.Modal;
  modalSave : bootstrap.Modal;
  modalCropgen : bootstrap.Modal;
  modalGenerateKeyword : bootstrap.Modal;

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private js: JsonService,
    private as: AdddriverService,
    private ts: TypeofstudyService,
    private ms: ManagementService,
    private siteservice: SitesoilService,
    private rs: RelationshipService,
    private router: Router,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.MANAGEMENT', 'MANAGEMENT.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }
    if (data['typeofstudy']) {
      this.typeofstudy = data['typeofstudy'];
    }

    if (data['management']) {
      this.management = data['management'];
      this.isEdit = true;
    }

    this.js.getbyData(['Management332', 'Properties1', 'UnitsGen', 'LandUse450']).subscribe(result => {
      if (result) {
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[0], 'value', 'Main crop')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[0], 'value', '529')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[0], 'value', '600')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[0], 'value', '530')));
        this.options_crops_form.push(new TreeNode(this.findByProperty(result[0], 'value', '869')));
        this.options_management.push(new TreeNode(result[0]));
        this.options_management.push(new TreeNode(result[3]));
        this.options_keyword.push(new TreeNode(result[1]));
        this.options_keyword.push(new TreeNode(result[2]));
        this.options_management_unformatted = result[0];
        this.options_landuse_unformatted = result[3];
        this.options_residues_unformatted.push(this.findByProperty(this.options_management_unformatted, 'value', '395'));
        this.options_residues_unformatted.push(this.findByProperty(this.options_management_unformatted, 'value', '706'));
        this.options_unformatted.push(result[0].children);
        this.options_unformatted.push(result[1].children);
        this.options_unformatted.push(result[3].children);
        this.siteservice.getAllCropsByUser().subscribe(results => {
          if (results) {
            for (let i = 0; i < results.length; i++) {
              this.options_crops_user.push(new TreeNode(results[i]));
            }
            this.options_usergen_unformatted.push(results);
          }
          this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
            if (resultsgen) {
              for (let i = 0; i < resultsgen.length; i++) {
                if (resultsgen[i].category === 'Management') {
                  this.options_genkeys_management.push(new TreeNode(resultsgen[i]));
                } else if (resultsgen[i].category === 'Land Use') {
                  this.options_genkeys_landuse.push(new TreeNode(resultsgen[i]));
                }
                this.options_genkeys.push(new TreeNode(resultsgen[i]));
              }
              this.options_usergen_unformatted.push(resultsgen);
            }
              const usergen = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000001',
                  'text': 'Crop Rotation',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_crops_user
                }, {
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [{
                    'value': '1000004',
                    'text': 'Management',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_management
                  },
                  {
                    'value': '1000005',
                    'text': 'Land Use',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_landuse
                  }]
                }]
              };
              this.options_management.unshift(new TreeNode(usergen));
              this.options_reference = this.options_management.slice();
              this.rs.getOneBySoildoc(this.soildoc._id).subscribe(relationship => {
                this.relationship = relationship;
                this.initManagement();
                this.resetTypeArrayManagement();
                this.initGenKey();
                this.initCropgen();
                this.updateErrorMessages();
                this.isLoading = false;
                timer(1).subscribe(val => {
                  this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));
                  this.modalSave = new bootstrap.Modal(document.getElementById('modalSave'));
                  this.modalCropgen = new bootstrap.Modal(document.getElementById('modalCropgen'));
                  this.modalGenerateKeyword = new bootstrap.Modal(document.getElementById('modalGenerateKeyword'));
                });
              });
          });
        });
      }
    });
  }

  convertObjToTreeNodes(arr: []) {
    const treeViewObj = [];
    for (let i = 0; i < arr.length; i++) {
      treeViewObj.push(new TreeNode(arr[i]));
    }
    return treeViewObj;
  }

  onValueChangeManagement(managementIndex: number, event: TreeNode) {
    this.items.controls[managementIndex]['controls']['name'].setValue(event.value ? event.text : '');
    this.items.controls[managementIndex]['controls']['parentText'].setValue(event.parentText ? event.parentText : '');
    this.items.controls[managementIndex]['controls']['flagged'].setValue(event.flagged ? event.flagged : false);
    this.actualizeTypeArrayManagement(managementIndex);
    this.actualizeResiduesArrayManagement(managementIndex);
    this.actualizeCategoryItem(managementIndex, event);
    this.checkUnique();
  }

  checkUnique() {
    for (let i = 0; i < this.managementForm.value.items.length; i++) {
      if (!this.items.controls[i]['controls']['id'].hasError('required')) {
        if (this.managementForm.value.items[i].isCategorical && this.managementForm.value.items[i].reference !== '') {
          if (this.managementForm.value.items.filter(item => (item.name !== '' && item.name === this.managementForm.value.items[i].name &&
            item.reference === this.managementForm.value.items[i].reference)).length > 1) {
              this.items.controls[i]['controls']['id'].setErrors({notunique: true});
            } else {
              this.items.controls[i]['controls']['id'].setErrors(null);
            }
        } else {
          if (this.managementForm.value.items.filter(item => (item.name !== '' && item.name === this.managementForm.value.items[i].name &&
            (!item.isCategorical || (item.isCategorical && item.reference === '')))).length > 1) {
              this.items.controls[i]['controls']['id'].setErrors({notunique: true});
            } else {
              this.items.controls[i]['controls']['id'].setErrors(null);
            }
        }
      }
    }
    this.updateErrorMessages();
  }

  actualizeCategoryItem(managementIndex: number, event: any) {
    let object = this.findByProperty(this.options_management_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
    if (object === undefined) {
      object = this.findByProperty(this.options_usergen_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
      if (object === undefined) {
        object = this.findByProperty(this.options_landuse_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
        if (object !== undefined) {
          this.items.controls[managementIndex]['controls']['category'].setValue('landuse');
        } else {
          this.items.controls[managementIndex]['controls']['category'].setValue('management');
        }
      } else {
        if (object.category === 'Land Use') {
          this.items.controls[managementIndex]['controls']['category'].setValue('landuse');
        } else {
          this.items.controls[managementIndex]['controls']['category'].setValue('management');
        }
      }
    } else {
      this.items.controls[managementIndex]['controls']['category'].setValue('management');
    }
  }

  onValueChangeReference(managementIndex: number, event: TreeNode) {
    this.items.controls[managementIndex]['controls']['reference'].setErrors(null);
    this.items.controls[managementIndex]['controls']['reference'].setValue(event.value ? event.text : '');
    this.items.controls[managementIndex]['controls']['reference_parentText'].setValue(event.parentText ? event.parentText : '');
    this.items.controls[managementIndex]['controls']['reference_flagged'].setValue(event.flagged ? event.flagged : false);
    if (event.value) {
      let object = this.findByProperty(this.options_management_unformatted, 'value', this.managementForm.value.items[managementIndex].reference_id);
      if (object === undefined) {
        object = this.findByProperty(this.options_usergen_unformatted, 'value', this.managementForm.value.items[managementIndex].reference_id);
        if (object === undefined) {
          object = this.findByProperty(this.options_landuse_unformatted, 'value', this.managementForm.value.items[managementIndex].reference_id);
          if (object !== undefined) {
            if (object.isNumeric) {
              this.items.controls[managementIndex]['controls']['reference'].setErrors({notcategorical: true});
            }
          }
        } else {
          if (object.isNumeric) {
            this.items.controls[managementIndex]['controls']['reference'].setErrors({notcategorical: true});
          }
        }
      } else {
        if (object.isNumeric) {
          this.items.controls[managementIndex]['controls']['reference'].setErrors({notcategorical: true});
        }
      }
    }
    this.updateErrorMessages();
    this.checkUnique();
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  resetTypeArrayManagement() {
    for (let i = 0; i < this.items.controls.length ; i++) {
      this.actualizeTypeArrayManagement(i);
    }
  }

  actualizeTypeArrayManagement(managementIndex: number) {
    let object = this.findByProperty(this.options_management_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
    if (object === undefined) {
      object = this.findByProperty(this.options_usergen_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
      if (object === undefined) {
        object = this.findByProperty(this.options_landuse_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
        if (object === undefined) {
          this.items.controls[managementIndex]['controls']['isCategorical'].setValue(false);
        } else {
          this.items.controls[managementIndex]['controls']['isCategorical'].setValue(object.isNumeric ? false : true);
        }
      } else {
        this.items.controls[managementIndex]['controls']['isCategorical'].setValue(object.isNumeric ? false : true);
      }
    } else {
      this.items.controls[managementIndex]['controls']['isCategorical'].setValue(object.isNumeric ? false : true);
    }
    if (!this.items.value[managementIndex].isCategorical) {
      this.items.controls[managementIndex]['controls']['reference'].setValue('');
      this.items.controls[managementIndex]['controls']['reference_id'].setValue('');
    }
    this.items.controls[managementIndex]['controls']['reference'].updateValueAndValidity();
    this.items.controls[managementIndex]['controls']['reference_id'].updateValueAndValidity();
  }

  actualizeResiduesArrayManagement(managementIndex: number) {
    const object = this.findByProperty(this.options_residues_unformatted, 'value', this.managementForm.value.items[managementIndex].id);
    if (object === undefined) {
      this.items.controls[managementIndex]['controls']['crop'].setValue('');
      this.items.controls[managementIndex]['controls']['isResidues'].setValue(false);
    } else {
      this.items.controls[managementIndex]['controls']['isResidues'].setValue(true);
    }
    this.items.controls[managementIndex]['controls']['crop'].updateValueAndValidity();
  }

  addManagement(id?: string) {
    this.items.insert(0,
      this.fb.group({
        name: this.fb.control('', Validators.required),
        id: this.fb.control(id ? id : '', Validators.required),
        parentText: this.fb.control(''),
        flagged: this.fb.control(false),
        reference: this.fb.control(''),
        reference_id: this.fb.control(''),
        reference_parentText: this.fb.control(''),
        reference_flagged: this.fb.control(false),
        isResidues: this.fb.control(false),
        crop: this.fb.control(''),
        isCategorical: this.fb.control(false),
        details: this.fb.control(''),
        combined: this.fb.control(''),
        category: this.fb.control('')
      })
    );
    timer(1).subscribe(val => {
      this.managementForm.markAsDirty();
    });
    this.resetTypeArrayManagement();
    this.checkUnique();
  }

  removeManagement(managementIndex: number) {
    this.items.removeAt(managementIndex);
    this.managementForm.markAsDirty();
    this.resetTypeArrayManagement();
    this.checkUnique();
  }

  initManagement() {
    this.items = this.fb.array(
      this.management.items.map(
        i => this.fb.group({
          name: this.fb.control(i.name, Validators.required),
          id: this.fb.control(i.id, Validators.required),
          parentText: this.fb.control(i.parentText),
          flagged: this.fb.control(i.flagged),
          reference: this.fb.control(i.reference),
          reference_id: this.fb.control(i.reference_id),
          reference_parentText: this.fb.control(i.reference_parentText),
          reference_flagged: this.fb.control(i.reference_flagged),
          isResidues: this.fb.control(i.isResidues),
          crop: this.fb.control(i.crop),
          isCategorical: this.fb.control(i.isCategorical),
          details: this.fb.control(i.details),
          combined: this.fb.control(i.combined),
          category: this.fb.control(i.category)
        })
      )
    );

    this.managementForm = this.fb.group({
      items: this.items
    });

    this.managementForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  updateErrorMessages() {
    this.resetItemErrorArray();
    for (const message of ManagementErrorMessages) {
      const control = this.managementForm.get(message.forControl);
      if (!control) {
        for (let i = 0; i < this.items.controls.length ; i++) {
          const controlitem = this.items.controls[i].get(message.forControl);
          if (controlitem &&
            controlitem.invalid &&
            controlitem.errors[message.forValidator] &&
            !this.itemErrors[i][message.forControl]) {
            this.itemErrors[i][message.forControl] = message.text;
          }
        }
      }
    }
  }

  resetItemErrorArray() {
    this.itemErrors = [{}];
    for (let i = 0; i < this.items.controls.length ; i++) {
      this.itemErrors[i] = {'id': '', 'reference': ''};
    }
  }

  initGenKey() {
    this.genkeyForm = this.fb.group({
      name: this.fb.control('', Validators.required, [
        AdddriverValidators.genkeyAvailable(this.as),
        AdddriverValidators.keywordInTree(this.options_unformatted) ]),
      isNumeric: this.fb.control(false),
      category: this.fb.control('', Validators.required)
    });
    this.genkeyForm.statusChanges.subscribe(() => this.updateErrorMessagesGenkey());
  }

  genkeyGenerator() {
    this.modalGenerateKeyword.show();
  }

  updateErrorMessagesGenkey() {
    this.errorsGenkey = {};
    for (const message of GenkeyErrorMessages) {
      const control = this.genkeyForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errorsGenkey[message.forControl]) {
        this.errorsGenkey[message.forControl] = message.text;
      }
    }
  }

  openModalSave() {
    this.modalSave.show();
  }

  submitFormManagement(redirect?: string) {
    if (this.managementForm.valid) {
      this.isSaveLoading = true;
      const management = ManagementFactory.fromObject(this.managementForm.value);
      management.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          management._id = this.management._id;
          this.ms.update(management)
            .subscribe(
                datas => {
                    this.isSaveLoading = false;
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.management = datas;
                    this.successSave = true;
                    this.initManagement();
                    window.scrollTo(0, 0);
                    this.ts.getOneBySoildoc(this.soildoc._id).subscribe(result => {
                      this.typeofstudy = result;
                    });
                    if (redirect && redirect !== '') {
                      this.router.navigateByUrl(redirect);
                    } else {
                      const source = timer(3000);
                      source.subscribe(val => {
                        this.successSave = false;
                      });
                    }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.ms.create(management)
          .subscribe(
              datas => {
                  this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.management = datas;
                  this.initManagement();
                  this.isEdit = true;
                  this.successSave = true;
                  window.scrollTo(0, 0);
                  this.ts.getOneBySoildoc(this.soildoc._id).subscribe(result => {
                    this.typeofstudy = result;
                  });
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  submitFormGenkey() {
    if (this.genkeyForm.valid) {
      const genkey = GenKeyFactory.fromObject(this.genkeyForm.value);
      this.as.createGenKey(genkey)
          .subscribe(
              datas => {
                  this.failSaveGenKey = false;
                  this.modalGenerateKeyword.hide();
                  if (datas.category === 'Management') {
                    this.options_management[0].children[1].children[0].children.unshift(new TreeNode(datas));
                  } else if (datas.category === 'Land Use') {
                    this.options_management[0].children[1].children[1].children.unshift(new TreeNode(datas));
                  }
                  this.options_management = this.options_management.slice();
                  this.options_reference = this.options_management.slice();
                  this.options_usergen_unformatted.unshift(datas);
                  this.isLoading = false;
                  this.addManagement(datas.value);
                  this.initGenKey();
              },
              error => this.failSaveGenKey = true
          );
    }
  }

  initCropgen() {
    this.crops = null;
    this.cropgenForm = null;
    this.crops = this.fb.array([],
      [ Validators.required ], SitesoilValidators.croprotationAvailable(this.siteservice)
      );

    this.cropgenForm = this.fb.group({
      crops: this.crops
    });

    this.cropgenForm.statusChanges.subscribe(() => this.updateErrorMessagesCrop());
  }

  updateErrorMessagesCrop() {
    this.errorsCrop = {};
    for (const message of CropErrorMessages) {
      const control = this.cropgenForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors &&
        control.errors[message.forValidator]) {
        this.errorsCrop[message.forControl] = message.text;
      }
    }
  }

  addCrop() {
    this.crops.push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
    this.cropgenForm.markAsDirty();
  }

  removeCrop(index: number) {
    this.crops.removeAt(index);
    this.cropgenForm.markAsDirty();
  }

  cropGenerator() {
    this.modalCropgen.show();
  }

  submitFormCropgen() {
    if (this.cropgenForm.valid) {
      const cropgen = CropGenFactory.fromObject(this.cropgenForm.value);
      this.siteservice.createCrop(cropgen)
          .subscribe(
              datas => {
                  this.failSaveCrop = false;
                  this.modalCropgen.hide();
                  this.options_management[0].children[0].children.unshift(new TreeNode(datas));
                  this.options_management = this.options_management.slice();
                  this.options_usergen_unformatted.unshift(datas);
                  this.addManagement(datas.value);
                  this.initCropgen();
              },
              error => this.failSaveCrop = true
          );
    }
  }

  checkifsaved(routerlink: string) {
    if (this.managementForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalRedirect.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }
}
