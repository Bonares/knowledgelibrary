import { MetaService } from './../../../shared/meta.service';
import { LayerSites } from './shared/LayerSites';
import { TreeNode } from './../../../visualization/treeview-custom/tree-node';
import { SitesValidators } from './shared/sites-validators';
import { timer } from 'rxjs';
import { SitesoilValidators } from './../site-soil/shared/sitesoil-validators';
import { SitesErrorMessages } from './sites-error-messages';
import { GenkeyErrorMessages } from './../add-driver/genkey-error-messages';
import { AdddriverValidators } from './../add-driver/shared/adddriver-validators';
import { CropErrorMessages } from './../site-soil/crop-error-messages';
import { SitesService } from './shared/sites.service';
import { SitesoilService } from './../site-soil/shared/sitesoil.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JsonService } from './../shared/json.service';
import { AdddriverService } from './../add-driver/shared/adddriver.service';
import { GenKeyFactory } from './../add-driver/shared/GenKeyFactory';
import { CropGenFactory } from './../site-soil/shared/CropGenFactory';
import { UntypedFormGroup, UntypedFormArray, UntypedFormBuilder, Validators } from '@angular/forms';
import { SitesFactory } from './shared/SitesFactory';
import { Typeofstudy } from './../type-of-study/shared/Typeofstudy';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { faSeedling, faTrashAlt, faMapMarkedAlt, faLayerGroup, faCloudSunRain, faMapMarkerAlt, faEdit, faSync, faTimes, faPlus, faSave,
  faLongArrowAltRight, faTractor, faMap, faCopy, faQuestionCircle, faSearch, faFileWord, faObjectGroup, faSpinner,
  faExclamationCircle, faInfoCircle, faBookOpen} from '@fortawesome/free-solid-svg-icons';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';
@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kl-sites',
  templateUrl: './sites.component.html',
  styleUrls: []
})
export class SitesComponent implements OnInit {

  faBookOpen = faBookOpen;
  faInfoCircle = faInfoCircle;
  faExclamationCircle = faExclamationCircle;
  faSpinner = faSpinner;
  faSave = faSave;
  faPlus = faPlus;
  faTimes = faTimes;
  faSync = faSync;
  faEdit = faEdit;
  faObjectGroup = faObjectGroup;
  faFileWord = faFileWord;
  faSearch = faSearch;
  faQuestionCircle = faQuestionCircle;
  faCopy = faCopy;
  faSeedling = faSeedling;
  faTrashAlt = faTrashAlt;
  faMapMarkedAlt = faMapMarkedAlt;
  faLayerGroup = faLayerGroup;
  faCloudSunRain = faCloudSunRain;
  faMapMarkerAlt = faMapMarkerAlt;
  faLongArrowAltRight = faLongArrowAltRight;
  faTractor = faTractor;
  faMap = faMap;

  soildoc: Soildoc;
  typeofstudy: Typeofstudy;
  errors: { [key: string]: string } = {};
  siteErrors: [{[key: string]: string}] = [{}];
  errorsCrop: { [key: string]: string } = {};
  layerErrors: [{[key: string]: string}] = [{}];
  addpropertyErrors: [[{[key: string]: string}]] = [[{}]];

  sites = SitesFactory.empty();
  sitesForm: UntypedFormGroup;
  cropgen = CropGenFactory.empty();
  cropgenForm: UntypedFormGroup;
  failSave: Boolean;
  failSaveCrop: Boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  isLoading: Boolean;
  isSaveLoading: Boolean = false;
  coordsLoading: Boolean = false;
  errorLoading = false;
  successSave = false;
  crops: UntypedFormArray;
  soils: UntypedFormArray;

  options_soil_classification: TreeNode[];
  options_soil_type: [TreeNode[]] = [[]];
  options_soil_type_wrb: TreeNode[];
  options_soil_type_usda: TreeNode[];
  options_soil_type_ger: TreeNode[];
  options_soil_type_fao: TreeNode[];
  options_soil_qualifiers: [TreeNode[]] = [[]];
  options_soil_specifiers_start: TreeNode[] = [];
  options_soil_specifiers: [TreeNode[]] = [[]];
  options_textures: TreeNode[];

  options_properties: TreeNode[];
  options_properties_original: any;
  options_countries: TreeNode[];
  options_climate_zone: TreeNode[];
  options_landuse: TreeNode[];

  options_management_practice: TreeNode[];
  options_management_practice_loaded: TreeNode[];
  options_management_practice_custom: TreeNode[];
  options_crops: [TreeNode[]] = [[]];
  options_crops_user: TreeNode[] = [];
  options_genkeys: TreeNode[] = [];
  options_genkeys_management: TreeNode[] = [];
  options_genkeys_landuse: TreeNode[] = [];

  maximal_soils = 20;
  maximal_qualifiers = 5;
  maximal_textures = 15;
  maximal_additional_properties = 10;
  maximal_land_use = 20;
  maximal_management_practices = 20;
  maximal_crops = 10;
  maximal_layers = 10;

  location_type_options = ['With Coordinates', 'Without Coordinates'];
  unitsNew = [];
  bulk_density_items = [];
  soil_organic_c_items = [];
  caco3_items = [];
  additional_properties_items = [[]];
  valuesLanduseCropGen = ['451', '452', '482', '483', '484', '485', '486', '487', '488', '489', '490', '491', '492'];
  landuseEnabled = [];
  pH_options = ['pH (water)', 'pH (KCl)', 'pH (CaCl2)'];
  category_options = ['Management', 'Land Use'];

  redirectUrl = '';

  qualifierUSDALink = [];
  qualifierUSDAnoLink: TreeNode;
  qualifierWRBLink = [];
  qualifierFAOnoLink: TreeNode;

  errorsGenkey: { [key: string]: string } = {};
  options_keyword: TreeNode[] = [];

  genkeyForm: UntypedFormGroup;
  genkey = GenKeyFactory.empty();

  failSaveGenKey: boolean;
  activeLayer = 0;
  layerDeleteID: number;
  layerDelete: LayerSites;

  modalRedirect : bootstrap.Modal;
  modalSave : bootstrap.Modal;
  modalCropgen : bootstrap.Modal;
  modalDeleteLayer : bootstrap.Modal;
  modalGenerateKeyword : bootstrap.Modal;

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private js: JsonService,
    private as: AdddriverService,
    private ss: SitesService,
    private siteservice: SitesoilService,
    private router: Router,
    private platformLocation: PlatformLocation,
    private cdRef: ChangeDetectorRef,
    private metaService: MetaService
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.SITES', 'SITES.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }
    if (data['typeofstudy']) {
      this.typeofstudy = data['typeofstudy'];
    }

    if (data['sites']) {
      this.isEdit = true;
      this.sites = data['sites'];
    }

    this.initCropgen();
    this.initGenKey();

    this.js.getbyData(['SoilClassification1080', 'Properties1', 'LandUse450', 'Management332',
    'unitsCompact', 'SoilTypeWRB', 'SoilTypeUSDA', 'SoilTypeQualifiersWRB',
    'SoilTypeQualifiersUSDA', 'SoilTypeQualifiersNoneUSDA', 'Geiger',
    'SoilTypeGER', 'UnitsGen', 'SoilTypeQualifiersUSDAoldgroup', 'SoilTypeFAO',
    'SoilTypeQualifiersFAO', 'SoilTypeSpecifiersWRB']).subscribe(result => {
      if (result) {
        this.options_soil_classification = this.convertObjToTreeNodes(result[0].children);
        this.options_soil_type_wrb = this.convertObjToTreeNodes(result[5].children);
        this.options_soil_type_usda = this.convertObjToTreeNodes(result[6].children);
        this.qualifierWRBLink = result[7];
        this.qualifierUSDALink = result[8];
        this.qualifierUSDALink.push(...result[13]);
        this.qualifierUSDAnoLink = new TreeNode(result[9]);
        this.qualifierFAOnoLink = new TreeNode(result[15]);
        this.options_climate_zone = this.convertObjToTreeNodes(result[10]);
        this.options_soil_type_ger = this.convertObjToTreeNodes(result[11].children);
        this.options_soil_type_fao = this.convertObjToTreeNodes(result[14].children);
        this.options_soil_specifiers_start = this.convertObjToTreeNodes(result[16]);
        this.options_keyword.push(new TreeNode(result[1]));
        this.options_keyword.push(new TreeNode(result[12]));
        this.options_textures = this.convertObjToTreeNodes(this.findByProperty(result[1], 'value', '973').children);
        if (data['sites']) {
          for (let i = 0; i < this.sites.soil.soils.length; i++) {
            if (this.sites.soil.soils[i].soil_classification_id === '1081') {
              this.options_soil_type[i] = this.options_soil_type_wrb;
              this.options_soil_specifiers[i] = this.options_soil_specifiers_start;
            } else if (this.sites.soil.soils[i].soil_classification_id === '1082') {
              this.options_soil_type[i] = this.options_soil_type_usda;
              this.options_soil_specifiers[i] = [];
            } else if (this.sites.soil.soils[i].soil_classification_id === '10840') {
              this.options_soil_type[i] = this.options_soil_type_fao;
              this.options_soil_specifiers[i] = [];
            } else {
              this.options_soil_type[i] = this.options_soil_type_ger;
              this.options_soil_specifiers[i] = [];
            }
          }
        }
        this.options_properties = this.convertObjToTreeNodes(result[1].children);
        this.options_properties_original = result[1];
        this.options_landuse = this.convertObjToTreeNodes(result[2].children);
        this.unitsNew = result[4];

        this.bulk_density_items = this.unitsNew.filter(unit => unit.id === 4);
        this.soil_organic_c_items = this.unitsNew.filter(unit => unit.id === 0 || unit.id === 1);
        this.caco3_items = this.unitsNew.filter(unit => unit.id === 0);

        this.options_management_practice_loaded = this.convertObjToTreeNodes(result[3].children);
        this.options_management_practice = this.options_management_practice_loaded.slice(0);
        this.siteservice.getAllCropsByUser().subscribe(results => {
          if (results) {
            for (let i = 0; i < results.length; i++) {
              this.options_crops_user.unshift(new TreeNode(results[i]));
            }
          }
          this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
            if (resultsgen) {
              for (let i = 0; i < resultsgen.length; i++) {
                if (resultsgen[i].category === 'Management') {
                  this.options_genkeys_management.push(new TreeNode(resultsgen[i]));
                } else if (resultsgen[i].category === 'Land Use') {
                  this.options_genkeys_landuse.push(new TreeNode(resultsgen[i]));
                }
                this.options_genkeys.push(new TreeNode(resultsgen[i]));
              }
            }
              const usergenerated = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000001',
                  'text': 'Crop Rotation',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_crops_user
                }, {
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [{
                    'value': '1000004',
                    'text': 'Management',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_management
                  },
                  {
                    'value': '1000005',
                    'text': 'Land Use',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_landuse
                  }]
                }]
              };
              const landuse = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': this.options_genkeys_landuse
              };
              this.options_management_practice.unshift(new TreeNode(usergenerated));
              this.options_landuse.unshift(new TreeNode(landuse));
              this.initSites();
              this.resetTypeArrayAddProperties();
              this.resetLandUseArray();
              this.isLoading = false;
              timer(1).subscribe(val => {
                this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));
                this.modalSave = new bootstrap.Modal(document.getElementById('modalSave'));
                this.modalCropgen = new bootstrap.Modal(document.getElementById('modalCropgen'));
                this.modalDeleteLayer = new bootstrap.Modal(document.getElementById('modalDeleteLayer'));
                this.modalGenerateKeyword = new bootstrap.Modal(document.getElementById('modalGenerateKeyword'));
              });
          });
        });
        this.options_crops.push(this.convertObjToTreeNodes(this.findByProperty(result[3], 'value', 'Main crop')));
        this.options_crops.push(this.convertObjToTreeNodes(this.findByProperty(result[3], 'value', '529')));
        this.options_crops.push(this.convertObjToTreeNodes(this.findByProperty(result[3], 'value', '600')));
        this.options_crops.push(this.convertObjToTreeNodes(this.findByProperty(result[3], 'value', '530')));
        this.options_crops.push(this.convertObjToTreeNodes(this.findByProperty(result[3], 'value', '869')));
      }
    });
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  initSites() {
    this.sitesForm = this.fb.group({
      soil: this.fb.group({
        soils: this.fb.array(this.sites.soil.soils.map(
          s => this.fb.group({
            soil_classification_id: this.fb.control(s.soil_classification_id, []),
            soil_type: this.fb.control(s.soil_type, []),
            qualifier: this.fb.array(s.qualifier.map(
              q => this.fb.group({
                name: this.fb.control(q.name, []),
                specifier: this.fb.control(q.specifier, [])
              })
            ))
          })
        )),
        layer: this.fb.array(this.sites.soil.layer.map(
          l => this.fb.group({
            layer_name: this.fb.control(l.layer_name, [Validators.required]),
            depth_from: this.fb.control(l.depth_from, [ SitesoilValidators.onlyNumber01000DOT2 ]),
            depth_to: this.fb.control(l.depth_to, [ SitesoilValidators.onlyNumber01000DOT2 ]),
            sand_content_min: this.fb.control(l.sand_content_min, [ SitesoilValidators.onlyNumber0100DOT2 ]),
            sand_content_max: this.fb.control(l.sand_content_max, [ SitesoilValidators.onlyNumber0100DOT2 ]),
            silt_content_min: this.fb.control(l.silt_content_min, [ SitesoilValidators.onlyNumber0100DOT2 ]),
            silt_content_max: this.fb.control(l.silt_content_max, [ SitesoilValidators.onlyNumber0100DOT2 ]),
            clay_content_min: this.fb.control(l.clay_content_min, [ SitesoilValidators.onlyNumber0100DOT2 ]),
            clay_content_max: this.fb.control(l.clay_content_max, [ SitesoilValidators.onlyNumber0100DOT2 ]),
            textures: this.fb.array(l.textures.map(
              text => this.fb.group({
                id: this.fb.control(text.id, [Validators.required]),
                name: this.fb.control(text.name, [])
              })
            )),
            pH_min: this.fb.control(l.pH_min, [ SitesoilValidators.onlyNumber014DOT2 ]),
            pH_max: this.fb.control(l.pH_max, [ SitesoilValidators.onlyNumber014DOT2 ]),
            pH_type: this.fb.control(l.pH_type, []),
            bulk_density_min: this.fb.control(l.bulk_density_min, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            bulk_density_max: this.fb.control(l.bulk_density_max, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            bulk_density_unit: this.fb.control(l.bulk_density_unit, []),
            bulk_density_default: this.fb.control(l.bulk_density_default, []),
            bulk_density_factor: this.fb.control(l.bulk_density_factor, []),
            organic_c_type: this.fb.control(l.organic_c_type, []),
            organic_c_min: this.fb.control(l.organic_c_min, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            organic_c_max: this.fb.control(l.organic_c_max, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            organic_c_unit: this.fb.control(l.organic_c_unit, []),
            organic_c_default: this.fb.control(l.organic_c_default, []),
            organic_c_factor: this.fb.control(l.organic_c_factor, []),
            caco3_min: this.fb.control(l.caco3_min, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            caco3_max: this.fb.control(l.caco3_max, [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
            caco3_unit: this.fb.control(l.caco3_unit, []),
            caco3_default: this.fb.control(l.caco3_default, []),
            caco3_factor: this.fb.control(l.caco3_factor, []),
            additional_properties: this.fb.array(l.additional_properties.map(
              ad => this.fb.group({
                name: this.fb.control(ad.name, []),
                id: this.fb.control(ad.id, [Validators.required]),
                type: this.fb.control(ad.type, []),
                value_min: this.fb.control(ad.value_min, [ SitesoilValidators.onlyNumberNegativeDOT5MAX15 ]),
                value_max: this.fb.control(ad.value_max, [ SitesoilValidators.onlyNumberNegativeDOT5MAX15 ]),
                unit: this.fb.control(ad.unit, []),
                unit_default: this.fb.control(ad.unit_default, []),
                factor: this.fb.control(ad.factor, [])
              }, {
                validators: [SitesValidators.minMaxValue]
              })
            ))
          }, {
            validators: [ SitesoilValidators.fromToDepth, SitesValidators.minMaxSilt,
              SitesValidators.minMaxClay, SitesValidators.minMaxpH, SitesValidators.minMaxBulk, SitesValidators.minMaxOrganic, SitesValidators.minMaxCACO3,
              SitesValidators.overallSand ]
          })
        ))
      }),
      location: this.fb.array(this.sites.location.map(
        loc => this.fb.group({
          search_field: this.fb.control(loc.search_field, []),
          latitude: this.fb.control(loc.latitude, [ SitesoilValidators.isLatitudeFormat ]),
          longitude: this.fb.control(loc.longitude, [ SitesoilValidators.isLongitudeFormat ]),
          display_name: this.fb.control({value: loc.display_name, disabled: true}, [])
        })
      )),
      climate: this.fb.group({
        // eslint-disable-next-line max-len
        mean_annualair_temperature_min: this.fb.control(this.sites.climate.mean_annualair_temperature_min, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
        mean_annualair_temperature_max: this.fb.control(this.sites.climate.mean_annualair_temperature_max, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
        // eslint-disable-next-line max-len
        annual_precipitation_sum_min: this.fb.control(this.sites.climate.annual_precipitation_sum_min, [ SitesoilValidators.onlyNumber030000DOT2 ]),
        annual_precipitation_sum_max: this.fb.control(this.sites.climate.annual_precipitation_sum_max, [ SitesoilValidators.onlyNumber030000DOT2 ]),
        climate_zone: this.fb.control(this.sites.climate.climate_zone, [ ]),
        min_temperature_min: this.fb.control(this.sites.climate.min_temperature_min, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
        min_temperature_max: this.fb.control(this.sites.climate.min_temperature_max, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
        max_temperature_min: this.fb.control(this.sites.climate.max_temperature_min, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
        max_temperature_max: this.fb.control(this.sites.climate.max_temperature_max, [ SitesoilValidators.onlyNumberNegative50DOT2 ]),
      }, {
        validators: [SitesValidators.minMaxMinTemp, SitesValidators.minMaxMaxTemp, SitesValidators.minMaxMeanTemp, SitesValidators.minMaxPrecSum]
      }),
      landuse: this.fb.group({
        land_use: this.fb.array(this.sites.landuse.land_use.map(
          lu => this.fb.group({
            land_use_name: this.fb.control(lu.land_use_name, []),
            land_use_id: this.fb.control(lu.land_use_id, [Validators.required]),
          })
        )),
        management_practice: this.fb.array(this.sites.landuse.management_practice.map(
          mp => this.fb.group({
            id: this.fb.control(mp.id, [Validators.required]),
            name: this.fb.control(mp.name, [])
          })
        ))
      })
    });

    this.resetAddPropertyErrorArray();
    this.resetSiteErrors();
    this.sitesForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  updateErrorMessages() {
    this.errors = {};
    this.resetAddPropertyErrorArray();
    this.resetSiteErrors();
    this.resetLayerErrors();
    for (const message of SitesErrorMessages) {
      let control = this.sitesForm.get(message.forControl);
      if (!control) {
        control = this.sitesForm.controls['soil'].get(message.forControl);
        if (!control) {
          control = this.sitesForm.controls['climate'].get(message.forControl);
        }
        if (!control) {
          control = this.sitesForm.controls['landuse'].get(message.forControl);
        }
        if (!control) {
          if (!control) {
            for (let i = 0; i < this.sitesForm.controls['location']['controls'].length ; i++) {
              control = this.sitesForm.controls['location']['controls'][i].get(message.forControl);
              if (control &&
                control.dirty &&
                control.invalid &&
                !control['controls'] &&
                control.errors[message.forValidator] &&
                !this.siteErrors[i][message.forControl]) {
                this.siteErrors[i][message.forControl] = message.text;
              }
            }
            if (!control) {
              for (let i = 0; i < this.sitesForm.controls['soil']['controls']['layer']['controls'].length ; i++) {
                control = this.sitesForm.controls['soil']['controls']['layer']['controls'][i].get(message.forControl);
                if (!control) {
                  // eslint-disable-next-line max-len
                  for (let j = 0; j < this.sitesForm.controls['soil']['controls']['layer']['controls'][i]['controls']['additional_properties']['controls'].length ; j++) {
                    // eslint-disable-next-line max-len
                    control = this.sitesForm.controls['soil']['controls']['layer']['controls'][i]['controls']['additional_properties']['controls'][j].get(message.forControl);
                    if (control &&
                      control.invalid &&
                      !control['controls'] &&
                      control.errors[message.forValidator] &&
                      !this.addpropertyErrors[i][j][message.forControl]) {
                      this.addpropertyErrors[i][j][message.forControl] = message.text;
                    }
                  }
                } else {
                  if (control &&
                    control.dirty &&
                    control.invalid &&
                    !control['controls'] &&
                    control.errors[message.forValidator] &&
                    !this.layerErrors[i][message.forControl]) {
                    this.layerErrors[i][message.forControl] = message.text;
                  }
                }
              }
            }
          }
        } else {
          if (control &&
            control.invalid &&
            !control['controls'] &&
            control.errors[message.forValidator] &&
            !this.errors[message.forControl]) {
            this.errors[message.forControl] = message.text;
          }
        }
    }
  }
}

resetSiteErrors() {
  this.siteErrors = [{}];
  for (let i = 0; i < this.sitesForm.controls['location']['controls'].length; i++) {
    this.siteErrors[i] = {latitude: '', longitude: ''
  };
  }
}

resetLayerErrors() {
  this.layerErrors = [{}];
  for (let i = 0; i < this.sitesForm.controls['soil']['controls']['layer']['controls'].length ; i++) {
    this.layerErrors[i] = {
      depth_from: '', depth_to: '',
      sand_content_min: '', sand_content_max: '',
      silt_content_min: '', silt_content_max: '',
      clay_content_min: '', clay_content_max: '',
      pH_min: '', pH_max: '',
      bulk_density_min: '', bulk_density_max: '',
      organic_c_min: '', organic_c_max: '',
      caco3_min: '', caco3_max: ''
    };
  }
}

  resetAddPropertyErrorArray() {
    this.addpropertyErrors = [[{}]];
    for (let i = 0; i < this.sitesForm.controls['soil']['controls']['layer']['controls'].length ; i++) {
      this.addpropertyErrors[i] = [{}];
      for (let j = 0; j < this.sitesForm.controls['soil']['controls']['layer']['controls'][i]['controls']['additional_properties']['controls'].length ; j++) {
        this.addpropertyErrors[i][j] = {
          name: '', type: '', value_min: '', value_max: '', unit: ''
        };
      }
    }

  }

  submitFormSites(redirect?: string) {
    if (this.sitesForm.valid) {
      this.isSaveLoading = true;
      const sites = SitesFactory.fromObject(this.sitesForm.getRawValue());
      sites.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          sites._id = this.sites._id;
          this.ss.update(sites)
            .subscribe(
                datas => {
                  this.isSaveLoading = false;
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.sites = datas;
                    this.successSave = true;
                    this.initSites();
                    window.scrollTo(0, 0);
                    if (redirect && redirect !== '') {
                      this.router.navigateByUrl(redirect);
                    } else {
                      const source = timer(3000);
                      source.subscribe(val => {
                        this.successSave = false;
                      });
                    }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.ss.create(sites)
          .subscribe(
              datas => {
                this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.sites = datas;
                  this.initSites();
                  this.isEdit = true;
                  this.successSave = true;
                  window.scrollTo(0, 0);
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  resetTypeArrayAddProperties() {
    // eslint-disable-next-line max-len
    for (let i = 0; i < this.sitesForm.controls['soil']['controls']['layer']['controls'].length ; i++) {
      this.additional_properties_items[i] = [[]];
      for (let j = 0; j < this.sitesForm.controls['soil']['controls']['layer']['controls'][i]['controls']['additional_properties']['controls'].length ; j++) {
        this.actualizeTypeArrayAddProperties(i, j);
      }
    }
  }

  actualizeTypeArrayAddProperties(layerIndex: number, addpropIndex: number) {
    if (!this.additional_properties_items[layerIndex]) {
      this.additional_properties_items[layerIndex] = [];
    }
    if (!this.additional_properties_items[layerIndex][addpropIndex]) {
      this.additional_properties_items[layerIndex][addpropIndex] = {};
    }

    const propitem = this.findByProperty(this.options_properties_original, 'id',
    this.sitesForm.value.soil.layer[layerIndex].additional_properties[addpropIndex].id);
    if (propitem) {
      const unitIds = propitem.unitsId;
      const unitItems = this.unitsNew.filter(unit => propitem.unitsId.includes(unit.id));
      const types = [];

      for (const unit of unitItems) {
        if (unit.type) {
          types.push(unit.type);
        }
      }

      if ((unitIds.length === 0)  || (this.additional_properties_items[layerIndex][addpropIndex].units &&
      this.additional_properties_items[layerIndex][addpropIndex].units.length > 0 &&
      !unitItems.some(unit => unit.units.some(singleunit =>
        singleunit.value === this.sitesForm.value.soil.layer[layerIndex].additional_properties[addpropIndex].unit)))) {
        this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['unit'].setValue('');
        this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['factor'].setValue('');
        this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['unit_default'].setValue('');
        this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['type'].setValue('');
      }
      this.additional_properties_items[layerIndex][addpropIndex] = {
        units: unitIds,
        unitItems: unitItems,
        types: types
      };

      if (types.length === 0) {
        this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['type'].setValue('');
      }
    }
  }

  resetUnitFactor(element: string, secelement: string, thirelement: string, layerIndex: number) {
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls'][element].setValue(null);
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls'][secelement].setValue('');
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls'][thirelement].setValue('');
  }

  updateFactorByElement(unitForm: string, element: string, secelement: string, layerIndex: number) {
    const singleitem = this.unitsNew.find(item => item.units.some(unit => unit.value === unitForm));
    const singleunit = singleitem.units.find(unit => unit.value === unitForm);
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls'][element].setValue(
    singleunit && singleunit.factor ? singleunit.factor : null);
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls'][secelement].setValue(
    singleitem && singleitem.default ? singleitem.default : '');
  }

  updateFactorAddProp(unitForm: string, layerIndex: number, addpropIndex: number) {
    const singleitem = this.unitsNew.find(item => item.units.some(unit => unit.value === unitForm));
    const singleunit = singleitem.units.find(unit => unit.value === unitForm);
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']
        ['additional_properties']['controls'][addpropIndex]['controls']['factor'].setValue(
      singleunit && singleunit.factor ? singleunit.factor : null);
      this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']
    ['additional_properties']['controls'][addpropIndex]['controls']['unit_default'].setValue(
    singleitem && singleitem.default ? singleitem.default : '');
  }

  resetUnitFactorAddProp(layerIndex: number, addpropIndex: number) {
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['factor'].setValue(null);
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['unit_default'].setValue('');
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['unit'].setValue('');
  }

  addSoil() {
    this.sitesForm.controls['soil']['controls']['soils'].push(
      this.fb.group({
        soil_classification_id: this.fb.control('', []),
        soil_type: this.fb.control('', []),
        qualifier: this.fb.array([])
      }));
    this.sitesForm.markAsDirty();
    this.cdRef.detectChanges();
    this.updateErrorMessages();
    this.resetQualifiers();
  }

  removeSoil(soilIndex: number) {
    this.sitesForm.controls['soil']['controls']['soils'].removeAt(soilIndex);
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
    this.resetQualifiers();
  }

  addQualifier(soilIndex: number) {
    this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].push(
      this.fb.group({
        name: this.fb.control('', []),
        specifier: this.fb.control('', [])
      }));
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeQualifier(soilIndex: number, qualifierIndex: number) {
    this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].removeAt(qualifierIndex);
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  resetSpecifier(soilIndex: number, qualifierIndex: number) {
    // eslint-disable-next-line max-len
    this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier']['controls'][qualifierIndex]['controls']['specifier'].setValue('');
    this.sitesForm.markAsDirty();
  }

  resetClassification(soilIndex: number) {
    this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['soil_classification_id'].setValue('');
    this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex].reset();
    this.sitesForm.markAsDirty();
  }

  resetClimateZone() {
    this.sitesForm.controls['climate']['controls']['climate_zone'].setValue('');
    this.sitesForm.markAsDirty();
  }

  addLayer() {
    this.sitesForm.controls['soil']['controls']['layer'].push(
      this.fb.group({
        layer_name: this.fb.control('Layer' + (this.sitesForm.controls['soil']['controls']['layer'].length + 1), [Validators.required]),
        depth_from: this.fb.control('', [ SitesoilValidators.onlyNumber01000DOT2 ]),
        depth_to: this.fb.control('', [ SitesoilValidators.onlyNumber01000DOT2 ]),
        sand_content_min: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        sand_content_max: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        silt_content_min: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        silt_content_max: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        clay_content_min: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        clay_content_max: this.fb.control('', [ SitesoilValidators.onlyNumber0100DOT2 ]),
        textures: this.fb.array([], []),
        pH_min: this.fb.control('', [ SitesoilValidators.onlyNumber014DOT2 ]),
        pH_max: this.fb.control('', [ SitesoilValidators.onlyNumber014DOT2 ]),
        pH_type: this.fb.control('', []),
        bulk_density_min: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        bulk_density_max: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        bulk_density_unit: this.fb.control('', []),
        bulk_density_default: this.fb.control('', []),
        bulk_density_factor: this.fb.control('', []),
        organic_c_type: this.fb.control('', []),
        organic_c_min: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        organic_c_max: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        organic_c_unit: this.fb.control('', []),
        organic_c_default: this.fb.control('', []),
        organic_c_factor: this.fb.control('', []),
        caco3_min: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        caco3_max: this.fb.control('', [ SitesoilValidators.onlyNumberDOT5MAX15 ]),
        caco3_unit: this.fb.control('', []),
        caco3_default: this.fb.control('', []),
        caco3_factor: this.fb.control('', []),
        additional_properties: this.fb.array([])
      }, {
        validators: [ SitesoilValidators.fromToDepth, SitesValidators.minMaxSilt,
          SitesValidators.minMaxClay, SitesValidators.minMaxpH, SitesValidators.minMaxBulk, SitesValidators.minMaxOrganic, SitesValidators.minMaxCACO3,
          SitesValidators.overallSand ]
      }));
    this.activeLayer = this.sitesForm.controls['soil']['controls']['layer'].length - 1;
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeLayer(siteIndex: number, layerIndex: number) {
    this.layerDeleteID = layerIndex;
    this.layerDelete = this.sitesForm.value.soil.layer[layerIndex];
    this.modalDeleteLayer.show();
  }

  deleteLayer() {
    if (this.activeLayer === this.layerDeleteID) {
      this.activeLayer = 0;
    }
    this.sitesForm.controls['soil']['controls']['layer'].removeAt(this.layerDeleteID);
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  changeLayer(layerIndex: number) {
    if (this.activeLayer !== layerIndex) {
      this.activeLayer = layerIndex;
    }
  }

  addTexture(layerIndex: number) {
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['textures'].push(
      this.fb.group({
        id: this.fb.control('', [Validators.required]),
        name: this.fb.control('', [])
      }));
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeTexture(layerIndex: number, textureIndex: number) {
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['textures'].removeAt(textureIndex);
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  onValueChangeTexture(node: TreeNode, layerIndex: number, textureIndex: number) {
    if (node.value !== undefined) {
      this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']
      ['textures']['controls'][textureIndex]['controls']['name'].setValue(node.text);
    } else {
      this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['textures']
      ['controls'][textureIndex]['controls']['name'].setValue('');
    }
  }

  addAdditionalProperty(layerIndex: number) {
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties'].push(
    this.fb.group({
      name: this.fb.control('', [ ]),
      id: this.fb.control('', [Validators.required]),
      type: this.fb.control('', []),
      value_min: this.fb.control('', [SitesoilValidators.onlyNumberNegativeDOT5MAX15]),
      value_max: this.fb.control('', [SitesoilValidators.onlyNumberNegativeDOT5MAX15]),
      unit: this.fb.control('', []),
      unit_default: this.fb.control('', []),
      factor: this.fb.control('', [])
    }, {
      validators: [SitesValidators.minMaxValue]
    }));
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeAdditionalProperty(layerIndex: number, addPropIndex: number) {
    this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties'].removeAt(addPropIndex);
    this.resetTypeArrayAddProperties();
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  addLandUse(id?: string) {
    this.sitesForm.controls['landuse']['controls']['land_use'].push(
      this.fb.group({
        land_use_id: this.fb.control(id ? id : '', [Validators.required]),
        land_use_name: this.fb.control('', []),
      }));
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeLandUse(index: number) {
    this.sitesForm.controls['landuse']['controls']['land_use'].removeAt(index);
    this.resetLandUseArray();
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  addManagementPractice(id?: string) {
    this.sitesForm.controls['landuse']['controls']['management_practice'].push(
      this.fb.group({
        id: this.fb.control(id ? id : '', [Validators.required]),
        name: this.fb.control('', [])
      }));
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeManagementPractice(index: number) {
    this.sitesForm.controls['landuse']['controls']['management_practice'].removeAt(index);
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  addLocation() {
    const locarr = <UntypedFormArray>this.sitesForm.controls['location'];
    locarr.push(
      this.fb.group({
        search_field: this.fb.control('', []),
        latitude: this.fb.control('', [ SitesoilValidators.isLatitudeFormat ]),
        longitude: this.fb.control('', [ SitesoilValidators.isLongitudeFormat ]),
        display_name: this.fb.control({value: '', disabled: true}, [])
      }));
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  removeLocation(locationIndex: number) {
    const locarr = <UntypedFormArray>this.sitesForm.controls['location'];
    locarr.removeAt(locationIndex);
    this.sitesForm.markAsDirty();
    this.updateErrorMessages();
  }

  updateCoords(locationIndex: number) {
    this.coordsLoading = true;
    if (this.sitesForm.value.location[locationIndex].search_field && this.sitesForm.value.location[locationIndex].search_field !== '') {
      this.siteservice.getLocationByString(this.sitesForm.value.location[locationIndex].search_field)
    .subscribe(datas => {
      if (datas.length && datas.length === 1) {
        if (datas[0].lat && datas[0].lat !== '') {
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['latitude'].setValue(datas[0].lat.substring(0, 8));
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['latitude'].markAsDirty();
        } else {
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['latitude'].setValue('');
        }
        if (datas[0].lon && datas[0].lon !== '') {
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['longitude'].setValue(datas[0].lon.substring(0, 8));
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['longitude'].markAsDirty();
        } else {
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['longitude'].setValue('');
        }
        if (datas[0].display_name && datas[0].display_name !== '') {
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['display_name'].setValue(datas[0].display_name);
        } else {
          this.sitesForm.controls['location']['controls'][locationIndex]['controls']['display_name'].setValue('');
        }
      } else {
        this.sitesForm.controls['location']['controls'][locationIndex]['controls']['latitude'].setValue('');
        this.sitesForm.controls['location']['controls'][locationIndex]['controls']['longitude'].setValue('');
        this.sitesForm.controls['location']['controls'][locationIndex]['controls']['display_name'].setValue('');
      }
      timer(1000).subscribe(val => {
        this.coordsLoading = false;
      });
    });
    }
  }

  resetDisplayName(locationIndex: number) {
    this.sitesForm.controls['location']['controls'][locationIndex]['controls']['display_name'].setValue('');
    this.sitesForm.markAsDirty();
  }

  initCropgen() {
    this.crops = null;
    this.cropgenForm = null;
    this.crops = this.fb.array([],
      [ Validators.required ], SitesoilValidators.croprotationAvailable(this.siteservice)
      );

    this.cropgenForm = this.fb.group({
      crops: this.crops
    });

    this.cropgenForm.statusChanges.subscribe(() => this.updateErrorMessagesCrop());
  }

  addCrop() {
    this.crops.push(
      this.fb.group({
        name: this.fb.control('', [ Validators.required ])
      }));
    this.cropgenForm.markAsDirty();
  }

  removeCrop(index: number) {
    this.crops.removeAt(index);
    this.cropgenForm.markAsDirty();
  }

  cropGenerator() {
    this.modalCropgen.show();
  }

  openModalSave() {
    this.modalSave.show();
  }

  submitFormCropgen() {
    if (this.cropgenForm.valid) {
      const cropgen = CropGenFactory.fromObject(this.cropgenForm.value);
      this.siteservice.createCrop(cropgen)
          .subscribe(
              datas => {
                  this.failSaveCrop = false;
                  this.modalCropgen.hide();
                  this.options_management_practice[0].children[0].children.unshift(new TreeNode(datas));
                  this.options_management_practice = this.options_management_practice.slice();
                  this.addManagementPractice(datas.value);
                  this.initCropgen();
              },
              error => this.failSaveCrop = true
          );
    }
  }

  updateErrorMessagesCrop() {
    this.errorsCrop = {};
    for (const message of CropErrorMessages) {
      const control = this.cropgenForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors &&
        control.errors[message.forValidator]) {
        this.errorsCrop[message.forControl] = message.text;
      }
    }
  }

  initGenKey() {
    this.genkeyForm = this.fb.group({
      name: this.fb.control('', Validators.required, AdddriverValidators.genkeyAvailable(this.as)),
      isNumeric: this.fb.control(false),
      category: this.fb.control('', Validators.required)
    });
    this.genkeyForm.statusChanges.subscribe(() => this.updateErrorMessagesGenkey());
  }

  updateErrorMessagesGenkey() {
    this.errorsGenkey = {};
    for (const message of GenkeyErrorMessages) {
      const control = this.genkeyForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errorsGenkey[message.forControl]) {
        this.errorsGenkey[message.forControl] = message.text;
      }
    }
  }

  genkeyGenerator() {
    this.modalGenerateKeyword.show();
  }

  submitFormGenkey() {
    if (this.genkeyForm.valid) {
      const genkey = GenKeyFactory.fromObject(this.genkeyForm.value);
      this.as.createGenKey(genkey)
          .subscribe(
              datas => {
                  this.failSaveGenKey = false;
                  this.modalGenerateKeyword.hide();
                  if (datas.category === 'Management') {
                    this.options_management_practice[0].children[1].children[0].children.unshift(new TreeNode(datas));
                  } else if (datas.category === 'Land Use') {
                    this.options_landuse[0].children.unshift(new TreeNode(datas));
                    this.options_management_practice[0].children[1].children[1].children.unshift(new TreeNode(datas));
                    this.options_landuse = this.options_landuse.slice();
                  }
                  this.options_management_practice = this.options_management_practice.slice();
                  this.initGenKey();
              },
              error => this.failSaveGenKey = true
          );
    }
  }

  checkifsaved(routerlink: string) {
    if (this.sitesForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalSave.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }

  onValueChangeClassification(obj: any, soilIndex: number) {
    if (obj.value) {
      if (obj.value === '1081') {
        this.options_soil_type[soilIndex] = this.options_soil_type_wrb;
        this.options_soil_specifiers[soilIndex] = this.options_soil_specifiers_start;
      } else if (obj.value === '1082') {
        this.options_soil_type[soilIndex] = this.options_soil_type_usda;
        this.options_soil_specifiers[soilIndex] = [];
      } else if (obj.value === '10840') {
        this.options_soil_type[soilIndex] = this.options_soil_type_fao;
        this.options_soil_specifiers[soilIndex] = [];
      } else {
        this.options_soil_type[soilIndex] = this.options_soil_type_ger;
        this.options_soil_specifiers[soilIndex] = [];
        this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].reset();
        this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].clear();
      }
    } else {
      this.options_soil_type[soilIndex] = this.options_soil_type_ger;
      this.options_soil_specifiers[soilIndex] = [];
      this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].reset();
      this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].clear();
    }
  }

  onValueChangeType(obj: any, soilIndex: number) {
    this.options_soil_qualifiers[soilIndex] = [];
    if (obj.value && this.sitesForm.value.soil.soils[soilIndex].soil_classification_id !== '') {
      if (this.sitesForm.value.soil.soils[soilIndex].soil_classification_id === '1081') {
        const qualifierArr = this.qualifierWRBLink.filter(qualifier => qualifier.value === obj.value);
        const qualifierItemsPrincipal = [];
        const qualifierItemsSupplementary = [];
        for (let i = 0; i < qualifierArr[0].supplementary_qualifiers.length; i++) {
          qualifierItemsSupplementary.push(new TreeNode({
            'value': qualifierArr[0].supplementary_qualifiers[i],
            'text': qualifierArr[0].supplementary_qualifiers[i],
            'collapsed': true,
            'checked': false,
          }));
        }
        for (let i = 0; i < qualifierArr[0].principal_qualifiers.length; i++) {
          qualifierItemsPrincipal.push(new TreeNode({
            'value': qualifierArr[0].principal_qualifiers[i],
            'text': qualifierArr[0].principal_qualifiers[i],
            'collapsed': true,
            'checked': false,
          }));
        }
        const principalTreeview = {
          'value': 'Principal Qualifiers',
          'text': 'Principal Qualifiers',
          'collapsed': true,
          'checked': false,
          'children': qualifierItemsPrincipal
        };
        const supplementaryTreeview = {
          'value': 'Supplementary Qualifiers',
          'text': 'Supplementary Qualifiers',
          'collapsed': true,
          'checked': false,
          'children': qualifierItemsSupplementary
        };
        if (qualifierItemsPrincipal.length > 0) {
          this.options_soil_qualifiers[soilIndex].push(new TreeNode(principalTreeview));
        }
        if (qualifierItemsSupplementary.length > 0) {
          this.options_soil_qualifiers[soilIndex].push(new TreeNode(supplementaryTreeview));
        }
      } else if (this.sitesForm.value.soil.soils[soilIndex].soil_classification_id === '1082') {
        const qualifierArr = this.qualifierUSDALink.filter(qualifier => qualifier.greatgroup.includes(obj.value));
        const qualifierItems = [];
        for (let i = 0; i < qualifierArr.length; i++) {
          qualifierItems.push(new TreeNode({
            'value': qualifierArr[i].value,
            'text': qualifierArr[i].value,
            'collapsed': true,
            'checked': false,
          }));
        }
        const adjectiveTreeview = {
          'value': 'Adjectives',
          'text': 'Adjectives',
          'collapsed': true,
          'checked': false,
          'children': qualifierItems
        };
        if (qualifierItems.length > 0) {
          this.options_soil_qualifiers[soilIndex].push(new TreeNode(adjectiveTreeview));
        }
        this.options_soil_qualifiers[soilIndex].push(this.qualifierUSDAnoLink);
      } else if (this.sitesForm.value.soil.soils[soilIndex].soil_classification_id === '10840') {
        this.options_soil_qualifiers[soilIndex].push(this.qualifierFAOnoLink);
      } else {
        this.options_soil_qualifiers[soilIndex] = [];
        this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].reset();
        this.sitesForm.controls['soil']['controls']['soils']['controls'][soilIndex]['controls']['qualifier'].clear();
      }
    }
  }

  resetQualifiers() {
    for (let i = 0; i < this.sitesForm.controls['soil']['controls']['soils']['controls'].length; i++) {
      if (this.sitesForm.value.soil.soils[i].soil_classification_id && this.sitesForm.value.soil.soils[i].soil_classification_id !== '') {
        this.onValueChangeClassification({value: this.sitesForm.value.soil.soils[i].soil_classification_id}, i);
        this.onValueChangeType({value: this.sitesForm.value.soil.soils[i].soil_type}, i);
      }
    }
  }

  onValueChangeAdditionalProperty(node: TreeNode, layerIndex: number, addpropIndex: number) {
    this.actualizeTypeArrayAddProperties(layerIndex, addpropIndex);
    if (node.value !== undefined) {
      this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['name'].setValue(node.text);
    } else {
      this.sitesForm.controls['soil']['controls']['layer']['controls'][layerIndex]['controls']['additional_properties']['controls'][addpropIndex]['controls']['name'].setValue('');
    }
  }

  onValueChangeLandUse(obj: any, landIndex: number) {
    this.landuseEnabled[landIndex] = this.valuesLanduseCropGen.includes(obj.value);
    if (obj.value !== undefined) {
      this.sitesForm.controls['landuse']['controls']['land_use']['controls'][landIndex]['controls']['land_use_name'].setValue(obj.text);
    } else {
      this.sitesForm.controls['landuse']['controls']['land_use']['controls'][landIndex]['controls']['land_use_name'].setValue('');
    }
  }

  onValueChangeManPractice(obj: any, manIndex: number) {
    if (obj.value !== undefined) {
      this.sitesForm.controls['landuse']['controls']['management_practice']['controls'][manIndex]['controls']['name'].setValue(obj.text);
    } else {
      this.sitesForm.controls['landuse']['controls']['management_practice']['controls'][manIndex]['controls']['name'].setValue('');
    }
  }

  resetLandUseArray() {
    for (let i = 0; i < this.sitesForm.controls['landuse']['controls']['land_use']['controls'].length; i++) {
      this.landuseEnabled[i] = this.valuesLanduseCropGen.includes(this.sitesForm.controls['landuse']['controls']['land_use']['controls'][i]['controls']['land_use_id']);
    }
  }

  convertObjToTreeNodes(arr: []) {
    const treeViewObj = [];
    for (let i = 0; i < arr.length; i++) {
      treeViewObj.push(new TreeNode(arr[i]));
    }
    return treeViewObj;
  }

}
