import { LayerSites } from './LayerSites';
import { Soils } from './Soils';
export class Soil {
  constructor(
    public soils: Soils[],
    public layer: LayerSites[]
   ) {}
}
