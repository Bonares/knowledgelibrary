import { Sites } from './Sites';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AuthGuard } from './../../../../shared/auth.guard';
import { SitesService } from './sites.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SitesResolverService {

  constructor(private ss: SitesService,
    private ag: AuthGuard
     ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Sites> {
    if (this.ag.canActivateMethod()) {
    return this.ss.getOneBySoildoc(route.params['id']);
    }
  }
}
