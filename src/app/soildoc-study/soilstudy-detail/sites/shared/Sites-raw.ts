import { Soil } from './Soil';
import { Location } from '../../site-soil/shared/Location';
import { ClimateSites } from './ClimateSites';
import { LanduseSites } from './LanduseSites';
export interface SitesRaw {
  _id: string;
  soil: Soil;
  location: Location;
  climate: ClimateSites;
  landuse: LanduseSites;
  soildoc_id: string;
}
