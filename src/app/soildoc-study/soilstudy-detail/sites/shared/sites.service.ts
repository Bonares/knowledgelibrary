import { Inject, Injectable } from '@angular/core';
import { AuthGuard } from './../../../../shared/auth.guard';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { SitesFactory } from './SitesFactory';
import { SitesRaw } from './Sites-raw';
import { timeout, catchError, retry, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Sites } from './Sites';
@Injectable({
  providedIn: 'root'
})
export class SitesService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Sites> {
    return this.httpClient
      .get<SitesRaw>(`${this.api}/sites/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawSites => SitesFactory.fromObject(rawSites),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Sites> {
    return this.httpClient
      .get<SitesRaw>(`${this.api}/sites/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawSites => SitesFactory.fromObject(rawSites),
        ),
        catchError(this.handleError)
      );
  }


  create(sites: Sites): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/sites`, sites)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(sites: Sites): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/sites/${sites._id}`, sites)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
