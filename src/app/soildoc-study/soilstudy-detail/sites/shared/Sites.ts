import { LanduseSites } from './LanduseSites';
import { ClimateSites } from './ClimateSites';
import { Location } from './../../site-soil/shared/Location';
import { Soil } from './Soil';
export class Sites {
  constructor(
    public _id: string,
    public soil: Soil,
    public location: Location[],
    public climate: ClimateSites,
    public landuse: LanduseSites,
    public soildoc_id: string
   ) {}
}
