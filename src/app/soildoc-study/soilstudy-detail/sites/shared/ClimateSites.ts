export class ClimateSites {
  constructor(
    public mean_annualair_temperature_min: number,
    public mean_annualair_temperature_max: number,
    public annual_precipitation_sum_min: number,
    public annual_precipitation_sum_max: number,
    public climate_zone: string,
    public min_temperature_min: number,
    public min_temperature_max: number,
    public max_temperature_min: number,
    public max_temperature_max: number
   ) {}
}
