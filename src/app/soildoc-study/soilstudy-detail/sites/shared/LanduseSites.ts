import { ManagementPractice } from './../../site-soil/shared/ManagementPractice';
import { SingleLanduse } from './SingleLanduse';
export class LanduseSites {
  constructor(
    public land_use: SingleLanduse[],
    public management_practice: ManagementPractice[]
   ) {}
}
