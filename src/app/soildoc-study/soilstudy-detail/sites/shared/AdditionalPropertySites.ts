export class AdditionalPropertySites {
  constructor(
    public name: string,
    public id: string,
    public type: string,
    public value_min: number,
    public value_max: number,
    public unit: string,
    public unit_default: string,
    public factor: number
   ) {}
}
