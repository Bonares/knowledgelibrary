import { FormControl, AbstractControl } from '@angular/forms';
export class SitesValidators {
  static overallSand(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('sand_content_max').valid && (ac.get('sand_content_min').hasError('notvalid') || ac.get('sand_content_min').valid)) {
      if ((!isNaN(parseFloat(ac.get('sand_content_min').value))) && (!isNaN(parseFloat(ac.get('sand_content_max').value)))) {
        if (parseFloat(ac.get('sand_content_min').value) >= parseFloat(ac.get('sand_content_max').value)) {
          ac.get('sand_content_min').setErrors( {notvalid: true});
        } else {
          ac.get('sand_content_min').setErrors(null);
          return null;
        }
      } else {
        ac.get('sand_content_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxSilt(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('silt_content_max').valid && !ac.get('silt_content_min').hasError('numberFormat0100DOT2')) {
      if ((!isNaN(parseFloat(ac.get('silt_content_min').value))) && (!isNaN(parseFloat(ac.get('silt_content_max').value)))) {
        if (parseFloat(ac.get('silt_content_min').value) >= parseFloat(ac.get('silt_content_max').value)) {
          ac.get('silt_content_min').setErrors( {notvalid: true});
        } else {
          ac.get('silt_content_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('silt_content_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxClay(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('clay_content_max').valid && !ac.get('clay_content_min').hasError('numberFormat0100DOT2')) {
      if ((!isNaN(parseFloat(ac.get('clay_content_min').value))) && (!isNaN(parseFloat(ac.get('clay_content_max').value)))) {
        if (parseFloat(ac.get('clay_content_min').value) >= parseFloat(ac.get('clay_content_max').value)) {
          ac.get('clay_content_min').setErrors( {notvalid: true});
        } else {
          ac.get('clay_content_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('clay_content_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxpH(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('pH_max').valid && !ac.get('pH_min').hasError('numberFormat014DOT2')) {
      if ((!isNaN(parseFloat(ac.get('pH_min').value))) && (!isNaN(parseFloat(ac.get('pH_max').value)))) {
        if (parseFloat(ac.get('pH_min').value) >= parseFloat(ac.get('pH_max').value)) {
          ac.get('pH_min').setErrors( {notvalid: true});
        } else {
          ac.get('pH_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('pH_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxBulk(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('bulk_density_max').valid && !ac.get('bulk_density_min').hasError('numberFormatDOT5MAX15')) {
      if ((!isNaN(parseFloat(ac.get('bulk_density_min').value))) && (!isNaN(parseFloat(ac.get('bulk_density_max').value)))) {
        if (parseFloat(ac.get('bulk_density_min').value) >= parseFloat(ac.get('bulk_density_max').value)) {
          ac.get('bulk_density_min').setErrors( {notvalid: true});
        } else {
          ac.get('bulk_density_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('bulk_density_min').setErrors(null);

          return null;
      }
    } else {

      return null;
    }
  }

  static minMaxOrganic(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('organic_c_max').valid && !ac.get('organic_c_min').hasError('numberFormatDOT5MAX15')) {
      if ((!isNaN(parseFloat(ac.get('organic_c_min').value))) && (!isNaN(parseFloat(ac.get('organic_c_max').value)))) {
        if (parseFloat(ac.get('organic_c_min').value) >= parseFloat(ac.get('organic_c_max').value)) {
          ac.get('organic_c_min').setErrors( {notvalid: true});
        } else {
          ac.get('organic_c_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('organic_c_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxCACO3(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('caco3_max').valid && !ac.get('caco3_min').hasError('numberFormatDOT5MAX15')) {
      if ((!isNaN(parseFloat(ac.get('caco3_min').value))) && (!isNaN(parseFloat(ac.get('caco3_max').value)))) {
        if (parseFloat(ac.get('caco3_min').value) >= parseFloat(ac.get('caco3_max').value)) {
          ac.get('caco3_min').setErrors( {notvalid: true});
        } else {
          ac.get('caco3_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('caco3_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxValue(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('value_max').valid && !ac.get('value_min').hasError('numberFormatNegativeDOT5MAX15')) {
      if ((!isNaN(parseFloat(ac.get('value_min').value))) && (!isNaN(parseFloat(ac.get('value_max').value)))) {
        if (parseFloat(ac.get('value_min').value) >= parseFloat(ac.get('value_max').value)) {
          ac.get('value_min').setErrors( {notvalid: true});
        } else {
          ac.get('value_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('value_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxMinTemp(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('min_temperature_max').valid && !ac.get('min_temperature_min').hasError('numberFormatNegative50DOT2')) {
      if ((!isNaN(parseFloat(ac.get('min_temperature_min').value))) && (!isNaN(parseFloat(ac.get('min_temperature_max').value)))) {
        if (parseFloat(ac.get('min_temperature_min').value) >= parseFloat(ac.get('min_temperature_max').value)) {
          ac.get('min_temperature_min').setErrors( {notvalid: true});
        } else {
          ac.get('min_temperature_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('min_temperature_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxMaxTemp(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('max_temperature_max').valid && !ac.get('max_temperature_min').hasError('numberFormatNegative50DOT2')) {
      if ((!isNaN(parseFloat(ac.get('max_temperature_min').value))) && (!isNaN(parseFloat(ac.get('max_temperature_max').value)))) {
        if (parseFloat(ac.get('max_temperature_min').value) >= parseFloat(ac.get('max_temperature_max').value)) {
          ac.get('max_temperature_min').setErrors( {notvalid: true});
        } else {
          ac.get('max_temperature_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('max_temperature_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxMeanTemp(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('mean_annualair_temperature_max').valid && !ac.get('mean_annualair_temperature_min').hasError('numberFormatNegative50DOT2')) {
      if ((!isNaN(parseFloat(ac.get('mean_annualair_temperature_min').value))) && (!isNaN(parseFloat(ac.get('mean_annualair_temperature_max').value)))) {
        if (parseFloat(ac.get('mean_annualair_temperature_min').value) >= parseFloat(ac.get('mean_annualair_temperature_max').value)) {
          ac.get('mean_annualair_temperature_min').setErrors( {notvalid: true});
        } else {
          ac.get('mean_annualair_temperature_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('mean_annualair_temperature_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }

  static minMaxPrecSum(ac: AbstractControl): { [error: string]: any } {
    if (ac.get('annual_precipitation_sum_max').valid && !ac.get('annual_precipitation_sum_min').hasError('numberFormat030000DOT2')) {
      if ((!isNaN(parseFloat(ac.get('annual_precipitation_sum_min').value))) && (!isNaN(parseFloat(ac.get('annual_precipitation_sum_max').value)))) {
        if (parseFloat(ac.get('annual_precipitation_sum_min').value) >= parseFloat(ac.get('annual_precipitation_sum_max').value)) {
          ac.get('annual_precipitation_sum_min').setErrors( {notvalid: true});
        } else {
          ac.get('annual_precipitation_sum_min').setErrors(null);
          return null;
        }
      } else {
          ac.get('annual_precipitation_sum_min').setErrors(null);
          return null;
      }
    } else {
      return null;
    }
  }
}
