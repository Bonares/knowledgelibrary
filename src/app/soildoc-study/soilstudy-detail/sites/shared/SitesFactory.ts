import { LayerSites } from './LayerSites';
import { Location } from './../../site-soil/shared/Location';
import { SitesRaw } from './Sites-raw';
import { Soils } from './Soils';
import { Soil } from './Soil';
import { Sites } from './Sites';
import { ClimateSites } from './ClimateSites';
import { LanduseSites } from './LanduseSites';

export class SitesFactory {

static empty(): Sites {
  return new Sites('',
   new Soil([new Soils('', '', [])], [new LayerSites('Layer1', null, null, null, null, null, null, null, null, [],
    null, null, '', null,  null, '', '', null, '', null, null, '', '', null, null, null, '', '', null, [])] ),
   [new Location('', null, null, '')],
   new ClimateSites(null, null, null, null, '', null, null, null, null),
   new LanduseSites([], []),
   null
  );
}

static fromObject(rawSites: SitesRaw | any): Sites {
  if (rawSites) {
    return new Sites(
      rawSites._id,
      rawSites.soil,
      rawSites.location,
      rawSites.climate,
      rawSites.landuse,
      rawSites.soildoc_id
    );
  } else {
    return null;
  }
}
}
