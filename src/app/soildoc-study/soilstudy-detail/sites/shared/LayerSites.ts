import { Texture } from './Texture';
import { AdditionalPropertySites } from './AdditionalPropertySites';
export class LayerSites {
  constructor(
    public layer_name: string,
    public depth_from: number,
    public depth_to: number,
    public sand_content_min: number,
    public sand_content_max: number,
    public silt_content_min: number,
    public silt_content_max: number,
    public clay_content_min: number,
    public clay_content_max: number,
    public textures: Texture[],
    public pH_min: number,
    public pH_max: number,
    public pH_type: string,
    public bulk_density_min: number,
    public bulk_density_max: number,
    public bulk_density_unit: string,
    public bulk_density_default: string,
    public bulk_density_factor: number,
    public organic_c_type: string,
    public organic_c_min: number,
    public organic_c_max: number,
    public organic_c_unit: string,
    public organic_c_default: string,
    public organic_c_factor: number,
    public caco3_min: number,
    public caco3_max: number,
    public caco3_unit: string,
    public caco3_default: string,
    public caco3_factor: number,
    public additional_properties: AdditionalPropertySites[]
   ) {}
}
