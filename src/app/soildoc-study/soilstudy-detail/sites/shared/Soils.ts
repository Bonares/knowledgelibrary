import { Qualifier } from './../../site-soil/shared/Qualifier';
export class Soils {
  constructor(
    public soil_classification_id: string,
    public soil_type: string,
    public qualifier: Qualifier[]
  ) {}
}
