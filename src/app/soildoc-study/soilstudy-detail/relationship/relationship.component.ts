import { MetaService } from './../../../shared/meta.service';
import { Sites } from './../sites/shared/Sites';
import { SitesService } from './../sites/shared/sites.service';
import { Sitesoil } from './../site-soil/shared/Sitesoil';
import { SitesoilService } from './../site-soil/shared/sitesoil.service';
import { ManagementFactory } from './../management/shared/ManagementFactory';
import { RelationshipService } from './shared/relationship.service';
import { timer } from 'rxjs';
import { faSearch, faTrashAlt, faLongArrowAltRight, faArrowsAltH, faCaretSquareDown, faTimesCircle,
faQuestionCircle, faCheckCircle, faEdit, faPlus, faSave, faSpinner, faSync, faInfoCircle, faCopy, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import { PropertiesProcessFactory } from './../properties-process/shared/PropertiesProcessFactory';
import { Typeofstudy } from './../type-of-study/shared/Typeofstudy';
import { RelationshipFactory } from './shared/RelationshipFactory';
import { PropertiesProcess } from './../properties-process/shared/PropertiesProcess';
import { PropertiesProcessService } from './../properties-process/shared/propertiesprocess.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, UntypedFormArray, Validators } from '@angular/forms';
import { Soildoc } from './../../../soildoc/soil-doc/shared/Soildoc';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ManagementService } from '../management/shared/management.service';
import { Management } from '../management/shared/Management';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kl-relationship',
  templateUrl: './relationship.component.html',
  providers: []
})
export class RelationshipComponent implements OnInit {

  faBookOpen = faBookOpen;
  faCopy = faCopy;
  faSync = faSync;
  faSpinner = faSpinner;
  faSave = faSave;
  faPlus = faPlus;
  faEdit = faEdit;
  faCaretSquareDown = faCaretSquareDown;
  faSearch = faSearch;
  faTrashAlt = faTrashAlt;
  faLongArrowAltRight = faLongArrowAltRight;
  faArrowsAltH = faArrowsAltH;
  faTimesCircle = faTimesCircle;
  faQuestionCircle = faQuestionCircle;
  faCheckCircle = faCheckCircle;
  faInfoCircle = faInfoCircle;

  isLoading: Boolean = true;
  isSaveLoading: Boolean = false;
  soildoc: Soildoc;
  typeofstudy: Typeofstudy;
  relationship = RelationshipFactory.empty();
  propertiesprocess: PropertiesProcess;
  management: Management;
  sitesoil: Sitesoil;
  sites: Sites;
  relationshipForm: UntypedFormGroup;
  directional_relationships: UntypedFormArray;
  undirectional_relationships: UntypedFormArray;
  tableItems = [];
  failSave: boolean;
  failSaveAuthorized: Boolean;
  isEdit: Boolean;
  successSave = false;

  driverOptions: any[] = [];
  drivenOptions: any[] = [];
  undirectionalOptions: any[] = [];
  boundaryOptions: any[] = [];

  speciesDriverOptions = [[]];
  speciesDrivenOptions = [[]];
  speciesPropertyFirstOptions = [[]];
  speciesPropertyLastOptions = [[]];

  maximal_relationship = 150;
  maximal_boundaries = 3;
  maximal_species = 5;

  redirectUrl = '';

  modalRedirect : bootstrap.Modal;
  pictoOptions = [
    {label: 'chart_1.png'},
    {label: 'chart_2.png'},
    {label: 'chart_3.png'},
    {label: 'chart_4.png'},
    {label: 'chart_5.png'},
    {label: 'chart_6.png'},
    {label: 'chart_7.png'},
    {label: 'chart_8.png'},
    {label: 'chart_9.png'},
    {label: 'chart_10.png'},
    {label: 'chart_11.png'},
    {label: 'chart_12.png'},
    {label: 'chart_13.png'},
    {label: 'chart_15.png'}
  ];

  constructor(
    private fb: UntypedFormBuilder,
    private route: ActivatedRoute,
    private ps: PropertiesProcessService,
    private ms: ManagementService,
    private rs: RelationshipService,
    private ss: SitesoilService,
    private sitesService: SitesService,
    private router: Router,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILSTUDY_DETAIL.RELATIONSHIPS', 'RELATIONSHIP.DESCRIPTIONMETA');
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
    }
    if (data['typeofstudy']) {
      this.typeofstudy = data['typeofstudy'];
    }
    if (data['relationship']) {
      this.relationship = data['relationship'];
      this.isEdit = true;
    }
    this.ps.getOneBySoildoc(this.soildoc._id).subscribe(resulttt => {
      if (resulttt) {
        this.propertiesprocess = resulttt;
        for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
          this.driverOptions.push(this.propertiesprocess.properties[i]);
          this.drivenOptions.push(this.propertiesprocess.properties[i]);
          this.undirectionalOptions.push(this.propertiesprocess.properties[i]);
          if (this.propertiesprocess.properties[i].qualitiesEnabled &&
            this.propertiesprocess.properties[i].combined && this.propertiesprocess.properties[i].combined !== '') {
              if (!this.boundaryOptions.includes(this.propertiesprocess.properties[i].combined)) {
                this.boundaryOptions.push(this.propertiesprocess.properties[i].combined);
              }
          } else {
            if (!this.boundaryOptions.includes(this.propertiesprocess.properties[i].name)) {
              this.boundaryOptions.push(this.propertiesprocess.properties[i].name);
            }
          }
        }
      } else {
        this.propertiesprocess = PropertiesProcessFactory.empty();
      }
      this.ms.getOneBySoildoc(this.soildoc._id).subscribe(resultttt => {
        if (resultttt) {
          this.management = resultttt;
          for (let i = 0; i < this.management.items.length; i++) {
            this.driverOptions.push(this.management.items[i]);
            if (!this.boundaryOptions.includes(this.management.items[i].name)) {
              this.boundaryOptions.push(this.management.items[i].name);
            }
            if (this.management.items[i].reference && this.management.items[i].reference !== '' && this.management.items[i].isCategorical) {
              if (!this.boundaryOptions.includes(this.management.items[i].reference)) {
                this.boundaryOptions.push(this.management.items[i].reference);
              }
            }
          }
        } else {
          this.management = ManagementFactory.empty();
        }
        if (this.typeofstudy.name === 'Experiment' && this.typeofstudy.experimentContext &&
        (!this.typeofstudy.experimentContext.site_input_mode || this.typeofstudy.experimentContext.site_input_mode === 'Individual' )) {
          this.ss.getOneBySoildoc(this.soildoc._id).subscribe(resulttttt => {
            if (resulttttt) {
              this.sitesoil = resulttttt;
              if (this.sitesoil && this.sitesoil.sites && this.sitesoil.sites.length > 0) {
                for (let i = 0; i < this.sitesoil.sites.length; i++) {
                  if (!this.boundaryOptions.includes(this.sitesoil.sites[i].name)) {
                    this.boundaryOptions.push(this.sitesoil.sites[i].name);
                  }
                  if (this.sitesoil.sites[i].layer.length > 1) {
                    for (let j = 0; j < this.sitesoil.sites[i].layer.length; j++) {
                      if (!this.boundaryOptions.includes(this.sitesoil.sites[i].name + ' - ' + this.sitesoil.sites[i].layer[j].name_layer)) {
                        this.boundaryOptions.push(this.sitesoil.sites[i].name + ' - ' + this.sitesoil.sites[i].layer[j].name_layer);
                      }
                    }
                  }
                }
              }
            }
            this.initRelationship();
            this.resetSpeciesOptions();
            this.isLoading = false;
          });
        } else if (this.typeofstudy.name === 'Meta-analysis' || (this.typeofstudy.name === 'Experiment' && this.typeofstudy.experimentContext &&
        (this.typeofstudy.experimentContext.site_input_mode === 'Combined' ))) {
          this.sitesService.getOneBySoildoc(this.soildoc._id).subscribe(resulttttt => {
            if (resulttttt) {
              this.sites = resulttttt;
              if (this.sites && this.sites.soil.layer && this.sites.soil.layer.length > 0) {
                for (let i = 0; i < this.sites.soil.layer.length; i++) {
                  if (!this.boundaryOptions.includes(this.sites.soil.layer[i].layer_name)) {
                    this.boundaryOptions.push(this.sites.soil.layer[i].layer_name);
                  }
                }
              }
            }
            this.initRelationship();
            this.resetSpeciesOptions();
            this.isLoading = false;
          });
        } else {
          for (let i = 0; i < this.relationshipForm.value.directional_relationship.length; i++) {
            this.speciesDriverOptions[i] = [];
            this.speciesDrivenOptions[i] = [];
          }
          for (let i = 0; i < this.relationshipForm.value.undirectional_relationship.length; i++) {
            this.speciesPropertyFirstOptions[i] = [];
            this.speciesPropertyLastOptions[i] = [];
          }
          this.initRelationship();
          this.isLoading = false;
          timer(1).subscribe(val => {
            this.modalRedirect = new bootstrap.Modal(document.getElementById('modalRedirect'));
          });
        }
      });
    });
  }

  initRelationship() {
    this.directional_relationships = this.fb.array(
      this.relationship.directional_relationship.map(
        d => this.fb.group({
          driver_name: this.fb.control(d.driver_name),
          driver_unique: this.fb.control(d.driver_unique, Validators.required),
          driver_category: this.fb.control(d.driver_category),
          driver_species: this.fb.array(
            d.driver_species.map(
              ds => this.fb.group({
                name: this.fb.control(ds.name, Validators.required)
              })
            )
          ),
          driven_name: this.fb.control(d.driven_name),
          driven_unique: this.fb.control(d.driven_unique, Validators.required),
          driven_category: this.fb.control(d.driven_category),
          driven_species: this.fb.array(
            d.driven_species.map(
              ds => this.fb.group({
                name: this.fb.control(ds.name, Validators.required)
              })
            )
          ),
          reference: this.fb.control(d.reference),
          faunal_combined_driver: this.fb.control(d.faunal_combined_driver),
          faunal_combined_driven: this.fb.control(d.faunal_combined_driven),
          effect: this.fb.control(d.effect),
          boundary_condition: this.fb.array(
            d.boundary_condition.map(
              bc => this.fb.group({
                name: this.fb.control(bc.name, Validators.required)
              })
            )
          ),
          details: this.fb.control(d.details)
        })
      )
    );

    this.undirectional_relationships = this.fb.array(
      this.relationship.undirectional_relationship.map(
        u => this.fb.group({
          property_first_name: this.fb.control(u.property_first_name),
          property_first_unique: this.fb.control(u.property_first_unique, Validators.required),
          property_first_category: this.fb.control(u.property_first_category),
          property_first_species: this.fb.array(
            u.property_first_species.map(
              us => this.fb.group({
                name: this.fb.control(us.name, Validators.required)
              })
            )
          ),
          property_last_name: this.fb.control(u.property_last_name),
          property_last_unique: this.fb.control(u.property_last_unique, Validators.required),
          property_last_category: this.fb.control(u.property_last_category),
          property_last_species: this.fb.array(
            u.property_last_species.map(
              us => this.fb.group({
                name: this.fb.control(us.name, Validators.required)
              })
            )
          ),
          relation: this.fb.control(u.relation),
          faunal_combined_first: this.fb.control(u.faunal_combined_first),
          faunal_combined_last: this.fb.control(u.faunal_combined_last),
          boundary_condition: this.fb.array(
            u.boundary_condition.map(
              bc => this.fb.group({
                name: this.fb.control(bc.name, Validators.required)
              })
            )
          ),
          details: this.fb.control(u.details)
        })
      )
    );

    this.relationshipForm = this.fb.group({
      directional_relationship: this.directional_relationships,
      undirectional_relationship: this.undirectional_relationships
    });
  }

  addDirectionalRelationship(index?: number) {
    this.directional_relationships.insert(index ? index : 0,
      this.fb.group({
        driver_name: this.fb.control(''),
        driver_unique: this.fb.control('', Validators.required),
        driver_category: this.fb.control(''),
        driver_species: this.fb.array([]),
        driven_name: this.fb.control(''),
        driven_unique: this.fb.control('', Validators.required),
        driven_category: this.fb.control(''),
        driven_species: this.fb.array([]),
        reference: this.fb.control(''),
        faunal_combined_driver: this.fb.control(''),
        faunal_combined_driven: this.fb.control(''),
        effect: this.fb.control(''),
        boundary_condition: this.fb.array([]),
        details: this.fb.control('')
      })
    );
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
    this.resetSpeciesOptions();
  }

  addBoundaryDirectional(relIndex: number) {
    this.directional_relationships.controls[relIndex]['controls'].boundary_condition.push(
      this.fb.group({
        name: this.fb.control('', Validators.required)
      })
    );
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
  }

  removeBoundaryDirectional(relIndex: number, boundIndex: number) {
    this.directional_relationships.controls[relIndex]['controls'].boundary_condition.removeAt(boundIndex);
    this.relationshipForm.markAsDirty();
  }

  addSpecieDirectional(relIndex: number, element: string) {
    this.directional_relationships.controls[relIndex]['controls'][element].push(
      this.fb.group({
        name: this.fb.control('', Validators.required)
      })
    );
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
  }

  removeSpecieDirectional(relIndex: number, boundIndex: number, element: string) {
    this.directional_relationships.controls[relIndex]['controls'][element].removeAt(boundIndex);
    this.relationshipForm.markAsDirty();
  }

  addUndirectionalRelationship(index?: number) {
    this.undirectional_relationships.insert(index ? index : 0,
      this.fb.group({
        property_first_name: this.fb.control(''),
        property_first_unique: this.fb.control('', Validators.required),
        property_first_category: this.fb.control(''),
        property_first_species: this.fb.array([]),
        property_last_name: this.fb.control(''),
        property_last_unique: this.fb.control('', Validators.required),
        property_last_category: this.fb.control(''),
        property_last_species: this.fb.array([]),
        relation: this.fb.control(''),
        faunal_combined_first: this.fb.control(''),
        faunal_combined_last: this.fb.control(''),
        boundary_condition: this.fb.array([]),
        details: this.fb.control('')
      })
    );
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
    this.resetSpeciesOptions();
  }

  addBoundaryUndirectional(relIndex: number) {
    this.undirectional_relationships.controls[relIndex]['controls'].boundary_condition.push(
      this.fb.group({
        name: this.fb.control('', Validators.required)
      })
    );
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
  }

  removeBoundaryUndirectional(relIndex: number, boundIndex: number) {
    this.undirectional_relationships.controls[relIndex]['controls'].boundary_condition.removeAt(boundIndex);
    this.relationshipForm.markAsDirty();
  }

  addSpecieUndirectional(relIndex: number, element: string) {
    this.undirectional_relationships.controls[relIndex]['controls'][element].push(
      this.fb.group({
        name: this.fb.control('', Validators.required)
      })
    );
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
  }

  removeSpecieUndirectional(relIndex: number, boundIndex: number, element: string) {
    this.undirectional_relationships.controls[relIndex]['controls'][element].removeAt(boundIndex);
    this.relationshipForm.markAsDirty();
  }

  removeDirectionalRelationship(relIndex: number) {
    this.directional_relationships.removeAt(relIndex);
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
    this.resetSpeciesOptions();
  }

  removeUndirectionalRelationship(relIndex: number) {
    this.undirectional_relationships.removeAt(relIndex);
    timer(1).subscribe(val => {
      this.relationshipForm.markAsDirty();
    });
    this.resetSpeciesOptions();
  }

  copyDirectionalRelationship(relIndex: number) {
    this.addDirectionalRelationship(relIndex);
    for (const boundary of this.relationshipForm.getRawValue().directional_relationship[relIndex + 1].boundary_condition) {
      this.addBoundaryDirectional(relIndex);
    }
    for (const specie of this.relationshipForm.getRawValue().directional_relationship[relIndex + 1].driver_species) {
      this.addSpecieDirectional(relIndex, 'driver_species');
    }
    for (const specie of this.relationshipForm.getRawValue().directional_relationship[relIndex + 1].driven_species) {
      this.addSpecieDirectional(relIndex, 'driven_species');
    }
    this.directional_relationships.controls[relIndex].patchValue(this.relationshipForm.getRawValue().directional_relationship[relIndex + 1]);
    this.resetSpeciesOptions();
    updateTreeValidity(this.directional_relationships.controls[relIndex] as UntypedFormGroup);
  }

  copyUndirectionalRelationship(relIndex: number) {
    this.addUndirectionalRelationship(relIndex);
    for (const boundary of this.relationshipForm.getRawValue().undirectional_relationship[relIndex + 1].boundary_condition) {
      this.addBoundaryUndirectional(relIndex);
    }
    for (const specie of this.relationshipForm.getRawValue().undirectional_relationship[relIndex + 1].property_first_species) {
      this.addSpecieUndirectional(relIndex, 'property_first_species');
    }
    for (const specie of this.relationshipForm.getRawValue().undirectional_relationship[relIndex + 1].property_last_species) {
      this.addSpecieUndirectional(relIndex, 'property_last_species');
    }
    this.undirectional_relationships.controls[relIndex].patchValue(this.relationshipForm.getRawValue().undirectional_relationship[relIndex + 1]);
    this.resetSpeciesOptions();
    updateTreeValidity(this.undirectional_relationships.controls[relIndex] as UntypedFormGroup);
  }

  changePicto(picto: string, relationIndex: number) {
    this.directional_relationships.controls[relationIndex]['controls']['effect'].setValue(picto);
    this.relationshipForm.markAsDirty();
  }

  changePictoUD(picto: string, relationIndex: number) {
    this.undirectional_relationships.controls[relationIndex]['controls']['relation'].setValue(picto);
    this.relationshipForm.markAsDirty();
  }

  updateCategoryDirectional(id: string, index: number, element: string) {
    let found = false;
    this.directional_relationships.controls[index]['controls'][element].setValue('');
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        this.directional_relationships.controls[index]['controls'][element].setValue(
          this.propertiesprocess.properties[i].category ?
          this.propertiesprocess.properties[i].category : '');
          found = true;
          break;
      }
    }
    if (!found) {
      for (let i = 0; i < this.management.items.length; i++) {
        if (id === this.management.items[i].name || id === this.management.items[i].combined) {
          this.directional_relationships.controls[index]['controls'][element].setValue(
            this.management.items[i].category ?
            this.management.items[i].category : '');
            break;
        }
      }
    }
  }

  updateCategoryUndirectional(id: string, index: number, element: string) {
    this.undirectional_relationships.controls[index]['controls'][element].setValue('');
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        this.undirectional_relationships.controls[index]['controls'][element].setValue(
          this.propertiesprocess.properties[i].category ?
          this.propertiesprocess.properties[i].category : '');
        break;
      }
    }
  }

  updateReferenceFaunal(id: string, directional_index: number) {
    this.directional_relationships.controls[directional_index]['controls']['reference'].setValue('');
    this.directional_relationships.controls[directional_index]['controls']['faunal_combined_driver'].setValue('');

    for (let i = 0; i < this.management.items.length; i++) {
      if (id === this.management.items[i].name || id === this.management.items[i].combined) {
        this.directional_relationships.controls[directional_index]['controls']['reference'].setValue(
          this.management.items[i].isCategorical && this.management.items[i].reference !== '' ? this.management.items[i].reference : '');
          break;
      }
    }
    if (!this.relationshipForm.value.directional_relationship[directional_index].reference ||
      this.relationshipForm.value.directional_relationship[directional_index].reference === '') {
      for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
        if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
          this.directional_relationships.controls[directional_index]['controls']['faunal_combined_driver'].setValue(
            this.propertiesprocess.properties[i].qualitiesEnabled && this.propertiesprocess.properties[i].combined ?
            this.propertiesprocess.properties[i].combined : '');
            break;
        }
      }
    }
  }

  updateFaunalKeywordDirectional(id: string, index: number, element: string) {
    this.directional_relationships.controls[index]['controls'][element].setValue('');
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        this.directional_relationships.controls[index]['controls'][element].setValue(
          this.propertiesprocess.properties[i].qualitiesEnabled && this.propertiesprocess.properties[i].combined !== '' ?
          this.propertiesprocess.properties[i].combined : '');
          break;
      }
    }
  }

  updateFaunalKeywordUndirectional(id: string, index: number, element: string) {
    this.undirectional_relationships.controls[index]['controls'][element].setValue('');
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        this.undirectional_relationships.controls[index]['controls'][element].setValue(
          this.propertiesprocess.properties[i].qualitiesEnabled && this.propertiesprocess.properties[i].combined !== '' ?
          this.propertiesprocess.properties[i].combined : '');
          break;
      }
    }
  }

  updateNameDirectional(id: string, directional_index: number, element: string) {
    this.directional_relationships.controls[directional_index]['controls'][element].setValue('');

    for (let i = 0; i < this.management.items.length; i++) {
      if (id === this.management.items[i].name || id === this.management.items[i].combined) {
        this.directional_relationships.controls[directional_index]['controls'][element].setValue(
          this.management.items[i].name !== '' ? this.management.items[i].name : '');
          break;
      }
    }
    if (!this.relationshipForm.value.directional_relationship[directional_index][element] ||
      this.relationshipForm.value.directional_relationship[directional_index][element] === '') {
      for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
        if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
          this.directional_relationships.controls[directional_index]['controls'][element].setValue(
            this.propertiesprocess.properties[i].name !== '' ? this.propertiesprocess.properties[i].name : '');
            break;
        }
      }
    }
  }

  updateNameUndirectional(id: string, index: number, element: string) {
    this.undirectional_relationships.controls[index]['controls'][element].setValue('');
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        this.undirectional_relationships.controls[index]['controls'][element].setValue(
          this.propertiesprocess.properties[i].name !== '' ? this.propertiesprocess.properties[i].name : '');
          break;
      }
    }
  }

  resetSpeciesOptions() {
    for (let i = 0; i < this.relationshipForm.value.directional_relationship.length; i++) {
      this.updateSpeciesDriver(this.relationshipForm.value.directional_relationship[i].driver_unique, i);
      this.updateSpeciesDriven(this.relationshipForm.value.directional_relationship[i].driven_unique, i);
    }
    for (let i = 0; i < this.relationshipForm.value.undirectional_relationship.length; i++) {
      this.updateSpeciesPropFirst(this.relationshipForm.value.undirectional_relationship[i].property_first_unique, i);
      this.updateSpeciesPropLast(this.relationshipForm.value.undirectional_relationship[i].property_last_unique, i);
    }
  }

  updateSpeciesDriver(id: string, index: number) {
    this.speciesDriverOptions[index] = [];
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        if (this.propertiesprocess.properties[i].speciesEnabled && this.propertiesprocess.properties[i].species.length > 0) {
          for (const specie of this.propertiesprocess.properties[i].species) {
            this.speciesDriverOptions[index].push(specie.name);
          }
        }
        break;
      }
    }
    if (this.speciesDriverOptions[index].length === 0) {
      this.directional_relationships.controls[index]['controls']['driver_species'].clear();
      this.directional_relationships.controls[index]['controls']['driver_species'].reset();
    } else {
      for (let i = 0; i < this.directional_relationships.controls[index]['controls']['driver_species'].length; i++) {
        if (this.directional_relationships.controls[index]['controls']['driver_species']['controls'][i]['controls']['name'].value !== '' &&
        !this.speciesDriverOptions[index].includes(this.directional_relationships.controls[index]['controls']['driver_species']['controls'][i]['controls']['name'].value)) {
          this.directional_relationships.controls[index]['controls']['driver_species']['controls'][i]['controls']['name'].setValue('');
        }
      }
    }
  }

  updateSpeciesDriven(id: string, index: number) {
    this.speciesDrivenOptions[index] = [];
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        if (this.propertiesprocess.properties[i].speciesEnabled && this.propertiesprocess.properties[i].species.length > 0) {
          for (const specie of this.propertiesprocess.properties[i].species) {
            this.speciesDrivenOptions[index].push(specie.name);
          }
        }
        break;
      }
    }
    if (this.speciesDrivenOptions[index].length === 0) {
      this.directional_relationships.controls[index]['controls']['driven_species'].clear();
      this.directional_relationships.controls[index]['controls']['driven_species'].reset();
    } else {
      for (let i = 0; i < this.directional_relationships.controls[index]['controls']['driven_species'].length; i++) {
        if (this.directional_relationships.controls[index]['controls']['driven_species']['controls'][i]['controls']['name'].value !== '' &&
        !this.speciesDriverOptions[index].includes(this.directional_relationships.controls[index]['controls']['driven_species']['controls'][i]['controls']['name'].value)) {
          this.directional_relationships.controls[index]['controls']['driven_species']['controls'][i]['controls']['name'].setValue('');
        }
      }
    }
  }

  updateSpeciesPropFirst(id: string, index: number) {
    this.speciesPropertyFirstOptions[index] = [];
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        if (this.propertiesprocess.properties[i].speciesEnabled && this.propertiesprocess.properties[i].species.length > 0) {
          for (const specie of this.propertiesprocess.properties[i].species) {
            this.speciesPropertyFirstOptions[index].push(specie.name);
          }
        }
        break;
      }
    }
    if (this.speciesPropertyFirstOptions[index].length === 0) {
      this.undirectional_relationships.controls[index]['controls']['property_first_species'].clear();
      this.undirectional_relationships.controls[index]['controls']['property_first_species'].reset();
    } else {
      for (let i = 0; i < this.undirectional_relationships.controls[index]['controls']['property_first_species'].length; i++) {
        if (this.undirectional_relationships.controls[index]['controls']['property_first_species']['controls'][i]['controls']['name'].value !== '' &&
        !this.speciesPropertyFirstOptions[index].includes(
          this.undirectional_relationships.controls[index]['controls']['property_first_species']['controls'][i]['controls']['name'].value)) {
          this.undirectional_relationships.controls[index]['controls']['property_first_species']['controls'][i]['controls']['name'].setValue('');
        }
      }
    }
  }

  updateSpeciesPropLast(id: string, index: number) {
    this.speciesPropertyLastOptions[index] = [];
    for (let i = 0; i < this.propertiesprocess.properties.length; i++) {
      if (id === this.propertiesprocess.properties[i].name || id === this.propertiesprocess.properties[i].combined) {
        if (this.propertiesprocess.properties[i].speciesEnabled && this.propertiesprocess.properties[i].species.length > 0) {
          for (const specie of this.propertiesprocess.properties[i].species) {
            this.speciesPropertyLastOptions[index].push(specie.name);
          }
        }
        break;
      }
    }
    if (this.speciesPropertyLastOptions[index].length === 0) {
      this.undirectional_relationships.controls[index]['controls']['property_last_species'].clear();
      this.undirectional_relationships.controls[index]['controls']['property_last_species'].reset();
    } else {
      for (let i = 0; i < this.undirectional_relationships.controls[index]['controls']['property_last_species'].length; i++) {
        if (this.undirectional_relationships.controls[index]['controls']['property_last_species']['controls'][i]['controls']['name'].value !== '' &&
        !this.speciesPropertyLastOptions[index].includes(
          this.undirectional_relationships.controls[index]['controls']['property_last_species']['controls'][i]['controls']['name'].value)) {
          this.undirectional_relationships.controls[index]['controls']['property_last_species']['controls'][i]['controls']['name'].setValue('');
        }
      }
    }
  }

  submitFormRelationship(redirect?: string) {
    if (this.relationshipForm.valid) {
      this.isSaveLoading = true;
      const relationship = RelationshipFactory.fromObject(this.relationshipForm.value);
      relationship.soildoc_id = this.soildoc._id;
        if (this.isEdit) {
          relationship._id = this.relationship._id;
          this.rs.update(relationship)
            .subscribe(
                datas => {
                    this.isSaveLoading = false;
                    this.failSave = false;
                    this.failSaveAuthorized = false;
                    this.relationship = datas;
                    this.successSave = true;
                    this.initRelationship();
                    window.scrollTo(0, 0);
                    if (redirect && redirect !== '') {
                      this.router.navigateByUrl(redirect);
                    } else {
                      const source = timer(3000);
                      source.subscribe(val => {
                        this.successSave = false;
                      });
                    }
                },
                error => {
                  window.scrollTo(0, 0);
                  if (error.status === 401) {
                    this.failSaveAuthorized = true;
                  } else {
                    this.failSave = true;
                  }
                  this.isSaveLoading = false;
                }
            );
        } else {
          this.rs.create(relationship)
          .subscribe(
              datas => {
                  this.isSaveLoading = false;
                  this.failSave = false;
                  this.failSaveAuthorized = false;
                  this.relationship = datas;
                  this.initRelationship();
                  this.isEdit = true;
                  this.successSave = true;
                  window.scrollTo(0, 0);
                  if (redirect && redirect !== '') {
                    this.router.navigateByUrl(redirect);
                  } else {
                    const source = timer(3000);
                    source.subscribe(val => {
                      this.successSave = false;
                    });
                  }
              },
              error => {
                window.scrollTo(0, 0);
                if (error.status === 401) {
                  this.failSaveAuthorized = true;
                } else {
                  this.failSave = true;
                }
                this.isSaveLoading = false;
              }
          );
        }
    }
  }

  checkifsaved(routerlink: string) {
    if (this.relationshipForm.dirty) {
      this.redirectUrl = routerlink;
      this.modalRedirect.show();
    } else {
      this.router.navigateByUrl(routerlink);
    }
  }
}

/**
 * Re-calculates the value and validation status of the entire controls tree.
 */
function updateTreeValidity(group: UntypedFormGroup | UntypedFormArray): void {
  Object.keys(group.controls).forEach((key: string) => {
    const abstractControl = group.controls[key];

    if (abstractControl instanceof UntypedFormGroup || abstractControl instanceof UntypedFormArray) {
      updateTreeValidity(abstractControl);
    } else {
      abstractControl.updateValueAndValidity();
      abstractControl.markAsDirty();
    }
  });
}
