import { Species } from './Species';
import { Boundary } from './Boundary';
export class DirectionalRelationship {
  constructor(
    public driver_name: string,
    public driver_unique: string,
    public driver_category: string,
    public driver_species: Species[],
    public driven_name: string,
    public driven_unique: string,
    public driven_category: string,
    public driven_species: Species[],
    public reference: string,
    public faunal_combined_driver: string,
    public faunal_combined_driven: string,
    public effect: string,
    public boundary_condition: Boundary[],
    public details: string
   ) {}
}
