import { retry, timeout, map, catchError } from 'rxjs/operators';
import { RelationshipRaw } from './Relationship-raw';
import { throwError, Observable } from 'rxjs';
import { AuthGuard } from './../../../../shared/auth.guard';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Relationship } from './Relationship';
import { RelationshipFactory } from './RelationshipFactory';
@Injectable({
  providedIn: 'root'
})
export class RelationshipService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getOne(id: String): Observable<Relationship> {
    return this.httpClient
      .get<RelationshipRaw>(`${this.api}/relationship/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawRelationship => RelationshipFactory.fromObject(rawRelationship),
        ),
        catchError(this.handleError)
      );
  }

  getOneBySoildoc(id: String): Observable<Relationship> {
    return this.httpClient
      .get<RelationshipRaw>(`${this.api}/relationship/soildoc/${id}`)
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(15000),
        map(rawRelationship => RelationshipFactory.fromObject(rawRelationship),
        ),
        catchError(this.handleError)
      );
  }


  create(relationship: Relationship): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .post(`${this.api}/relationship`, relationship)
      .pipe(
        timeout(15000),
        catchError(this.handleError)
      );
    }
  }

  update(relationship: Relationship): Observable<any> {
    if (this.ag.canActivateMethod()) {
      return this.httpClient
      .put(`${this.api}/relationship/${relationship._id}`, relationship)
      .pipe(
        timeout(15000),
        catchError(this.handleError) // then handle the error
      );
    }
  }
}
