import { Species } from './Species';
import { Boundary } from './Boundary';
export class UndirectionalRelationship {
  constructor(
    public property_first_name: string,
    public property_first_unique: string,
    public property_first_category: string,
    public property_first_species: Species[],
    public property_last_name: string,
    public property_last_unique: string,
    public property_last_category: string,
    public property_last_species: Species[],
    public relation: string,
    public faunal_combined_first: string,
    public faunal_combined_last: string,
    public boundary_condition: Boundary[],
    public details: string
   ) {}
}
