import { UndirectionalRelationship } from './UndirectionalRelationship';
import { DirectionalRelationship } from './DirectionalRelationship';
export interface RelationshipRaw {
  _id: string;
  directional_relationship: DirectionalRelationship[];
  undirectional_relationship: UndirectionalRelationship[];
  soildoc_id: string;
}
