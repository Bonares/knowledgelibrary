import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AuthGuard } from './../../../../shared/auth.guard';
import { RelationshipService } from './relationship.service';
import { Injectable } from '@angular/core';
import { Relationship } from './Relationship';
@Injectable({
  providedIn: 'root'
})
export class RelationshipResolverService {

  constructor(private rs: RelationshipService,
    private ag: AuthGuard) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Relationship> {
    return this.rs.getOneBySoildoc(route.params['id']);
  }
}
