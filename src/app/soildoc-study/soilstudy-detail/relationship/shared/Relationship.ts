import { UndirectionalRelationship } from './UndirectionalRelationship';
import { DirectionalRelationship } from './DirectionalRelationship';
export class Relationship {
  constructor(
    public _id: string,
    public directional_relationship: DirectionalRelationship[],
    public undirectional_relationship: UndirectionalRelationship[],
    public soildoc_id: string
   ) {}
}
