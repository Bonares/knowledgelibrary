import { RelationshipRaw } from './Relationship-raw';
import { Relationship } from './Relationship';

export class RelationshipFactory {

  static empty(): Relationship {
    return new Relationship('', [], [], null);
  }

  static fromObject(rawRelationship: RelationshipRaw | any): Relationship {
    if (rawRelationship) {
      return new Relationship(
        rawRelationship._id,
        rawRelationship.directional_relationship,
        rawRelationship.undirectional_relationship,
        rawRelationship.soildoc_id
      );
    } else {
      return null;
    }
  }
  }
