import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { DropdownTreeviewModule } from './../shared/DropdownTreeview/dropdown-treeview-module';
import { SharedModule } from './../shared/shared.module';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { SoildocStudyRoutingModule } from './soildoc-study-routing';
import { SoilstudyDetailComponent } from './soilstudy-detail/soilstudy-detail.component';
import { TypeOfStudyComponent } from './soilstudy-detail/type-of-study/type-of-study.component';
import { SiteSoilComponent } from './soilstudy-detail/site-soil/site-soil.component';
import { PropertiesProcessComponent } from './soilstudy-detail/properties-process/properties-process.component';
import { MethodsComponent } from './soilstudy-detail/methods/methods.component';
import { ModelsComponent } from './soilstudy-detail/models/models.component';
import { RelationshipComponent } from './soilstudy-detail/relationship/relationship.component';
import { ManagementComponent } from './soilstudy-detail/management/management.component';
import { SitesComponent } from './soilstudy-detail/sites/sites.component';
import { ViewGenkeyComponent } from './soilstudy-detail/view-genkey/view-genkey.component';

@NgModule({
  declarations: [SoilstudyDetailComponent,
    TypeOfStudyComponent,
    SiteSoilComponent,
    ManagementComponent,
    PropertiesProcessComponent,
    MethodsComponent,
    ModelsComponent,
    RelationshipComponent,
    ManagementComponent,
    SitesComponent,
    ViewGenkeyComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SoildocStudyRoutingModule,
    TranslateModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    SharedModule,
    DropdownTreeviewModule,
    NgbTooltipModule
  ],
  exports: [
  ]
})
export class SoildocStudyModule { }
