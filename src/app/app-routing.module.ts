import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'soildoc',
    pathMatch: 'full'
  },
  {
    path: 'user',
    loadChildren: () => import('./../app/user/user.module').then(m => m.UserModule)
  },
  {
    path: 'soildoc',
    loadChildren: () => import('./../app/soildoc/soildoc.module').then(m => m.SoildocModule)
  },
  {
    path: 'soilstudy-detail',
    loadChildren: () => import('./../app/soildoc-study/soildoc-study.module').then(m => m.SoildocStudyModule)
  },
  {
    path: 'visualization',
    loadChildren: () => import('./../app/visualization/visualization.module').then(m => m.VisualizationModule)
  },
  {
    path: 'learn-more',
    loadChildren: () => import('./../app/learn-more/learn-more.module').then(m => m.LearnMoreModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'top'
})],
  exports: [RouterModule]
})

export class AppRoutingModule { }
