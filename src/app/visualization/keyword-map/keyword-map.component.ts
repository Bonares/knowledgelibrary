import { take } from 'rxjs/operators';
import { MetaService } from './../../shared/meta.service';
import { AuthService } from './../../shared/authentication/auth.service';
import { FilterVisualization } from './../filter-visualization/FilterVisualization';
import { TreeNode } from './../treeview-custom/tree-node';
import { AdddriverService } from './../../soildoc-study/soilstudy-detail/add-driver/shared/adddriver.service';
import { SitesoilService } from './../../soildoc-study/soilstudy-detail/site-soil/shared/sitesoil.service';
import { SoildocService } from './../../soildoc/soil-doc/shared/soildoc.service';
import { Soildoc } from './../../soildoc/soil-doc/shared/Soildoc';
import { Observable, Subscription, timer, fromEvent} from 'rxjs';
import { VisualizationService } from './../shared/visualization.service';
import { JsonService } from './../../soildoc-study/soilstudy-detail/shared/json.service';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { faProjectDiagram, faExclamationCircle, faSync, faExpandAlt, faCompressAlt, faSearch, faExternalLinkAlt, faFileAlt, faBookOpen, faFilter, faInfoCircle, faUser } from '@fortawesome/free-solid-svg-icons';
import * as d3 from 'd3';
import { PlatformLocation } from '@angular/common';
import ForceGraph, { ForceGraphInstance } from 'force-graph';
import * as bootstrap from 'bootstrap';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kl-keyword-map',
  templateUrl: './keyword-map.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})

export class KeywordMapComponent implements OnInit, OnDestroy {
  
  faUser = faUser;

  faInfoCircle = faInfoCircle;
  faFilter = faFilter;
  faBookOpen = faBookOpen;
  faExclamationCircle = faExclamationCircle;
  faProjectDiagram = faProjectDiagram;
  faSync = faSync;

  faExpandAlt = faExpandAlt;
  faCompressAlt = faCompressAlt;
  faSearch = faSearch;
  faExternalLinkAlt = faExternalLinkAlt;
  faFileAlt = faFileAlt;

  isLoading: Boolean;
  isLoadingModal: Boolean;
  isVisualizationLoading: Boolean = false;
  isVisualizationEnabled: Boolean = false;
  errorLoadingVisualization: Boolean = false;
  errorLoading: Boolean = false;
  errorLoadingSoildocs: Boolean = false;
  dataEmpty: Boolean = false;
  treeviewDisabled: Boolean = false;
  triggerOverview = false;
  triggerDetail = false;

  options_complete: any[];
  options_complete_unformatted: any[];
  options_crops_user: any[] = [];
  options_genkeys: any[] = [];
  options_genkeys_geo: TreeNode[] = [];
  options_genkeys_land: TreeNode[] = [];
  options_genkeys_prop: TreeNode[] = [];
  options_genkeys_man: TreeNode[] = [];
  options_combinekeys: any[] = [];

  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;

  soildocs: Soildoc[] = [];
  propertyFirst: String;
  propertyLast: String;

  soildocOverview: Soildoc;

  isFullscreen = false;

  scrollOffset = 0;

  selectedNodes: [] = [];
  filterVis: FilterVisualization;

  soildocsVis: any[] = [];

  graph: ForceGraphInstance;

  modalPapers: bootstrap.Modal;
  modalOverview: bootstrap.Modal;
  modalFilter: bootstrap.Modal;

  showOnlyOwn = false;
  loggedInUser: string;
  isLoggedIn$: Observable<boolean>;

  constructor(
    private authService: AuthService,
    private js: JsonService,
    private vs: VisualizationService,
    private ss: SoildocService,
    private siteservice: SitesoilService,
    private as: AdddriverService,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILDOC.KEYVISUALIZATION', 'SOILDOC.KEYMETA');
    this.modalPapers = new bootstrap.Modal(document.getElementById('modalPapers'));
    this.modalFilter = new bootstrap.Modal(document.getElementById('modalFilter'));
    this.modalOverview = new bootstrap.Modal(document.getElementById('modalOverview'));
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authService.isLoggedInUser.pipe(take(1)).subscribe(loggedInUser => {
      this.loggedInUser = loggedInUser;
    });;
    document.getElementById('modalPapers').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerOverview) {
        this.modalOverview.show();
        this.triggerOverview = false;
      }
    });
    document.getElementById('modalOverview').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerDetail) {
        this.modalPapers.show();
        this.triggerDetail = false;
      }
    });
    this.js.getbyData(['Properties1', 'Management332', 'LandUse450', 'Geography511']).subscribe(result => {
      if (result) {
        this.options_complete = [];
        this.options_complete_unformatted = result;
        this.options_complete.push(result[0].children[0]);
        this.options_complete.push(result[0].children[1]);
        this.options_complete.push(result[0].children[2]);
        this.options_complete.push(result[1]);
        this.options_complete.push(result[2]);
        this.options_complete.push(result[3]);
        this.siteservice.getAllCropsByUser().subscribe(results => {
          if (results) {
            for (let i = 0; i < results.length; i++) {
              this.options_crops_user.unshift(results[i]);
            }
          }
          this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
            if (resultsgen) {
              for (let i = 0; i < resultsgen.length; i++) {
                if (resultsgen[i].category && resultsgen[i].category !== '') {
                  if (resultsgen[i].category === 'Properties') {
                    this.options_genkeys_prop.unshift(resultsgen[i]);
                  } else if (resultsgen[i].category === 'Geography') {
                    this.options_genkeys_geo.unshift(resultsgen[i]);
                  } else if (resultsgen[i].category === 'Management') {
                    this.options_genkeys_man.unshift(resultsgen[i]);
                  } else if (resultsgen[i].category === 'Land Use') {
                    this.options_genkeys_land.unshift(resultsgen[i]);
                  }
                }
                this.options_genkeys.unshift(resultsgen[i]);
              }
            }
            this.as.getAllKeywordsCombinedByUser().subscribe(resultscom => {
              if (resultscom) {
                for (let i = 0; i < resultscom.length; i++) {
                  this.options_combinekeys.unshift(resultscom[i]);
                }
              }
              const usergenerated = {
                'value': '1000000',
                'text': 'User Generated',
                'collapsed': true,
                'checked': false,
                'children': [{
                  'value': '1000001',
                  'text': 'Crop Rotation',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_crops_user
                }, {
                  'value': '1000002',
                  'text': 'Custom Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': [{
                    'value': '1000004',
                    'text': 'Properties',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_prop
                  },
                  {
                    'value': '1000005',
                    'text': 'Geography',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_geo
                  },
                  {
                    'value': '1000006',
                    'text': 'Management',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_man
                  },
                  {
                    'value': '1000007',
                    'text': 'Land Use',
                    'collapsed': true,
                    'checked': false,
                    'children': this.options_genkeys_land
                  }]
                }, {
                  'value': '1000003',
                  'text': 'Combined Keywords',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_combinekeys
                }]
              };
              this.options_complete.push(usergenerated);
              this.errorLoading = false;
              this.isLoading = false;
              this.initMap();
              this.resizeObservable$ = fromEvent(window, 'resize');
              this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
                this.setSimulationCenter();
              });
            });
          });
        });
      }
    },
    error => {
      this.isLoading = false;
      this.errorLoading = true;
    });
  }

  initMap() {
    this.graph = ForceGraph()
    (document.getElementById('graph'))
    .graphData({nodes: [], links: [] })
    .width(document.getElementById('graph').getBoundingClientRect().width)
    .height(document.getElementById('graph').getBoundingClientRect().height)
    .onNodeClick((node : any, event) => node.group === 'papergroup' ? this.nodeClickEvent(node, event) : null)
    .d3Force('collision', d3.forceCollide(15))
    .d3Force('manyBody', d3.forceManyBody().strength(-400))
    .linkWidth(0.5)
    .linkColor(() => 'rgba(92,116,40,0.4)')
    .nodeCanvasObjectMode((node: any) => node.group === 'papergroup' ? 'replace' : 'replace')
    .nodeCanvasObject((node, ctx) => this.nodePaint(node, ctx))
    .nodePointerAreaPaint((node, color, ctx) => this.nodePaintArea(node, color, ctx))
    .minZoom(0.1).maxZoom(4);
  }

  nodePaint(node: any, ctx: any) {
    if (node && node.group) {
      if (node.group !== 'papergroup') {
        if (node.group === 'management') {
          var r = 6;
          ctx.beginPath();
          ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
          ctx.fillStyle = '#1d8b5a';
          ctx.fill();
          ctx.lineWidth = '2';
          ctx.strokeStyle = '#a5bb28';
          ctx.stroke();
        } else if (node.group === 'properties') {
          var r = 6;
          ctx.beginPath();
          ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
          ctx.fillStyle = '#983400';
          ctx.fill();
          ctx.lineWidth = '2';
          ctx.strokeStyle = '#ff7100';
          ctx.stroke();
        } else {
          var r = 6;
          ctx.beginPath();
          ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
          ctx.fillStyle = '#004cf1';
          ctx.fill();
          ctx.lineWidth = '2';
          ctx.strokeStyle = '#01a4b9';
          ctx.stroke();
        }
        const label = node.id;
        ctx.font = '16px Titillium Web';
        ctx.textAlign = 'left';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = 'rgba(0, 0, 0, 0.8)';
        ctx.fillText(label, node.x + 15, node.y);
      } else {
        var r = this.getRadiusByValue(node.papers.length);
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#919191';
        ctx.fill();
        ctx.lineWidth = '3';
        ctx.strokeStyle = '#5e5e5e';
        ctx.stroke();
      }  
    }
  }

  nodePaintArea(node, color, ctx) {
    ctx.fillStyle = color;
    if (node && node.id && node.group) {
      if (node.group !== 'papergroup') {
        ctx.font = '16px Titillium Web';
        const textWidth = ctx.measureText(node.id).width;
        ctx.fillRect(node.x - 7 , node.y - 11, textWidth + 23, 19);
      } else {
        ctx.beginPath();
        ctx.arc(node.x, node.y, this.getRadiusByValue(node.papers.length) + 2, 0, 2 * Math.PI, false);
        ctx.fill()
      }
    }
  }

  onSelectedChange(event?: any) {
    this.isVisualizationLoading = true;
    this.isVisualizationEnabled = false;
    this.treeviewDisabled = true;
    this.selectedNodes = event ? event : this.selectedNodes;
    if (this.selectedNodes && this.selectedNodes.length > 1) {
      this.vs.getVisualizationByKeywords(this.selectedNodes, this.filterVis, this.showOnlyOwn, this.loggedInUser).subscribe({
        next: (result: any) => {
          if (result) {
            this.errorLoadingVisualization = false;
            if (result.nodes && result.nodes.length > 0 && result.links && result.links.length > 0) {
              this.soildocsVis = result.papers;
              this.graph
                .graphData({nodes: result.nodes, links: result.links });
              this.isVisualizationLoading = false;
              this.treeviewDisabled = false;
              this.isVisualizationEnabled = true;
              this.dataEmpty = false;
              this.setSimulationCenter();
            } else {
              this.soildocsVis = [];
              this.dataEmpty = true;
              this.isVisualizationLoading = false;
              this.treeviewDisabled = false;
            }
          } else {
            this.soildocsVis = [];
            this.isVisualizationLoading = false;
            this.errorLoadingVisualization = true;
            this.treeviewDisabled = false;
          }
        },
        error: () => {
          this.soildocsVis = [];
          this.isVisualizationLoading = false;
          this.errorLoadingVisualization = true;
          this.treeviewDisabled = false;
        }
      });
    } else {
      this.soildocsVis = [];
      this.dataEmpty = false;
      this.isVisualizationEnabled = false;
      this.isVisualizationLoading = false;
      this.errorLoadingVisualization = false;
      this.treeviewDisabled = false;
    }
    
  }

  setSimulationCenter() {
    timer(10).subscribe(val => {
      this.graph
      .width(document.getElementById('graph').getBoundingClientRect().width)
      .height(document.getElementById('graph').getBoundingClientRect().height);
      const bbox = this.graph.getGraphBbox();
      this.graph.centerAt((bbox.x[0] + bbox.x[1]) / 2, (bbox.y[0] + bbox.y[1]) / 2);
    });
  }

  nodeClickEvent(node: any, event: any) {
    this.openModalPapers(node.papers, node.first, node.last);
  }

  ngOnDestroy() {
    if (this.resizeSubscription$) {
      this.resizeSubscription$.unsubscribe();
    }
  }

  maximizeVisualization() {
    this.isFullscreen = true;
    this.setSimulationCenter();
  }

  minimizeVisualization() {
    this.isFullscreen = false;
    this.setSimulationCenter();
  }

  openModalPapers(soildocs: String[], propertyFirst: String, propertyLast: String) {
    this.modalPapers.show();
    this.propertyFirst = propertyFirst;
    this.propertyLast = propertyLast;
    this.ss.getAllById(soildocs).subscribe(result => {
      if (result.length && result.length > 0) {
        this.soildocs = result;
      } else {
        this.soildocs = [];
      }
    },
    error => {
      this.soildocs = [];
    });
  }

  openOverview(soildoc_id: any) {
    this.triggerOverview = true;
    if (this.soildocOverview && (this.soildocOverview._id === soildoc_id)) {
      this.triggerDetail = true;
      this.modalPapers.hide();
    } else {
      this.triggerDetail = true;
      this.modalPapers.hide();
      this.ss.getOne(soildoc_id).subscribe(resultttt => {
        this.soildocOverview = resultttt;
      });
    }
  }

  openModalFilter() {
    this.modalFilter.show();
  }

  updateFilter(filter: FilterVisualization) {
    this.filterVis = filter;
    this.onSelectedChange();
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  getRadiusByValue(length: number) {
    if (this.between(length, 1, 5)) {
      return 10;
    } else if (this.between(length, 6, 10)) {
      return 11;
    } else if (this.between(length, 11, 15)) {
      return 12;
    } else if (this.between(length, 16, 20)) {
      return 13;
    } else if (this.between(length, 21, 25)) {
      return 14;
    } else if (this.between(length, 26, 30)) {
      return 15;
    } else if (this.between(length, 31, 35)) {
      return 16;
    } else if (this.between(length, 36, 40)) {
      return 17;
    } else if (this.between(length, 41, 45)) {
      return 18;
    } else if (this.between(length, 46, 50)) {
      return 19;
    } else {
      return 20;
    }
  }

  between(x: number, min: number, max: number) {
    return x >= min && x <= max;
  }

}
