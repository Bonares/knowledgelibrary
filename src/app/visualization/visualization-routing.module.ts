import { DriverMapComponent } from './driver-map/driver-map.component';
import { AuthGuard } from './../shared/auth.guard';
import { UserStatisticsComponent } from './user-statistics/user-statistics.component';
import { PropertyMapComponent } from './property-map/property-map.component';
import { KeywordMapComponent } from './keyword-map/keyword-map.component';
import { ManagementMapComponent } from './management-map/management-map.component';
import { LocationMapComponent } from './location-map/location-map.component';
import { SoildocResolverService } from './../soildoc/soil-doc/shared/soildoc-resolver.service';
import { PaperMapComponent } from './paper-map/paper-map.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'keyword-map',
    pathMatch: 'full'
  },
  {
    path: 'keyword-map',
    component: KeywordMapComponent
  },
  {
    path: 'paper-map/:id',
    component: PaperMapComponent,
    resolve: {
      soildoc: SoildocResolverService
    },
  },
  {
    path: 'location-map',
    component: LocationMapComponent
  },
  {
    path: 'management-map',
    component: ManagementMapComponent
  },
  {
    path: 'property-map',
    component: PropertyMapComponent
  },
  {
    path: 'driver-map',
    component: DriverMapComponent
  },
  {
    path: 'userstatistics-map',
    component: UserStatisticsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizationRoutingModule { }
