import { MetaService } from './../../shared/meta.service';
import { FilterVisualization } from './../filter-visualization/FilterVisualization';
import { SoildocService } from './../../soildoc/soil-doc/shared/soildoc.service';
import { Soildoc } from './../../soildoc/soil-doc/shared/Soildoc';
import { SoildocMarker } from './SoildocMarker';
import { VisualizationService } from './../shared/visualization.service';
import { timer } from 'rxjs';
import { SitesoilService } from './../../soildoc-study/soilstudy-detail/site-soil/shared/sitesoil.service';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, NgZone, ChangeDetectorRef } from '@angular/core';
import * as L from 'leaflet';
import { faPlus, faMinus, faSearch, faSpinner, faMapMarkerAlt,
  faSync, faExpandAlt, faCompressAlt, faBookOpen, faFilter } from '@fortawesome/free-solid-svg-icons';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';
@Component({
  selector: 'kl-location-map',
  templateUrl: './location-map.component.html',
  styleUrls: []
})
export class LocationMapComponent implements OnInit, AfterViewInit {

  faFilter = faFilter;
  faBookOpen = faBookOpen;
  faSync = faSync;
  faPlus = faPlus;
  faMinus = faMinus;
  faSearch = faSearch;
  faSpinner = faSpinner;
  faMapMarkerAlt = faMapMarkerAlt;
  faExpandAlt = faExpandAlt;
  faCompressAlt = faCompressAlt;

  isLoading = false;
  errorLoading = false;
  emptyLoading = false;
  isDesktop = false;
  searchValue = '';
  isLoadingSearch = false;
  noLocationFound = false;
  isOwnLoading = false;

  soildocOverview: Soildoc;

  zoom = 6;
  maxZoom = 18;
  options = {
    layers: [
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' })
    ],
    zoom: this.zoom,
    zoomControl: false,
    center: L.latLng(51.163375, 10.447683)
  };

  customIcon = L.icon({
    iconSize: [ 25, 41 ],
    iconAnchor: [ 13, 41 ],
    iconUrl: 'assets/marker-icon.png',
    shadowUrl: 'assets/marker-shadow.png'
  });

  isFullscreen = false;
  scrollOffset = 0;

  filterVis: FilterVisualization;

  markerCluster: L.MarkerClusterGroup;
  map: L.Map;
  @ViewChild('topleft')
  private topleftContainer: ElementRef<HTMLElement>;
  @ViewChild('bottomright')
  private bottomrightContainer: ElementRef<HTMLElement>;

  modalOverview: bootstrap.Modal;
  modalFilter: bootstrap.Modal;
  constructor(
    private siteService: SitesoilService,
    private vs: VisualizationService,
    private ss: SoildocService,
    private zone: NgZone,
    private changeref: ChangeDetectorRef,
    private platformLocation: PlatformLocation,
    private metaService: MetaService
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('SOILDOC.MAPVISUALIZATION', 'SOILDOC.MAPMETA');
    this.modalFilter = new bootstrap.Modal(document.getElementById('modalFilter'));
    this.modalOverview = new bootstrap.Modal(document.getElementById('modalOverview'));
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.initMarkerCluster();
  }

  ngAfterViewInit() {
    L.DomEvent.disableClickPropagation(this.topleftContainer.nativeElement);
    L.DomEvent.disableClickPropagation(this.bottomrightContainer.nativeElement);
  }

  initMarkerCluster() {
    this.markerCluster = L.markerClusterGroup({
      spiderfyOnMaxZoom: false,
      iconCreateFunction: function(cluster) {
        let point: L.Point;
        let classe = 'custom-marker custom-marker-';
        if (cluster.getChildCount() < 50) {
          classe += 'small';
          point = new L.Point(40, 40);
        } else if (cluster.getChildCount() < 100) {
          classe += 'medium';
          point = new L.Point(50, 50);
        } else {
          classe += 'large';
          point = new L.Point(60, 60);
        }
        return L.divIcon({ html: '<div><span>' + cluster.getChildCount() + '</span></div>', iconSize: point, className: classe});
      }
    });
    const clusterClick = (a: L.LeafletEvent) => {
      if (this.map.getZoom() === this.maxZoom) {
        a.propagatedFrom.spiderfy();
      }
    };
    this.markerCluster.on('clusterclick', clusterClick);
  }

  onMapReady(map: any) {
    this.map = map;
    this.isLoading = true;
    this.changeref.detectChanges();
    this.updateMap();
  }

  updateMap() {
    this.isLoading = true;
    this.markerCluster.clearLayers();
    this.vs.getVisualizationLocations(this.filterVis).subscribe(datas => {
      if (datas && Array.isArray(datas) && datas.length > 0) {
        for (const locationObj of datas) {
          this.markerCluster.addLayer(
            new SoildocMarker(L.latLng(locationObj.lat, locationObj.lon), locationObj.soildoc_id, { icon: this.customIcon }).bindTooltip(
              locationObj.title, { direction: 'top', offset: new L.Point(0, -20) }).on('click', (e) => {
                this.zone.run(() => { this.openOverview(e.target); });
              })
          );
        }
        this.map.addLayer(this.markerCluster);
        this.errorLoading = false;
        this.emptyLoading = false;
      } else {
        if (Array.isArray(datas) && datas.length === 0) {
          this.emptyLoading = true;
          this.errorLoading = false;
        } else {
          this.errorLoading = true;
          this.emptyLoading = false;
        }
      }
      this.isLoading = false;
      this.changeref.detectChanges();
    },
    error => {
      this.isLoading = false;
      this.emptyLoading = false;
      this.errorLoading = true;
      this.changeref.detectChanges();
    });
  }

  openOverview(marker: SoildocMarker) {
    const soildoc_id = marker.getSoildocID();
    if (this.soildocOverview && (this.soildocOverview._id === soildoc_id)) {
      this.modalOverview.show();
    } else {
      this.modalOverview.show();
      this.ss.getOne(soildoc_id).subscribe(resultttt => {
        this.soildocOverview = resultttt;
      });
    }
  }

  openModalFilter() {
    this.modalFilter.show();
  }

  updateFilter(filter: FilterVisualization) {
    this.filterVis = filter;
    this.updateMap();
  }

  searchCoords() {
    this.isLoadingSearch = true;
    if (this.searchValue !== '') {
      this.siteService.getLocationByString(this.searchValue).subscribe(datas => {
        if (Array.isArray(datas) && datas.length === 1) {
          if (datas[0].lat && datas[0].lat !== '' && datas[0].lon && datas[0].lon !== '') {
            this.resultSearchCoords(true, datas[0].lat, datas[0].lon);
          } else {
            this.resultSearchCoords(false);
          }
        } else {
          this.resultSearchCoords(false);
        }
      },
      error => {
        this.resultSearchCoords(false);
      });
    }
  }

  resultSearchCoords(found: boolean, latitude?: number, longitude?: number) {
    timer(1500).subscribe(val => {
      this.isLoadingSearch = false;
      this.noLocationFound = !found;
      if (found) {
        this.map.setView(L.latLng(latitude, longitude), 15);
      }
    });
  }

  zoomIn() {
    this.map.zoomIn(1);
  }

  zoomOut() {
    this.map.zoomOut(1);
  }

  getOwnLocation(event: any) {
    event.stopPropagation();
    this.isOwnLoading = true;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position: any) => {
          this.map.setView(L.latLng(position.coords.latitude, position.coords.longitude), 15);
          this.isOwnLoading = false;
        },
        () => {
          this.isOwnLoading = false;
        }
      );
    } else {
      // Browser doesn't support Geolocation
      this.isOwnLoading = false;
    }
  }
  maximizeMap(event: any) {
    event.stopPropagation();
    this.isFullscreen = true;
    timer(1).subscribe(val => {
      this.map.invalidateSize();
    });
  }

  minimizeMap(event: any) {
    event.stopPropagation();
    this.isFullscreen = false;
    timer(1).subscribe(val => {
      this.map.invalidateSize();
    });
  }

}
