import * as L from 'leaflet';

export class SoildocMarker extends L.Marker {
  soildoc_id: string;

  constructor(latLng: L.LatLngExpression, soildoc_id: string, options?: L.MarkerOptions) {
    super(latLng, options);
    this.setSoildocID(soildoc_id);
  }

  getSoildocID() {
    return this.soildoc_id;
  }

  setSoildocID(soildoc_id: string) {
    this.soildoc_id = soildoc_id;
  }
}
