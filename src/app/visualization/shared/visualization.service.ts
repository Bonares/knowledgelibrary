import { FilterVisualization } from './../filter-visualization/FilterVisualization';
import { Injectable, Inject } from '@angular/core';
import { AuthGuard } from './../../shared/auth.guard';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { timeout, catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class VisualizationService {

  constructor(
    @Inject('API_URL') private api: string,
    private httpClient: HttpClient,
    private ag: AuthGuard
  ) { }

  private handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError(error);
  }

  getVisualizationByPaper(id: string, activeKeywords: String[], showOnlyOwn: boolean, loggedUser: string): Observable<any> {
    return this.httpClient
    .post(`${this.api}/visualization/paper-map`, {id: id, keywords: activeKeywords, onlyOwn: showOnlyOwn, loggedUser: loggedUser})
    .pipe(
      retry(3),
      timeout(15000),
      catchError(this.handleError) // then handle the error
    );
  }

  getVisualizationByKeywords(keywords: String[], filter: FilterVisualization, showOnlyOwn: boolean, loggedUser: string): Observable<any> {
    return this.httpClient
      .post(`${this.api}/visualization/keyword-map`, {keywords: keywords, filter: filter, onlyOwn: showOnlyOwn, loggedUser: loggedUser})
      .pipe(
        retry(3), // retry a failed request up to 3 times
        timeout(30000),
        catchError(this.handleError)
      );
  }

  getVisualizationLocations(filter: FilterVisualization): Observable<any> {
    return this.httpClient
    .post(`${this.api}/visualization/locations`, {filter: filter})
    .pipe(
      retry(3),
      timeout(15000),
      catchError(this.handleError) // then handle the error
    );
  }

  getVisualizationManagement(keyword: string, filter: FilterVisualization, keywords: String[], showOnlyOwn: boolean, loggedUser: string): Observable<any> {
    return this.httpClient
    .post(`${this.api}/visualization/management-map`, {keyword: keyword, filter: filter, keywords: keywords, onlyOwn: showOnlyOwn, loggedUser: loggedUser})
    .pipe(
      retry(3),
      timeout(15000),
      catchError(this.handleError) // then handle the error
    );
  }

  getVisualizationProperty(keyword: string, filter: FilterVisualization, keywords: String[], showOnlyOwn: boolean, loggedUser: string): Observable<any> {
    return this.httpClient
    .post(`${this.api}/visualization/property-map`, {keyword: keyword, filter: filter, keywords: keywords, onlyOwn: showOnlyOwn, loggedUser: loggedUser})
    .pipe(
      retry(3),
      timeout(15000),
      catchError(this.handleError) // then handle the error
    );
  }

  getVisualizationDriver(obj: any, filter: FilterVisualization, showOnlyOwn: boolean, loggedUser: string): Observable<any> {
    return this.httpClient
    .post(`${this.api}/visualization/driver-map`, {obj: obj, filter: filter, onlyOwn: showOnlyOwn, loggedUser: loggedUser})
    .pipe(
      retry(3),
      timeout(15000),
      catchError(this.handleError) // then handle the error
    );
  }
}
