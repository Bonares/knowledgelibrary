export class ErrorMessage {
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string
  ) {}
}

export const FilterErrorMessages = [
  new ErrorMessage('sand_content_min', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('sand_content_max', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('silt_content_min', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('silt_content_max', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('clay_content_min', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('clay_content_min', 'numberFormat0100DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT0100DOT2'),
  new ErrorMessage('pH_min', 'numberFormat014DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT014DOT2'),
  new ErrorMessage('pH_max', 'numberFormat014DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT014DOT2'),
  new ErrorMessage('bulk_density_min', 'numberFormatDOT2MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT2MAX15'),
  new ErrorMessage('bulk_density_max', 'numberFormatDOT2MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT2MAX15'),
  new ErrorMessage('organic_c_min', 'numberFormatDOT2MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT2MAX15'),
  new ErrorMessage('organic_c_max', 'numberFormatDOT2MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT2MAX15'),
  new ErrorMessage('caco3_min', 'numberFormatDOT2MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT2MAX15'),
  new ErrorMessage('caco3_max', 'numberFormatDOT2MAX15', 'SITESOIL.FAILFORM.NUMBERFORMATDOT2MAX15'),
  new ErrorMessage('mean_annualair_temperature_min', 'numberFormatNegative50DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT50NEGATIVEDOT2'),
  new ErrorMessage('mean_annualair_temperature_max', 'numberFormatNegative50DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT50NEGATIVEDOT2'),
  new ErrorMessage('annual_precipitation_sum_min', 'numberFormat030000DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT030000DOT2'),
  new ErrorMessage('annual_precipitation_sum_max', 'numberFormat030000DOT2', 'SITESOIL.FAILFORM.NUMBERFORMAT030000DOT2')
];
