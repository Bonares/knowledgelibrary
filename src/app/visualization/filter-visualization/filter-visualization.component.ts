import { AdddriverService } from './../../soildoc-study/soilstudy-detail/add-driver/shared/adddriver.service';
import { SitesoilValidators } from './../../soildoc-study/soilstudy-detail/site-soil/shared/sitesoil-validators';
import { FilterErrorMessages } from './filter-error-messages';
import { TreeNode } from './../treeview-custom/tree-node';
import { JsonService } from './../../soildoc-study/soilstudy-detail/shared/json.service';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { FilterVisualizationFactory } from './FilterVisualizationFactory';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'kl-filter-visualization',
  templateUrl: './filter-visualization.component.html',
  styleUrls: []
})
export class FilterVisualizationComponent implements OnInit {

  faTimes = faTimes;

  filtervisualization = FilterVisualizationFactory.empty();
  filterForm: UntypedFormGroup;

  options_soil_type_wrb: TreeNode;
  options_soil_type_usda: TreeNode;
  options_soil_type_ger: TreeNode;
  options_soil_type_fao: TreeNode;
  options_soil_type: TreeNode[] = [];
  options_soil_texture1187: TreeNode;
  options_soil_texture1097: TreeNode;
  options_soil_texture: TreeNode[] = [];
  options_land_use: TreeNode[] = [];
  options_genkeys_landuse: TreeNode[] = [];

  unitArray = [];
  bulk_density_items = [];
  soil_organic_c_items = [];
  caco3_items = [];

  isLoading = true;
  errors: { [key: string]: string } = {};

  pH_options = ['pH (water)', 'pH (KCl)', 'pH (CaCl2)'];

  constructor(
    private js: JsonService,
    private fb: UntypedFormBuilder,
    private as: AdddriverService
  ) { }

  @Output() filterChange = new EventEmitter<any>();

  ngOnInit(): void {
    this.js.getbyData(['SoilTypeWRB', 'SoilTypeUSDA', 'SoilTypeGER', 'SoilTypeFAO', 'Properties1', 'unitsNew', 'LandUse450']).subscribe(result => {
      if (result) {
        this.options_soil_type_wrb = new TreeNode(result[0]);
        this.options_soil_type_usda = new TreeNode(result[1]);
        this.options_soil_type_ger = new TreeNode(result[2]);
        this.options_soil_type_fao = new TreeNode(result[3]);
        this.options_soil_type.push(this.options_soil_type_wrb);
        this.options_soil_type.push(this.options_soil_type_usda);
        this.options_soil_type.push(this.options_soil_type_ger);
        this.options_soil_type.push(this.options_soil_type_fao);
        this.options_soil_texture1187 = new TreeNode(this.findByProperty(result[4], 'value', '1187'));
        this.options_soil_texture1097 = new TreeNode(this.findByProperty(result[4], 'value', '1097'));
        this.options_soil_texture.push(this.options_soil_texture1187);
        this.options_soil_texture.push(this.options_soil_texture1097);
        this.options_land_use = this.convertObjToTreeNodes(result[6].children);

        this.unitArray = result[5];
        this.bulk_density_items = this.unitArray.filter(unit => {
          if (Array.isArray(unit.treeViewId)) {
              if (unit.treeViewId.includes('132')) {
                return true;
              }
          }
          return false;
        });
        this.soil_organic_c_items = this.unitArray.filter(unit => {
          if (Array.isArray(unit.treeViewId)) {
              if (unit.treeViewId.includes('6')) {
                return true;
              }
          }
          return false;
        });
        this.caco3_items = this.unitArray.filter(unit => {
          if (Array.isArray(unit.treeViewId)) {
              if (unit.treeViewId.includes('1160')) {
                return true;
              }
          }
          return false;
        });

        this.initFilter();
        this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
          if (resultsgen) {
            for (let i = 0; i < resultsgen.length; i++) {
              if (resultsgen[i].category === 'Land Use') {
                this.options_genkeys_landuse.push(new TreeNode(resultsgen[i]));
              }
            }
          }
          const landuse = {
            'value': '1000000',
            'text': 'User Generated',
            'collapsed': true,
            'checked': false,
            'children': this.options_genkeys_landuse
          };
          this.options_land_use.unshift(new TreeNode(landuse));
          this.isLoading = false;
        });
      }
    });
  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  convertObjToTreeNodes(arr: []) {
    const treeViewObj = [];
    for (let i = 0; i < arr.length; i++) {
      treeViewObj.push(new TreeNode(arr[i]));
    }
    return treeViewObj;
  }

  initFilter() {
    this.filterForm = this.fb.group({
      soil_type: this.fb.control(this.filtervisualization.soil_type, []),
      soil_texture: this.fb.control(this.filtervisualization.soil_texture, []),
      mean_annualair_temperature_min: this.fb.control(this.filtervisualization.mean_annualair_temperature_min, [SitesoilValidators.onlyNumberNegative50DOT2]),
      mean_annualair_temperature_max: this.fb.control(this.filtervisualization.mean_annualair_temperature_max, [SitesoilValidators.onlyNumberNegative50DOT2]),
      annual_precipitation_sum_min: this.fb.control(this.filtervisualization.annual_precipitation_sum_min, [SitesoilValidators.onlyNumber030000DOT2]),
      annual_precipitation_sum_max: this.fb.control(this.filtervisualization.annual_precipitation_sum_max, [SitesoilValidators.onlyNumber030000DOT2]),
      sand_content_min: this.fb.control(this.filtervisualization.sand_content_min, [SitesoilValidators.onlyNumber0100DOT2]),
      sand_content_max: this.fb.control(this.filtervisualization.sand_content_max, [SitesoilValidators.onlyNumber0100DOT2]),
      silt_content_min: this.fb.control(this.filtervisualization.silt_content_min, [SitesoilValidators.onlyNumber0100DOT2]),
      silt_content_max: this.fb.control(this.filtervisualization.silt_content_max, [SitesoilValidators.onlyNumber0100DOT2]),
      clay_content_min: this.fb.control(this.filtervisualization.clay_content_min, [SitesoilValidators.onlyNumber0100DOT2]),
      clay_content_max: this.fb.control(this.filtervisualization.clay_content_max, [SitesoilValidators.onlyNumber0100DOT2]),
      pH_min: this.fb.control(this.filtervisualization.pH_min, [SitesoilValidators.onlyNumber014DOT2]),
      pH_max: this.fb.control(this.filtervisualization.pH_max, [SitesoilValidators.onlyNumber014DOT2]),
      pH_type: this.fb.control(this.filtervisualization.pH_type, []),
      bulk_density_min: this.fb.control(this.filtervisualization.bulk_density_min, [SitesoilValidators.onlyNumberDOT2MAX15]),
      bulk_density_max: this.fb.control(this.filtervisualization.bulk_density_max, [SitesoilValidators.onlyNumberDOT2MAX15]),
      bulk_density_unit: this.fb.control(this.filtervisualization.bulk_density_unit, []),
      bulk_density_default: this.fb.control(this.filtervisualization.bulk_density_default, []),
      bulk_density_factor: this.fb.control(this.filtervisualization.bulk_density_factor, []),
      organic_c_type: this.fb.control(this.filtervisualization.organic_c_type, []),
      organic_c_min: this.fb.control(this.filtervisualization.organic_c_min, [SitesoilValidators.onlyNumberDOT2MAX15]),
      organic_c_max: this.fb.control(this.filtervisualization.organic_c_max, [SitesoilValidators.onlyNumberDOT2MAX15]),
      organic_c_unit: this.fb.control(this.filtervisualization.organic_c_unit, []),
      organic_c_default: this.fb.control(this.filtervisualization.organic_c_default, []),
      organic_c_factor: this.fb.control(this.filtervisualization.organic_c_factor, []),
      caco3_min: this.fb.control(this.filtervisualization.caco3_min, [SitesoilValidators.onlyNumberDOT2MAX15]),
      caco3_max: this.fb.control(this.filtervisualization.caco3_max, [SitesoilValidators.onlyNumberDOT2MAX15]),
      caco3_unit: this.fb.control(this.filtervisualization.caco3_unit, []),
      caco3_default: this.fb.control(this.filtervisualization.caco3_default, []),
      caco3_factor: this.fb.control(this.filtervisualization.caco3_factor, []),
      land_use: this.fb.control(this.filtervisualization.land_use, []),
    });
    this.filterForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  updateFactorByElement(unitForm: string, element: string, secelement: string) {
    const singleunit = this.unitArray.find(unit => unit.value === unitForm);
    this.filterForm.controls[element].setValue(
      singleunit && singleunit.factor ? singleunit.factor : null);
    this.filterForm.controls[secelement].setValue(
      singleunit && singleunit.default ? singleunit.default : '');
  }

  resetUnitFactor(element: string, secelement: string, thirelement: string) {
    this.filterForm.controls[element].setValue(null);
    this.filterForm.controls[secelement].setValue('');
    this.filterForm.controls[thirelement].setValue('');
  }

  updateErrorMessages() {
    this.errors = {};

    for (const message of FilterErrorMessages) {
      const control = this.filterForm.get(message.forControl);
        if (control &&
          control.dirty &&
          control.invalid &&
          control.errors[message.forValidator] &&
          !this.errors[message.forControl]) {
          this.errors[message.forControl] = message.text;
        }
    }
  }

  applyFilter() {
    this.filterChange.emit(FilterVisualizationFactory.fromObject(this.filterForm.getRawValue()));
  }

  resetFilter(elements: string[]) {
    for (const element of elements) {
      this.filterForm.controls[element].reset();
    }
  }

  resetFilterComplete() {
    this.filtervisualization = FilterVisualizationFactory.empty();
    this.initFilter();
    this.filterChange.emit(FilterVisualizationFactory.fromObject(this.filterForm.getRawValue()));
  }

  convertToFloat(val) {
    return parseFloat(parseFloat(val).toFixed(2));
  }
}
