import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterVisualizationComponent } from './filter-visualization.component';

describe('FilterVisualizationComponent', () => {
  let component: FilterVisualizationComponent;
  let fixture: ComponentFixture<FilterVisualizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterVisualizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterVisualizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
