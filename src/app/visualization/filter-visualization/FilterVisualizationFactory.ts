import { FilterVisualizationRaw } from './FilterVisualization-raw';
import { FilterVisualization } from './FilterVisualization';
export class FilterVisualizationFactory {

  static empty(): FilterVisualization {
    return new FilterVisualization('', '', null, null, null, null, null, null, null, null, null, null, null, null,
      '', null, null, '', '', null, '', null, null, '', '', null, null, null, '', '', null, '');
  }

  static fromObject(rawFilterVisualization: FilterVisualizationRaw | any): FilterVisualization {
    if (rawFilterVisualization) {
      return new FilterVisualization(
        rawFilterVisualization.soil_type,
        rawFilterVisualization.soil_texture,
        rawFilterVisualization.mean_annualair_temperature_min,
        rawFilterVisualization.mean_annualair_temperature_max,
        rawFilterVisualization.annual_precipitation_sum_min,
        rawFilterVisualization.annual_precipitation_sum_max,
        rawFilterVisualization.sand_content_min,
        rawFilterVisualization.sand_content_max,
        rawFilterVisualization.silt_content_min,
        rawFilterVisualization.silt_content_max,
        rawFilterVisualization.clay_content_min,
        rawFilterVisualization.clay_content_max,
        rawFilterVisualization.pH_min,
        rawFilterVisualization.pH_max,
        rawFilterVisualization.pH_type,
        rawFilterVisualization.bulk_density_min,
        rawFilterVisualization.bulk_density_max,
        rawFilterVisualization.bulk_density_unit,
        rawFilterVisualization.bulk_density_default,
        rawFilterVisualization.bulk_density_factor,
        rawFilterVisualization.organic_c_type,
        rawFilterVisualization.organic_c_min,
        rawFilterVisualization.organic_c_max,
        rawFilterVisualization.organic_c_unit,
        rawFilterVisualization.organic_c_default,
        rawFilterVisualization.organic_c_factor,
        rawFilterVisualization.caco3_min,
        rawFilterVisualization.caco3_max,
        rawFilterVisualization.caco3_unit,
        rawFilterVisualization.caco3_default,
        rawFilterVisualization.caco3_factor,
        rawFilterVisualization.land_use
      );
    } else {
      return null;
    }
  }
  }
