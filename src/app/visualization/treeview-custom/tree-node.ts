export interface TreeNodeInterface {
  text: string;
  id?: string;
  parentId?: string;
  value: string;
  collapsed?: boolean;
  checked?: boolean;
  children?: TreeNode[];
  flagged?: boolean;
  parentText?: string;
  unitsId?: number[];
}

export class TreeNode implements TreeNodeInterface {
  text: string;
  id?: string;
  parentId?: string;
  value: string;
  collapsed?: boolean;
  checked?: boolean;
  children?: TreeNode[];
  flagged?: boolean;
  parentText?: string;
  unitsId?: number[];
  constructor(item: TreeNodeInterface) {
    if (typeof item.text === 'string') {
      this.text = item.text;
    }
    if (typeof item.id === 'string') {
      this.id = item.id;
    }
    if (typeof item.parentId === 'string') {
      this.parentId = item.parentId;
    }
    if (item.value) {
      this.value = item.value.toString();
    } else {
      this.value = undefined;
    }
    this.children = [];
    if (item.children && item.children.length > 0) {
      for (const child of item.children) {
        this.children.push(new TreeNode(child));
      }
    }
    if (typeof item.checked === 'boolean') {
      this.checked = item.checked;
    } else {
      this.checked = false;
    }
    if (item.collapsed !== undefined && typeof item.collapsed === 'boolean') {
      this.collapsed = item.collapsed;
    } else {
      this.collapsed = true;
    }
    if (item.flagged !== undefined && typeof item.flagged === 'boolean') {
      this.flagged = item.flagged;
    } else {
      this.flagged = false;
    }
    if (typeof item.parentText === 'string') {
      this.parentText = item.parentText;
    }
    if (Array.isArray(item.unitsId)) {
      this.unitsId = item.unitsId;
    }
  }
}
