import { TreeNode } from './tree-node';
import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'treeview-custom',
  templateUrl: './treeview-custom.component.html',
  styleUrls: []
})
export class TreeviewCustomComponent implements OnInit, OnChanges {

  selected: any[] = [];
  filterText: string;
  originalTree: TreeNode[];
  filterTree: TreeNode[];
  helpFunc = false;
  selectAll = false;

  constructor() { }

  @Input() treeData: TreeNode[];
  @Input() placeholder: string;
  @Output() valueChange = new EventEmitter<any>();
  @Input() selectAllAvailable?: boolean;
  @Input() selectAllInitial?: boolean;


  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.treeData !== undefined) {
      this.originalTree = JSON.parse(JSON.stringify(this.treeData));
      this.filterTree = JSON.parse(JSON.stringify(this.treeData));
      if (this.selectAllInitial !== undefined) {
        this.selectAll = this.selectAllInitial;
      } else {
        this.selectAll = false;
      }
      this.updateCheckedInitial(this.originalTree);
    }
  }

  onItemCheckedChange(item: TreeNode, event: TreeNode) {
    if (!event.checked) {
      this.selectAll = false;
      if (this.selected.filter(s => s === event.text).length > 1) {
        this.selected.splice(this.selected.indexOf(event.text), 1);
      } else {
        this.selected = this.selected.filter(s => s !== event.text);
      }
    } else {
      this.selected.push(event.text);
    }
    if (event.children) {
      this.updateCheckedRecursive(event.children, event.checked);
    }
    this.valueChange.emit(this.selected);
  }

  onFilterTextChange(event: any) {
    this.updateFilterItems();
    this.updateChecked();
  }

  updateFilterItems() {
    if (this.filterText !== '' && this.filterText.length > 1) {
      const filterItems_1 = [];
      const filterText_1 = this.filterText.toLowerCase();
      JSON.parse(JSON.stringify(this.originalTree)).forEach((item) => {
        const newItem = this.filterItem(item, filterText_1);
        if (!isNil(newItem)) {
          filterItems_1.push(newItem);
        }
      });
      this.updateCollapsed(filterItems_1, filterText_1);
      this.filterTree = filterItems_1;
    } else {
      this.filterTree = JSON.parse(JSON.stringify(this.originalTree));
    }
  }

  updateCollapsed(items, filterText) {
    for (const item of items) {
      if (item.children && item.children.length > 0) {
        this.helpFunc = true;
        this.checkIfText(item, filterText);
        item.collapsed = this.helpFunc;
        if (item.children && item.children.length > 0) {
          this.updateCollapsed(item.children, filterText);
        }
      }
    }
  }

  checkIfText(item, filterText) {
    if (item.children) {
      if (item.children.some(children => children.text.toLowerCase().includes(filterText))) {
        this.helpFunc = false;
      } else {
        for (const child of item.children) {
          this.checkIfText(child, filterText);
        }
      }
    }
  }

  filterItem(item, filterText) {
    if (item.text.toLowerCase().includes(filterText)) {
      item.checked = this.selected.includes(item.text);
      return item;
    } else {
      if (!isNil(item.children) && item.children.length > 0) {
          const children_1 = [];
          item.children.forEach((child) => {
            const newChild = this.filterItem(child, filterText);
            if (!isNil(newChild)) {
                children_1.push(newChild);
            }
          });
          if (children_1.length > 0) {
            const newItem = item;
            item.checked = this.selected.includes(item.text);
            newItem.collapsed = false;
            newItem.children = children_1;
            return newItem;
          }
      }
    }
    return undefined;
  }

  updateChecked() {
    this.filterTree.forEach((item) => {
      item.checked = this.selected.includes(item.text);
      if (item.children) {
        this.updateChildren(item.children);
      }
    });
  }

  updateChildren(item: any) {
    const _this = this;
    item.forEach(function (child) {
      child.checked = _this.selected.includes(child.text);
      if (child.children) {
        _this.updateChildren(child.children);
      }
    });
  }

  updateCheckedRecursive(items: any, checked: boolean) {
    items.forEach((item) => {
      item.checked = checked;
      if (checked) {
        if (!this.selected.includes(item.text)) {
          this.selected.push(item.text);
        }
      } else {
        if (this.selected.filter(s => s === item.text).length > 1) {
          this.selected.splice(this.selected.indexOf(item.text), 1);
        } else {
          this.selected = this.selected.filter(s => s !== item.text);
        }
      }
      if (item.children) {
        this.updateCheckedRecursive(item.children, checked);
      }
    });
  }

  updateCheckedInitial(items: any) {
    items.forEach((item) => {
      if (item.checked && !this.selected.includes(item.text)) {
        this.selected.push(item.text);
      }
      if (item.children) {
        this.updateCheckedInitial(item.children);
      }
    })
  }

  selectAllFunc() {
    this.updateCheckedRecursive(this.filterTree, this.selectAll);
    this.valueChange.emit(this.selected);
  }
}

function isNil(elem) {
  return ((elem === undefined) || (elem === null));
}
