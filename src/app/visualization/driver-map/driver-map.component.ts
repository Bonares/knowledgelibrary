import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import ForceGraph, { ForceGraphInstance } from 'force-graph';
import { MetaService } from './../../shared/meta.service';
import { take } from 'rxjs/operators';
import { UserService } from './../../shared/user.service';
import { AuthService } from './../../shared/authentication/auth.service';
import { SitesoilService } from './../../soildoc-study/soilstudy-detail/site-soil/shared/sitesoil.service';
import { FilterVisualization } from './../filter-visualization/FilterVisualization';
import { Soildoc } from 'src/app/soildoc/soil-doc/shared/Soildoc';
import { SoildocService } from './../../soildoc/soil-doc/shared/soildoc.service';
import { VisualizationService } from './../shared/visualization.service';
import { faSync, faInfoCircle, faExpandAlt, faCompressAlt, faTimes, faEyeSlash, faEye, faSearch, faProjectDiagram,
  faFileAlt, faExternalLinkAlt, faBookOpen, faFilter, faBookmark, faTrashAlt, faDownload, faTractor, faCubes, faUser } from '@fortawesome/free-solid-svg-icons';
import { faBookmark as farBookmark } from '@fortawesome/free-regular-svg-icons';
import { AdddriverService } from './../../soildoc-study/soilstudy-detail/add-driver/shared/adddriver.service';
import { JsonService } from './../../soildoc-study/soilstudy-detail/shared/json.service';
import { TreeNode } from './../treeview-custom/tree-node';
import { fromEvent, Observable, Subscription, timer } from 'rxjs';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, Inject } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import * as d3 from 'd3';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-driver-map',
  templateUrl: './driver-map.component.html',
  styleUrls: [
  ]
})
export class DriverMapComponent implements OnInit, OnDestroy {

  faUser = faUser;
  faTractor = faTractor;
  faCubes = faCubes;

  faDownload = faDownload;
  faTrashAlt = faTrashAlt;
  farBookmark = farBookmark;
  faBookmark = faBookmark;

  faFilter = faFilter;
  faBookOpen = faBookOpen;
  faExternalLinkAlt = faExternalLinkAlt;
  faFileAlt = faFileAlt;
  faProjectDiagram = faProjectDiagram;
  faSearch = faSearch;

  faEyeSlash = faEyeSlash;
  faEye = faEye;
  faSync = faSync;
  faInfoCircle = faInfoCircle;
  faExpandAlt = faExpandAlt;
  faCompressAlt = faCompressAlt;
  faTimes = faTimes;

  options_crops_user: any[] = [];
  options_combinekeys: any[] = [];
  options_management: TreeNode[] = [];
  options_genkeys_management: TreeNode[] = [];
  options_genkeys_landuse: TreeNode[] = [];
  options_genkeys_properties: TreeNode[] = [];
  options_genkeys_geography: TreeNode[] = [];
  options_reference: TreeNode[] = [];
  options_properties: TreeNode[] = [];

  options_management_unformatted: any;
  options_landuse_unformatted: any;

  isCategorical_management = false;

  driverValue = '';
  driverText = '';
  referenceValue = '';
  variablesValue = [];

  simulation: any;
  linkg: any;
  nodeg: any;
  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;
  grabbedActive = false;

  isLoading: Boolean;
  isLoadingModal: Boolean;
  isVisualizationLoading: Boolean = false;
  isVisualizationEnabled: Boolean = false;
  errorLoadingVisualization: Boolean = false;
  errorLoading: Boolean = false;
  errorLoadingSoildocs: Boolean = false;
  dataEmpty: Boolean = false;
  isFullscreen = false;

  propertyTextsActive: Boolean = true;
  driverTextsActive: Boolean = true;

  triggerOverview = false;
  triggerDetail = false;

  scrollOffset = 0;

  startItem = 0;
  endItem: number;

  filterOptions = [];

  isSorted = false;
  sortTarget: any;
  searchValue = '';
  driverModal = '';
  referenceModal = '';
  propertyModal = '';
  soildocCollection: any[];
  soildocCollectionOriginal: any[];
  soildocCollectionFilter: any[];

  soildocOverview: Soildoc;

  filterVis: FilterVisualization;

  isLoggedIn$: Observable<boolean>;
  showOnlyMarked = false;
  markedPapers: String[] = [];

  soildocsVis: any[] = [];

  graph: ForceGraphInstance;
  
  activeKeyword = '';

  modalPapers: bootstrap.Modal;
  modalOverview: bootstrap.Modal;
  modalFilter: bootstrap.Modal;

  loggedInUser: string;
  showOnlyOwn = false;

  constructor(
    @Inject('API_URL') public api: string,
    private as: AdddriverService,
    private js: JsonService,
    private platformLocation: PlatformLocation,
    private vs: VisualizationService,
    private ss: SoildocService,
    private cdRef: ChangeDetectorRef,
    private siteservice: SitesoilService,
    private authService: AuthService,
    private us: UserService,
    private metaService: MetaService,
    private ac: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('SOILDOC.DRIVERVISUALIZATION', 'SOILDOC.DRIVERMETA');
    this.modalPapers = new bootstrap.Modal(document.getElementById('modalPapers'));
    this.modalFilter = new bootstrap.Modal(document.getElementById('modalFilter'));
    this.modalOverview = new bootstrap.Modal(document.getElementById('modalOverview'));
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoading = true;
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authService.isLoggedInUser.pipe(take(1)).subscribe(loggedInUser => {
      this.loggedInUser = loggedInUser;
    });;
    this.isLoggedIn$.pipe(take(1)).subscribe(login => {
      if (login) {
        this.us.getMarkedPapers().subscribe({next: (papers) => {
          this.markedPapers = papers && papers.length ? papers : [];
        }});
      }
    });
    document.getElementById('modalPapers').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerOverview) {
        this.modalOverview.show();
        this.triggerOverview = false;
      }
    });
    document.getElementById('modalOverview').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerDetail) {
        this.modalPapers.show();
        this.triggerDetail = false;
      }
    });
    this.js.getbyData(['Management332', 'LandUse450', 'Properties1', 'Geography511']).subscribe(result => {
      if (result) {
        this.options_management_unformatted = result[0];
        this.options_landuse_unformatted = result[1];
        this.options_management.push(new TreeNode(result[0]));
        this.options_management.push(new TreeNode(result[1]));
        this.options_management.push(new TreeNode(result[2]));
        this.options_management.push(new TreeNode(result[3]));
        this.as.getAllKeywordsCombinedByUser().subscribe(resultscom => {
          if (resultscom) {
            for (let i = 0; i < resultscom.length; i++) {
              this.options_combinekeys.unshift(resultscom[i]);
            }
          }
        this.siteservice.getAllCropsByUser().subscribe(results => {
          if (results) {
            for (let i = 0; i < results.length; i++) {
              this.options_crops_user.unshift(results[i]);
            }
          }
        this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
          if (resultsgen) {
            for (let i = 0; i < resultsgen.length; i++) {
              if (resultsgen[i].category === 'Properties') {
                this.options_genkeys_properties.unshift(resultsgen[i]);
              } else if (resultsgen[i].category === 'Geography') {
                this.options_genkeys_geography.unshift(resultsgen[i]);
              } else if (resultsgen[i].category === 'Management') {
                this.options_genkeys_management.unshift(resultsgen[i]);
              } else if (resultsgen[i].category === 'Land Use') {
                this.options_genkeys_landuse.unshift(resultsgen[i]);
              }
            }
          }
          const usergenerated = {
            'value': '1000000',
            'text': 'User Generated',
            'collapsed': true,
            'checked': false,
            'children': [{
              'value': '1000001',
              'text': 'Crop Rotation',
              'collapsed': true,
              'checked': false,
              'children': this.options_crops_user
            }, {
              'value': '1000002',
              'text': 'Custom Keywords',
              'collapsed': true,
              'checked': false,
              'children': [{
                'value': '1000004',
                'text': 'Properties',
                'collapsed': true,
                'checked': false,
                'children': this.options_genkeys_properties
              },
              {
                'value': '1000005',
                'text': 'Geography',
                'collapsed': true,
                'checked': false,
                'children': this.options_genkeys_geography
              },
              {
                'value': '1000006',
                'text': 'Management',
                'collapsed': true,
                'checked': false,
                'children': this.options_genkeys_management
              },
              {
                'value': '1000007',
                'text': 'Land Use',
                'collapsed': true,
                'checked': false,
                'children': this.options_genkeys_landuse
              }]
            }, {
              'value': '1000003',
              'text': 'Combined Keywords',
              'collapsed': true,
              'checked': false,
              'children': this.options_combinekeys
            }]
          };
          this.options_management.unshift(new TreeNode(usergenerated));
          this.isLoading = false;
          this.initMap()
          this.resizeObservable$ = fromEvent(window, 'resize');
          this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
            this.setSimulationCenter();
          });
          if (this.ac.snapshot.queryParams['keyword']) {
            this.driverText = this.ac.snapshot.queryParams['keyword'];
          }
        });
      });
      });
      }
    });
  }

  initMap() {
    this.graph = ForceGraph()
    (document.getElementById('graph'))
    .graphData({nodes: [], links: [] })
    .width(document.getElementById('graph').getBoundingClientRect().width)
    .height(document.getElementById('graph').getBoundingClientRect().height)
    .onNodeClick((node : any, event) => node.group === 'papergroup' ? this.nodeClickEvent(node, event) : node.group !== 'driver' ? this.nodeClickEventKeyword(node, event) : null)
    .d3Force('collision', d3.forceCollide(15))
    .d3Force('manyBody', d3.forceManyBody().strength(-400))
    .linkWidth(0.5)
    .linkColor(() => 'rgba(92,116,40,0.4)')
    .nodeCanvasObjectMode((node: any) => node.group === 'papergroup' ? 'replace' : 'replace')
    .nodeCanvasObject((node, ctx) => this.nodePaint(node, ctx))
    .nodePointerAreaPaint((node, color, ctx) => this.nodePaintArea(node, color, ctx))
    .minZoom(0.1).maxZoom(4)
    .onZoom(() => this.removeContextMenu())
    .onNodeDrag(() => this.removeContextMenu());
  }

  removeContextMenu() {
    document.getElementById('contextMenuMeasured').style.visibility = 'hidden';
  }

  nodePaint(node: any, ctx: any) {
    if (node && node.group) {
      if (node.group !== 'papergroup') {
        var r = node.center ? 9 : 6;
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = node.group === 'driver' ? '#1d8b5a' : '#983400';
        ctx.fill();
        ctx.lineWidth = node.center ? '4': '2';
        ctx.strokeStyle = node.group === 'driver' ? '#a5bb28' : '#ff7100';
        ctx.stroke();

        const label = node.id;
        ctx.font = '16px Titillium Web';
        ctx.textAlign = 'left';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = 'rgba(0, 0, 0, 0.8)';
        ctx.fillText(label, node.x + 15, node.y);
      } else {
        var r = this.getRadiusByValue(node.papers.length);
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#919191';
        ctx.fill();
        ctx.lineWidth = '3';
        ctx.strokeStyle = '#5e5e5e';
        ctx.stroke();
      }  
    }
  }

  nodePaintArea(node, color, ctx) {
    ctx.fillStyle = color;
    if (node && node.id && node.group) {
      if (node.group !== 'papergroup') {
        ctx.font = '16px Titillium Web';
        const textWidth = ctx.measureText(node.id).width;
        ctx.fillRect(node.x - (node.center ? 12 : 7) , node.y - 11, textWidth + (node.center ? 28 : 23), 19);
      } else {
        ctx.beginPath();
        ctx.arc(node.x, node.y, this.getRadiusByValue(node.papers.length) + 2, 0, 2 * Math.PI, false);
        ctx.fill()
      }
    }
  }

  nodeClickEvent(node: any, event: any) {
    this.openModalPapers(node);
    this.removeContextMenu();
  }

  nodeClickEventKeyword(node: any, event: any) {
    this.removeContextMenu();
    this.activeKeyword = node.id;
    if (node.group !== 'driver') {
      let div = document.getElementById('contextMenuMeasured');
      div.style.left = event.x + 10 + 'px';
      div.style.top = event.y - 30 + 'px';
      div.style.visibility = 'unset';
    }
  }

  ngOnDestroy() {
    if (this.resizeSubscription$) {
      this.resizeSubscription$.unsubscribe();
    }
  }

  onValueChangeManagement(node: TreeNode) {
    this.referenceValue = '';
    this.variablesValue = [];
    this.driverValue = node.value;
    this.actualizeTypeArrayManagement();
    this.updateVis(true, true);
  }

  onValueChangeReference(node: TreeNode) {
    this.variablesValue = [];
    this.updateVis(false, true);
  }

  onSelectedChange(event: Array<string>) {
    this.variablesValue = event;
    if (this.variablesValue.length > 1) {
      this.updateVis(false, false);
    }
  }

  updateVis(updateRef: boolean, updateVar: boolean) {
    if (this.driverText !== '') {
      this.isVisualizationLoading = true;
      this.isVisualizationEnabled = false;
      this.dataEmpty = false;
      this.vs.getVisualizationDriver({driver: this.driverText, reference: this.referenceValue, variables: this.variablesValue}, this.filterVis, this.showOnlyOwn, this.loggedInUser).subscribe(result => {
        if (result) {
          if (result.nodes && result.nodes.length > 0 && result.links && result.links.length > 0) {
            this.soildocsVis = result.papers;
            if (updateRef) {
              this.options_reference = [];
            }
            if (result.keywords && result.keywords.length > 0 && updateRef) {
              for (const keyword of result.keywords) {
                this.options_reference.push(new TreeNode({
                  value: keyword,
                  text: keyword
                }));
              }
              this.options_reference = this.options_reference.slice();
            }
            if (updateVar) {
              this.variablesValue = result.keywordsprop.map((key: any)=> {return key.text});
              this.options_properties = result.keywordsprop.slice();
            }
            this.graph
            .graphData({nodes: result.nodes, links: result.links });
            this.isVisualizationLoading = false;
            this.errorLoadingVisualization = false;
            this.isVisualizationEnabled = true;
            this.dataEmpty = false;
            this.setSimulationCenter();
          } else {
            if (updateRef) {
              this.options_reference = [];
            }
            if (updateVar) {
              this.options_properties = [];
            }
            this.dataEmpty = true;
            this.isVisualizationLoading = false;
            this.soildocsVis = [];
            this.errorLoadingVisualization = false;
          }
        } else {
          this.soildocsVis = [];
          this.isVisualizationLoading = false;
          this.errorLoadingVisualization = true;
        }
      },
      error => {
        this.isVisualizationLoading = false;
        this.errorLoadingVisualization = true;
      });
    }
    this.cdRef.detectChanges();
  }

  actualizeTypeArrayManagement() {
    if (this.driverValue) {
      let object = this.findByProperty(this.options_management_unformatted, 'value', this.driverValue);
      if (object === undefined) {
        object = this.findByProperty(this.options_landuse_unformatted, 'value', this.driverValue);
        if (object === undefined) {
          object = this.findByProperty(this.options_genkeys_landuse, 'value', this.driverValue);
          if (object === undefined) {
            object = this.findByProperty(this.options_genkeys_management, 'value', this.driverValue);
              if (object === undefined) {
                this.isCategorical_management = false;
              } else {
                this.isCategorical_management =  object.isNumeric ? (object.isNumeric === true ? false : true) : true;
              }
          } else {
            this.isCategorical_management =  object.isNumeric ? (object.isNumeric === true ? false : true) : true;
          }
        } else {
          this.isCategorical_management =  object.isNumeric ? (object.isNumeric === true ? false : true) : true;
        }
      } else {
        this.isCategorical_management =  object.isNumeric ? (object.isNumeric === true ? false : true) : true;
      }
    } else {
      this.isCategorical_management = false;
    }

  }

  findByProperty(o: any, prop: string, value: any) {
    if ( o[prop] === value) {
      return o;
    }
    let result, p;
    for (p in o) {
        if ( o.hasOwnProperty(p) && typeof o[p] === 'object' ) {
            result = this.findByProperty(o[p], prop, value);
            if (result) {
                return result;
            }
        }
    }
    return result;
  }

  setSimulationCenter() {
    timer(10).subscribe(val => {
      this.graph
      .width(document.getElementById('graph').getBoundingClientRect().width)
      .height(document.getElementById('graph').getBoundingClientRect().height);
      const bbox = this.graph.getGraphBbox();
      bbox ? this.graph.centerAt((bbox.x[0] + bbox.x[1]) / 2, (bbox.y[0] + bbox.y[1]) / 2) : null;
    });
  }

  maximizeVisualization() {
    this.isFullscreen = true;
    this.setSimulationCenter();
    this.removeContextMenu();
  }

  minimizeVisualization() {
    this.isFullscreen = false;
    this.setSimulationCenter();
    this.removeContextMenu();
  }

  openModalPapers(node: any) {
    this.filterOptions = [];
    this.searchValue = '';
    this.modalPapers.show();
    this.isLoadingModal = true;
    this.driverModal = node.driver;
    this.referenceModal = node.reference;
    this.propertyModal = node.property;
    this.soildocCollection = [];
    this.soildocCollectionOriginal = node.papers;
    this.soildocCollection = this.soildocCollectionOriginal.slice(0);
    this.soildocCollectionFilter = this.soildocCollectionOriginal.slice(0);
    this.updateFilterOptions();
    this.resetSorting();
    this.isLoadingModal = false;
  }

  updateFilterOptions() {
    this.filterOptions = [];
    for (const paper of this.soildocCollectionOriginal) {
      if (!this.filterOptions.find(filter => filter.effect === paper.effect)) {
        this.filterOptions.push({
          effect: paper.effect,
          selected: true,
          count: 1
        });
      } else {
        const filteropt = this.filterOptions.find(filter => filter.effect === paper.effect);
        filteropt.count += 1;
      }
    }
  }

  updateSoildocsFilter() {
    this.soildocCollection = [];
    let collection = this.soildocCollectionOriginal.slice(0);
    this.soildocCollectionFilter = collection.filter(paper => this.filterOptions.find(filter => (paper.effect === filter.effect) && filter.selected === true)).slice(0);
  }

  resetSorting() {
    if (this.isSorted && this.sortTarget) {
      this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
    }
  }

  sortPaper(event: any, identifier: string) {
    if (event.target.classList.value === 'sort-icon-search fa fa-sort') {
      if (this.isSorted && this.sortTarget) {
        this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      }
      this.isSorted = true;
      this.sortTarget = event.target;
      event.target.classList = 'sort-icon-search fa fa-sort-up';

      this.sortOneIdentifier(identifier, 1, -1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-up') {

      event.target.classList = 'sort-icon-search fa fa-sort-down';
      this.sortOneIdentifier(identifier, -1, 1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-down') {
      event.target.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
      this.sortOneIdentifier('modifiedDate', 1, -1, true);
    }
  }

  sortOneIdentifier(identifier: string, first: number, second: number, isDate?: boolean) {
    if (!isDate) {
      if (identifier === 'authors') {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a.soildoc[identifier][0].name > b.soildoc[identifier][0].name) {
            return first;
          }
          if (a.soildoc[identifier][0].name < b.soildoc[identifier][0].name) {
            return second;
          }
          return 0;
        });
      } else {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a.soildoc[identifier] > b.soildoc[identifier]) {
            return first;
          }
          if (a.soildoc[identifier] < b.soildoc[identifier]) {
            return second;
          }
          return 0;
        });
      }
    } else {
      this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
        return new Date(b.soildoc.modifiedDate).getTime() - new Date(a.soildoc.modifiedDate).getTime();
      });
    }
  }

  openOverview(soildoc_id: any) {
    this.triggerOverview = true;
    if (this.soildocOverview && (this.soildocOverview._id === soildoc_id)) {
      this.triggerDetail = true;
      this.modalPapers.hide();
    } else {
      this.triggerDetail = true;
      this.modalPapers.hide();
      this.ss.getOne(soildoc_id).subscribe(resultttt => {
        this.soildocOverview = resultttt;
      });
    }
  }

  openModalFilter() {
    this.modalFilter.show();
  }

  updateFilter(filter: FilterVisualization) {
    this.filterVis = filter;
    this.updateVis(false, false);
  }

  getRadiusByValue(length: number) {
    if (this.between(length, 1, 5)) {
      return 10;
    } else if (this.between(length, 6, 10)) {
      return 11;
    } else if (this.between(length, 11, 15)) {
      return 12;
    } else if (this.between(length, 16, 20)) {
      return 13;
    } else if (this.between(length, 21, 25)) {
      return 14;
    } else if (this.between(length, 26, 30)) {
      return 15;
    } else if (this.between(length, 31, 35)) {
      return 16;
    } else if (this.between(length, 36, 40)) {
      return 17;
    } else if (this.between(length, 41, 45)) {
      return 18;
    } else if (this.between(length, 46, 50)) {
      return 19;
    } else {
      return 20;
    }
  }

  between(x: number, min: number, max: number) {
    return x >= min && x <= max;
  }

  ConvertToInt(val) {
    return parseInt(val, 10);
  }

  markPaper(soildoc_id: string) {
    if (!this.markedPapers.includes(soildoc_id)) {
      this.markedPapers.push(soildoc_id);
      this.markedPapers = this.markedPapers.slice(0);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  unmarkPaper(soildoc_id: string) {
    if (this.markedPapers.includes(soildoc_id)) {
      this.markedPapers = this.markedPapers.filter(paper => paper !== soildoc_id);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  untagAllPapers() {
    this.markedPapers = [];
    this.us.updateMarkedPapers(this.markedPapers).subscribe({
      next: () => {},
      error: (err) => {console.error(err)}
    });
  }
}
