import { ActivatedRoute } from '@angular/router';
import ForceGraph, { ForceGraphInstance } from 'force-graph';
import { MetaService } from './../../shared/meta.service';
import { take } from 'rxjs/operators';
import { UserService } from './../../shared/user.service';
import { FilterVisualization } from './../filter-visualization/FilterVisualization';
import { SoildocService } from './../../soildoc/soil-doc/shared/soildoc.service';
import { AuthService } from './../../shared/authentication/auth.service';
import { Soildoc } from './../../soildoc/soil-doc/shared/Soildoc';
import { timer, Observable, Subscription, fromEvent } from 'rxjs';
import { VisualizationService } from './../shared/visualization.service';
import { AdddriverService } from './../../soildoc-study/soilstudy-detail/add-driver/shared/adddriver.service';
import { JsonService } from './../../soildoc-study/soilstudy-detail/shared/json.service';
import { TreeNode } from './../treeview-custom/tree-node';
import { faSync, faSearch, faExpandAlt, faCompressAlt, faExternalLinkAlt,
  faFileAlt, faProjectDiagram, faInfoCircle, faBookOpen, faFilter, faBookmark, faTrashAlt, faDownload, faLongArrowAltRight, faUser } from '@fortawesome/free-solid-svg-icons';
import { faBookmark as farBookmark } from '@fortawesome/free-regular-svg-icons';
import { Component, OnInit, OnDestroy, Inject, ChangeDetectorRef } from '@angular/core';
import * as d3 from 'd3';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'kl-management-map',
  templateUrl: './management-map.component.html',
  styleUrls: []
})
export class ManagementMapComponent implements OnInit, OnDestroy {

  faUser = faUser;

  faLongArrowAltRight = faLongArrowAltRight;

  faDownload = faDownload;
  faTrashAlt = faTrashAlt;
  farBookmark = farBookmark;
  faBookmark = faBookmark;

  faFilter = faFilter;
  faBookOpen = faBookOpen;
  faInfoCircle = faInfoCircle;
  faProjectDiagram = faProjectDiagram;
  faFileAlt = faFileAlt;
  faExpandAlt = faExpandAlt;
  faCompressAlt = faCompressAlt;
  faSearch = faSearch;
  faSync = faSync;
  faExternalLinkAlt = faExternalLinkAlt;

  options_properties: TreeNode[];
  options_genkeys_properties: TreeNode[] = [];
  options_genkeys_geography: TreeNode[] = [];
  options_combinekeys: TreeNode[] = [];

  isLoading: Boolean;
  isLoadingModal: Boolean;
  isVisualizationLoading: Boolean = false;
  isVisualizationEnabled: Boolean = false;
  errorLoadingVisualization: Boolean = false;
  errorLoading: Boolean = false;
  errorLoadingSoildocs: Boolean = false;
  dataEmpty: Boolean = false;
  treeviewDisabled: Boolean = false;
  isFullscreen = false;

  simulation: any;
  linkg: any;
  nodeg: any;
  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;
  grabbedActive = false;

  isSorted = false;
  sortTarget: any;
  searchValue = '';
  propertyFirst = '';
  propertyLast = '';
  soildocCollection: any[];
  soildocCollectionOriginal: any[];
  soildocCollectionFilter: any[];

  startItem = 0;
  endItem: number;

  filterOptions = [];

  influencedProperty = '';

  soildocOverview: Soildoc;

  triggerOverview = false;
  triggerDetail = false;

  isLoggedIn$: Observable<boolean>;

  scrollOffset = 0;

  filterVis: FilterVisualization;

  activeKeywords = [];
  options_keywords: TreeNode[] = [];

  showOnlyMarked = false;
  markedPapers: String[] = [];

  soildocsVis: any[] = [];

  graph: ForceGraphInstance;

  activeKeyword = '';

  modalPapers: bootstrap.Modal;
  modalOverview: bootstrap.Modal;
  modalFilter: bootstrap.Modal;

  showOnlyOwn = false;
  loggedInUser: string;

  constructor(
    @Inject('API_URL') public api: string,
    private as: AdddriverService,
    private js: JsonService,
    private vs: VisualizationService,
    private ss: SoildocService,
    private authService: AuthService,
    private platformLocation: PlatformLocation,
    private us: UserService,
    private metaService: MetaService,
    private ac: ActivatedRoute,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.metaService.changeMeta('SOILDOC.MANAGEMENTVISUALIZATION', 'SOILDOC.MANAGEMENTMETA');
    this.modalPapers = new bootstrap.Modal(document.getElementById('modalPapers'));
    this.modalFilter = new bootstrap.Modal(document.getElementById('modalFilter'));
    this.modalOverview = new bootstrap.Modal(document.getElementById('modalOverview'));
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authService.isLoggedInUser.pipe(take(1)).subscribe(loggedInUser => {
      this.loggedInUser = loggedInUser;
    });;
    this.isLoggedIn$.pipe(take(1)).subscribe(login => {
      if (login) {
        this.us.getMarkedPapers().subscribe({next: (papers) => {
          this.markedPapers = papers && papers.length ? papers : [];
        }});
      }
    });
    this.isLoading = true;
    document.getElementById('modalPapers').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerOverview) {
        this.modalOverview.show();
        this.triggerOverview = false;
      }
    });
    document.getElementById('modalOverview').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerDetail) {
        this.modalPapers.show();
        this.triggerDetail = false;
      }
    });
    this.js.getbyData(['Properties1', 'Geography511']).subscribe(result => {
      if (result) {
        this.options_properties = [];
        this.options_properties.push(new TreeNode(result[0]));
        this.options_properties.push(new TreeNode(result[1]));
        this.as.getAllKeywordsGenByUser().subscribe(resultsgen => {
          if (resultsgen) {
            for (let i = 0; i < resultsgen.length; i++) {
              if (resultsgen[i].category === 'Properties') {
                this.options_genkeys_properties.push(new TreeNode(resultsgen[i]));
              } else if (resultsgen[i].category === 'Geography') {
                this.options_genkeys_geography.push(new TreeNode(resultsgen[i]));
              }
            }
          }
          this.as.getAllKeywordsCombinedByUser().subscribe(resultscom => {
            if (resultscom) {
              for (let i = 0; i < resultscom.length; i++) {
                this.options_combinekeys.push(new TreeNode(resultscom[i]));
              }
            }
            const usergenerated = {
              'value': '1000000',
              'text': 'User Generated',
              'collapsed': true,
              'checked': false,
              'children': [{
                'value': '1000002',
                'text': 'Custom Keywords',
                'collapsed': true,
                'checked': false,
                'children': [
                {
                  'value': '1000006',
                  'text': 'Properties',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_genkeys_properties
                },
                {
                  'value': '1000007',
                  'text': 'Geography',
                  'collapsed': true,
                  'checked': false,
                  'children': this.options_genkeys_geography
                }]
              }, {
                'value': '1000003',
                'text': 'Combined Keywords',
                'collapsed': true,
                'checked': false,
                'children': this.options_combinekeys
              }]
            };
            this.options_properties.unshift(new TreeNode(usergenerated));
            this.isLoading = false;
            this.initMap();
            this.resizeObservable$ = fromEvent(window, 'resize');
            this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
              this.setSimulationCenter();
            });
            if (this.ac.snapshot.queryParams['keyword']) {
              this.influencedProperty = this.ac.snapshot.queryParams['keyword'];
              this.cdRef.detectChanges();
            }
          });
        });
      }
    });
  }

  initMap() {
    this.graph = ForceGraph()
    (document.getElementById('graph'))
    .graphData({nodes: [], links: [] })
    .width(document.getElementById('graph').getBoundingClientRect().width)
    .height(document.getElementById('graph').getBoundingClientRect().height)
    .onNodeClick((node : any, event) => node.group === 'papergroup' ? this.nodeClickEvent(node, event) : this.nodeClickEventKeyword(node, event))
    .d3Force('collision', d3.forceCollide(15))
    .d3Force('manyBody', d3.forceManyBody().strength(-400))
    .linkWidth(0.5)
    .linkColor(() => 'rgba(92,116,40,0.4)')
    .nodeCanvasObjectMode((node: any) => node.group === 'papergroup' ? 'replace' : 'replace')
    .nodeCanvasObject((node, ctx) => this.nodePaint(node, ctx))
    .nodePointerAreaPaint((node, color, ctx) => this.nodePaintArea(node, color, ctx))
    .minZoom(0.1).maxZoom(4)
    .onZoom(() => this.removeContextMenu())
    .onNodeDrag(() => this.removeContextMenu());
  }

  removeContextMenu() {
    document.getElementById('contextMenuManagement').style.visibility = 'hidden';
  }

  nodePaint(node: any, ctx: any) {
    if (node && node.group) {
      if (node.group !== 'papergroup') {
        var r = 6;
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#1d8b5a';
        ctx.fill();
        ctx.lineWidth = '2';
        ctx.strokeStyle = '#a5bb28';
        ctx.stroke();

        const label = node.id;
        ctx.font = '16px Titillium Web';
        ctx.textAlign = 'left';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = 'rgba(0, 0, 0, 0.8)';
        ctx.fillText(label, node.x + 15, node.y);
      } else {
        var r = this.getRadiusByValue(node.papers.length);
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#919191';
        ctx.fill();
        ctx.lineWidth = '3';
        ctx.strokeStyle = '#5e5e5e';
        ctx.stroke();
      }  
    }
  }

  nodePaintArea(node, color, ctx) {
    ctx.fillStyle = color;
    if (node && node.id && node.group) {
      if (node.group !== 'papergroup') {
        ctx.font = '16px Titillium Web';
        const textWidth = ctx.measureText(node.id).width;
        ctx.fillRect(node.x - 7 , node.y - 11, textWidth + 23, 19);
      } else {
        ctx.beginPath();
        ctx.arc(node.x, node.y, this.getRadiusByValue(node.papers.length) + 2, 0, 2 * Math.PI, false);
        ctx.fill()
      }
    }
  }

  onValueChangeProperty(resetKeywords: boolean, item?: TreeNode) {
    if (this.influencedProperty !== '') {
      this.treeviewDisabled = true;
      this.isVisualizationLoading = true;
      this.isVisualizationEnabled = false;
      this.dataEmpty = false;
      if (resetKeywords) {
        this.activeKeywords = [];
      }
      this.vs.getVisualizationManagement(this.influencedProperty, this.filterVis, this.activeKeywords, this.showOnlyOwn, this.loggedInUser).subscribe(result => {
        if (result) {
          if (result.nodes && result.nodes.length > 0 && result.links && result.links.length > 0) {
            this.soildocsVis = result.papers;
            this.graph
              .graphData({nodes: result.nodes, links: result.links });
            if (resetKeywords) {
              this.options_keywords = [];
              for (let key of result.keywords) {
                this.options_keywords.push(new TreeNode({
                  text: key,
                  value: key,
                  checked: true
                }));
                this.activeKeywords.push(key);
              }
              this.options_keywords = this.options_keywords.slice();
            }
            this.isVisualizationLoading = false;
            this.errorLoadingVisualization = false;
            this.treeviewDisabled = false;
            this.isVisualizationEnabled = true;
            this.dataEmpty = false;
            this.setSimulationCenter();
          } else {
            this.soildocsVis = [];
            this.dataEmpty = true;
            this.isVisualizationLoading = false;
            this.treeviewDisabled = false;
          }
        } else {
          this.soildocsVis = [];
          this.isVisualizationLoading = false;
          this.errorLoadingVisualization = true;
          this.treeviewDisabled = false;
        }
      },
      error => {
        this.soildocsVis = [];
        this.isVisualizationLoading = false;
        this.errorLoadingVisualization = true;
        this.treeviewDisabled = false;
      });
    }
  }

  ngOnDestroy() {
    if (this.resizeSubscription$) {
      this.resizeSubscription$.unsubscribe();
    }
  }

  setSimulationCenter() {
    timer(10).subscribe(val => {
      this.graph
      .width(document.getElementById('graph').getBoundingClientRect().width)
      .height(document.getElementById('graph').getBoundingClientRect().height);
      const bbox = this.graph.getGraphBbox();
      this.graph.centerAt((bbox.x[0] + bbox.x[1]) / 2, (bbox.y[0] + bbox.y[1]) / 2);
    });
  }

  nodeClickEvent(node: any, event: any) {
    this.removeContextMenu();
    this.openModalPapers(node);
  }

  nodeClickEventKeyword(node: any, event: any) {
    this.removeContextMenu();
    this.activeKeyword = node.id;
    let div = document.getElementById('contextMenuManagement');
    div.style.left = event.x + 10 + 'px';
    div.style.top = event.y - 30 + 'px';
    div.style.visibility = 'unset';
  }

  maximizeVisualization() {
    this.isFullscreen = true;
    this.setSimulationCenter();
    this.removeContextMenu();
  }

  minimizeVisualization() {
    this.isFullscreen = false;
    this.setSimulationCenter();
    this.removeContextMenu();
  }

  openModalPapers(node: any) {
    this.filterOptions = [];
    this.searchValue = '';
    this.modalPapers.show();
    this.isLoadingModal = true;
    this.propertyFirst = node.first;
    this.propertyLast = node.last;
    this.soildocCollection = [];
    this.soildocCollectionOriginal = node.papers;
    this.soildocCollection = this.soildocCollectionOriginal.slice(0);
    this.soildocCollectionFilter = this.soildocCollectionOriginal.slice(0);
    this.updateFilterOptions();
    this.resetSorting();
    this.isLoadingModal = false;
  }

  onSelectedChange(keywords: any) {
    this.activeKeywords = keywords;
    this.activeKeywords = this.activeKeywords.slice();
    if (keywords.length > 1) {
      this.onValueChangeProperty(false);
    } 
  }

  updateFilterOptions() {
    this.filterOptions = [];
    for (const paper of this.soildocCollectionOriginal) {
      if (!this.filterOptions.find(filter => filter.effect === paper.effect)) {
        this.filterOptions.push({
          effect: paper.effect,
          selected: true,
          count: 1
        });
      } else {
        const filteropt = this.filterOptions.find(filter => filter.effect === paper.effect);
        filteropt.count += 1;
      }
    }
  }

  updateSoildocsFilter() {
    this.soildocCollection = [];
    this.soildocCollectionFilter = this.soildocCollectionOriginal.slice(0).filter(
      paper => {
        if (this.filterOptions.find(filter => (paper.effect === filter.effect) && filter.selected === true)) {
          return true;
        } else {
          return false;
        }
      });
    this.updateSearch(this.searchValue);
    this.resetSorting();
  }

  updateSearch(text: string) {
    this.soildocCollection = [];
    let collection = this.soildocCollectionFilter.slice(0);
    if (this.showOnlyMarked && collection.length > 0) {
      collection = collection.filter(paper => this.markedPapers.includes(paper.soildoc._id));
    }
    if (text !== '') {
      collection = collection.filter(soildoc => soildoc.title.toLowerCase().includes(text.toLowerCase()) || 
      soildoc.year.toString().includes(text.toLowerCase()) || 
      soildoc.authors.some(author => author.name.toLowerCase().includes(text.toLowerCase())));
    }
    this.resetSorting();
    this.soildocCollection = collection.slice(0);
  }

  resetSorting() {
    if (this.isSorted && this.sortTarget) {
      this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
    }
  }

  sortPaper(event: any, identifier: string) {
    if (event.target.classList.value === 'sort-icon-search fa fa-sort') {
      if (this.isSorted && this.sortTarget) {
        this.sortTarget.classList = 'sort-icon-search fa fa-sort';
      }
      this.isSorted = true;
      this.sortTarget = event.target;
      event.target.classList = 'sort-icon-search fa fa-sort-up';

      this.sortOneIdentifier(identifier, 1, -1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-up') {

      event.target.classList = 'sort-icon-search fa fa-sort-down';
      this.sortOneIdentifier(identifier, -1, 1);

    } else if (event.target.classList.value === 'sort-icon-search fa fa-sort-down') {
      event.target.classList = 'sort-icon-search fa fa-sort';
      this.isSorted = false;
      this.sortTarget = null;
      this.sortOneIdentifier('modifiedDate', 1, -1, true);
    }
  }

  sortOneIdentifier(identifier: string, first: number, second: number, isDate?: boolean) {
    if (!isDate) {
      if (identifier === 'authors') {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a.soildoc[identifier][0].name > b.soildoc[identifier][0].name) {
            return first;
          }
          if (a.soildoc[identifier][0].name < b.soildoc[identifier][0].name) {
            return second;
          }
          return 0;
        });
      } else {
        this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
          if (a.soildoc[identifier] > b.soildoc[identifier]) {
            return first;
          }
          if (a.soildoc[identifier] < b.soildoc[identifier]) {
            return second;
          }
          return 0;
        });
      }
    } else {
      this.soildocCollection = this.soildocCollection.slice().sort(function(a, b) {
        return new Date(b.soildoc.modifiedDate).getTime() - new Date(a.soildoc.modifiedDate).getTime();
      });
    }
  }

  openOverview(soildoc_id: any) {
    this.triggerOverview = true;
    if (this.soildocOverview && (this.soildocOverview._id === soildoc_id)) {
      this.triggerDetail = true;
      this.modalPapers.hide();
    } else {
      this.triggerDetail = true;
      this.modalPapers.hide();
      this.ss.getOne(soildoc_id).subscribe(resultttt => {
        this.soildocOverview = resultttt;
      });
    }
  }

  openModalFilter() {
    this.modalFilter.show();
  }

  updateFilter(filter: FilterVisualization) {
    this.filterVis = filter;
    this.onValueChangeProperty(false);
  }

  getRadiusByValue(length: number) {
    if (this.between(length, 1, 5)) {
      return 10;
    } else if (this.between(length, 6, 10)) {
      return 11;
    } else if (this.between(length, 11, 15)) {
      return 12;
    } else if (this.between(length, 16, 20)) {
      return 13;
    } else if (this.between(length, 21, 25)) {
      return 14;
    } else if (this.between(length, 26, 30)) {
      return 15;
    } else if (this.between(length, 31, 35)) {
      return 16;
    } else if (this.between(length, 36, 40)) {
      return 17;
    } else if (this.between(length, 41, 45)) {
      return 18;
    } else if (this.between(length, 46, 50)) {
      return 19;
    } else {
      return 20;
    }
  }

  between(x: number, min: number, max: number) {
    return x >= min && x <= max;
  }

  ConvertToInt(val) {
    return parseInt(val, 10);
  }

  updatePapersMarked() {
    this.updateSearch(this.searchValue);
  }

  markPaper(soildoc_id: string) {
    if (!this.markedPapers.includes(soildoc_id)) {
      this.markedPapers.push(soildoc_id);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  unmarkPaper(soildoc_id: string) {
    if (this.markedPapers.includes(soildoc_id)) {
      this.markedPapers = this.markedPapers.filter(paper => paper !== soildoc_id);
      this.us.updateMarkedPapers(this.markedPapers).subscribe({
        next: () => {},
        error: (err) => {console.error(err)}
      });
    }
  }

  untagAllPapers() {
    this.markedPapers = [];
    this.us.updateMarkedPapers(this.markedPapers).subscribe({
      next: () => {},
      error: (err) => {console.error(err)}
    });
  }

}
