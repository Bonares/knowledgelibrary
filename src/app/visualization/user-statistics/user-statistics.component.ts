import { faSync } from '@fortawesome/free-solid-svg-icons';
import { SoildocService } from './../../soildoc/soil-doc/shared/soildoc.service';
import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'kl-user-statistics',
  templateUrl: './user-statistics.component.html',
  styleUrls: []
})
export class UserStatisticsComponent implements OnInit {

  faSync = faSync;

  isLoading: Boolean;
  errorLoading: Boolean = false;

  totalCount = 0;

  constructor(
    private ss: SoildocService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.ss.getCountUsers().subscribe(result => {
      if (result && result.totalcount && result.totalcount > 0) {
        this.totalCount = result.totalcount;
        this.isLoading = false;
        this.errorLoading = false;
        const margin = {top: 30, right: 30, bottom: 40, left: 50};
        const svg = d3.select('.svg-visualization-bar')
        .append('g')
        .attr('transform',
              'translate(' + margin.left + ',' + margin.top + ')');
        // X axis
        const x = d3.scaleBand()
          .range([ 0, d3.select('#userstatisticsMap').node().getBoundingClientRect().width - margin.left - margin.right ])
          .domain(result.users.map(function(d) { return d.username; }))
          .padding(0.2);
        svg.append('g')
        .attr('transform', 'translate(0,' + (d3.select('#userstatisticsMap').node().getBoundingClientRect().height - margin.bottom - margin.top) + ')')
          .call(d3.axisBottom(x))
          .selectAll('text')
          .classed('text-statistics', true);

        // Add Y axis
        const y = d3.scaleLinear()
          .domain([0, d3.max(result.users, u => u.count)])
          .range([ d3.select('#userstatisticsMap').node().getBoundingClientRect().height - margin.top - margin.bottom , 0]);
        svg.append('g')
          .call(d3.axisLeft(y));

        const bars = svg.selectAll()
        .data(result.users)
        .enter();
        bars.append('rect')
          .attr('x', (d) => x(d.username))
          .attr('height', function(d) { return d3.select('#userstatisticsMap').node().getBoundingClientRect().height - margin.top - margin.bottom - y(0); })
          .attr('y', function(d) { return y(0); })
          .attr('width', x.bandwidth())
          .attr('fill', '#69b3a2');

        // Animation
        svg.selectAll('rect')
        .transition()
        .duration(800)
        .attr('y', (d) => y(d.count))
        .attr('height', (d) => d3.select('#userstatisticsMap').node().getBoundingClientRect().height - margin.top - margin.bottom - y(d.count))
        .delay((d, i) => (i * 100 ));

        bars.append('text')
          .text((d) => d.count)
          .attr('x', (d) => x(d.username) + x.bandwidth() / 2)
          .attr('y', (d) => y(d.count) - 8)
          .classed('text-count', true);

      } else {
        this.isLoading = false;
        this.errorLoading = true;
      }
    },
    error => {
      this.isLoading = false;
      this.errorLoading = true;

    });
  }

}
