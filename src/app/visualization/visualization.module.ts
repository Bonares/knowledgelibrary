import { SearchbarModule } from './../shared/searchbar/searchbar.module';
import { PaginationModule } from './../shared/pagination/pagination.module';
import { DownloadPaperModule } from './../shared/download-paper/download-paper.module';
import { PaperModalModule } from './../shared/paper-modal-mod/paper-modal.module';
import { PaperOverviewModule } from './../shared/paper-overview/paper-overview.module';
import { DropdownTreeviewModule } from './../shared/DropdownTreeview/dropdown-treeview-module';
import { SharedModule } from './../shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisualizationRoutingModule } from './visualization-routing.module';
import { PaperMapComponent } from './paper-map/paper-map.component';
import { KeywordMapComponent } from './keyword-map/keyword-map.component';
import { TreeviewCustomComponent } from './treeview-custom/treeview-custom.component';
import { TreeviewItemCustomComponent } from './treeview-item-custom/treeview-item-custom.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationMapComponent } from './location-map/location-map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import { ManagementMapComponent } from './management-map/management-map.component';
import { PropertyMapComponent } from './property-map/property-map.component';
import { UserStatisticsComponent } from './user-statistics/user-statistics.component';
import { DriverMapComponent } from './driver-map/driver-map.component';
import { FilterVisualizationComponent } from './filter-visualization/filter-visualization.component';


@NgModule({
  declarations: [PaperMapComponent, KeywordMapComponent, TreeviewCustomComponent, TreeviewItemCustomComponent,
    LocationMapComponent, ManagementMapComponent, PropertyMapComponent, UserStatisticsComponent, DriverMapComponent, FilterVisualizationComponent],
  imports: [
    CommonModule,
    VisualizationRoutingModule,
    TranslateModule,
    FontAwesomeModule,
    FormsModule,
    SharedModule,
    LeafletModule,
    LeafletMarkerClusterModule,
    DropdownTreeviewModule,
    PaperOverviewModule,
    ReactiveFormsModule,
    PaperModalModule,
    DownloadPaperModule,
    PaginationModule,
    SearchbarModule
  ]
})
export class VisualizationModule { }
