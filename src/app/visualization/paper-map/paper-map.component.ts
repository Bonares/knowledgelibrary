import { take } from 'rxjs/operators';
import { AuthService } from './../../shared/authentication/auth.service';
import ForceGraph, { ForceGraphInstance, NodeObject } from 'force-graph';
import { Soildoc } from 'src/app/soildoc/soil-doc/shared/Soildoc';
import { MetaService } from './../../shared/meta.service';
import { TreeNode } from './../treeview-custom/tree-node';
import { SoildocService } from './../../soildoc/soil-doc/shared/soildoc.service';
import { timer, Observable, Subscription, fromEvent } from 'rxjs';
import { VisualizationService } from './../shared/visualization.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import * as d3 from 'd3';
import { faEye, faEyeSlash, faFile, faSync, faExpandAlt, faCompressAlt, faBookOpen, faInfoCircle, faLongArrowAltRight, faTractor, faCubes, faUser } from '@fortawesome/free-solid-svg-icons';
import { PlatformLocation } from '@angular/common';
import * as bootstrap from 'bootstrap';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kl-paper-map',
  templateUrl: './paper-map.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None
})
export class PaperMapComponent implements OnInit, OnDestroy {

  faUser = faUser;
  faLongArrowAltRight = faLongArrowAltRight;
  faTractor = faTractor;
  faCubes = faCubes;

  faInfoCircle = faInfoCircle;
  faBookOpen = faBookOpen;
  faSync = faSync;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  faFile = faFile;
  faExpandAlt = faExpandAlt;
  faCompressAlt = faCompressAlt;

  soildoc: Soildoc;
  isLoading: Boolean = true;
  isVisualizationLoading: Boolean = false;
  
  errorLoading: Boolean = false;
  errorLoadingSoildocs: Boolean = false;
  dataEmpty: Boolean = false;

  soildocOverview: Soildoc;

  paperTextsActive: Boolean = true;
  propertyTextsActive: Boolean = true;
  manlanduseTextsActive: Boolean = true;

  simulation: any;
  linkg: any;
  nodeg: any;
  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;
  grabbedActive = false;

  isFullscreen = false;

  scrollOffset = 0;

  triggerOverview = false;
  triggerDetail = false;

  soildocs: Soildoc[] = [];
  propertyFirst: String;
  propertyLast: String;

  activeKeywords = [];
  options_keywords: TreeNode[] = [];

  soildocsVis: any[] = [];

  graph: ForceGraphInstance;

  activeKeyword = '';

  modalPapers: bootstrap.Modal;
  modalOverview: bootstrap.Modal;

  isLoggedIn$: Observable<boolean>;
  loggedInUser: string;
  showOnlyOwn = false;
  
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private vs: VisualizationService,
    private ss: SoildocService,
    private platformLocation: PlatformLocation,
    private metaService: MetaService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.metaService.changeMeta('SOILDOC.PAPERVISUALIZATION', 'SOILDOC.PAPERMETA');
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.modalPapers = new bootstrap.Modal(document.getElementById('modalPapers'));
    this.modalOverview = new bootstrap.Modal(document.getElementById('modalOverview'));
    this.platformLocation.onPopState(() => document.querySelectorAll('.modal').forEach(elem => {bootstrap.Modal.getInstance(elem).hide()}));
    document.getElementById('modalPapers').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerOverview) {
        this.modalOverview.show();
        this.triggerOverview = false;
      }
    });
    document.getElementById('modalOverview').addEventListener('hidden.bs.modal', (e) => {
      if (this.triggerDetail) {
        this.modalPapers.show();
        this.triggerDetail = false;
      }
    });
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authService.isLoggedInUser.pipe(take(1)).subscribe(loggedInUser => {
      this.loggedInUser = loggedInUser;
    });;
    const data = this.route.snapshot.data;
    if (data['soildoc']) {
      this.soildoc = data['soildoc'];
      this.vs.getVisualizationByPaper(this.soildoc._id, this.activeKeywords, this.showOnlyOwn, this.loggedInUser).subscribe(result => {
        this.errorLoading = false;
        if (result.length === undefined && (result.nodes && result.nodes.length > 0)) {
          this.soildocsVis = result.papers;
          for (let key of result.keywords) {
            this.options_keywords.push(new TreeNode({
              text: key,
              value: key,
              checked: true
            }));
            this.activeKeywords.push(key);
          }
          this.initMap()
          this.resizeObservable$ = fromEvent(window, 'resize');
          this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
            this.setSimulationCenter();
          });
          this.graph
            .graphData({nodes: result.nodes, links: result.links });
          this.setSimulationCenter();
          this.isLoading = false;
        } else {
          this.soildocsVis = [];
          this.isLoading = false;
          this.dataEmpty = true;
        }
      },
      error => {
        this.soildocsVis = [];
        this.isLoading = false;
        this.errorLoading = true;
      });
    } else {
      this.soildocsVis = [];
      this.isLoading = false;
      this.errorLoading = true;
    }
  }

  ngOnDestroy() {
    if (this.resizeSubscription$) {
      this.resizeSubscription$.unsubscribe();
    }
  }

  reloadMap() {
    this.isVisualizationLoading = true;
    this.dataEmpty = false;
    this.errorLoading = false;
    this.vs.getVisualizationByPaper(this.soildoc._id, this.activeKeywords, this.showOnlyOwn, this.loggedInUser).subscribe(result => {
      if (result.length === undefined && (result.nodes && result.nodes.length > 0)) {
        this.soildocsVis = result.papers;
        this.graph
          .graphData({nodes: result.nodes, links: result.links });
        this.isLoading = false;
        this.isVisualizationLoading = false;
      } else {
        this.soildocsVis = [];
        this.isVisualizationLoading = false;
        this.dataEmpty = true;
      }
    },
    error => {
      this.soildocsVis = [];
      this.isVisualizationLoading = false;
      this.errorLoading = true;
    });
  }

  initMap() {
    this.graph = ForceGraph()
    (document.getElementById('graph'))
    .graphData({nodes: [], links: [] })
    .width(document.getElementById('graph').getBoundingClientRect().width)
    .height(document.getElementById('graph').getBoundingClientRect().height)
    .onNodeClick((node : any, event) => node.group === 'papergroup' ? this.nodeClickEvent(node, event) : !node.center ? this.nodeClickEventKeyword(node, event) : this.removeContextMenu())
    .d3Force('collision', d3.forceCollide(15))
    .d3Force('manyBody', d3.forceManyBody().strength(-400))
    .linkWidth(0.5)
    .linkColor(() => 'rgba(92,116,40,0.4)')
    .nodeCanvasObjectMode((node: any) => node.group === 'papergroup' ? 'replace' : 'replace')
    .nodeCanvasObject((node, ctx) => this.nodePaint(node, ctx))
    .nodePointerAreaPaint((node, color, ctx) => this.nodePaintArea(node, color, ctx))
    .minZoom(0.1).maxZoom(4)
    .onZoom(() => this.removeContextMenu())
    .onNodeDrag(() => this.removeContextMenu());
  }

  removeContextMenu() {
    document.getElementById('contextMenuManagement').style.visibility = 'hidden';
    document.getElementById('contextMenuMeasured').style.visibility = 'hidden';
  }

  nodePaint(node: any, ctx: any) {
    if (node && node.group) {
      if (node.group !== 'papergroup' && !node.center) {
        var r = 6;
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = node.group === 'management' ? '#1d8b5a' : '#983400';
        ctx.fill();
        ctx.lineWidth = '2';
        ctx.strokeStyle = node.group === 'management' ? '#a5bb28' : '#ff7100';
        ctx.stroke();

        const label = node.id;
        ctx.font = '16px Titillium Web';
        ctx.textAlign = 'left';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = 'rgba(0, 0, 0, 0.8)';
        ctx.fillText(label, node.x + 15, node.y);

        if (node.clicked) {
          ctx.font = '16px Titillium Web';
          ctx.fillStyle = 'rgba(0, 0, 0, 0.8)';
          ctx.fillRect(node.x - (node.center ? 12 : 7) , node.y - 11, 100 + (node.center ? 28 : 23), 19);
        }
      } else if (node.group === 'paper' && node.center) {

        ctx.beginPath();
        ctx.moveTo(node.x - 10.5, node.y - 14.5);
        ctx.lineTo(node.x - 10.5, node.y + 14.5);
        ctx.lineTo(node.x + 10.5, node.y + 14.5);
        ctx.lineTo(node.x + 10.5 , node.y - 6.5);
        ctx.lineTo(node.x + 2.5, node.y - 14.5);
        ctx.lineTo(node.x - 10.5, node.y - 14.5);
        ctx.fillStyle = '#b92652';
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(node.x + 2.5, node.y - 14.5);
        ctx.lineTo(node.x + 2.5, node.y - 6.5);
        ctx.lineTo(node.x + 10.5, node.y - 6.5);
        ctx.fillStyle = '#68152e';
        ctx.fill();

        const width = document.getElementById('graph').getBoundingClientRect().width * 0.4;
        const words = node.title.split(/\s+/);
        ctx.font = ctx.font = '16px Titillium Web';
        ctx.textAlign = 'left';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = '#177e90';
        let index = 0;
        let lines = [];
        for (let word of words) {
          lines[index] ? lines[index] += word : lines[index] = '' + word;
          if (ctx.measureText(lines[index]).width > width) {
            index += 1;
            lines[index] = '';
          }
          lines[index] += ' ';
        }
        let widths = [];
        for (let i = 0; i < lines.length; i++) {
          ctx.fillText(lines[i].trim(), node.x + 15, node.y - 6 + (i * 16));
          widths[i] = ctx.measureText(lines[i]).width;
        }
        node.widths = widths;

      } else {
        var r = this.getRadiusByValue(node.papers.length);
        ctx.beginPath();
        ctx.arc(node.x, node.y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#919191';
        ctx.fill();
        ctx.lineWidth = '3';
        ctx.strokeStyle = '#5e5e5e';
        ctx.stroke();
      }  
    }
  }

  nodePaintArea(node, color, ctx) {
    ctx.fillStyle = color;
    if (node && node.id && node.group) {
      if (node.group !== 'papergroup' && !node.center) {
        ctx.font = '16px Titillium Web';
        const textWidth = ctx.measureText(node.id).width;
        ctx.fillRect(node.x - (node.center ? 12 : 7) , node.y - 11, textWidth + (node.center ? 28 : 23), 19);
      } else if (node.group === 'paper' && node.center && node.widths) {
        for (let i = 0; i < node.widths.length; i++) {
          ctx.fillRect(node.x - 11.5 , node.y - 16 + (i * 16), node.widths[i] + 27.5, 16);
        }
      } else if (node.papers) {
        ctx.beginPath();
        ctx.arc(node.x, node.y, this.getRadiusByValue(node.papers.length) + 2, 0, 2 * Math.PI, false);
        ctx.fill()
      }
    }
  }

  setSimulationCenter() {
    timer(10).subscribe(val => {
      this.graph
      .width(document.getElementById('graph').getBoundingClientRect().width)
      .height(document.getElementById('graph').getBoundingClientRect().height);
      const bbox = this.graph.getGraphBbox();
      bbox ? this.graph.centerAt((bbox.x[0] + bbox.x[1]) / 2, (bbox.y[0] + bbox.y[1]) / 2) : null;
    });
  }

  nodeClickEvent(node: any, event: any) {
    this.removeContextMenu();
    this.openModalPapers(node);
  }

  nodeClickEventKeyword(node: any, event: any) {
    this.removeContextMenu();
    this.activeKeyword = node.id;
    if (node.group === 'management') {
      let div = document.getElementById('contextMenuManagement');
      div.style.left = event.x + 10 + 'px';
      div.style.top = event.y - 30 + 'px';
      div.style.visibility = 'unset';
    } else {
      let div = document.getElementById('contextMenuMeasured');
      div.style.left = event.x + 10 + 'px';
      div.style.top = event.y - 30 + 'px';
      div.style.visibility = 'unset';
    }
  }

  maximizeVisualization() {
    this.isFullscreen = true;
    this.setSimulationCenter();
    this.removeContextMenu();
  }

  minimizeVisualization() {
    this.isFullscreen = false;
    this.setSimulationCenter();
    this.removeContextMenu();
  }

  openModalOverview(soildoc_id: string) {
    this.triggerOverview = true;
    if (this.soildocOverview && (this.soildocOverview._id === soildoc_id)) {
      this.triggerDetail = true;
      this.modalPapers.hide();
    } else {
      this.triggerDetail = true;
      this.modalPapers.hide();
      this.ss.getOne(soildoc_id).subscribe(resultttt => {
        this.soildocOverview = resultttt;
      });
    }
  }

  getRadiusByValue(length: number) {
    if (this.between(length, 1, 5)) {
      return 10;
    } else if (this.between(length, 6, 10)) {
      return 11;
    } else if (this.between(length, 11, 15)) {
      return 12;
    } else if (this.between(length, 16, 20)) {
      return 13;
    } else if (this.between(length, 21, 25)) {
      return 14;
    } else if (this.between(length, 26, 30)) {
      return 15;
    } else if (this.between(length, 31, 35)) {
      return 16;
    } else if (this.between(length, 36, 40)) {
      return 17;
    } else if (this.between(length, 41, 45)) {
      return 18;
    } else if (this.between(length, 46, 50)) {
      return 19;
    } else {
      return 20;
    }
  }

  between(x: number, min: number, max: number) {
    return x >= min && x <= max;
  }

  openModalPapers(node: any) {
    this.soildocs = node.papers;
    this.propertyFirst = node.first;
    this.propertyLast = node.last;
    this.modalPapers.show();
  }

  onSelectedChange(keywords: any) {
    this.activeKeywords = keywords;
    if (keywords.length > 1) {
      this.reloadMap();
    } 
  }
}
