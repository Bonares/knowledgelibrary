import { TreeNode } from './../treeview-custom/tree-node';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'treeview-item-custom',
  templateUrl: './treeview-item-custom.component.html',
  styleUrls: []
})
export class TreeviewItemCustomComponent implements OnInit {

  constructor() { }

  @Input() node: TreeNode;
  @Output() checkedChange = new EventEmitter<any>();

  ngOnInit(): void {
  }

  toggleChild(node: TreeNode) {
    node.collapsed = !node.collapsed;
  }

  onCheckedChange(node: TreeNode) {
    this.checkedChange.emit(node);
  }

  onChildCheckedChange(node: TreeNode, event: TreeNode) {
    this.checkedChange.emit(event);
  }

}
