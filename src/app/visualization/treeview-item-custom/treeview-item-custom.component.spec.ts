import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TreeviewItemCustomComponent } from './treeview-item-custom.component';

describe('TreeviewItemCustomComponent', () => {
  let component: TreeviewItemCustomComponent;
  let fixture: ComponentFixture<TreeviewItemCustomComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeviewItemCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeviewItemCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
