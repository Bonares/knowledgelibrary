export const environment = {
  production: true,
  apiUrl: 'https://klibrary.bonares.de/api',
  local: false
};
